0.5467479674796748

C count                  = bad
DG count                 = bad
DeepMsPeptide            = good
Digestable C             = bad
Digestable N             = bad
Exists in Pride          = good
H count                  = bad
K in linker              = bad
KP count                 = bad
Linker missing           = bad
M count                  = good
MMIs in proteome         = bad
N genome locations       = bad
N observations           = good
N protein_samples        = good
Proteotypic score        = bad
Q start                  = bad
R in linker              = bad
RP count                 = good
Rank: DeepMsPeptide      = bad
Rank: Proteotypic score  = good
Reps in proteome         = bad


--------------


ORIGINAL
0.5467479674796748
COLUMN AA composition
COLUMN Average mass
COLUMN C count
PRIORITY 0
0.547800586510264
COLUMN C flank
COLUMN Consequence score
PRIORITY 0
0.5467479674796748
COLUMN DG count
PRIORITY 0
0.549286199864038
COLUMN DP count
PRIORITY 0
0.547375596455351
COLUMN DeepMsPeptide
PRIORITY 0
0.5174418604651163
COLUMN Digestable C
PRIORITY 0
0.5496915695681974
COLUMN Digestable N
PRIORITY 0
0.5487389229720518
COLUMN End position
COLUMN Exists in Pride
PRIORITY 0
0.5419803126809496
COLUMN From OpenMs?
PRIORITY 0
0.5467479674796748
COLUMN H count
PRIORITY 0
0.5497661990647963
COLUMN HH count
PRIORITY 0
0.5466216216216216
COLUMN Isoelec
COLUMN Isoelectric point
COLUMN K in linker
PRIORITY 0
0.5507544581618655
COLUMN KP count
PRIORITY 0
0.5453324378777703
COLUMN Length
PRIORITY 0
0.5467479674796748
COLUMN Linker missing
PRIORITY 0
0.5488873904248146
COLUMN M count
PRIORITY 0
0.5417515274949084
COLUMN MMIs in proteome
PRIORITY 0
0.553680981595092
COLUMN Molecular formula
COLUMN Molecular weight
COLUMN Monoisotopic mass
PRIORITY 0
0.5467479674796748
COLUMN N flank
COLUMN N genome locations
PRIORITY 0
0.5515463917525774
COLUMN N observations
PRIORITY 0
0.545515846257586
COLUMN N protein_samples
PRIORITY 0
0.545515846257586
COLUMN NG count
PRIORITY 0
0.5467479674796748
COLUMN Protein accession
COLUMN Protein organism
COLUMN Protein title
COLUMN Proteotypic score
PRIORITY 0
0.5721605465414176
COLUMN Q start
PRIORITY 0
0.5482781904118839
COLUMN R in linker
PRIORITY 0
0.5567010309278351
COLUMN RP count
PRIORITY 0
0.5422488356620093
COLUMN Rank: Consequence score
PRIORITY 0
0.5467479674796748
COLUMN Rank: DeepMsPeptide
PRIORITY 0
0.5630165289256198
COLUMN Rank: Proteotypic score
PRIORITY 0
0.5350578624914908
COLUMN Rel hydrophobicity
COLUMN Repeats in input
PRIORITY 0
0.5468644639244774
COLUMN Repeats in protein
PRIORITY 0
0.5467479674796748
COLUMN Reps in proteome
PRIORITY 0
0.5508474576271186
COLUMN Start position
COLUMN Terminal type