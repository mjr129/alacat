# Build HATMUL
import os
from myptools.common_build import common_build
from myptools.compile_scss import compile_scss


print( "[ALACAT BUILD]" )
my_dir = os.path.dirname( __file__ )

#
# Also rebuild our dependencies
#
common_build( my_dir,
              ["mhelper", "yolanda", "hatmul", "doesitfly"],
              docs_dir = os.path.join( my_dir, "alacat", "gui", "static", "docs"))

#
# SCSS
#
compile_scss( "alacat/gui/static/extra.scss" )

print( "[ALACAT BUILD] COMPLETE" )
