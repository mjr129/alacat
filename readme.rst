==============
𝓐𝓵𝓪𝓬𝓪𝓽𝕕𝕖𝕤𝕚𝕘𝕟𝕖𝕣
==============

`Alacat𝕕𝕖𝕤𝕚𝕘𝕟𝕖𝕣`:t: provides a common API for querying protein and peptide metrics and
helps the user create qconcats [Pratt-2006]_ of quantotypic peptides [Worboys-2014]_.


* Translate protein accessions into sequences
* Query peptide metrics (e.g. chance of miscleavage)
* Evaluate and compare simulated digestion routines.
* Rank metrics to determine an optimum qconcat.

.. contents::

--------------------------------------------------------------------------------
Quick start
--------------------------------------------------------------------------------

Install Alacat𝕕𝕖𝕤𝕚𝕘𝕟𝕖𝕣 and run the Gui::

    git clone https://bitbucket.org/mjr129/alacat
    python3.7 -m virtualenv venv
    venv/bin/pip install -e alacat
    venv/bin/alacat --gui
    
Note that ``venv/bin`` is ``venv\Scripts`` on Windows.
    
    
--------------------------------------------------------------------------------
Basic workflow
--------------------------------------------------------------------------------

The basic workflow is as follows:


+----------------------+--------------------------+--------------------------+---------------------------+
| Stage                | Purpose                  | Examples                 | Output                    |
+======================+==========================+==========================+===========================+
| Acquire protein      | Convert user input into  | * Uniprot accessions     | List of protein sequences |
|                      | protein sequences        | * Ensembl accessions     |                           |
|                      |                          | * FASTA file name        |                           |
|                      |                          | * Raw protein sequence   |                           |
|                      |                          | * Raw peptide sequence   |                           |
+----------------------+--------------------------+--------------------------+---------------------------+
| Acquire peptides     | Produce peptuide         | * Internal trypsin       | List of peptides          |
|                      | sequences from protein   |   digestor               |                           |
|                      | sequences                | * Online services such   |                           |
|                      |                          |   as PPA                 |                           |
+----------------------+--------------------------+--------------------------+---------------------------+
| Score peptides       | Produce various metrics  | * Isoelectric point      | Metrics table             |
|                      | from peptide sequences   | * AAIndex1 metrics       |                           |
|                      |                          | * Number of methionines  |                           |
|                      |                          | * CONSEQUENCE score      |                           |
+----------------------+--------------------------+--------------------------+---------------------------+
| Grade scores         | Evaluate what the scores | * CONSEQUENCE score is   | Proposed peptides         |
|                      | mean in terms of         |   above 0.5              |                           |
|                      | quantotypicness          | * No methionines         |                           |
+----------------------+--------------------------+--------------------------+---------------------------+
| Acquire qblocks      | Produce qblocks from     | * 2 peptides per qblock, | Metrics table             |
|                      | highest graded peptides  |   both permutations      |                           |
+----------------------+--------------------------+--------------------------+---------------------------+
| Score, grade qblocks | As for peptides                                     | Proposed qbricks          |
+----------------------+--------------------------+--------------------------+---------------------------+ 
| Acquire qmenus       | Produce qmenus from      | * 1000 random            | Metrics table             |
|                      | highest graded qbricks   |   permutations           |                           |
+----------------------+--------------------------+--------------------------+---------------------------+
| Score, grade qmenus  | As for peptides                                     | Proposed qconcats         |
+----------------------+--------------------------+--------------------------+---------------------------+


--------------------------------------------------------------------------------
Terminology
--------------------------------------------------------------------------------

* Protein
    * An amino acid sequence given using the 1-letter code. 
      Only the 20 essential amino acids (ARNDCEQGHILKMFPSTWYV), plus 
      selenocysteine (U) are supported. The protein must be backed up by a
      globally unique *accession*.
* Peptide
    * Fragment of a protein.
* Qbrick
    * *nq* peptides (usually 2) from the same protein.
* Qblock
    * It is possible that the user wishes to track *nu* peptides from a
      protein, where *nu > nq*. This requires multiple *qbricks*, hence a
      *qblock* is a the minimal set of *qbricks* that provides *nu* peptides.
* Qconcat
    * A sequence of *qbricks*, with some constraint, *nl*, on the maximum length (usually
      100).
* Qmenu
    * It is possible that the selected *qbricks* will not fit into a single
      *qconcat* of size *nl*. Hence a *qmenu* comprises the minimal set of
      *qconcats* to encompass all selected *qbricks*.

* Entity
    * One of the above terms
* Favoured entities
    * The user may favour certain entities (e.g. peptides) in the pipeline.
* Selected entities
    * The entities chosen to progress through the pipeline.
      If the user favours more entities than permitted (i.e. *nu > nq*) then
      these will all be forced through the pipeline (e.g. by increasing the
      number of qbricks in a qblock). The program may also operate non-mandated
      mode, in which case the user's choice will be truncated to
      a appropriate size (i.e. *nq*) by discarding what the program considers
      to be the worst of the user's selection. If the user selects fewer entities than
      permitted (i.e. *nu < nq*) then the program will append what it considers
      to be the next most suitable.
* System selected entities
    * The choices the program would make if the user hadn't favoured anything.
* Candidate entities
    * The set of all entities able to be selected for a particular stage.
      This is a superset of all the above sets.
* Column
    * A metric that can be assigned to entities (e.g. Isoelectic point)    
* Score
    * A metric assigned for a specific entity and column
* Handler
    * An object capable of producing output for one or more columns.
* Assessor
    * A means of determining if the scores in a column suggest that a
      candidate entity should or should not be selected.
* Grade
    * Results of an assessor applied to a score: Pass, Fail, Info or Error.


--------------------------------------------------------------------------------
Installation
--------------------------------------------------------------------------------

Installation is via `pip`::

    pip install alacat


Using a virtual environment is usually a good idea, as is getting the latest source from Bitbucket::

    git clone https://bitbucket.org/mjr129/alacat
    python3.7 -m virtualenv venv
    venv/bin/pip install -e alacat
    venv/bin/alacat --help
    
.. note::

    If installing from the Bitbucket source then you may also wish to install AlacatDesigner's dependencies
    in the same manner, e.g.::

        git clone https://bitbucket.org/mjr129/mhelper
        venv/bin/pip install -e mhelper
        
    These must be installed *before* AlacatDesigner.
    The list of dependencies can be found in `setup.py` in AlacatDesigner's main folder.
    
.. tip:: 

    If AlacatDesigner is installed into a virtual environment, it will keep its
    data files in that environment, instead of in the user folder.
    See `mhelper/specific_locations.py` for more details.
    

--------------------------------------------------------------------------------
Cached data
--------------------------------------------------------------------------------

Application-specific cached data is written to the virtual environment folder
by default.
Run the following command to see the location and size of cached data::

    alacat --cached
    
The ``maintenance/`` page also displays this information in the GUI.
    
--------------------------------------------------------------------------------
Usage
--------------------------------------------------------------------------------

Workflow
--------

You can use the web interface, the Python API or the command line client to
execute the workflow. Use of the API is recommended.

API:
:::


* The API is controlled through the `Model` object in Python::

    import alacat
    my_model = alacat.Model( ... )
    results = my_model.exact()
    
* The API is well documented inline using RST and PEP484 and this documentation
  is not repeated here. To view such documentation invoke ``help`` in Python,
  see the compiled output in ``doc/`` or, preferably, use an IDE such as
  PyCharm [JetBrains]_.

HTML/GUI
::::::::

From your terminal::

    alacat gui
    
* Help can be obtained via the command line and is not repeated here.
* The GUI can be executed as a public server.
  Please see the `Flask documentation`_ [Ronacher-2015]_ for more details.
* Note that the GUI is limited to a basic workflow.

Cli
:::

From your terminal::

    alacat cli
    
* Help can be obtained via the command line and is not repeated here.
* Note that the CLI is limited to a minimal workflow.

Notes
-----

The software uses *downloads and parses large biological data datasets*.
For instance, when you process a human protein the human proteome will be
downloaded and parsed.
This process may take several minutes the first time you request a specific
protein.

Approximate times:

* New proteome:     Hours
* New protein:      Minutes
* Existing protein: Seconds

"New protein" times taken are largely due to querying external services and
cannot be sped up, for instance by using a more powerful computer. Specific
metrics ("handlers") can be disabled in the software if they become problematic.

--------------------------------------------------------------------------------
Tutorial
--------------------------------------------------------------------------------

Start the GUI.

* http://localhost:5000
    * On the main screen click to load the 'walnut-uniprot' example, then
      click `OK` to load it.
    * You should get a "Your model has been located" message. Follow the link to
      go to the results.
* http://localhost:5000/FC3C-6445/index
    * Go to the final report section.
* http://localhost:5000/FC3C-6445/report
    * Notice one peptide `EEEQQR` has a bad N-linker. Let's change this
      in our model. 
    * Hover your mouse over any peptide. You should see a little flag icon
      appear. Click the icon to make sure it is selected. Do this for all the
      other peptides.
    * Click on the main body of our offending peptide `EEEQQR` to go to
      the view-peptide page.
* http://localhost:5000/FC3C-6445/peptides/PEP-P02662-VPQLEIVPNSAEER
    * From here, click on the offending protein `Q15K67` to go to the
      view-protein page.
* http://localhost:5000/FC3C-6445/proteins/Q15K67
    * Expand the "All peptides" section to see all the available peptides for
      this protein.
    * Notice that the peptides that reached the "Final report" earlier all have
      little stars by them. `EWGPDQASPR` should also have a little flag by it
      because you flagged it before.
    * Compare our "bad" `EEEQQR` peptide against the next best choice,
      `HNPYYFHSQGLR`, by checking their respective "compare" boxes. Click 
      the "Compare selected" button below to run the comparison.
* http://localhost:5000/FC3C-6445/compare?compare=PEP-Q15K67-EEEQQR&compare=PEP-Q15K67-HNPYYFHSQGLR&submit=1
    * We see that `EEEQQR` is better in 2 out of 4 metrics, but `HNPYYFHSQGLR`
      is better in the other two. You can click on the metric names to see more
      details, but for now hit your browser's back button to return to the
      view-protein screen.
* http://localhost:5000/FC3C-6445/proteins/Q15K67
    * Let's imagine that we still really don't want to use `EEEQQR`
      any more. Maybe we have already tested it in the lab and it was rubbish.
      We're going to swap it for a similar alternative - `HNPYYFHSQGLR`.
    * Expand the all peptide section again.
    * Remove any flag next to `EEEQQR` (if present) and add a new flag next to
      `HNPYYFHSQGLR`
    * In the top right corner of the screen there should be a big flag with the
      number "10" next to it (that's the number of peptides we have flagged).
      Click the flag.
* http://localhost:5000/FC3C-6445/cart?submit=0
    * Click the "Checkout" button to create a new model with our flagged
      peptides.
* Main screen (long URL)
    * Back at the main screen, we should see a query much like the original
      `walnut-uniprot` example, but our peptides have now been specified
      explicitly. You may also notice our proteins also now specified the
      sequences verbatim, rather than providing just the ID, but this shouldn't
      make any difference.
    * Click on "OK" to submit our new query.
    * When the query finishes, click to go to the results.
* http://localhost:5000/2D7A-391C/index
    * Click on "Final Report"
    * There are many options to explore. We can swap out qblocks and qmenus
      using the same manner as for the peptides, and we can view the raw score
      tables from the main screen. Most screens are documented in-line, and 
      you can hover over many elements to see more information.
    

References
----------

.. [Pratt-2006] Pratt, J., Simpson, D., Doherty, M. et al. 2006. *Multiplexed absolute quantification for proteomics using concatenated signature peptides encoded by QconCAT genes.* Nat Protoc. https://doi.org/10.1038/nprot.2006.129

.. [Worboys-2014] Worboys, J., Sinclair, J., Yuan, Y. et al. 2014. *Systematic evaluation of quantotypic peptides for targeted analysis of the human kinome.* Nat Methods. https://doi.org/10.1038/nmeth.3072

.. [JetBrains] **Software.** Jetbrains s.r.o. *PyCharm: the Python IDE for Professional Developers.* https://www.jetbrains.com/pycharm/ 

.. [Ronacher-2015] **Software.** Armin Ronacher. *Flask, The Pallets Projects.* (2015) https://palletsprojects.com/p/flask/

.. _`this Stack Overflow post`: https://stackoverflow.com/questions/40273767/clear-all-lru-cache-in-python/50699209

.. _`Flask documentation`: https://flask.palletsprojects.com/en/1.1.x/server/