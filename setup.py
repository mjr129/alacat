from distutils.core import setup


def readme():
    try:
        import os
        readme_fn = os.path.join( os.path.dirname( __file__ ), "readme.rst" )
        with open( readme_fn ) as f:
            return f.read()
    except Exception as ex:
        return f"Failed to read readme.rst due to an error: {ex}"


setup( name = "alacat",
       url = "https://bitbucket.org/mjr129/alacat",
       version = "1.0.1.1068",
       description = "Alacat project.",
       long_description = readme(),
       author = "Martin Rusilowicz",
       license = "https://www.gnu.org/licenses/agpl-3.0.html",
       python_requires = ">=3.7",
       include_package_data = True,
       entry_points = { "console_scripts": ["alacat = alacat.__main__:main"] },

       packages = ["alacat"],


       install_requires = \
           [
               #
               # The following can also be installed from https://bitbucket.org/mjr129/*:
               #
               "mhelper",  # Utilities
               "hatmul",  # GUI
               # "doesitfly", # Predictor. Optional. Please install tensorflow before this.
               # "yolanda", # Database. Optional.
               
               #
               # The remainder are external Python packages:
               #
               "flask", # Not required for CLI, but required for Desktop app and web interface
               "requests",  # For mhelper.web_helper
               "filelock",  # For mhelper.web_helper
               "pyopenms",  # Digestions 
               #"mysqlclient", # This must be INSTALLED MANUALLY
               "keyring",
               "numpy", # This is only used a couple of times and should probably not be a requirement
               ],

       classifiers = \
           [                                                                                                                                                                                                                                                                                                                                                "Development Status :: 3 - Alpha",
            "Environment :: Console",
            "Environment :: Win32 (MS Windows)",
            "Environment :: X11 Applications",
            "Framework :: Flask",
            "Intended Audience :: End Users/Desktop",
            "Intended Audience :: Science/Research",
            "License :: Freely Distributable",
            "License :: OSI Approved",
            "License :: OSI Approved :: GNU Affero General Public License v3",
            "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
            "License :: OSI Approved :: GNU General Public License (GPL)",
            "Natural Language :: English",
            "Operating System :: MacOS",
            "Operating System :: Microsoft :: Windows",
            "Operating System :: OS Independent",
            "Operating System :: Unix",
            "Programming Language :: JavaScript",
            "Programming Language :: Python :: 3",
            "Programming Language :: Python :: 3 :: Only",
            "Programming Language :: Python :: 3.8",
            "Topic :: Education :: Computer Aided Instruction (CAI)",
            "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
            "Topic :: Scientific/Engineering",
            "Topic :: Scientific/Engineering :: Artificial Intelligence",
            "Topic :: Scientific/Engineering :: Bio-Informatics",
            "Topic :: Scientific/Engineering :: Visualization",
            "Topic :: Software Development :: Libraries",
            "Topic :: Software Development :: Libraries :: Python Modules",
            "Typing :: Typed"]
       )

# region attributions

# BEGIN ATTRIBUTION
# PURPOSE `pyOpenMS` is used by AlacatDesigner to provide simulated digestion of the proteins.
# TITLE   pyOpenMS
# AUTHOR  OpenMS Team
# WEBSITE https://pyopenms.readthedocs.io/en/latest/index.html
# AUTHOR  Rost HL, Sachsenberg T, Aiche S, Bielow C et al.
# ARTICLE Rost HL, Sachsenberg T, Aiche S, Bielow C et al.
#         OpenMS: a flexible open-source software platform for mass spectrometry data analysis.
#         Nat Meth. 2016; 13, 9: 741-748.
#         doi:10.1038/nmeth.3959.
# DATE-RETRIEVED 2021-05-25
# LICENSE-NAME three clause BSD licence
# LICENSE-URL https://github.com/OpenMS/OpenMS/blob/develop/LICENSE
# LICENSE-TEXT --------------------------------------------------------------------------
#              OpenMS -- Open-Source Mass Spectrometry
#              --------------------------------------------------------------------------
#              Copyright The OpenMS Team -- Eberhard Karls University Tuebingen,
#              ETH Zurich, and Freie Universitaet Berlin 2002-2020.
#              
#              This software is released under a three-clause BSD license:
#               * Redistributions of source code must retain the above copyright
#                 notice, this list of conditions and the following disclaimer.
#               * Redistributions in binary form must reproduce the above copyright
#                 notice, this list of conditions and the following disclaimer in the
#                 documentation and/or other materials provided with the distribution.
#               * Neither the name of any author or any participating institution
#                 may be used to endorse or promote products derived from this software
#                 without specific prior written permission.
#              For a full list of authors, refer to the file AUTHORS.
#              --------------------------------------------------------------------------
#              THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#              AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#              IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
#              ARE DISCLAIMED. IN NO EVENT SHALL ANY OF THE AUTHORS OR THE CONTRIBUTING
#              INSTITUTIONS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
#              EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#              PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#              OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#              WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#              OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#              ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# END ATTRIBUTION

# BEGIN ATTRIBUTION
# PURPOSE `filelock` is used by AlacatDesigner to allow access to the GUI models from multiple processes.
# TITLE  filelock
# WEBSITE https://pypi.org/project/filelock/ 
#         https://github.com/benediktschmitt/py-filelock 
# AUTHOR  Benedikt Schmitt
# LICENSE-NAME Public Domain (Public Domain <http://unlicense.org>)
# LICENSE-URL http://unlicense.org
# DATE-RETRIEVED 2021-05-25
# END ATTRIBUTION

# BEGIN ATTRIBUTION
# PURPOSE `requests` is used by AlacatDesigner to make web requests to online services.
# AUTHOR Kenneth Reitz
# WEBSITE https://pypi.org/project/requests/
#         https://requests.readthedocs.io/en/master/
# TITLE requests
# LICENSE-NAME Apache Software License (Apache 2.0)
# DATE-RETRIEVED 2021-05-25
# END ATTRIBUTION

# endregion
