import alacat
from mhelper.io_helper import read_all_text
from mhelper.location_helper import find_file


# Read our input files
protein_txt = read_all_text( find_file( "declans_alacats/proteins.txt" ) )
peptide_txt = read_all_text( find_file( "declans_alacats/peptides.txt" ) )

# Show Alacat messages
alacat.debugging.enable_diagnostics()

# Remove the `McPredDigester`, McPred is offline right now...
handlers = alacat.HandlerFactoryCollection.create_defaults()
handlers = [x for x in handlers if x.klass is not alacat.McPredDigester]

# Create our model
model = alacat.Model(
        digestion = alacat.EDigestion.TRYPSIN,
        handlers = handlers,
        organisms = [4932],
        proteins = [protein_txt],
        peptides = [peptide_txt],
        sql_backing = False )

# Run the model
results = model.exact()

# Save the results
results.save( find_file( "declans_alacats??alacat.pkl" ) )
