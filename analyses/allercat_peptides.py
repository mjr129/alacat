"""
Produces the ALLERCAT spreadsheet.
"""
import csv
import os
import sys
from dataclasses import dataclass
from typing import Dict, List, Sequence

from mhelper import arg_parser, file_helper, location_helper, log_helper, utf_table

import alacat


def main():
    arg_parser.execute( __main, interrupts = () )


@dataclass
class AllercatPeptide:
    food: str
    allergen: str
    uniprot_ids: Sequence[str]
    sequence: str


def __main( all: bool = False ) -> None:
    input_fn = location_helper.find_file( "allercat/peptides_map.tsv" )
    out_dir = location_helper.find_file( "allercat??outputs" )
    out_dir2 = os.path.join( out_dir, "by_protein" )
    file_helper.create_directory( out_dir2 )
    output_fn = os.path.join( out_dir, "analysis.tsv" )
    
    your_proteins_to_peptides = __load_input_file( input_fn )
    
    print( f"{len( your_proteins_to_peptides )} proteins." )
    
    tsvs = []
    handlers = alacat.HandlerFactoryCollection.create_defaults()
    n = len( your_proteins_to_peptides )
    
    for index, (protein_accession, your_seqs) in enumerate( sorted( your_proteins_to_peptides.items() ) ):
        tsvs.append( f"# PROTEIN {index} OF {n}" )
        
        print( utf_table.format_box( f"PROTEIN {index} OF {n}." ), file = sys.stderr )
        
        # Run the alacat pipeline
        model = alacat.Model( name = "Custom workflow for project Allercat",
                              digestion = alacat.EDigestion.TRYPSIN,
                              handlers = handlers,
                              proteins = (protein_accession,) )
        
        p = model.exact()
        
        html = alacat.reports.peptide_scores_html( p, select = your_seqs )
        
        output_fn2 = os.path.join( out_dir2, f"{protein_accession}.html" )
        
        with open( output_fn2, "w" ) as fout:
            fout.write( html )
        
        if not all:
            print( "Quitting now after first because --all was not specified." )
            exit( 0 )
        
        tsvs.append( alacat.reports.peptide_scores_tsv( p,
                                                        select = your_seqs,
                                                        filter = [lambda x: x.your_ref is not None or x.my_ref] ) )
    
    with open( output_fn, "w" ) as fout:
        fout.write( "\n".join( tsvs ) )
    
    print( output_fn )


def __load_input_file( input_fn ) -> Dict[str, List[str]]:
    with open( input_fn ) as fin:
        r = csv.reader( fin, delimiter = "\t" )
        h = next( r )  # headers
        
        col_flag = h.index( "flag" )
        # col_count = h.index( "count" )
        # col_food = h.index( "food" )
        # col_allergen = h.index( "allergen" )
        # col_uniprot = h.index( "uniprot" )
        col_peptide = h.index( "peptide" )
        col_accession = h.index( "accession" )
        # col_name = h.index( "name" )
        
        proteins = set()
        proteins_to_peptides = { }
        
        for row in r:
            if row:
                flag = row[col_flag]
                peptide = row[col_peptide]
                accession = row[col_accession]
                
                if not peptide:
                    continue
                
                if flag.strip():
                    proteins.add( accession )
                
                proteins_to_peptides.setdefault( accession, list() ).append( peptide )
    
    return { key: value for key, value in proteins_to_peptides.items() if key in proteins }


def __get_rank( p, seq ):
    pep = p.candidate_peptides.get( seq, None )
    
    if pep is not None:
        return p.peptide_scores.subject_to_rank[pep]
    
    return 0


if __name__ == "__main__":
    main()
