"""
Produces the ALLERCAT INPUT spreadsheet.
"""
import csv
import os
import sys
import time
from collections import defaultdict
from dataclasses import dataclass
from typing import Sequence, List

from mhelper import web_helper, utf_table, arg_parser, log_helper, location_helper, file_helper
from mhelper.bio_requests import core
import alacat


@dataclass()
class Protein:
    accession: str
    name: str


def main():
    a = location_helper.find_file( "allercat/peptides.tsv" )
    b = location_helper.find_file( "allercat/peptides_uniprot.tsv" )
    c = location_helper.find_file( "allercat??peptides_map.tsv" )
    
    pep_map = { }
    
    with open( b ) as fin:
        r = csv.reader( fin, delimiter = "\t" )
        
        h = next( r )
        
        col_uniprot = h.index( "Entry" )
        col_1 = h.index( "Entry name" )
        col_status = h.index( "Status" )
        col_names = h.index( "Protein names" )
        col_1 = h.index( "Gene names" )
        col_1 = h.index( "Organism" )
        col_1 = h.index( "Length" )
        col_peptide = h.index( "peptidesearch:P20200518A94466D2655679D1FD8953E075198DA8007BB9I" )
        
        for row in r:
            accession = row[col_uniprot].strip()
            name = row[col_names]
            status = row[col_status]
            peptides = [x.split( ":" )[0].strip() for x in row[col_peptide].split( "," )]
            
            protein = Protein( accession, f"{status} - {name}" )
            
            for peptide in peptides:
                pep_map.setdefault( peptide, list() ).append( protein )
    
    with open( c, "w" ) as fout:
        with open( a ) as fin:
            r = csv.reader( fin, delimiter = "\t" )
            
            h = next( r )
            
            col_food = h.index( "Food" )
            col_allergen = h.index( "Allergen/protein" )
            col_uniprot = h.index( "UniProt ID" )
            col_1 = h.index( "Peptide target residues" )
            col_peptide = h.index( "Peptide" )
            col_1 = h.index( "Lookup value" )
            col_1 = h.index( "Comments" )
            
            food = "?"
            allergen = "?"
            
            rout = ["count", "food", "allergen", "uniprot", "peptide", "accession", "name"]
            fout.write( "\t".join( rout ) + "\n" )
            
            fout.write( "\n" )
            
            routs = []
            
            amap = { }
            
            for row in r:
                food = row[col_food].strip() or food
                allergen = row[col_allergen].strip().replace( "α", "alpha" ).replace( "β", "beta" ) or allergen
                uniprot = row[col_uniprot].strip()
                peptide = row[col_peptide].strip()
                
                for protein in pep_map[peptide]:
                    routs.append( ["?", food, allergen, uniprot, peptide, protein.accession, protein.name] )
                    amap.setdefault( allergen, dict() ).setdefault( peptide, set() ).add( protein.accession )
            
            last_pep = None
            last_all = None
            
            for row in routs:
                allergen = row[2]
                peptide = row[4]
                protein = row[5]
                
                xx = amap[allergen].values()
                arr=[protein in pepprots for pepprots in xx]
                n = sum( arr )
                c = len( xx )
                x = 1 if n == c else 0
                y = "".join("1" if z else "0" for z in arr)
                
                row[0] = f"{x} ({y})"
                
                if allergen != last_all:
                    fout.write( "****\n" )
                elif peptide != last_pep:
                    fout.write( "\n" )
                
                last_pep = peptide
                last_all = allergen
                
                fout.write( "\t".join( row ) + "\n" )


if __name__ == "__main__":
    main()
