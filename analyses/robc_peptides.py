import csv
from dataclasses import dataclass
import random
from mhelper import ansi

from mhelper import arg_parser, ansi_format_helper, log_helper, string_helper
import alacat


def main():
    ansi_format_helper.install_error_hook()
    arg_parser.execute( __main )


@dataclass
class RobPeptide:
    Sequence: str
    Protein: str


def __main():
    input_file: str = "C:\\MJR\\Apps\\alacat_design\\case_studies\\crawford\\rob_crawford_peptides_dec_2019.tsv"
    rob_peptides = []
    protein_accessions = set()
    
    with open( input_file ) as fin:
        r = csv.reader( fin, delimiter = "\t" )
        headers = next( r )
        
        sequence_col = headers.index( "Sequence" )
        protein_col = headers.index( "Protein" )
        
        for row in r:
            if row:
                peptide = RobPeptide( row[sequence_col], row[protein_col] )
                protein_accessions.add( peptide.Protein )
                rob_peptides.append( peptide )

    # noinspection SpellCheckingInspection
    missing_seq = """
        >YOR312C
          1 MAHFKEYQVI GRRLPTESVP EPKLFRMRIF ASNEVIAKSR YWYFLQKLHK VKKASGEIVS 
         61 INQINEAHPT KVKNFGVWVR YDSRSGTHNM YKEIRDVSRV AAVETLYQDM AARHRARFRS 
        121 IHILKVAEIE KTADVKRQYV KQFLTKDLKF PLPHRVQKST KTFSYKRPST FY*            
            """
    
    ttl = 0
    ttl_ = 0
    ttl_r = 0
    ttl__r = 0
    
    for index, protein_accession in enumerate( protein_accessions ):
        peptides = [peptide for peptide in rob_peptides if peptide.Protein == protein_accession]
        
        rob_seqs = set( peptide.Sequence for peptide in peptides )
        
        # Load the default model
        
        result = alacat.Model( origin = "custom model for robc peptides workflow",
                               organisms = (4932,),  # Saccharomyces cerevisiae
                               digestion = alacat.EDigestion.TRYPSIN,
                               handlers = handlers,
                               proteins = (protein_accession,),
                               qmenus = (),
                               peptides = (),
                               qbricks = (),
                               qbrick_size = 2,
                               alacat_size = 10000,
                               permutations = 100 ).exact()
        
        ranked = result.peptide_scores.subject_order
        
        n = len( rob_seqs )
        my_two = ranked[:n]
        
        martin_seqs = set( peptide.sequence for peptide in my_two )
        
        random_seqs = random.sample( [x.sequence for x in result.candidate_peptides], n )
        
        print( f"PROTEIN {index + 1} of {len( protein_accessions )} : {protein_accession}" )
        print( f"About {len( result.candidate_peptides )} peptides." )
        
        for rob_seq in rob_seqs:
            print( f"Rob: {rob_seq}" )
        
        for martin_seq in martin_seqs:
            c = ansi.FORE_GREEN if martin_seq in rob_seqs else ansi.FORE_RED
            print( f"{c}Mar: {martin_seq}{ansi.RESET}" )
        
        all_seqs = set.union( rob_seqs, martin_seqs )
        
        for peptide_seq in all_seqs:
            if peptide_seq in rob_seqs:
                if peptide_seq in martin_seqs:
                    m = "both"
                else:
                    m = "rob only"
            else:
                m = "martin only"
            
            print( ansi.FORE_MAGENTA + peptide_seq + ansi.RESET + " " + ansi.FORE_CYAN + m + ansi.RESET )
            # print( alacat.reports.peptide_scores_html( result, rob_seqs ) )
        
        num_match = len( set.intersection( rob_seqs, martin_seqs ) )
        ttl += num_match
        ttl_ += n
        
        num_match_r = len( set.intersection( rob_seqs, random_seqs ) )
        ttl_r += num_match_r
        ttl__r += n
        
        print( f"----------> Martin matches: {num_match} (running {string_helper.percent_to_string( ttl, ttl_, t = 3 )})" )
        print( f"            Random matches: {num_match_r} (running {string_helper.percent_to_string( ttl_r, ttl__r, t = 3 )})" )


if __name__ == "__main__":
    main()
