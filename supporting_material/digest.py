#!/usr/bin/env python
#
# Proteome digestion script 
#

import getopt, re, sys, os

#
# def functions
#

def fastaread(filename):
#
# open Fasta file for reading
#
    """Opens and reads in a Fasta formatted file 

    This function takes a single argument, the name of an external file
    and opens it for reading to the fastafile handle, reads all the lines
    in, one go, into "lines". It expects protein names to be in lines beginning
    with the ">" character. It returns two objects, a list containing the protein
    names (first word on line) in order, as well as a dictionary with the actual
    sequences keyed on the protein names
    """
    fastafile = open( filename, "r")
    lines = fastafile.readlines();
    fastafile.close();
#
#  use a list and dictionary to store protein names and sequences  - nice structure for simple seq data
#
    order = []      # list
    seqs = {}       # dictionary 

    for line in iter(lines):
        if line.startswith('>'):
            header = line[1:].rstrip('\n')
            name = header.split()[0]
            order.append(name)
            seqs[name] = ''
        else:
            seqs[name] += line.rstrip('\n')

#   print ('%d sequences read in' % len(order))
    return (order, seqs) 
# -------------------------------------------------
#
# Digestion function - tryptic specificity is encoded, run twice to handle the dibasics problem
#
def digest(sequence):
    seq=sequence.rstrip('\*')  # remove stops if needed
    seq = re.sub(r'([KR])([^P*])', r'\1*\2', seq)
    seq = re.sub(r'([KR])([^P*])', r'\1*\2', seq)
    peps = seq.split(r'*')
    return (peps)

#
# Print usage statement if user inputs incorrect parameters or other error
#
def usage():
    print ('digest.py -i <inputfile> -o <outputfile> -hv')
    
#
# main program
#

inputfile = ""
outputfile = "digest.out"   # set default value
pepmin = 0                  # set default value
missedc = 0                 # set default value

try:
    opts, args = getopt.getopt(sys.argv[1:], "hi:o:m:x:v", ["help","ifile=","ofile=","min=","xc="])
except getopt.GetoptError as err:
    print(err)
    usage()
    sys.exit(2)

for opt, arg in opts:
    if opt in ('-h', "--help"):
        usage()
        sys.exit()
    elif opt in ("-i", "--ifile"):
        inputfile = arg
    elif opt in ("-o", "--ofile"):
        outputfile = arg
    elif opt in ("-m", "--min"):
        pepmin = int(arg) if int(arg) > 0 else 1  # default minimum peptide size  = 1
    elif opt in ("-x", "--xc"):
        missedc = int(arg) if int(arg) > 0 else 0  # default missed cleavages = 0

if inputfile:
    print ('Input file: ', inputfile)
    print ('Output file: ', outputfile)
else:
    print ('Please provide an input file')
    usage()
    sys.exit()

try:
    ofile = open( outputfile, "w")
except IOError as e:
    print ("I/O error with file {0} ({1}): {2}".format(outputfile, e.errno, e.strerror))


order = []      # list
seqs = {}       # dictionary 
(order, seqs) = fastaread(inputfile)

for prot in order:
#    print (prot, " length = ", len(seqs[prot]))
    peps = digest(seqs[prot])
    npep = 0;
    for pep in peps:
        pepseq = ""
        for mc in range(0, missedc+1):    # missed cleavages
#            print("npep {0}   mc {1}  npep+mc {2}   length(peps) {3}".format(npep,mc,npep+mc,len(peps)))
            if (npep + mc < len(peps)):
                pepseq += peps[npep + mc]
                ntype = "I"
                if (npep + mc == len(peps)):
                    ntype = "C"
                elif (npep == 0):
                    ntype = "N"
                if len(pepseq) > pepmin:
                    print ('>%s pepnum=%d len=%d mc=%d' % (prot,npep,len(pepseq),mc), file=ofile)
                    print (pepseq, file=ofile)
        npep += 1


