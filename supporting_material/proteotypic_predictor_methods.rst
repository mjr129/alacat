*******
Alacats
*******

================================================================================
Proteotypic Predictor Methods
================================================================================

--------------------------------------------------------------------------------
PPA by et al. 2015
--------------------------------------------------------------------------------

* Muntel, J., Boswell, S. A., Tang, S., Ahmed, S., Wapinski, I., Foley, G., … Springer, M. (2015). Abundance-based classifier for the prediction of mass spectrometric peptide detectability upon enrichment (PPA). Molecular & cellular proteomics : MCP, 14(2), 430–440. doi:10.1074/mcp.M114.044321
* http://software.steenlab.org/rc4/PPA.php

* "Download the executable version written in Perl" doesn't work.
* There is an online version


--------------------------------------------------------------------------------
STEPP by Robertson et al. 2008
--------------------------------------------------------------------------------

A support vector machine model for the prediction of proteotypic peptides for accurate mass and time proteomics.
Webb-Robertson BJ, Cannon WR, Oehmen CS, Shah AR, Gurumoorthi V, Lipton MS, Waters KM
Bioinformatics. 2008 Jul 1; 24(13):1503-9.

* Appears to be no longer available
    * https://omics.pnl.gov/software/STEPP.php


--------------------------------------------------------------------------------
Peptide detectability by Tang et al. 2006
--------------------------------------------------------------------------------

A computational approach toward label-free protein quantification using predicted peptide detectability.
Tang H, Arnold RJ, Alves P, Xun Z, Clemmer DE, Novotny MV, Reilly JP, Radivojac P
Bioinformatics. 2006 Jul 15; 22(14):e481-8.

As far as I can tell no program or source code is provided.


--------------------------------------------------------------------------------
PeptideSieve by Mallick et al. 2007
--------------------------------------------------------------------------------

Mallick, P., Schirle, M., Chen, S. et al. Computational prediction of proteotypic peptides for quantitative proteomics. Nat Biotechnol 25, 125–131 (2007) doi:10.1038/nbt1275

* Works (C++), but usage is poorly (not?) documented. What is the property file
  that is required?
* It is also very old.


--------------------------------------------------------------------------------
AP3 by Gao et al. 2018
--------------------------------------------------------------------------------

    integrates the peptide digestion and detection processes together to predict proteotypic peptides

* Gao, Z., Chang, C., Zhu, Y., & Fu, Y. (2018). Integrated modeling of peptide digestion and detection for the prediction of proteotypic peptides in targeted proteomics. bioRxiv, 399436.
* http://fugroup.amss.ac.cn/software/AP3/AP3.html

* It is Windows only
* It requires Matlab
* The distribution is in Rar format, but appears to be corrupt.


--------------------------------------------------------------------------------
CONSeQuence by Eyers et al. 2011
--------------------------------------------------------------------------------

* It is online only
* Attempting to probe the online service for its API brought the entire website down. 
  It is no longer accessible.


--------------------------------------------------------------------------------
ESP Predictor by Fusaro et al. 2009
--------------------------------------------------------------------------------

Fusaro, V. A., Mani, D. R., Mesirov, J. P., & Carr, S. A. (2009). Prediction of high-responding peptides for targeted protein assays by mass spectrometry. Nature biotechnology, 27(2), 190–198. doi:10.1038/nbt.1524

* The online GenePredictor doesn't seem to have it (anymore?)
* The source code is downloadable, but "The page you were looking for doesn't exist."
* The link to the source code in the paper is spam.

--------------------------------------------------------------------------------
Tables
--------------------------------------------------------------------------------

Many, e.g. PTM table at Chiva et al. 2017. "Peptide Selection for Targeted Protein Quantitation".

================================================================================
Alacat tool
================================================================================

.. code::

    alacat P02647 > P02647.tsv

.. image:: P02647.png
   :width: 100%
