################################################################################
#                                                                              #
# This script downloads, installs, tests or uninstalls the application.        #
#                                                                              #
# Installation downloads the MASTER version of *my* packages where possible,   #
# rather than the version (if any) on PyPi. Any version of code in the current #
# directory is also ignored, meaning this script can be used stand-alone.      #
#                                                                              #
# The script always runs a quick test of the tool after installation/upgrade,  #
# or if "test" is passed on the command line.                                  #
#                                                                              #
# install   - install, test                                                    #
# uninstall - uninstall (requires previous install)                            #
# update    - update, test (requires previous install)                         #
# test      - test (requires previous install)                                 #
# all       - install, update, test, uninstall                                 #
#                                                                              #
# Use bash to invoke this script.                                              #
# Do not source it.                                                            #
# E.g.: bash test.sh test                                                      #
#                                                                              #
################################################################################

# Exit on failure, echo on
set -e
set -x

TEST_DIR="alacat_installation"

if [ "${1}" = "" ]; then
  echo "install|uninstall|update|test|all. See comments in script for details.";
  exit 0;
fi

# Use a temporary directory for testing
# We store it in an environment variable that the application will use
mkdir "${TEST_DIR}"

# Create a virtual environment and switch to it
if [ "${1}" = "install" ] || [ "${1}" = "all" ]; then
  cd "${TEST_DIR}"
  
  python3.8 -m virtualenv venv -v
  
  # Write the directory we can find everything in (see mhelper.specific_locations.py)
  echo "${PWD}" > venv/.mjr_apps_alt.txt

  # Clone everything we'll need
  git clone https://github.com/vsegurar/DeepMSPeptide --depth 1
  git clone https://bitbucket.org/mjr129/alacat.git --depth 1
  git clone https://bitbucket.org/mjr129/mhelper.git --depth 1
  git clone https://bitbucket.org/mjr129/hatmul.git --depth 1
  git clone https://bitbucket.org/mjr129/yolanda.git --depth 1

  # Install our packages, in reverse order
  venv/bin/pip install -e mhelper
  venv/bin/pip install -e hatmul
  venv/bin/pip install -e yolanda
  venv/bin/pip install -e alacat
  
  cd ..
fi

if [ "${1}" = "update" ] || [ "${1}" = "all" ]; then
  cd "${TEST_DIR}"
  
  # Pull all our packages
  cd .mhelper
  git pull
  cd ../hatmul
  git pull
  cd ../yolanda
  git pull
  cd ../alacat
  git pull
  cd ..
  
  cd ..
fi

# Run a test
if [ "${1}" = "install" ] || [ "${1}" = "uninstall" ] || [ "${1}" = "test" ] || [ "${1}" = "all" ]; then
  test_venv/bin/alacat --cli P02662 --make execution_summary_ansi
fi

# Clean up the test directory
if [ "${1}" = "uninstall" ] || [ "${1}" = "all" ]; then
  rm -rf "${TEST_DIR}"
fi

# All done
echo "0 OK"
