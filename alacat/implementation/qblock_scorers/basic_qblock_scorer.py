from alacat.model import HArgs, Qblock, QblockScorer, HColumn, RawScore, Assessors
import statistics


class NaturalQblockScorer( QblockScorer ):
    """
    Overview
    --------
    
    When enabled, a score is produced that indicates if qbricks occur naturally in the protein sequence.
    
    
    Scores
    ------
    
    If the score is zero then the Qbrick sequence does not occur in the original protein. This means
    the environment of the peptides in the Qbrick will be less like the environment of the peptides
    in the original protein. This is the case for almost all Qbricks, unless concurrent peptides
    were chosen and no linker substitution was performed.
    """
    COLUMN = HColumn( "Is natural?", assessor = Assessors.Equal( 1 ) )

    def __init__( self, hargs: HArgs ):
        hargs.set_backing_data( False, () )  # Trivial
        super().__init__( hargs )

    def _on_query( self, subject: Qblock ) -> RawScore[Qblock]:
        return self._score( statistics.mean( qbrick.sequence in qbrick.protein.sequence for qbrick in subject.qbricks ),
                            self.COLUMN,
                            subject,
                            subject )
