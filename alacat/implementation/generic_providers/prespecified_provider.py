"""
Base class for providers that return predefined subjects.

:data TParent:      e.g. Protein
:data TChild:       e.g. Peptide
:data CParentLike:  Anything that represents the `TParent`:
                    * The `TParent` itself
                    * A `Pending` object containing the parent.
:data CChildCode:   A string parsable to a `TChild`
:data CScoreValue:  Value of the assignment from this `TParent` and this `PrespecifiedProvider` to this `TChild`
:data CMapping:     Mapping of parents to children, and their scores.
:data CValuelessMapping: As `CMapping`, without the scores.
"""
import json
from enum import Flag
from typing import Any, Dict, Generic, Iterable, List, Optional, Sequence, Tuple, TypeVar, Union
from mhelper import exception_helper
from alacat.model import Handler, HArgs, HColumn, Pending, PipelineError, RawScore, EProvided, DbData
from alacat.model.debugging import favoured_map_debugging as diagnostics


TParent = TypeVar( "TParent" )
TChild = TypeVar( "TChild" )
TParentKey = object
CParentLike = Union[TParent, Pending[Any, TParent]]
CChildCode = Union[str, DbData]
CScoreValue = EProvided
CMapping = Dict[CParentLike, List[Tuple[CChildCode, CScoreValue]]]
CValuelessMapping = Dict[CParentLike, List[CChildCode]]


class EPrespecified( Flag ):
    """
    Modes of `PrespecifiedProvider`.
    
    :cvar NONE:
        The provider always allows the user to denote - in code - the 
        derivation of certain outputs from certain inputs (e.g. certain peptides
        from certain proteins).
        
        A modifiable `dict` to is accepted and exposed, to specify the mapping.
    
    :cvar COUPLED:
        Includes 'output' from earlier stages of the pipeline.
        
        This allows a provider such as "TsvProteinProvider" to load both proteins
        and peptides from the same file simultaneously.
        
        Note: A error will occur if such output is loaded but not released.
    
    :cvar FAVOURED:
        Includes 'output' that was specified during model specification.
        
        Internally, this behaves similarly to `COUPLED` but the provider
        receives its mapping from a different source and the output is scored as
        `EProvided.FAVOURED` rather than `True`.
    """
    NONE = 0
    COUPLED = 1 << 0
    FAVOURED = 1 << 1


class PrespecifiedProvider( Handler[Pending[TParent, TChild],
                                    Union[TChild, Pending[TParent, TChild]]],
                            Generic[TParent,
                                    TChild] ):
    """
    Base class for providers that return predefined subjects.
    See `EPrespecified` for details.
    
    .. important::
    
        This class is a mixin and must be used in conjunction with PeptideProvider, QbrickProvider, etc.
    """
    __abstract__ = True
    COLUMN: HColumn = HColumn( "From spec?" )
    __slots__ = "__pkey_to_ch", "__pending_mapping"
    t_parent = None
    t_child = None
    t_parent_key = None
    t_parent_key_nullable = False
    t_child_code = str


    def __init_subclass__( cls, **kwargs ):
        super().__init_subclass__( **kwargs )
        assert cls.t_child is not None
        assert cls.t_parent is not None
        assert cls.t_parent_key is not None


    def __init__( self,
                  hargs: HArgs,
                  lut: Union[CMapping, EPrespecified] = EPrespecified.COUPLED | EPrespecified.FAVOURED
                  ) -> None:
        """
        :param hargs:   !INHERITED 
        :param lut:     Either a combination of `EPrespecified` flags or a `CDict`.
                        Note that using a `CDict` implies `EPrespecified.NONE`.
        """
        hargs.set_backing_data( False, () )  # Variable
        super().__init__( hargs )

        self.__pending_mapping: List = []
        self.__pkey_to_ch: CMapping = { }

        if isinstance( lut, dict ):
            self.update_mapping( lut )
        else:
            if lut & EPrespecified.COUPLED:
                self.update_mapping( self._on_get_coupled_state() )

            if lut & EPrespecified.FAVOURED:
                texts = self._on_get_favoured_state()

                for text in texts:
                    self.update_mapping_from_text( text, EProvided.FAVOURED )

        diagnostics( "[{}::CONSTRUCTED] n={}", type( self ).__name__, len( self.__pkey_to_ch ) )


    def update_mapping_from_text( self, text: str, value: CScoreValue = EProvided.NORMAL ) -> None:
        """
        Maps parents to children from a text-block.
        
        Use default relationships, i.e. ::
        
            PEPTIDE
            PEPTIDE
            PEPTIDE
        
        Or specify relationships verbatim, i.e. ::
        
            >PROTEIN
            PEPTIDE
            PEPTIDE
            
        Or use JSON to represent the objects, i.e. ::
        
            { "C":"Peptide", s: "XYZ" }
            
        Or specify objects by the accession, i.e. ::
        
            PEP-123
            
        Or their hash::
        
            01010101001
        
        
        See `add_mapping`. 
        """
        text = text.lstrip()

        if not text:
            return

        if text[0] == "{":
            # JSON dict
            j = json.loads( text )
            self.add_mapping( None, DbData( j ), value )
        elif text[0] == "[":
            # JSON list    
            J = json.loads( text )
            for j in J:
                self.add_mapping( None, DbData( j ), value )
        else:
            lines = text.split( "\n" )
            accession = None

            for line in lines:
                line = line.strip()

                if not line:
                    continue

                if line.startswith( ">" ):
                    accession = line[1:].strip()
                else:
                    self.add_mapping( accession, line, value )


    def update_mapping_from_lines( self, text: List[str], value: CScoreValue = True ) -> None:
        """
        Maps parents to children from multiple lines of text.
        See `add_mapping`. 
        """
        for line in text:
            self.update_mapping_from_text( line, value )


    def update_mapping( self, dictionary: CValuelessMapping, value: CScoreValue = True ) -> None:
        """
        Maps parents to children from a dictionary.
        See `add_mapping`. 
        """
        for k, vv in dictionary.items():
            for v in vv:
                self.add_mapping( k, v, value )


    def add_mapping( self, parent: Optional[CParentLike], child: CChildCode, value: CScoreValue = True ) -> None:
        """
        Maps a parent to a child.
        
        .. note::
        
            Parents may be mapped to multiple children by repeated calls to this method.
        
        :param parent:  Parent identifier
        :param child:   Child identifier 
        :param value:   Value of the assignment from this parent and this provider to this child.
        """

        # exception_helper.assert_type( "parent", parent, [None, self.t_parent, Pending] )
        exception_helper.assert_type( "child", child, CChildCode )

        self.__pending_mapping.append( (parent, child, value) )
        diagnostics( "[{}::PUSH] key={}; child={}", type( self ).__name__, parent, child )


    def _on_query_all( self, subjects: Sequence[TParent] ) -> Iterable[RawScore[Union[TChild, Pending[TParent, TChild]]]]:
        if self.__pending_mapping:
            self.__resolve_pending_mapping()

        return super()._on_query_all( subjects )


    def __resolve_pending_mapping( self ):
        """
        Converts the assigned mapping between parent (e.g. proteins) and child
        (e.g. peptide) objects.
        
        This is run prior to the query - by now the parents should exist, though
        the children will not.
        """
        parent_key: Union[TParentKey, None]

        for parent_code, child_code, value in self.__pending_mapping:
            # The user may have specified the parent to child mapping
            #   e.g. >P12345, AGILPVK, FWYDER
            # Or they may have left us to work it out
            #   e.g. AGILPVK, FWYDER
            # (we can usually work it out, e.g. based on protein sequence)
            # Either way, we must get the parent object
            if parent_code is None:
                #
                # Unknown parent - parse the child to find the parent
                # Parentless relationships almost always come from the user
                #
                if isinstance( child_code, DbData ):
                    parent_key = self._on_find_parent_key( child_code )
                elif isinstance( child_code, str ):
                    parent_key = self._on_find_parent_key_str( child_code )
                else:
                    raise exception_helper.type_error( "child_code", child_code, Union[DbData, str] )
            else:
                #
                # Known parent - get the parent
                # These could come from the user or an earlier pipeline stage
                #
                if isinstance( parent_code, Pending ):
                    parent_inst = self.__extract_from_pending( parent_code )
                elif isinstance( parent_code, self.t_parent ):
                    parent_inst = parent_code
                elif isinstance( parent_code, bytes ) or isinstance( parent_code, str ) or isinstance( parent_code, DbData ):
                    parent_inst = self.__extract_from_state( parent_code )
                else:
                    raise exception_helper.type_error( "parent", parent_code )

                parent_key = self._on_get_parent_key( parent_inst )

            #
            # Store the result in our LUT
            #
            exception_helper.assert_type( "parent_key", parent_key, self.t_parent_key )
            self.__pkey_to_ch.setdefault( parent_key, [] ).append( (child_code, value) )

        # No more pending
        self.__pending_mapping.clear()

    def __extract_from_state( self, parent_code ):
        try:
            return self.state.find( parent_code, self.t_parent )
        except KeyError as ex:
            raise PipelineError( f"A {self.t_parent.__name__} code ({parent_code!r}) cannot be resolved to an existing {self.t_parent.__name__}. Please check that the mapping between {self.t_parent.__name__} and {self.t_child.__name__} is valid in your arguments." ) from ex

    def __extract_from_pending( self, pending: Pending ) -> TParent:
        if not pending.resolved:
            raise PipelineError( "A `Pending` input could not be resolved in PrespecifiedProvider. This is unexpected - all mapped inputs should be resolved by the time this class is instantiated. Check the pipeline has been correctly set up.", True )
        elif pending.resolved.__len__() > 1:
            raise PipelineError( "A `Pending` input has multiple resolutions in PrespecifiedProvider. This is unexpected - mapped inputs should only be registered against inputs known resolve singularly. Check the source of this input.", True )
        
        pending = pending.resolved[0]
        return pending


    def _on_query( self,
                   subject_: Pending[TParent, TChild]
                   ) -> Optional[List[RawScore[Union[TChild, Pending[TParent, TChild]]]]]:
        parent: TParent = subject_.input
        exception_helper.assert_type( "parent", parent, self.t_parent )
        r = []

        # Find the key that gets the data for this parent
        parent_key = self._on_get_parent_key( parent )
        exception_helper.assert_type( "parent_key", parent_key, self.t_parent_key )

        # Construct the children from their codes
        child_codes: List = self.__pkey_to_ch.get( parent_key, None )

        if child_codes is not None:
            for index, (child_code, assignment_value) in reversed( list( enumerate( child_codes ) ) ):
                diagnostics( "[{}({})::PULL] parent={}; key={}; pulled={}", type( self ).__name__, len( self.__pkey_to_ch ), parent, parent_key, child_code )

                # Construct the child
                child = self._on_construct_child( parent, child_code )

                if child:
                    # Store the child
                    r.append( self._score( assignment_value, self.COLUMN, child, subject_ ) )

                # Remove the child from our index
                del child_codes[index]

            # If all children are removed, remove the parent
            if not child_codes:
                del self.__pkey_to_ch[parent_key]

        return r


    def _on_get_parent_key( self, parent: TParent ) -> TParentKey:
        """
        The derived class must convert the parent object to the internally used
        key. i.e. the same key `_on_find_parent_key` returns.
        
        :param parent:  Parent object 
        :return:        Key 
        """
        raise NotImplementedError( "abstract" )


    def _on_construct_child( self, parent: TParent, line: CChildCode ) -> Optional[TChild]:
        """
        The derived class must parse the input data to retrieve the child
        object.
        
        :param parent:  Parent object, if required. 
        :param line:    Parsable child text.
        :return:        Child object, or `None`.
        """
        raise NotImplementedError( "abstract" )


    def _on_find_parent_key( self, line: DbData ) -> TParentKey:
        """
        The derived class must parse the input data (typically free-text
        user-input) to retrieve the parent key.
        
        :param line:    See `CChildCode` 
        :return:        The parent object.
                        If this is `None` `_on_construct_child` will be called
                        for all parents until it returns not-`None`.
        """
        raise NotImplementedError( "abstract" )


    def _on_find_parent_key_str( self, line: str ) -> TParentKey:
        """
        The derived class must parse the input data (typically free-text
        user-input) to retrieve the parent key.
        
        :param line:    See `CChildCode` 
        :return:        The parent object.
                        If this is `None` `_on_construct_child` will be called
                        for all parents until it returns not-`None`.
        """
        raise PipelineError( f"{self.t_child.__name__!r}s must be specified in JSON format or the {self.t_parent.__name__!r} must be specified." )


    def _on_get_favoured_state( self ) -> Sequence[str]:
        """
        The derived class must retrieve the favoured state.
        e.g. `self.state.model.peptides`.
        
        See `EPrespecified.FAVOURED`.
        """
        raise NotImplementedError( "abstract" )


    def _on_get_coupled_state( self ) -> CValuelessMapping:
        """
        The derived class must retrieve the coupled state.
        e.g. `self.state.pending_peptides`.
        
        See `EPrespecified.COUPLED`.
        """
        raise NotImplementedError( "abstract" )
