import re
from dataclasses import dataclass
from typing import Iterable, List, Optional, Sequence

from alacat.model import Assessors, EDigestion, HArgs, HColumn, InstantiationError, PendingPeptide, PeptideLike, PeptideProvider, Protein, RawScore, WarningCollection
from mhelper import array_helper, bio_helper, web_helper, exception_recorder

__all__ = "PpaDigester",


class PpaDigester( PeptideProvider ):
    """
    Overview
    --------
    
    When enabled, this peptide provider uses PPA online to digest the peptides and score them
    based on their simulated detectability.
    
    
    Scores
    ------
    
    The score for each peptide is a single numerical value, indicating the simulated detectability
    of the peptide. Higher values indicate that the peptide is more likely to be detected. 
    """
    COLUMN = HColumn( "PPA score",
                      rank_order = 1,
                      assessor = Assessors.Min( 0.65 ) )


    def __init__( self, hargs: HArgs ):
        """
        :param hargs:   Inherited.  
        """
        hargs.set_backing_data( True, () )
        super().__init__( hargs )
        self.__enabled = True

        if self.state.digestion != EDigestion.TRYPSIN:
            raise InstantiationError( self, f"not a trypsin model ({self.state.digestion})." )


    def _on_query( self, subject: PendingPeptide ) -> Iterable[RawScore[PeptideLike]]:
        if not self.__enabled:
            return ()

        protein: Protein = subject.input
        r = []

        try:
            ppa_results = _ppa_request( [protein.sequence], True, True, self.state.warnings )
        except _PpaError as ex:
            ref = exception_recorder.print_traceback( ex, f"PPA error.", ref = True, is_verbose = True ).ref
            self.state.warnings.append( f"PpaDigester encountered an error and has disabled itself. {ref}\n{ex}." )
            self.__enabled = False
            return r
        except Exception as ex:
            ref = exception_recorder.print_traceback( ex, f"PPA error.", ref = True ).ref
            self.state.warnings.append( f"PpaDigester encountered an error and has disabled itself. "
                                        f"The error message may contain sensitive information and is being withheld. "
                                        f"Please see the log for more details. {ref}" )
            self.__enabled = False
            return r

        for ppa in ppa_results:
            peptide = self.state.try_acquire_peptide( ppa.sequence, protein )

            if peptide is not None:
                r.append( self._score( ppa.score, self.COLUMN, peptide, subject ) )

        return r


_ppa_cache = { }


@dataclass
class _PpaPeptide:
    name: str
    sequence: str
    start: Optional[int]
    end: Optional[int]
    score: float


class _PpaError( Exception ):
    # Safe to display
    pass


_k_ppa_limit = 900  # 1000 is the online limit


def _ppa_request( sequences: Sequence[str], digest, cache, warnings: WarningCollection ) -> List[_PpaPeptide]:
    if len( sequences ) > _k_ppa_limit:
        # Handle over-sized queries
        r = []

        for sequences2 in array_helper.batch_list( sequences, _k_ppa_limit ):
            r.extend( _ppa_request( sequences2, digest, cache, warnings ) )

        return r

    if not sequences:
        return []

    r = []

    if cache:
        sequences = list( sequences )

        with array_helper.Remover( sequences ) as sequences_:
            for sequence in sequences_:
                cached = _ppa_cache.get( sequence )

                if cached is not None:
                    r.append( cached )
                    sequences_.drop()

    if not sequences:
        return r

    rx1 = re.compile( '<table border="1" align="center" class="sortable"><tr><th>Protein ID.+</table>' )
    rx2 = re.compile( '<tr><td>([A-Z0-9]+)</td><td>([A-Z]+)</td><td>([0-9]*)</td><td>([0-9]*)</td><td>NA</td><td>([0-9.]+)</td><td>NA</td></tr>' )

    url = "http://software.steenlab.org/rc4/PPA.php"

    fasta = bio_helper.make_fasta( sequences )

    data = dict( sequence = fasta,
                 seqcov = "",
                 min_digest_fragment_mass = "600",
                 max_digest_fragment_mass = "6000",
                 min_peptide_length = "5",
                 seenscore = "0.65",
                 proteinradio = "Protein Sequence" if digest else "Peptide Sequence",
                 coverage = "Protein Sequence Coverage",
                 PostComment = "submit" )

    no_file = ('', '')
    files = dict( file = no_file, scfile = no_file, nnfile = no_file )

    req = web_helper.CachedRequest( url, data = data, files = files )
    page = req.get()

    if "Not Acceptable" in page:
        raise _PpaError( "Not Acceptable. Query refused by PPA." )

    if "Account Suspended" in page:
        raise _PpaError( "Account Suspended. PPA appears to be offline." )

    mat = rx1.search( page )

    if not mat:
        warnings.append( f"Unknown response from PPA.\n"
                         f"I couldn't find the results matrix in the result:\n"
                         f"URL: {url}\n"
                         f"CACHE: {req.cache_file_name}" )
        return r

    page = mat.group( 0 )

    for match in rx2.finditer( page ):
        name = match.group( 1 )
        seq = match.group( 2 )
        start = match.group( 3 )
        end = match.group( 4 )
        score = match.group( 5 )
        pep = _PpaPeptide( name, seq, int( start ) if start else None, int( end ) if end else None, float( score ) )

        if cache:
            _ppa_cache[pep.sequence] = pep

        r.append( pep )

    return r
