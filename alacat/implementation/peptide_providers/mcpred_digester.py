from typing import Iterable, Sequence

from alacat.model import HArgs, InstantiationError, Assessors, Peptide, PeptideProvider, HColumn, RawScore, EDigestion, NamedFactor, PendingPeptide, EGrade
from mhelper import string_helper
import re
from dataclasses import dataclass
from typing import Dict, Optional

from mhelper import web_helper, bio_helper
from mhelper.bio_requests import core


class McPredDigester( PeptideProvider ):
    """
    Overview
    --------
    
    When enabled, McPred online is used to digest the protein and score the peptides based on their
    simulated successful cleavage probability.
    
    Scores
    ------
    
    The output is a "mis-cleavage chance" in the range 0 to 1.
    Lower scores indicate less chance of mis-cleavage and are generally considered to be better.
    """
    SK_MC_PRED_C = HColumn( "McPred C score", assessor = Assessors.Max( 0.5 ), rank_order = 1 )
    SK_MC_PRED_N = HColumn( "McPred N score", assessor = Assessors.Max( 0.5 ), rank_order = 1 )
    TERMINAL = NamedFactor( "TERM", "TERMINAL", EGrade.FAIL )


    def __init__( self, hargs: HArgs ):
        """
        :param hargs:   Inherited.  
        """
        hargs.set_backing_data( True, () )
        super().__init__( hargs )

        if self.state.digestion != EDigestion.TRYPSIN:
            raise InstantiationError( self, f"Cannot use a trypsin digester for a non-trypsin ({self.state.digestion}) model." )

    def _on_query_all( self, subjects: Sequence[PendingPeptide] ) -> Iterable[RawScore[Peptide]]:
        r = []

        uids: Dict[str, PendingPeptide] = { str( index ): subject for index, subject in enumerate( subjects ) }
        sequences = { uid: subject.input.sequence for uid, subject in uids.items() }

        try:
            mcp_results = _request( sequences )
        except _McPredError as ex:
            self.state.warnings.append( "Error invoking McPred:\n * "
                                        + string_helper.indent( str( ex ), 3, first = False ) )
            return []

        for mcp_result in mcp_results:
            protein = uids[mcp_result.protein]
            peptide = self.state.try_acquire_peptide( mcp_result.peptide, protein.input )

            if peptide is not None:
                r.append( self._score( mcp_result.c if mcp_result.c is not None else self.TERMINAL, key = self.SK_MC_PRED_C, subject = peptide, input = protein ) )
                r.append( self._score( mcp_result.n if mcp_result.n is not None else self.TERMINAL, key = self.SK_MC_PRED_N, subject = peptide, input = protein ) )

        return r


_URL = "http://king.smith.man.ac.uk/mcpred/results.jsp"
_FOLDER = "http://king.smith.man.ac.uk/mcpred/"
_CSV_LINK_RX = re.compile( 'Results are available to download from <a target="_blank" href="([^"]+)">here</a><br>' )


@dataclass()
class _Result:
    protein: str
    peptide: str
    n: Optional[float]
    c: Optional[float]


class _McPredError( Exception ):
    pass


def _request( sequences: Dict[str, str] ):
    fasta = bio_helper.make_fasta( sequences )

    data = {
        "sequence" : fasta,
        "predictor": "0" }

    files = dict( seqfile = web_helper.NO_FILE )
    cr = core.make_request( _URL, data = data, files = files )

    try:
        page = cr.get()
    except ConnectionError:
        raise _McPredError( f"I cannot connect to the McPred online service.\n"
                            f"See http://king.smith.man.ac.uk/mcpred for details." )

    if "Service Temporarily Unavailable" in page:
        cr.uncache()
        raise _McPredError( f"The McPred online service is currently unavailable.\n"
                            f"See http://king.smith.man.ac.uk/mcpred for details." )

    mat = _CSV_LINK_RX.search( page )

    if not mat:
        cr_str = string_helper.max_width( str( cr ), 500, trim = 0, strip = False, ellipsis = "(...)" )
        data_str = string_helper.max_width( str( data ), 500, trim = 0, strip = False, ellipsis = "(...)" )
        raise _McPredError( f"Unexpected result from mcpred: {cr_str}. data = {data_str}" )

    url_ = _FOLDER + mat.group( 1 )

    csv = core.request( url_, cache_key = (_URL, data, url_) )
    csv_rows = [row.split( "," ) for row in csv.split( "\n" ) if row]
    rows = iter( csv_rows )

    header = next( rows )
    protein_col = header.index( "Protein" )
    peptide_col = header.index( "Peptide" )
    n_col = header.index( "N-Terminal" )
    c_col = header.index( "C-Terminal" )

    r = []

    for row in rows:
        r.append( _Result( row[protein_col], row[peptide_col], _to_float( row[n_col] ), _to_float( row[c_col] ) ) )

    return r


def _to_float( x: str ) -> Optional[float]:
    if x == "--":  # terminal
        return None

    return float( x )
