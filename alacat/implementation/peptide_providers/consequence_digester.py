import re
from collections import defaultdict

from dataclasses import dataclass
from typing import Dict, Iterable, List, Sequence, Set


from alacat.model import NamedFactor, EDigestion, HArgs, HColumn, InstantiationError, Assessors, PendingPeptide, Peptide, PeptideProvider, RawScore, EGrade
from mhelper import bio_helper, string_helper, web_helper

__all__ = "ConsequenceDigester",


class ConsequenceDigester( PeptideProvider ):
    """
    Overview
    --------
    
    When enabled, Consequence online is used to digest the protein and obtain scores indicating 
    the flyability of the digested peptides.
    
    
    Scores
    ------
    
    The score output is the "consensus" value from Consequence.
    This indicates the number of Consequence's predictors that indicate positive peptide flyability.
    The value lies in the range [0,4], with all 4 predictors being the ideal outcome for a flyable peptide.
    """
    SK_CONSEQUENCE = HColumn( "Consequence score", assessor = Assessors.Equal( 4 ), rank_order = 1 )
    UNDEFINED = NamedFactor( "NDEF", "UNDEFINED", EGrade.ERROR )

    def __init__( self, hargs: HArgs ):
        """
        :param hargs:   Inherited.  
        """
        hargs.set_backing_data( True, () )
        super().__init__( hargs )
        
        from .openms_digester import Digester
        self.digester = Digester( self, EDigestion.TRYPSIN )

        if self.state.digestion != EDigestion.TRYPSIN:
            raise InstantiationError( self, f"Cannot use a trypsin digester for a non-trypsin ({self.state.digestion}) model." )

    def _on_query_all( self, subjects: Sequence[PendingPeptide] ) -> Iterable[RawScore[Peptide]]:
        r = []

        # Map our proteins to temporary IDs
        uids: Dict[str, PendingPeptide] = { f"PROT{index}": subject for index, subject in enumerate( subjects ) }
        sequences = { uid: subject.input.sequence for uid, subject in uids.items() }

        try:
            con_results = _request_consequence( self.digester, sequences )
        except _ConsequenceError as ex:
            self.state.warnings.append( "Error invoking Consequence:\n * "
                                        + string_helper.indent( str( ex ), 3, first = False ) )
            return []

        for con_result in con_results:
            protein = uids[con_result.protein]
            peptide = self.state.try_acquire_peptide( con_result.peptide, protein.input )

            if peptide is not None:
                r.append( self._score( con_result.consensus, key = self.SK_CONSEQUENCE, subject = peptide, input = protein ) )

        return r


@dataclass()
class _ConsequenceResult:
    protein: str
    peptide: str
    num_miscleavages: int
    consensus: int


class _ConsequenceError( Exception ):
    pass


def _request_consequence( digester, sequences: Dict[str, str] ) -> List[_ConsequenceResult]:
    """
    .. note::
    
        * The consequence code is very much like the mcpred code and we follow the same
          procedure, however I haven't explored it as much as McPred and there may be some edge
          cases that are missing.
        * Finding the "csv" download link doesn't work, since this always reads
          "outputnull.csv". I have no idea why but Consequence has this difference when called
          from Python (at a guess it's relying on the caller to provide a host / origin /
          referer??).
        * Nonetheless, we can parse the results from the HTML table, though this truncates our
          protein accessions to 25 characters. This shouldn't be an issue since we use custom
          accessions for the duration of the call.
    """

    fasta = bio_helper.make_fasta( sequences )

    url = "http://king.smith.man.ac.uk/CONSeQuence/output2.jsp"
    data = { "sequence"   : fasta,
             "miscleavage": "0",
             "predictor"  : "0" }
    files = dict( seqfile = web_helper.NO_FILE )
    request = web_helper.CachedRequest( url, data = data, files = files )
    data = request.get()

    matches = re.finditer( r'<tr><td>([^<]+)</td><td>([^<]+)</td><td>([^<]+)</td><td>([^<]+)</td></tr><tr>', data )
    r = []
    consequence_found: Dict[str, Set[str]] = defaultdict( set )

    for match in matches:
        protein: str = match.group( 1 )
        peptide: str = match.group( 2 )
        num_miscleavages: int = int( match.group( 3 ) )
        consensus: int = int( match.group( 4 ) )

        consequence_found[protein].add( peptide )
        r.append( _ConsequenceResult( protein, peptide, num_miscleavages, consensus ) )

    # Unfortunately consequence doesn't report 0 valued results, so generate them ourselves now
    

    for accession, sequence in sequences.items():
        all_peptides: Set[str] = set( digester.digest( sequence ) )
        missed_out: Set[str] = all_peptides - consequence_found[accession]
        
        for peptide in missed_out:
            r.append( _ConsequenceResult( accession, peptide, 0, 0 ) )

    return r
