from typing import Optional

from alacat.implementation.generic_providers.prespecified_provider import PrespecifiedProvider, CChildCode
from alacat.model import Peptide, Protein, PeptideProvider, PipelineError, DbData

__all__ = "PrespecifiedPeptideProvider",


class PrespecifiedPeptideProvider( PrespecifiedProvider[Protein, Peptide], PeptideProvider ):
    """
    Overview
    --------
    
    When enabled this provider allows peptides to be entered into the workflow before the actual
    digest stage.
    
    Such peptides include peptides entered manually and proteins with known digestions.
    
    Disabling this provider in either of these cases will result in an error. 
      
    The base class, `PrespecifiedProvider` providers specific implementation details.

    
    Scores
    ------
    
    The score is a boolean value on a peptide, indicating if the peptide was produced by this provider.
    It is for information only and has no analytical purpose.
    """
    t_parent = Protein
    t_child = Peptide
    t_parent_key = Protein
    t_parent_key_nullable = True

    def _on_get_parent_key( self, parent: Protein ) -> Protein:
        return parent

    def _on_get_coupled_state( self ):
        return self.state.pending_peptides

    def _on_get_favoured_state( self ):
        return self.state.model.peptides

    def _on_find_parent_key( self, line: DbData ) -> Optional[Protein]:
        parsed = Peptide.parse_dbdata( line )
        return self.state.find( parsed.protein_hash, Protein )

    def _on_find_parent_key_str( self, line: str ) -> Optional[Protein]:
        proteins = [protein for protein in self.state.all_entities if isinstance( protein, Protein ) and line in protein.sequence]

        if not proteins:
            raise PipelineError( f"No protein identified for the peptide '{line}'." )
        elif len( proteins ) == 1:
            return proteins[0]
        else:
            raise PipelineError( f"Multiple proteins identified for the peptide '{line}'." )

    def _on_construct_child( self, parent: Protein, line: CChildCode ) -> Optional[Peptide]:
        protein: Protein
        sequence: str

        if isinstance( line, DbData ):
            parsed: Peptide.Parsed = Peptide.parse_dbdata( line )
            assert parsed
            protein = self.state.find( parsed.protein_hash, Protein )
            assert protein is parent
            sequence = parsed.peptide_sequence
        else:
            protein = parent
            sequence = line

        peptide = self.state.try_acquire_peptide( sequence, protein )
        return peptide
