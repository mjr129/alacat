from typing import Iterable, List

import alacat
from alacat.model import EDigestion, EProvided, HArgs, HColumn, InstantiationError, Assessors, PendingPeptide, PeptideLike, PeptideProvider, RawScore

import pyopenms # Important!
                # There is a bug in PyOpenMs (this will probably never be fixed, it's been there for over a year).
                # This manifests during debug mode but also in any scenarios that pre-load PyQt. If it occurs, it
                # can be worked around. For details, see: https://stackoverflow.com/questions/55875548.

__all__ = "OpenMsDigester", "Digester",


class OpenMsDigester( PeptideProvider ):
    """
    Overview
    --------
    
    When enabled, OpenMS is used to provide a wide variety of digests from a protein.
    
    The same enzyme specified in the model is assumed.
    
    
    Scores
    ------
    
    The score is a boolean value on a peptide, indicating if the peptide was produced by this provider.
    This has some analytical value, peptides not produced by this digester may be the result
    of non-standard digestion, such as miscleavage.  
    """
    # Note that OpenMS is rather bulky, if only Trypsin is required you may wish
    # to avoid installing it - this provider will simply disable itself.
    COLUMN = HColumn( "From OpenMs?",
                      assessor = Assessors.Equal( EProvided.NORMAL ) )


    # ignored: "no cleavage", "unspecific cleavage"


    def __init__( self, hargs: HArgs ):
        hargs.set_backing_data( True, (hargs.state.model.digestion.name,) )
        super().__init__( hargs )

        if not pyopenms:
            raise InstantiationError( self, f"pyopenms is not installed." )

        self.digester = Digester( self, self.state.model.digestion )


    def _on_query( self, protein: PendingPeptide ) -> Iterable[RawScore[PeptideLike]]:
        # Write FASTA
        aas = self.digester.digest( protein.input.sequence )
        r = []

        for aa in aas:
            peptide = self.state.try_acquire_peptide( aa, protein.input )

            if peptide is not None:
                r.append( self._score( EProvided.NORMAL, self.COLUMN, peptide, protein ) )

        return r


class Digester:
    """
    Provides a simple interface to digest proteins.
    The application generally uses this digester where a quick digest is required.
    Currently, it invokes PyOpenMs. 
    """
    MAP = {
        EDigestion.TRYP_CHYMO                   : "TrypChymo",
        EDigestion.ASP_N_AMBIC                  : "Asp-N_ambic",
        EDigestion.LYS_C_P                      : "Lys-C/P",
        EDigestion.V8_DE                        : "V8-DE",
        EDigestion.V8_E                         : "V8-E",
        EDigestion.FORMIC_ACID                  : "Formic_acid",
        EDigestion.CHYMOTRYPSIN_P               : "Chymotrypsin/P",
        EDigestion.LYS_C                        : "Lys-C",
        EDigestion.PEPSINA                      : "PepsinA",
        EDigestion.TRYPSIN_P                    : "Trypsin/P",
        EDigestion.ARG_C                        : "Arg-C",
        EDigestion.TRYPSIN                      : "Trypsin",
        EDigestion.CHYMOTRYPSIN                 : "Chymotrypsin",
        EDigestion.ASP_N                        : "Asp-N",
        EDigestion.C_N_BR                       : "CNBr",
        EDigestion.ARG_C_P                      : "Arg-C/P",
        EDigestion.ASP_N_B                      : "Asp-N/B",
        EDigestion.LYS_N                        : "Lys-N",
        EDigestion.LEUKOCYTE_ELASTASE           : "leukocyte elastase",
        EDigestion.CYANOGEN_BROMIDE             : "cyanogen-bromide",
        EDigestion.IODOSOBENZOATE               : "iodosobenzoate",
        EDigestion.STAPHYLOCOCCAL_PROTEASE_D    : "staphylococcal protease/D",
        EDigestion.PEPSINA_P                    : "PepsinA + P",
        EDigestion.PROLINE_ENDOPEPTIDASE        : "proline endopeptidase",
        EDigestion.CLOSTRIPAIN_P                : "Clostripain/P",
        EDigestion.ELASTASE_TRYPSIN_CHYMOTRYPSIN: "elastase-trypsin-chymotrypsin",
        EDigestion.ALPHA_LYTIC_PROTEASE         : "Alpha-lytic protease",
        EDigestion.TWO_IODOBENZOATE             : "2-iodobenzoate",
        EDigestion.PROLINE_ENDOPEPTIDASE_HKR    : "proline-endopeptidase/HKR",
        EDigestion.GLUTAMYL_ENDOPEPTIDASE       : "glutamyl endopeptidase",
        EDigestion.GLU_C_P                      : "Glu-C+P" }


    def __init__( self, handler : "alacat.Handler", enzyme: EDigestion ):
        if pyopenms is None:
            if enzyme != EDigestion.TRYPSIN:
                raise InstantiationError( handler, "`pyopenms` is not installed and the enzyme is not `TRYPSIN`." )

            self.enzyme_name = None
            return

        enzyme_name = self.MAP.get( enzyme )

        if enzyme_name is None:
            raise InstantiationError( handler, f"not a supported digest ({enzyme})." )

        self.enzyme_name = enzyme_name
        self.digester = pyopenms.ProteaseDigestion()
        self.digester.setEnzyme( enzyme_name )


    def __getstate__( self ):
        return { "enzyme_name": self.enzyme_name }


    def __setstate__( self, state ):
        self.enzyme_name = state["enzyme_name"]
        self.digester = None  # Don't bother restoring, when reloaded the model won't be run again, only inspected


    def digest( self, sequence: str ) -> List[str]:
        if self.enzyme_name is None:
            from .trypsin_digester import SimonsTrypsinDigester
            return [x[1] for x in SimonsTrypsinDigester.digest( sequence )]

        if not sequence:
            raise ValueError( "Cannot digest an empty sequence." )

        results = []
        # noinspection PyArgumentList
        self.digester.digest( pyopenms.AASequence.fromString( sequence ), results )
        return [x.toString() for x in results]
