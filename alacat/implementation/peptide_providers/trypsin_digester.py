import os
from typing import Iterable, List, Tuple

from alacat import ex_bin
from alacat.model import EDigestion, HArgs, HColumn, InstantiationError, Assessors, PendingPeptide, PeptideLike, PeptideProvider, RawScore, EProvided
from mhelper import bio_helper, io_helper, subprocess_helper

__slots__="SimonsTrypsinDigester",

class SimonsTrypsinDigester( PeptideProvider ):
    """
    Overview
    --------
    
    This peptide provider produces peptides using an internal simulated digestion algorithm
    (biol60201-task2-sjh-1.pl).
    
    
    Scores
    ------
    
    The output is a boolean value, indicating if a peptide was produced by this digester.
    This has some analytical use, peptides not produced by this digester may be the result
    of non-standard digestion, such as miscleavage.
    """
    COLUMN = HColumn( "From trypsin?", assessor = Assessors.Equal( EProvided.NORMAL ) )
    
    
    def __init__( self, hargs: HArgs ):
        hargs.set_backing_data( True, () )
        super().__init__( hargs )
        
        if self.state.digestion != EDigestion.TRYPSIN:
            raise InstantiationError( self, f"not a trypsin model ({self.state.digestion})." )

    @classmethod
    def _on_create_default_factories( cls ) -> None:
        """
        Don't include by default.
        We now use PyOpenMs.
        """
        return None


    def _on_query( self, protein: PendingPeptide ) -> Iterable[RawScore[PeptideLike]]:
        # Write FASTA
        stdout = self.digest( protein.input.sequence )
        r = [self.__rename( protein, seq ) for acc, seq in stdout]
        return [x for x in r if x is not None]
    
    
    def __rename( self, protein: PendingPeptide, sequence ):
        if not sequence:  # TODO: Why is SEQ sometimes empty?
            return None
        
        peptide = self.state.try_acquire_peptide( sequence, protein.input )
        
        if peptide is not None:
            return self._score( EProvided.NORMAL, self.COLUMN, peptide, protein )
    
    
    @staticmethod
    def digest( sequence: str ) -> List[Tuple[str, str]]:
        if not sequence:
            raise ValueError( "Cannot digest an empty sequence." )
        
        fn = io_helper.get_temporary_file()
        
        with open( fn, "w" ) as fout:
            fout.write( f">UNTITLED\n{sequence}\n" )
        
        exe = "perl"
        script = ex_bin.get_file_name( "trypsin_digest.pl" )
        
        assert os.path.isfile( script ), f"Script not found: {script!r}"
        
        cmd = [exe, script, "--", fn]
        inf = subprocess_helper.execute( cmd, rx_stdout = True, rx_stderr = True )
        
        assert isinstance( inf.stdout, str ), f"{inf.stdout!r}"
        assert inf.stdout, f"`SimonsTrypsinDigester` command produced no stdout. Stderr reads( {inf.stderr})"
        
        return list( bio_helper.parse_fasta( text = inf.stdout ) )
