from typing import FrozenSet, Sequence, Tuple
import collections.abc
import alacat.model as model
from alacat.implementation.generic_providers.prespecified_provider import PrespecifiedProvider


class PrespecifiedQblockProvider( PrespecifiedProvider[Sequence[model.Peptide],
                                                       model.Qblock],
                                  model.QblockProvider ):
    """
    Overview
    --------
    
    When enabled this provider allows qblocks to be entered into the workflow before the actual
    qblock-producing stage.
    
    Such qblocks include qblocks entered manually.
    
    Disabling this provider will result in an error if qblocks are entered into the initial model. 
      
    The base class, `PrespecifiedProvider` providers specific implementation details.
    
    Scores
    ------
    
    The score is a boolean value on a qblock, indicating if the qblock was produced by this provider.
    It is for information only and has no analytical purpose.
    """
    t_parent = collections.abc.Sequence
    t_child = model.Qblock
    t_parent_key = frozenset
    
    
    def _on_get_parent_key( self, parent: Tuple[model.Peptide, ...] ) -> FrozenSet[model.Peptide]:
        return frozenset( parent )
    
    
    def _on_get_coupled_state( self ):
        return self.state.pending_qblocks
    
    
    def _on_get_favoured_state( self ):
        return self.state.model.qblocks
    
    
    def _on_find_parent_key( self, line: model.DbData ) -> FrozenSet[model.Peptide]:
        parsed : model.Qblock.Parsed = model.Qblock.parse_dbdata( line )
        assert parsed, f"Failed to parse qblock in {line!r}"
        peptides = []
        
        for qbrick_db_data in parsed.qbrick_db_data:
            parsed2 : model.Qbrick.Parsed = model.Qbrick.parse_dbdata( qbrick_db_data )
            assert parsed2, f"Failed to parse qbrick {qbrick_db_data!r} in {line!r}"
            
            for qpeptide_db_data in parsed2.linked_peptide_db_data:
                parsed3 : model.LinkedPeptide.Parsed = model.LinkedPeptide.parse_dbdata( qpeptide_db_data )
                assert parsed3, f"Failed to parse qpeptide {qpeptide_db_data!r} in {line!r}"
                
                peptide_hash = parsed3.peptide_hash
                peptide = self.state.find( peptide_hash, model.Peptide )
                peptides.append( peptide )
        
        return frozenset( peptides )
    
    
    def _on_construct_child( self,
                             parent: Sequence[model.Peptide],
                             line: model.DbData ) -> model.Qblock:
        return self.state.find_or_create(line, model.Qblock)
        
