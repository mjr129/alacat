from typing import Iterable, Optional, Sequence

from mhelper.bio_requests import core
import json as _json
from alacat.model import HArgs, InstantiationError, Assessors, Peptide, PeptideScorer, HColumn, TScoreResult, NamedFactor, EWeight, RawScore, EGrade
from mhelper.bio_requests import ncbi_taxonomy


TOO_SHORT = NamedFactor( "SRT","TOO_SHORT",EGrade.ERROR )
TOO_LONG = NamedFactor("LNG", "TOO_LONG",EGrade.ERROR )
NOT_FOUND = NamedFactor( "NFN", "NOT_FOUND" ,EGrade.ERROR)


class NextProtScorer( PeptideScorer ):
    """
    Overview
    --------
    
    When enabled, output is produced for NextProt's uniqueness scorer.
    
    Scores
    ------
    
    * NextProt idents
    * NextProt variants
    
    The output is in two columns- the number of similar peptides including and not
    including variants. Ideally, peptides will have only 1 match - for the
    protein they originate from. Values above 1 indicate the peptide is non-
    proteotypic, while zero-values indicate the data is not present in NextProt.
    """
    title = "NextProt uniqueness"
    SK_NO_VAR = HColumn( "NextProt idents",
                         weight = EWeight.NORMAL,
                         assessor = Assessors.Equal( 1 ) )
    SK_VAR = HColumn( "NextProt variants",
                      weight = EWeight.NORMAL,
                      assessor = Assessors.Equal( 1 ) )
    
    
    def __init__( self,
                  hargs: HArgs,
                  min_query_size = 6,
                  max_query_size = 30,
                  max_num_queries = 500 ):
        """
        :param hargs: 
        :param min_query_size:  Peptide min length for query, inclusive 
        :param max_query_size:  Peptide max length for query, inclusive
        :param max_num_queries: Maximum number of queries.
        """
        hargs.set_backing_data(True, ())
        super().__init__( hargs )
        
        # this is enforced by NextProt
        if min_query_size < 6:
            raise ValueError( "A minimum minimum query size of 6 is enforced by NextProt. Please choose a larger value." )
        
        self.min_query_size = min_query_size
        self.max_query_size = max_query_size
        self.max_num_queries = max_num_queries
        
        if not any( ncbi_taxonomy.Taxon( taxon ).inherits( 9606 ) if taxon != -1 else False for taxon in hargs.organisms ):
            raise InstantiationError( self, f"Your data specifies organism {hargs.organisms!r}, but NextProt is human (9606) only." )
    
    
    def _on_query_all( self, subjects: Sequence[Peptide] ) -> Iterable[RawScore[Peptide]]:
        if len( subjects ) > self.max_num_queries:
            self.state.warnings.append( f"{self.accession!r} is not processing {len( subjects )} peptides. This is more than current limit of {self.max_num_queries} peptides. Please query fewer peptides or increase `max_num_queries`." )
            self.do_not_cache(subjects)
            return ()
        
        return super()._on_query_all( subjects )
    
    
    def _on_query( self, peptide: Peptide ) -> TScoreResult:
        if peptide.length < self.min_query_size:
            a = b = TOO_SHORT
            self.do_not_cache([peptide])
        elif peptide.length > self.max_query_size:
            a = b = TOO_LONG
            self.do_not_cache([peptide])
        else:
            a = request( peptide.sequence, False )
            b = request( peptide.sequence, True )
            
            if not a:
                a = NOT_FOUND
            
            if not b:
                b = NOT_FOUND
        
        yield self._score( a, key = self.SK_NO_VAR, subject = peptide, input = peptide )
        yield self._score( b, key = self.SK_VAR, subject = peptide, input = peptide )


def request( sequence: str, variant_match: bool = False ) -> Optional[int]:
    url = "https://api.nextprot.org/entries/search/peptide?peptide={}&no-variant-match={}"
    
    url = url.format( sequence, (not variant_match).__str__().lower() )
    
    page = core.request( url )
    
    try:
        json = _json.loads( page )
    except Exception:
        return None
    
    # This is a bit odd, we get a list back of proteins the peptides occur within.
    # For instance NDVVPTMAQGVLEYK [1] results in 2 proteins [3].
    # however the web page [2] says the peptide is unique. 
    # [1]: /entries/search/peptide?peptide=NDVVPTMAQGVLEYK&no-variant-match=true
    # [2]: https://www.nextprot.org/entry/NX_P02649/peptides
    # [3]: no-variant-match=false mode gives us 3 proteins
    
    if not isinstance( json, list ):
        # "Result is not a list, has the API changed?"
        # more probably we have an error because we've not presented a human peptide
        return None
    
    return len( json )
