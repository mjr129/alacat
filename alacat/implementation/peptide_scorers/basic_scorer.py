from alacat.model import ETerm, HArgs, Assessors, Peptide, PeptideScorer, HColumn, TScoreResult, NamedFactor, EWeight, EGrade, Score
import re
from typing import cast

NO_PROTEIN = NamedFactor( "NOPR", "NO_PROTEIN", EGrade.ERROR )
RHK_REGEX = re.compile( "([RHK]+)" )


def _nc_terminal_formatter( score: Score ) -> str:
    if not isinstance( score.value, str ):
        return "Error - Score value is not a string?"

    v: str = cast( str, score.value )

    is_c: bool = v.endswith( "~" )

    v = v.replace( "~", "" )

    if not is_c:
        suffix = f"<span class='alacat_aa_end'>{v[-1]}</span>"
        v = v[:-1]
    else:
        suffix = ""

    elements = RHK_REGEX.split( v )

    is_good = True
    output = []
    import hatmul

    for element in elements:
        is_good = not is_good
        css_class = "alacat_aa_red" if is_good else "alacat_aa_normal"

        element_html = hatmul.forms.escape( element )
        output.append( f'<span class="{css_class}">{element_html}</span>' )

    output.append( suffix )

    return "".join( output )


class BasicScorer( PeptideScorer ):
    """
    Overview
    --------
    
    When enabled, basic information about the peptides is output into several columns.
    
    This information includes peptide start/end positions, terminal type, and flanking sequences.
    
    
    Scores
    ------
    
    Several scores are produced:
    
    * Length
        The length of the peptide, in amino-acids.
        Excessively small or large peptides may be problematic for different reasons.
        The default assessor flags up lengths outside of the 6-30 range.
    * Terminal type        
        Which terminal the peptide appears on, if any. 
        While non-terminal peptides are ideal, this issue is usually caught by the missing-linkers assessment.
    * Start position. 
        The starting position of the peptide within the protein. 
        By default, for information only. 
    * End position. 
        The end position of the peptide within the protein. 
        By default, this column is for information only.
    * N-flanking sequence. 
        The adjacent amino-acid sequence to the N-side. 
        By default, this column is for information only.
    * C-flanking sequence.
        The adjacent amino-acid sequence to the C-side. 
        By default, this column is for information only.
    * Organism of protein.
        This is the NCBI taxonomic ID of the organism for the protein, if known. 
        By default, this column is for information only.
    * Accession of protein. 
        This is the accession as provided in the input. 
        By default, this column is for information only.
    * Title of protein.
        This is the human-readable title of the protein, if known.
        By default, this column is for information only.
                                                      
                                                      
    Usage notes
    -----------
    
    Note this `Handler` only considers the originating (specified) protein, not
    any other proteins this peptide might occur in. Hence, viable output is not
    produced if no protein was specified (i.e. if the peptide was specified
    manually).
    """
    SK_LENGTH = HColumn( "Length",
                         weight = EWeight.HIGH,
                         assessor = Assessors.MinMax( 6, 30 ) )
    SK_TERMINAL = HColumn( "Terminal type" )
    SK_N_TERMINAL = HColumn( "N flank", display_order = -9, html_formatter = _nc_terminal_formatter )
    SK_SEQUENCE = HColumn( "Sequence", display_order = -8 )
    SK_C_TERMINAL = HColumn( "C flank", display_order = -7, html_formatter = _nc_terminal_formatter )
    SK_START = HColumn( "Start position" )
    SK_END = HColumn( "End position" )
    SK_PROTEIN_AC = HColumn( "Protein accession" )
    SK_PROTEIN_NAME = HColumn( "Protein title" )
    SK_PROTEIN_ORG = HColumn( "Protein organism" )
    NOT_TERMINAL = NamedFactor( "MIDL", "Not terminal", EGrade.ERROR )


    def __init__( self,
                  hargs: HArgs,
                  flank_length: int = 3 ):
        """
        :param hargs:           !INHERITED
        """
        hargs.set_backing_data( True, (flank_length,) )
        super().__init__( hargs )
        self.flank_length = flank_length


    def _on_query( self, peptide: Peptide ) -> TScoreResult:
        np = peptide.terminal == ETerm.FULL  # Peptide is protein, or protein is unknown, terminal and position values are meaningless

        n = peptide.get_flank( ETerm.N, self.flank_length )
        c = peptide.get_flank( ETerm.C, self.flank_length )

        return [
            self._score( value = peptide.sequence.__len__(),
                         key = self.SK_LENGTH,
                         subject = peptide,
                         input = peptide ),
            self._score( key = self.SK_TERMINAL,
                         value = NO_PROTEIN if np else (peptide.terminal.name if peptide.terminal != ETerm.NONE else self.NOT_TERMINAL),
                         subject = peptide,
                         input = peptide ),
            self._score( key = self.SK_START,
                         value = NO_PROTEIN if np else peptide.start,
                         subject = peptide,
                         input = peptide ),
            self._score( key = self.SK_END,
                         value = NO_PROTEIN if np else peptide.end,
                         subject = peptide,
                         input = peptide ),
            self._score( key = self.SK_N_TERMINAL,
                         value = NO_PROTEIN if np else n,
                         subject = peptide,
                         input = peptide ),
            self._score( key = self.SK_SEQUENCE,
                         value = peptide.sequence,
                         subject = peptide,
                         input = peptide ),
            self._score( key = self.SK_C_TERMINAL,
                         value = NO_PROTEIN if np else c,
                         subject = peptide,
                         input = peptide ),
            self._score( peptide.protein.accession,
                         self.SK_PROTEIN_AC,
                         subject = peptide,
                         input = peptide ),
            self._score( peptide.protein.title,
                         self.SK_PROTEIN_NAME,
                         subject = peptide,
                         input = peptide ),
            self._score( peptide.protein.organism,
                         self.SK_PROTEIN_ORG,
                         subject = peptide,
                         input = peptide )]
