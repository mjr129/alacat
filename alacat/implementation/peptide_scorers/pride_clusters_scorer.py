from functools import lru_cache
from typing import FrozenSet, Optional

from alacat.model import HArgs, InstantiationError, Peptide, PeptideScorer, HColumn, EWeight, Assessors, PerOrganismMixin
from mhelper.bio_requests import pride_clusters


class PrideClustersScorer( PeptideScorer, PerOrganismMixin ):
    """
    Overview
    --------
    
    When enabled, output is produced that indicates if this peptide is present in the Pride Clusters human data dump.
    
    
    Scores
    ------
    
    The output is a single presence/absence (boolean) column and indicates
    whether this peptide has been seen before, in the Pride Clusters database
    """
    COLUMN = HColumn( "Exists in Pride",
                      assessor = Assessors.Equal( True ),
                      weight = EWeight.LOW )
    
    
    @staticmethod
    @lru_cache
    def __get_peptides( taxon_id: int ) -> Optional[FrozenSet[str]]:
        if taxon_id is None or taxon_id == -1:
            return None
        
        try:
            return pride_clusters.pride_clusters.parse( taxon_id ).peptides
        except pride_clusters.pride_clusters.DataError:
            return None
    
    
    def __init__( self, hargs: HArgs, taxon_id : int ):
        """
        :param hargs:       !INHERITED
        """
        hargs.set_backing_data( True, (taxon_id,))
        super().__init__( hargs )

        self.taxon_id = taxon_id
        peptides_ = self.__get_peptides( taxon_id )
        
        if not peptides_:
            raise InstantiationError( self, f"A Pride Clusters database is not available for taxon: {taxon_id}" )
            
        self.peptides: FrozenSet[str] = frozenset( peptides_ )
    
    
    def _on_query( self, peptide: Peptide ):
        value = peptide.sequence in self.peptides
        return self._score( value, self.COLUMN, subject = peptide, input = peptide )
