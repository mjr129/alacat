import re
import sys
from typing import Iterable, Sequence

from alacat.model import Assessors, NamedFactor, EWeight, HArgs, HColumn, InstantiationError, Peptide, PeptideScorer,    RawScore, EGrade
from mhelper import web_helper

TOO_SHORT = NamedFactor( "SHRT", "TOO_SHORT",EGrade.ERROR )
TOO_LONG = NamedFactor( "LONG","TOO_LONG",EGrade.ERROR )
TEXT_NOT_FOUND = NamedFactor( "TXNF", "text_not_found",EGrade.ERROR )
IS_SUSPENDED = NamedFactor( "SUS", "is_suspended",EGrade.ERROR )


class GoogleSearchScorer( PeptideScorer ):
    """
    Overview
    --------
    
    When enabled, a column is producced that indicates how many times the peptide occurs on Google.

    This scorer is not enabled by default.
    
    
    Scores
    ------
    
    Values above or equal to 1 suggest that the peptide has been reported in the
    literature before, though a manual confirmation is recommended.
    
    
    Usage
    -----
    
    This scorer uses the Google webpage and is throttled. It is not suitable
    for use with large numbers of peptides*. A regex is used to pull out the
    number of hits from the English language results and performance may vary.
    
    *Since Google Scholar blocks large numbers of automated requests, the scorer
    will deactivate itself if there are more than `query_limit` peptides in your
    collection. The default `query_limit` is 50.
    
    TODO: Replace with something better than Google Scholar
    """
    __slots__ = "min_query_size", "max_query_size", "query_limit"

    COLUMN = HColumn( "GScholar count",
                      weight = EWeight.VERY_LOW,
                      assessor = Assessors.Min( 1 ) )

    __google_url = "https://scholar.google.com/scholar?hl=en&q=peptide+%22{}%22"
    __regex = re.compile( "(About )?([,0-9]+) result(s)?" )
    __headers = {'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:71.0) Gecko/20100101 Firefox/71.0'}
    __suspended: bool = False

    def __init__( self,
                  hargs: HArgs,
                  min_query_size = 6,
                  max_query_size = 30,
                  query_limit = 50):
        if self.__suspended:
            raise InstantiationError( self, "`Handler` has been suspended." )

        hargs.set_backing_data( True, () )
        super().__init__( hargs )
        self.min_query_size = min_query_size
        self.max_query_size = max_query_size
        self.query_limit = query_limit

    @classmethod
    def _on_create_default_factories( cls ) -> None:
        # Do not include in defaults
        return None

    def _on_query_all( self, peptides: Sequence[Peptide] ) -> Iterable[RawScore]:
        if len( peptides ) > self.query_limit:
            return []

        return super()._on_query_all( peptides )

    def _on_query( self, peptide: Peptide ):
        l = len( peptide.sequence )

        if l <= self.min_query_size:
            self.do_not_cache( (peptide,) )
            return self._score( TOO_SHORT, self.COLUMN, subject = peptide, input = peptide )
        elif l > self.max_query_size:
            self.do_not_cache( (peptide,) )
            return self._score( TOO_LONG, self.COLUMN, subject = peptide, input = peptide )

        url = self.__google_url.format( peptide.sequence )
        cr = web_helper.CachedRequest( url, headers = self.__headers, throttle = 3 )

        while True:
            if not self.__suspended:
                html = cr.get()
            else:
                html = cr.get_cached()

            mat = self.__regex.search( html ) if html else None

            if mat:
                count = int( mat.group( 2 ).replace( ",", "" ) )
                return self._score( count, self.COLUMN, comment = url, subject = peptide, input = peptide )

            if html and "No pages were found containing" in html:
                return self._score( TEXT_NOT_FOUND, self.COLUMN, comment = url, subject = peptide, input = peptide )

            if self.__suspended:
                return self._score( IS_SUSPENDED, self.COLUMN, comment = url, subject = peptide, input = peptide )

            if "unusual traffic from your computer network" in html:
                print( "Google is requesting user intervention.\n"
                       "Please visit the following page, follow the instructions, and then press RETURN at this prompt:\n"
                       f"    {url}\n"
                       f"    {cr.cache_file_name}\n"
                       "Enter 'D' to disable this scorer for the rest of this session, enter 'X' to forcibly terminate the application.\n"
                       ">", file = sys.stderr, end = "" )
                input_ = input().upper()

                if input_ == "X":
                    exit( 1 )
                    return None
                elif input_ == "D":
                    GoogleSearchScorer.__suspended = True
                    return self._score( TEXT_NOT_FOUND, self.COLUMN, comment = url, subject = peptide, input = peptide )

                cr.uncache()
                continue

            raise RuntimeError( f"text_not_found: {cr.cache_file_name!r}" )
