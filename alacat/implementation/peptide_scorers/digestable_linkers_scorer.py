from alacat.model import HArgs, Assessors, Peptide, PeptideScorer, HColumn, TScoreResult, ETerm, EWeight, NamedFactor, EGrade
from alacat.implementation.peptide_providers.openms_digester import Digester


class DigestableLinkersScorer( PeptideScorer ):
    """
    Overview
    --------
    
    When enabled, a column is produced that red-flags digestable linkers.
    
    
    Scores
    ------
    
    The value in the output columns is the number of cuts obtained by digesting
    the linker. Ideally this is 0. This is 1 less than the number of fragments.
    
    If there is no linker, or if the linker is too short, the output is
    "SHORT_LINKER". This is also unfavourable.
    
    * Digestable N linker?
        If this is non-zero, the N linker is digestible by the selected enzyme.
        This means that the linker will need to be modified in the Qbrick, making the environment of the peptide different to that in the original protein.
    * Digestable C linker?
        If this is non-zero, the N linker is digestible by the selected enzyme.
        This means that the linker will need to be modified in the Qbrick, making the environment of the peptide different to that in the original protein.
    """
    SK_DIG_C = HColumn( "Digestable N",
                        weight = EWeight.NORMAL,
                        assessor = Assessors.Equal( 0 ) )
    SK_DIG_N = HColumn( "Digestable C",
                        weight = EWeight.NORMAL,
                        assessor = Assessors.Equal( 0 ) )
    SHORT_LINKER = NamedFactor( "SHRT", "Short linker", EGrade.FAIL )
    
    
    def __init__( self,
                  hargs: HArgs,
                  linker_size: int = 3 ):
        """
        :param hargs:        !INHERITED 
        :param linker_size:  Size of linkers to check.
        """
        hargs.set_backing_data( True, (hargs.state.model.digestion.name, linker_size) )
        super().__init__( hargs )
        self.linker_size = linker_size
        self.digester = Digester( self, self.state.model.digestion )
    
    
    def _on_query( self, peptide: Peptide ) -> TScoreResult:
        yield self._score( self.__get_score( peptide, ETerm.N ), self.SK_DIG_N, peptide, peptide )
        yield self._score( self.__get_score( peptide, ETerm.C ), self.SK_DIG_C, peptide, peptide )
    
    
    def __get_score( self, peptide: Peptide, terminal: ETerm ):
        flank = peptide.get_flank( terminal, self.linker_size, False )
        
        if len( flank ) < self.linker_size:
            return self.SHORT_LINKER
        
        return len( self.digester.digest( flank ) ) - 1
