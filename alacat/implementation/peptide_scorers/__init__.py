"""
Implementations of peptide scorers.

Note that some peptide scores are produced when the protein is digested.
"""
from .aaindex1_scorer import AaIndex1Scorer, CustomAaIndex
from .basic_scorer import BasicScorer
from .google_scholar_scorer import GoogleSearchScorer
from .isolelectric_point_scorer import IsoelectricPointScorer
from .mass_scorer import PeptideMassScorer
from .nextprot_scorer import NextProtScorer
from .peptide_atlas_scorer import PeptideAtlasScorer
from .peptide_sieve_scorer import PeptideSieveScorer
from .pride_clusters_scorer import PrideClustersScorer
from .proteome_scorer import ProteomeScorer
from .repeat_scorer import RepeatScorer
from .robrules_scorer import RobRulesScorer
from .yolanda_scorer import YolandaScorer
from .deepmspeptide_scorer import DeepmspeptideScorer
from .digestable_linkers_scorer import DigestableLinkersScorer
from .doesitfly_scorer import DoesItFlyScorer
