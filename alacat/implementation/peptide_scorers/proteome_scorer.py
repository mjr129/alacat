from collections import Counter, defaultdict
from functools import lru_cache
from typing import Dict, Iterable, List, Optional

from alacat.implementation.peptide_providers.openms_digester import Digester
from alacat.model import Assessors, EDigestion, EWeight, HArgs, HColumn, Peptide, PeptideScorer, standard_log, PerOrganismMixin, InstantiationError, RawScore
from alacat.utilities import cache, peptide_helper
from mhelper import bio_helper, io_helper
from mhelper.bio_requests import uniprot_proteomes


class ProteomeScorer( PeptideScorer, PerOrganismMixin ):
    """
    Overview
    --------
    
    When enabled, this scorer counts the number of times this peptide's sequence and monoisotopic mass
    appears in the proteome.
    
    The "proteome" .
    
    By default, the scorer applies the following filters to the proteome:
     
    * Forbid protein isoforms
    * Forbid historic (Uniparc) proteins
    * Forbid protein fragments
    * Permit unreviewed (TREMBL) proteins
    * Use the *organisms of the state* (the organism(s) mentioned in the model
      *plus* any organism(s) covered by the input protein(s)). 
    
    In code, these values may be changed via their respective constructor arguments, whilst in the
    web interface, the values are hardcoded to the defaults. 
    
    Scores
    ------
    
    The output values are the sequence occurrence count, and the monoisotopic
    mass occurrence count, within the specified proteome(s). Two more columns
    are provided that give the actual IDs of the proteins in which the sequences
    and MMIs are repeated:
    
    * Rep count
    * MMI-rep
    * Rep IDs
    * MMI-rep IDs
    
    By default, the rep-count and MMI count are assessed by the rule "n<=1".
    This allows peptides found in their own protein to pass, as well as peptides
    not found in any protein (e.g. if the organism differs or the reference
    proteome is missing the user's entry). A more ideal rule might be "n=1" *or*
    "n!=1" depending on whether the user expects to see their protein in the
    proteome or not.
    
    Known limitations
    -----------------
    
    The scorer has some limitations and manual checking the IDs of the proteins
    actually detected is recommended:
    
    * By default, duplicates are red-flagged, however in some cases, such as
      when trying to find protein isoforms, these might be exactly what the user
      is looking for.
    * Duplicate sequences may appear in the proteome due to multiple entries for
      the same protein under different names in the FASTA file.
    * The simulated digest does *not* include missed cleavages and the scorer
      is unable to offer accurate results on miscleaved peptides.
    
    Usage notes
    ----------- 
    
    An initial index file is generated, whereby the digester must be invoked to
    produce the digests for the entire proteome. This process may take some
    time: proteomes require a download of many megabytes and a simulated digest
    must be performed on their contents.    
    """
    COL_SEQ = HColumn( "Rep count",
                       weight = EWeight.NORMAL,
                       assessor = Assessors.Max( 1 ) )
    COL_SEQ_V = HColumn( "Rep IDs" )
    COL_MMI = HColumn( "MMI-rep count",
                       weight = EWeight.NORMAL,
                       assessor = Assessors.Max( 1 ) )
    COL_MMI_V = HColumn( "MMI-rep IDs" )
    __CACHE_MAGIC_NUMBER = 7
    _digester = None


    class DigestedProteome:
        """
        Serialised version of the parsed proteome.
        """

        def __init__( self,
                      taxon_id: int,
                      enzyme: EDigestion,
                      peptides: Dict[str, List[str]],
                      mmis: Dict[float, List[int]],
                      proteome_id: str
                      ) -> None:
            """
            :param taxon_id:        ID whos proteome we represent. 
            :param enzyme:          Enzyme used to perform the digest. 
            :param peptides:        Maps peptides to the accessions of their protein(s). 
            :param mmis:            Maps peptide MMIs to the accessions of their protein(s). 
            :param proteome_id:     ID of the proteome we represent. 
            """
            self.taxon_id = taxon_id
            self.enzyme = enzyme
            self.peptides = peptides
            self.mmis = mmis
            self.proteome_id = proteome_id


    def __init__( self,
                  hargs: HArgs,
                  taxon_id: int,
                  include_isoforms: bool = False,
                  include_uniparc: bool = False,
                  include_trembl: bool = True,
                  include_fragments: bool = False,
                  ) -> None:
        """
        Constructor.
        
        .. note:: To avoid downloading a new proteome, `include_trembl` and `include_fragments`
                  are applied as a simple text-based filter and may fail to exclude proteins that
                  are not marked as "tr" or "(Fragment)". If this becomes an issue the filter
                  can be performed by the Uniprot API when requesting the proteome, but this would
                  require downloading and storing a seperate proteome for each set of arguments,
                  and I am not currently aware of any unflagged entries.
        
        :param hargs:               !INHERITED 
        :param taxon_id:            ID of the taxon who's proteome should be used. 
        :param include_isoforms:    Whether or not to include isoforms in the proteome.
        :param include_uniparc:     Whether or not to use Uniparc instead of Uniprot.
        :param include_trembl:      Whether or not to include TREMBL in the proteome.
        :param include_fragments:   Whether or not to include fragments in the proteome.
        """
        hargs.set_backing_data( True, (taxon_id, include_isoforms, include_uniparc, include_trembl, include_fragments) )
        super().__init__( hargs )
        self.taxon_id = taxon_id
        self.include_trembl = include_trembl
        self.include_fragments = include_fragments

        if ProteomeScorer._digester is None:
            ProteomeScorer._digester = Digester( self, self.state.model.digestion )

        self.proteome = self.__acquire( self.state.model.digestion, taxon_id, include_isoforms, include_uniparc )

        if self.proteome is None:
            raise InstantiationError( self, f"Proteome not available for taxon {taxon_id}." )

        standard_log( f"Note: {self} is using proteome {self.proteome.proteome_id} for taxon {self.proteome.taxon_id}." )


    def _on_query( self,
                   peptide: Peptide
                   ) -> Iterable[RawScore[Peptide]]:
        mmi = peptide_helper.Peptide( peptide.sequence ).calc_monoisotopic_mass()

        seq_occur: List[str] = self.__filter( self.proteome.peptides.get( peptide.sequence, [] ) )
        mmi_occur: List[str] = self.__filter( self.proteome.mmis.get( mmi, [] ) )

        seq_occur_str: str = "\n".join( seq_occur )
        mmi_occur_str: str = "\n".join( mmi_occur )

        yield self._score( len( seq_occur ), key = self.COL_SEQ, subject = peptide, input = peptide )
        yield self._score( seq_occur_str, key = self.COL_SEQ_V, subject = peptide, input = peptide )
        yield self._score( len( mmi_occur ), key = self.COL_MMI, subject = peptide, input = peptide )
        yield self._score( mmi_occur_str, key = self.COL_MMI_V, subject = peptide, input = peptide )

    def __filter( self, seq: List[str] ):
        if not self.include_trembl:
            # unreviewed are marked by "tr|" at the start of the accession (trembl, instead of "sp|" for swissprot)
            seq = [x for x in seq if not x.startswith( "tr|" )]

        if not self.include_fragments:
            # fragments contain "(Fragment)" (almost all) or "(Fragments)" (1 instance) in the accession.
            seq = [x for x in seq if not "(Fragment" in x]

        return seq


    @staticmethod
    @lru_cache
    def __acquire( enzyme: EDigestion,
                   organism: int,
                   include_isoforms: bool,
                   include_uniparc: bool
                   ) -> Optional["ProteomeScorer.DigestedProteome"]:
        #
        # Check cache
        #
        a = "ISOFORMS" if include_isoforms else "CANONICAL"
        b = "UNIPARC" if include_uniparc else "UNIPROT"
        cache_file = cache.get_cache_file_name( "ProteomeScorer", f"TAXON_{organism}_DIGEST_{enzyme.name}_{a}_{b}" )

        with io_helper.SequentialReader( cache_file, default = None ) as reader:
            reader.read_magic( ProteomeScorer.__CACHE_MAGIC_NUMBER )
            result = reader.read_pickle( ProteomeScorer.DigestedProteome )

        if result is not None:
            standard_log( f"`ProteomeScorer` is using cached file: {cache_file}" )
            return result

        receiver = uniprot_proteomes.get_api( include_isoforms = include_isoforms, include_uniparc = include_uniparc )

        with standard_log.time( f"Building proteome for {organism} {enzyme.name}." ):
            #
            # Get proteome ID
            #
            try:
                proteome_id = receiver.map_accession( organism )
                standard_log( f"Organism {organism} maps to {proteome_id}" )
            except receiver.DataError:
                return None  # Proteome does not exist for this organism

            #
            # Download proteome
            #
            try:
                fasta_file = receiver.download( proteome_id )
            except receiver.DataError:
                return None  # Download failed

            standard_log( f"FASTA is {fasta_file}" )

            #
            # Read FASTA
            #
            fasta = bio_helper.parse_fasta( file = fasta_file )

            #
            # Digest into peptides
            #
            peptide_seqs = defaultdict( list )
            peptide_mmis = defaultdict( list )

            digester = ProteomeScorer._digester
            assert digester is not None, "ERROR: ProteomeScorer._digester should have been initialised first."

            for protein_accession, protein_sequence in standard_log.iterate( fasta, title = "Digesting fasta" ):
                for peptide_seq in digester.digest( protein_sequence ):
                    peptide_seqs[peptide_seq].append( protein_accession )

                    try:
                        peptide = peptide_helper.Peptide( peptide_seq )
                    except KeyError:
                        # Placeholder amino acids like X, or something esoteric - ignore.
                        continue

                    peptide_mmi = peptide.calc_monoisotopic_mass()
                    peptide_mmis[peptide_mmi].append( protein_accession )

            #
            # Create result
            #
            result = ProteomeScorer.DigestedProteome( organism,
                                                      enzyme,
                                                      dict( peptide_seqs ),
                                                      dict( peptide_mmis ),
                                                      str( proteome_id ) )

            #
            # Save to cache
            #
            with io_helper.SequentialWriter( cache_file ) as writer:
                writer.write_magic( ProteomeScorer.__CACHE_MAGIC_NUMBER )
                writer.write_pickle( result )

            #
            # Return wrapper
            #
            return result
