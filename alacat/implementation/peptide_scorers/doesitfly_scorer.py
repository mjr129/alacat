from typing import Sequence, Tuple, List

from alacat import ex_bin
from alacat.model import PeptideScorer, HColumn, NamedFactor, Assessors, EWeight, HArgs, InstantiationError, Peptide, RawScore, EGrade


class DoesItFlyScorer( PeptideScorer ):
    """
    Overview
    --------
    
    When enabled, a scores column is produced for the peptides based on a machine
    learning method that determines the quantotypicness of each peptide.
    
    
    Scores
    ------
    
    For the default model, the output is an indication of the peptides
    variability, with higher values being more variable (less quantotypic).
    
    The default model has been trained to assess the variability of the peptide
    in relation to the protein quantification, as assessed by SeaMassSigma's
    Bayesian MCMC. Variabilities of the training set have been scaled to lie in
    the range [0, 1]. Output is thus *predicted variability*, with lower values
    indicating more quantotypic peptides. Note that the the median value of the
    training data is 0.14, so values above this suggest that at least half of
    all peptides in the training data may be more quantotypic.
    
    
    Usage notes
    -----------
    
    A `file_name` may be provided to specify a custom ML model. 
    
    Example using the default ML algorithm::
    
        scorer = HandlerFactory( DoesItFlyScorer )
        
    Example using a custom ML algorithm which performs classification, where
    the "1" classification is more favourable::
    
        scorer = HandlerFactory(
            klass            = DoesItFlyScorer,
            kwargs           = { "file_name": "my_classification_model.doesitfly" },
            column_overrides = [ ColumnOverride( column   = "doesitfly",
                                                 weight = EWeight.NORMAL,
                                                 assessor   = Assessors.Equal( 1 ) ) ] ) 
    """
    FAILURE = NamedFactor( "FAIL", "Prediction failure", EGrade.ERROR )
    COLUMN = HColumn( "DoesItFly score",
                      rank_order = -1,
                      assessor = Assessors.Max( 0.143492575 ),  # .14 is the median of the training data, ideally we should UVSC the training data first, then we wouldn't have this problem!
                      weight = EWeight.LOW )


    def __init__( self,
                  hargs: HArgs,
                  file_name: str = "" ):
        hargs.set_backing_data( True, (file_name,) )
        super().__init__( hargs )

        if not file_name:
            file_name = ex_bin.get_file_name( "circ3.doesitfly" )

        try:
            import doesitfly
        except ImportError:
            raise InstantiationError( self, "The `doesitfly` package is not installed." )

        self.predictor = doesitfly.FlyModel.load( file_name )

        # raise InstantiationError( self, "The `doesitfly` predictor has been temporarily disabled for testing." )


    def _on_query_all( self,
                       subjects: Sequence[Peptide]
                       ) -> Tuple[RawScore[Peptide], ...]:
        # noinspection PyUnresolvedReferences
        import doesitfly

        # Format data
        sequences: List[str] = [subject.sequence for subject in subjects]
        data = doesitfly.FlyData( is_regression = True,
                                  peptides = sequences )

        # Make predictions
        predictions: doesitfly.FlyPredictions = self.predictor.predict( data )

        # Return scores
        r = []

        for subject, prediction in zip( subjects, predictions.predicted ):
            if prediction is None:
                r.append( self._score( self.FAILURE, self.COLUMN, subject, subject ) )
                continue

            r.append( self._score( prediction, self.COLUMN, subject, subject ) )

        return tuple( r )
