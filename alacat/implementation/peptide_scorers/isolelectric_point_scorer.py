import os
from typing import Iterable, Sequence, List, Optional
from mhelper import subprocess_helper, io_helper
from alacat.model import HArgs, Peptide, PeptideScorer, HColumn, RawScore
from alacat import ex_bin


class IsoelectricPointScorer( PeptideScorer ):
    """
    Overview
    --------
    
    When enabled, an output column is produced that shows the isoelectric point estimate for each
    of the peptides.
    
    
    Scores
    ------
    
    * Isoelec
        An estimate of the isoelectric point for the peptide.
    """
    COLUMN = HColumn( "Isoelec" )
    
    
    def __init__( self, hargs: HArgs ):
        hargs.set_backing_data( True, () )
        super().__init__( hargs )
    
    
    def _on_query_all( self, subjects: Sequence[Peptide] ) -> Iterable[RawScore[Peptide]]:
        pis = IsoelectricPointScorer.request( [x.sequence for x in subjects] )
        war = []
        
        for peptide, pi in zip( subjects, pis ):
            if pi is None:
                war.append( peptide )
                continue
            
            yield self._score( pi, self.COLUMN, subject = peptide, input = peptide )
        
        if war:
            self.state.warnings.append( f"Could not determine the PI for the following peptides: {war}" )
    
    
    @staticmethod
    def request( sequences: Sequence[str] ) -> List[Optional[float]]:
        fn = io_helper.get_temporary_file()
        
        with open( fn, "w" ) as fout:
            for index, sequence in enumerate( sequences ):
                fout.write( f">S{index}\n{sequence}\n" )
        
        exe = "perl"
        script = ex_bin.get_file_name( "isoelectric_point.pl" )
        
        assert os.path.isfile( script ), f"Script not found: {script!r}"
        
        cmd = [exe, script, fn]
        inf = subprocess_helper.execute( cmd, rx_stdout = True, rx_stderr = True )
        
        assert inf.stdout, f"Command produced no stdout. Stderr reads: {inf.stderr}"
        
        r: List[Optional[float]] = [None] * len( sequences )
        lines = inf.stdout.split( "\n" )
        
        for line in lines:
            if "\t" not in line:
                continue
            
            k, v = line.split( "\t", 1 )
            index: int = int( k[1:] )
            
            assert r[index] is None
            r[index] = float( v )
        
        return r
