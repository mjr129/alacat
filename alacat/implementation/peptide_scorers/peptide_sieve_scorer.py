import os
from typing import Dict, Iterable, List, Optional, Sequence

from alacat.model import Assessors, Peptide, PeptideScorer, HColumn, RawScore, EWeight, HArgs, InstantiationError
from mhelper import io_helper, subprocess_helper, location_helper


PROPS_PATH = os.path.join( os.path.dirname( os.path.dirname( __file__ ) ), "ex_bin", "peptidesieve_properties.txt" )

__exe_path = None


def _get_exe_path(): # ~~> FileNotFoundError:
    """
    Obtains the path to PeptideSieve. 
    :return: Absolute path
    :exception FileNotFoundError: File not found.
    """
    global __exe_path
    
    if __exe_path is None:
        __exe_path = location_helper.find_file( [
                "PeptideSieve.exe",
                "PeptideSieve.linux.i386",
                "PeptideSieve.osx.i386",
                "PeptideSieve",
                "peptide_sieve/PeptideSieve.exe",
                "peptide_sieve/PeptideSieve.linux.i386",
                "peptide_sieve/PeptideSieve.osx.i386"],
                    any = True ) # ~~> FileNotFoundError
    
    return __exe_path


class PeptideSieveScorer( PeptideScorer ):
    """
    Overview
    --------
    
    When enabled this scorer invokes PeptideSieve.
    
    
    Scores
    ------
    
    Scores are produced indicating the likelihood each peptide is proteotypic for
    various mass-spec platforms:
    
    * ICAT ESI
    * MUDPIT ESI
    * PAGE ESI
    * PAGE MALDI
    
    If PeptideSieve does *not* return a score, the peptide is assigned a score of 0, 
    since this usually indicates that PeptideSieve considers the peptide to be non-
    proteotypic.       
    
    By default, all non-zero values are considered good.
    To avoid multiple similar scores affecting the output, only the MUDPIT ESI
    has a non VERY-LOW priority.
    """
    DECLINED = 0  # PeptideSieve thinks the value is so low it has declined to provide it
    COL_ICAT_ESI = HColumn( "Sieve ICAT ESI",
                            weight = EWeight.VERY_LOW,
                            rank_order = 1,
                            assessor = Assessors.NotEqual( DECLINED ) )
    COL_MUDPIT_ESI = HColumn( "Sieve MUDPIT ESI",
                              weight = EWeight.LOW,
                              rank_order = 1,
                              assessor = Assessors.NotEqual( DECLINED ) )
    COL_PAGE_ESI = HColumn( "Sieve PAGE ESI",
                            weight = EWeight.VERY_LOW,
                            rank_order = 1,
                            assessor = Assessors.NotEqual( DECLINED ) )
    COL_PAGE_MALDI = HColumn( "Sieve PAGE MALDI",
                              weight = EWeight.VERY_LOW,
                              rank_order = 1,
                              assessor = Assessors.NotEqual( DECLINED ) )
    __COL_MAP = {"icat_esi"    : COL_ICAT_ESI,
                   "mudpit_esi": COL_MUDPIT_ESI,
                   "page_esi"  : COL_PAGE_ESI,
                   "page_maldi": COL_PAGE_MALDI}
    
    
    def __init__( self, hargs: HArgs ):
        hargs.set_backing_data( True, ())
        super().__init__( hargs )
        
        try:
            _get_exe_path()
        except FileNotFoundError as ex:
            raise InstantiationError( self, "PeptideSieve not installed." ) from ex
    
    
    def _on_query_all( self, peptides: Sequence[Peptide] ) -> Iterable[RawScore]:
        results: List[RawScore] = []
        
        r = request( [x.sequence for x in peptides] )
        
        for key, values in r.items():
            score_key = self.__COL_MAP[key]
            
            for value, peptide in zip( values, peptides ):
                if value is None:
                    value = self.DECLINED
                
                results.append( self._score( key = score_key,
                                             subject = peptide,
                                             value = value,
                                             input = peptide ) )
        
        return results


def __get_array( source: Dict[str, List[Optional[float]]],
                 name: str,
                 length: int
                 ) -> List[Optional[float]]:
    r: List[Optional[float]]
    
    r = source.get( name )
    
    if r is not None:
        return r
    
    r = [None] * length
    source[name] = r
    return r


def request( peptides: Sequence[str] ) -> Dict[str, List[Optional[float]]]:
    peptide_data: List[str] = []
    length = len( peptides )
    results: Dict[str, List[Optional[float]]] = { }
    
    for index, peptide in enumerate( peptides ):
        accession = f"p{index}"
        peptide_data.append( f"{accession} {peptide}" )
    
    with io_helper.with_temporary() as peptide_file:
        with io_helper.with_temporary() as file_out:
            # Input
            io_helper.write_all_text( peptide_file, peptide_data )
            
            # Process
            subprocess_helper.execute( [_get_exe_path(),
                                        "-e", ".txt",
                                        "-O", os.path.dirname( file_out ),
                                        "-o", file_out,
                                        "-P", PROPS_PATH,
                                        "-f", "TXT",
                                        peptide_file,
                                        ] )
            
            # Output
            with open( file_out ) as fin:
                for line in fin:
                    method, accession, sequence, p_value = line.rstrip( "\n" ).split( "\t" )
                    value: float = float( p_value.strip() )
                    index: int = int( accession[1:] )
                    
                    __get_array( results, method.lower(), length )[index] = value
    
    return results
