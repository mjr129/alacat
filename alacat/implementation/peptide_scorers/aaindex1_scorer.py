from functools import lru_cache
from typing import Dict, Iterable, List, Protocol, Sequence, Tuple, Union

from alacat.model import HArgs, InstantiationError, Peptide, PeptideScorer, HColumn, TScoreResult, NamedFactor, HColumnGroup, HandlerFactory, EGrade
from mhelper.array_helper import WriteOnceDict
from mhelper import exception_helper, string_helper
from mhelper.bio_requests import genomenet_aaindex1
import statistics
from mhelper.special_types import Sentinel


class _TSummariser( Protocol ):
    """
    Protocol of summaries.
    """
    
    
    def __call__( self, vector: Iterable[float] ) -> float:
        raise NotImplementedError( "protocol" )


class _TMetric( Protocol ):
    """
    Protocol of metrics.
    """
    TAminoAcid = str
    
    
    def __getitem__( self, item: TAminoAcid ) -> float:
        raise NotImplementedError( "protocol" )
    
    
    def values( self ) -> Iterable[float]:
        raise NotImplementedError( "protocol" )


# noinspection SpellCheckingInspection
class AaIndex1Scorer( PeptideScorer ):
    """
    Overview
    --------
    
    When this scorer is enabled, the AaIndex1 metrics are used to score the peptide.
    
    The exact metrics included can be set in the constructor.
    
    This scorer is not enabled by default.
    
    Scores
    ------
    
    One score (column) is produced for each metric requested.
    The meaning of the value depends on the requested metric itself. 
    """
    # BEGIN ATTRIBUTION
    # purpose The AAIndex table is available to query in AlacatDesigner if the AaIndex1Scorer is enabled.  
    # title AAindex
    # author Nakai et al.
    # website https://www.genome.jp/aaindex/
    # date-retrieved 2021-05-25
    # article Nakai, K., Kidera, A., and Kanehisa, M.; Cluster analysis of amino acid indices for prediction of protein structure and function. Protein Eng. 2, 93-100 (1988). [PMID:3244698]
    #         Tomii, K. and Kanehisa, M.; Analysis of amino acid indices and mutation matrices for sequence comparison and structure prediction of proteins. Protein Eng. 9, 27-36 (1996). [PMID:9053899]
    #         Kawashima, S., Ogata, H., and Kanehisa, M.; AAindex: amino acid index database. Nucleic Acids Res. 27, 368-369 (1999). [PMID:9847231]
    #         Kawashima, S. and Kanehisa, M.; AAindex: amino acid index database. Nucleic Acids Res. 28, 374 (2000). [PMID:10592278]
    # license-url https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2238890/
    # license-text Copyright © 2007 The Author(s)
    #              This is an Open Access article distributed under the terms of the Creative Commons Attribution Non-Commercial License (http://creativecommons.org/licenses/by-nc/2.0/uk/) which permits unrestricted non-commercial use, distribution, and reproduction in any medium, provided the original work is properly cited.
    # END ATTRIBUTION
    
    ATYPICAL_AA = NamedFactor( "ATYP", "atypical_aa", EGrade.ERROR )
    COLUMN_GROUP = HColumnGroup( "Aaindex1 columns" )
    
    
    class ESummaries:
        """
        A selection of summary methods.
        
        These can either be specified by their class variable name in
        `Index.from_string`, or requested directly in code.
        """
        
        
        @staticmethod
        def mean( x ):
            """
            Mean, *excluding* missing values, 0 on empty sequence.
            """
            seq = [xi for xi in x if _is_not_nan( xi )]
            
            if not seq:
                return 0
            
            return statistics.mean( seq )
        
        
        @staticmethod
        def meanz( x ):
            """
            Mean, zeroing missing values, 0 on empty sequence.
            """
            seq = [xi if _is_not_nan( xi ) else 0 for xi in x]
            
            if not seq:
                return 0
            
            return statistics.mean( x )
        
        
        @staticmethod
        def sum( x ):
            """
            Sum, excluding missing values, 0 on empty sequence.
            """
            return sum( [xi for xi in x if _is_not_nan( xi )] )
        
        
        @staticmethod
        def min( x ):
            """
            Min, excluding missing values, 0 on empty sequence.
            """
            seq = [xi if _is_not_nan( xi ) else 0 for xi in x]
            
            if not seq:
                return 0
            
            return min( seq )
        
        
        @staticmethod
        def max( x ):
            """
            Max, excluding missing values, 0 on empty sequence.
            """
            seq = [xi if _is_not_nan( xi ) else 0 for xi in x]
            
            if not seq:
                return 0
            
            return max( seq )
    
    
    class EMetricSets:
        """
        A selection of metric sets.
        
        These can either be specified by their class variable name in
        `Index.from_string`, or requested directly in code.
        
        This does not cover individual metrics these are governed by
        `_get_metrics`.
        """
        
        
        @staticmethod
        def all() -> Dict[str, _TMetric]:
            """
            All AAIndex1 and custom metrics.
            """
            return _get_metrics()
        
        
        @staticmethod
        def all_complete() -> Dict[str, _TMetric]:
            """
            Same as `all`, but excludes metrics with missing values.
            """
            return { k: v for k, v in _get_metrics() if _is_complete( v ) }
    
    
    class Index:
        """
        Names an AAIndex-type metric, how to summarise it, and what column of
        the output it should go into. 
        """
        
        
        def __init__( self,
                      metric: Union[_TMetric, str],
                      summary: Union[_TSummariser, str],
                      column: Union[HColumn, str] = None ):
            """
            :param metric:  The metric lookup table (e.g. aaindex1["ARGP820101"])
            :param summary: The summary method (e.g. `statistics.mean`)
            :param column:  The name of the score column (to be created within the
                            `AaIndex1Scorer.COLUMN_GROUP`), e.g. "mean hydrophobicity index"
                            
                            Can also be a user-defined `HColumn` object. 
            """
            if isinstance( metric, str ):
                metric = self._accession_to_metric( metric )
            
            if isinstance( summary, str ):
                summary = self.__accession_to_summary( summary )
            
            if isinstance( column, str ):
                score = AaIndex1Scorer.COLUMN_GROUP.new_column( column )
            elif isinstance( column, HColumn ):
                score = column
            else:
                raise exception_helper.type_error( "column", column )
            
            self.score: HColumn = score
            self.lookup = metric
            self.summary = summary
        
        
        def __repr__( self ):
            return string_helper.auto_repr( self )
        
        
        @staticmethod
        def from_string( key: str ) -> List["AaIndex1Scorer.Index"]:
            """
            Converts a `str` to an list of `Index`\es.
            
            The string should be in the format::
            
                "summary:accession"
                
            e.g. for the mean hydrophobicity index::
            
                "mean:ARGP820101"
                
            * The *summary* should be the accession of an inbuilt method.
                * e.g. ``mean``
                * For the options, see the `ESummaries` class documentation
            * The *accession* should be one or more (comma delimited) of:
                * The accession of an AAIndex1 metric
                    * e.g. ``ANDN920101``
                    * See https://www.genome.jp/aaindex/AAindex/list_of_indices 
                * The accession of an inbuilt metric
                    * e.g. ``IS_NEUTRAL``.
                    * See `CustomAaIndex` class documentation 
                * An accession of a set of metrics
                    * e.g. ``ALL`
                    * See `EMetricSets` class documentation 
            """
            cls = AaIndex1Scorer.Index
            
            if ":" not in key:
                raise ValueError( f"Not a valid {AaIndex1Scorer.Index.from_string.__qualname__} argument." )
            
            summary, accessions = key.split( ":", 1 )
            
            index_luts = cls.__accession_to_metrics( accessions )
            summary_fn = cls.__accession_to_summary( summary )
            
            r = []
            
            for name, index_lut in index_luts.items():
                r.append( AaIndex1Scorer.Index( index_lut, summary_fn, f"{summary}:{name}" ) )
            
            return r
        
        
        @staticmethod
        def __accession_to_summary( accession: str ):
            return getattr( AaIndex1Scorer.ESummaries, accession )
        
        
        @staticmethod
        def __accession_to_metrics( accessions: str ):
            index_luts: Dict[str, _TMetric] = WriteOnceDict()
            for accession in accessions.split( "," ):
                if hasattr( AaIndex1Scorer.EMetricSets, accession ) and not accession.startswith( "_" ):
                    index_luts.update( getattr( AaIndex1Scorer.EMetricSets, accession )() )
                else:
                    index_luts[accession] = AaIndex1Scorer.Index._accession_to_metric( accession )
            return index_luts
        
        
        @staticmethod
        def _accession_to_metric( accession: str ):
            master: Dict[str, _TMetric] = _get_metrics()
            if accession not in _get_metrics():
                raise ValueError( f"There is no such AAIndex1 metric as {accession!r}." )
            return master[accession]
    
    
    def __init__( self,
                  hargs: HArgs,
                  keys: Union[str, Index, Sequence[Union[str, Index]]] = ("mean:all", "sum:all") ):
        """
        :param hargs: !INHERITED 
        :param keys:  One or more indexes to use.
                      Indexes may be passed as `Index` objects or strings.
                      For the string representation format, see `Index.from_string`.
        """
        hargs.set_backing_data( False, () )  # too much to store
        super().__init__( hargs )
        
        indexes: List[AaIndex1Scorer.Index] = []
        
        if not keys:
            raise InstantiationError( self, "A valid AAIndex1." )
        
        # Convert keys to tuple
        if not isinstance( keys, tuple ) and not isinstance( keys, list ):
            keys = keys,
        
        for key in keys:
            if isinstance( key, str ):
                indexes.extend( AaIndex1Scorer.Index.from_string( key ) )
            elif isinstance( key, AaIndex1Scorer.Index ):
                indexes.append( key )
            else:
                raise exception_helper.type_error( "key", key )
        
        self.indexes: Tuple[AaIndex1Scorer.Index, ...] = tuple( indexes )
        
        self.bind_columns( x.score for x in self.indexes )
    
    
    @classmethod
    def _on_create_default_factories( cls ) -> Union[HandlerFactory, Iterable[HandlerFactory]]:
        # Do not include by default
        return ()
    
    
    def _on_query( self, subject: Peptide ) -> TScoreResult:
        r = []
        is_valid = all( x in _Constants.AMINO_ACIDS for x in subject.sequence )
        
        for index in self.indexes:
            s: Union[NamedFactor, float]
            
            if not is_valid:
                s = self.ATYPICAL_AA
            else:
                v: List[float] = [index.lookup[aa] for aa in subject.sequence]
                s = index.summary( v )
            
            r.append( self._score( s, index.score, subject, subject ) )
        
        return r


@lru_cache
def _get_metrics() -> Dict[str, _TMetric]:
    """
    Cached reference to the selection of metrics and their accessions.
    
    These can either be specified by their accession in
    `Index.from_string`, or requested directly in code.
    
    Does not include the metric *sets*, these are governed by `EMetricSets`.
    """
    aaindex = genomenet_aaindex1.aaindex1.parse()
    custom = CustomAaIndex.ALL
    return { **aaindex, **custom }


def _is_complete( table: _TMetric ) -> bool:
    """
    If a metric contains *no* missing values.
    """
    return all( _is_not_nan( x ) for x in table.values() )


def _is_not_nan( x ):
    """
    If a float is not NaN.
    """
    return x == x


# noinspection SpellCheckingInspection
class _Constants:
    AMINO_ACIDS = frozenset( "RKWDENQHSTYACGILMFPV" )
    
    E_HYDRO_PHOBIC = Sentinel( "E_HYDROPHOBIC" )
    E_HYDRO_NEUTRAL = Sentinel( "E_NEUTRAL" )
    E_HYDRO_PHILIC = Sentinel( "E_HYDROPHILIC" )
    E_CHEM_ALIPHATIC = Sentinel( "E_CHEM_ALIPHATIC" )
    E_CHEM_AROMATIC = Sentinel( "E_CHEM_AROMATIC" )
    E_CHEM_SULFUR = Sentinel( "E_CHEM_SULFUR" )
    E_CHEM_HYDROXYL = Sentinel( "E_CHEM_HYDROXYL" )
    E_CHEM_BASIC = Sentinel( "E_CHEM_BASIC" )
    E_CHEM_ACIDIC = Sentinel( "E_CHEM_ACIDIC" )
    E_CHEM_AMIDE = Sentinel( "E_CHEM_AMIDE" )
    E_H_ACCEPTOR = Sentinel( "E_H_ACCEPTOR" )
    E_H_DONOR = Sentinel( "E_H_DONOR" )
    E_H_BOTH = Sentinel( "E_H_BOTH" )
    E_H_NONE = Sentinel( "E_H_NONE" )
    K_VOL_MAX_VERY_SMALL = 89.0
    K_VOL_MIN_SMALL = 108.5
    K_VOL_MAX_SMALL = 116.1
    K_VOL_MIN_MEDIUM = 138.4
    K_VOL_MAX_MEDIUM = 153.2


# noinspection SpellCheckingInspection
class CustomAaIndex:
    ALL = { }
    
    HYDROPATHY = {
        **{ k: _Constants.E_HYDRO_PHOBIC for k in "ACILMFWV" },
        **{ k: _Constants.E_HYDRO_NEUTRAL for k in "GHPSTY" },
        **{ k: _Constants.E_HYDRO_PHILIC for k in "RNDQEK" }
        }
    
    CHEMICAL = {
        **{ k: _Constants.E_CHEM_ALIPHATIC for k in "AGILPV" },
        **{ k: _Constants.E_CHEM_AROMATIC for k in "FWY" },
        **{ k: _Constants.E_CHEM_SULFUR for k in "CM" },
        **{ k: _Constants.E_CHEM_HYDROXYL for k in "ST" },
        **{ k: _Constants.E_CHEM_BASIC for k in "RHK" },
        **{ k: _Constants.E_CHEM_ACIDIC for k in "DE" },
        **{ k: _Constants.E_CHEM_AMIDE for k in "NQ" },
        }
    
    HDONOR = {
        **{ k: _Constants.E_H_ACCEPTOR for k in "RKW" },
        **{ k: _Constants.E_H_DONOR for k in "DE" },
        **{ k: _Constants.E_H_BOTH for k in "NQHSTY" },
        **{ k: _Constants.E_H_NONE for k in "ACGILMFPV" },
        }
    
    CHARGE = {
        **{ k: 1 for k in "RHK" },
        **{ k: -1 for k in "DE" },
        **{ k: 0 for k in "ANCQGILMFPSTWYV" },
        }
    
    POLARITY = {
        **{ k: 1 for k in "RNDQEHKSTY" },
        **{ k: 0 for k in "ACGILMFPWV" },
        }
    
    VOLUME = {
        "G": 60.1,  # VERY SMALL
        "A": 88.6,
        "S": 89.0,
        "C": 108.5,  # SMALL
        "D": 111.1,
        "P": 112.7,
        "N": 114.1,
        "T": 116.1,
        "E": 138.4,  # MEDIUM
        "V": 140.0,
        "Q": 143.8,
        "H": 153.2,
        "M": 162.9,  # LARGE
        "I": 166.7,
        "L": 166.7,
        "K": 168.6,
        "R": 173.4,
        "F": 189.9,  # VERY LARGE
        "Y": 193.6,
        "W": 227.8
        }
    
    ZERO = { k: 0 for k in _Constants.AMINO_ACIDS }
    ONE = { k: 1 for k in _Constants.AMINO_ACIDS }
    IS_BASIC = { k: v == 1 for k, v in CHARGE.items() }
    IS_NEUTRAL = { k: v == 0 for k, v in CHARGE.items() }
    IS_K = { **ZERO, "K": 1 }
    IS_R = { **ZERO, "R": 1 }
    IS_MEDIUM = { k: _Constants.K_VOL_MIN_MEDIUM <= v <= _Constants.K_VOL_MAX_MEDIUM for k, v in VOLUME.items() }
    IS_SMALL = { k: v < _Constants.K_VOL_MIN_MEDIUM for k, v in VOLUME.items() }
    
    
    @staticmethod
    def _initialise_class():
        for name, dictionary in CustomAaIndex.__dict__.items():
            if name.startswith( "_" ) or name == "ALL":
                continue
            
            if not isinstance( dictionary, dict ):
                continue
            
            v0 = dictionary["R"]
            
            if isinstance( v0, Sentinel ):
                continue
            
            assert len( dictionary ) == len( _Constants.AMINO_ACIDS )
            assert all( x in dictionary for x in _Constants.AMINO_ACIDS )
            
            CustomAaIndex.ALL[name] = dictionary


CustomAaIndex._initialise_class()
