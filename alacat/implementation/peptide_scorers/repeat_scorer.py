from alacat.model import HArgs, Assessors, Peptide, PeptideScorer, HColumn, TScoreResult, NamedFactor, EWeight, EGrade


NO_PROTEIN = NamedFactor("NPR", "NO_PROTEIN",EGrade.ERROR )


class RepeatScorer( PeptideScorer ):
    """
    Overview
    --------
    
    When enabled, this scorer counts the number of times the peptide occurs within its
    protein and input set.
    
    Scores
    ------
    
    * Repeats in protein
        Indicates the within-protein sequence occurrence count
    * Repeats in input
        Indicates the within-input sequence occurrence count
    """
    title = "Protein repeats"
    type = int
    REPEAT = HColumn( "Repeats in protein",
                      weight = EWeight.NORMAL,
                      assessor = Assessors.Equal( 1 ) )
    INPUT_REPEAT = HColumn( "Repeats in input",
                            weight = EWeight.NORMAL,
                            assessor = Assessors.Equal( 1 ) )
    
    
    def __init__( self, hargs: HArgs ):
        hargs.set_backing_data( False, () )# Depends on input set
        super().__init__( hargs )
        
    
    
    def _on_query( self, peptide: Peptide ) -> TScoreResult:
        count = self.__count_protein( peptide, peptide.protein )
        input_count = sum( self.__count_protein( peptide, protein ) for protein in self.state.all_proteins )
        
        yield self._score( count, self.REPEAT, subject = peptide, input = peptide )
        yield self._score( input_count, self.INPUT_REPEAT, subject = peptide, input = peptide )
    
    
    def __count_protein( self, peptide, protein ):
        s = 0
        count = 0
        
        while True:
            s = protein.sequence.find( peptide.sequence, s )
            
            if s == -1:
                break
            
            count += 1
            s += 1
        
        return count
