from alacat.model import HArgs, Assessors, Peptide, PeptideScorer, HColumn, TScoreResult, EWeight, NamedFactor,     InstantiationError, EGrade
from mhelper.exception_recorder import print_traceback


class YolandaScorer( PeptideScorer ):
    """
    Overview
    --------
    
    This scorer contacts the Yolanda database to score the peptides, based on
    whether they are found in various experiments.
    
    Scores
    ------
    
    One output column is produced, which indicates the presence of the peptide
    within the database. Positive scores indicate evidence has been recorded
    for the peptide, while zero scores indicate a lack of evidence. -1
    indicates the peptide is unknown to Yolanda. 
    
    Note that no attempt is made to filter out false positives, non-proteotypic
    evidence, non-intensity evidence, or zero-valued evidence.
    
    Usage notes
    -----------
    
    If Yolanda is not installed, no output is produced.
    """
    COLUMN = HColumn( "Yolanda evidence",
                      assessor = Assessors.Min( 1 ),
                      weight = EWeight.LOW )
    ERROR = NamedFactor( "ERR","error",EGrade.ERROR )

    def __init__( self, hargs: HArgs, config: str = "default" ):
        hargs.set_backing_data( False, (config,) )  # Already a local database
        super().__init__( hargs )
        self.has_warned = False
        self.config = config

        try:
            import yolanda.controller as yolanda
        except ImportError:
            raise InstantiationError( self, "Yolanda is not installed." )

    def _on_query( self, peptide: Peptide ) -> TScoreResult:
        try:
            import yolanda.controller as yolanda
        except ImportError:
            return None

        yolanda.Environment.start()
        accession = yolanda.Peptide.translate_code( peptide.sequence )

        try:
            yo_peptide = yolanda.Peptide.filter( accession ).one_or_none()
        except Exception as ex:
            if not self.has_warned:
                ref = print_traceback( ex, ref = True ).ref
                self.has_warned = True
                self.state.warnings.append(
                        f"An error ({ex.__class__.__name__}; {ref}) was encountered while trying to invoke Yolanda." )

            return self._score( self.ERROR, self.COLUMN, subject = peptide, input = peptide )

        if not yo_peptide:
            return self._score( -1, self.COLUMN, subject = peptide, input = peptide )

        return self._score( yolanda.Evidence.filter( yo_peptide ).count(), self.COLUMN, subject = peptide,
                            input = peptide )
