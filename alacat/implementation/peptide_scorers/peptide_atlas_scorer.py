from typing import Iterable, Optional, Sequence

from alacat.model import Assessors as _A, debugging, EWeight as _P, HArgs, HColumn, InstantiationError, Peptide, \
    PeptideScorer, RawScore, PerOrganismMixin, NamedFactor, EGrade
from mhelper.bio_requests.peptide_atlas_builds import peptide_atlas_builds, DataError
from mhelper.system_event_helper import SystemMutex

try:
    from mhelper import sql_helper
except ImportError as ex:
    sql_helper = None
    DATABASE_ERROR = ex
    TRemote = None
else:
    DATABASE_ERROR = None
    TRemote = sql_helper.Remote


class PeptideAtlasScorer( PeptideScorer, PerOrganismMixin ):
    """
    Overview
    --------
    
    When enabled this scorer retrieves data from peptide-atlas, notably the "empirical_proteotypic_score".
    
    Scores
    ------
    
    A number of scores are output into columns for each peptide:

    * Isoelectric point
    * Molecular weight
    * Rel hydrophobicity
    * N observations
    * N genome location
    * N protein_samples
    * Proteotypic score    
    
    Usage notes
    -----------
    
    This scorer requires prior setup:
        
    * Ensure the ``mysqlclient`` Python package is installed.
    * Ensure MySql is running on localhost.
    * Ensure the ``alacat@localhost`` user exists
    * Ensure the ``alacat`` user has write-access to the ``alacat_*`` databases::
        
        GRANT ALL PRIVILEGES ON `alacat\_%` .  * TO 'alacat'@'localhost';
        
    * Ensure AlacatDesigner knows the database password by adding it to the
      keyring::
      
        python -m mhelper.password_helper --set alacat dbpassword
    """

    COL_ISOELECTRIC_POINT = HColumn( "Isoelectric point", weight = _P.NORMAL, assessor = _A.indifferent )
    COL_MOLECULAR_WEIGHT = HColumn( "Molecular weight", weight = _P.NORMAL, assessor = _A.indifferent )
    COL_REL_HYDROPHOBICITY = HColumn( "Rel hydrophobicity", weight = _P.NORMAL, assessor = _A.indifferent )
    COL_N_OBSERVATIONS = HColumn( "N observations", weight = _P.NORMAL, assessor = _A.NotEqual( 0 ) )
    COL_N_GENOME_LOCATIONS = HColumn( "N genome locations", weight = _P.LOW, assessor = _A.Equal( 1 ) ) # Low priority, similar to proteome repeats
    COL_N_PROTEIN_SAMPLES = HColumn( "N protein_samples", weight = _P.LOW, assessor = _A.NotEqual( 0 ) ) # Low priority, similar to N observations
    COL_PROTEOTYPIC_SCORE = HColumn( "Proteotypic score", weight = _P.NORMAL, assessor = _A.Min( 0.5 ), rank_order = 1 )

    __slots__ = ()

    QUERY = \
        """
        SELECT
            peptide.peptide_sequence,
            peptide.peptide_isoelectric_point,
            peptide.molecular_weight,
            peptide.SSRCalc_relative_hydrophobicity,
            peptide_instance.n_observations,
            peptide_instance.n_genome_locations,
            peptide_instance.n_protein_samples,
            peptide_instance.empirical_proteotypic_score       
        FROM
        peptide
        LEFT JOIN peptide_instance
        ON peptide_instance.peptide_id = peptide.peptide_id
        WHERE peptide.peptide_sequence IN %s;
        """

    NOT_FOUND = NamedFactor( "NFN", "NotFound", EGrade.FAIL )

    def __init__( self, hargs: HArgs, taxon_id: int ):
        hargs.set_backing_data( False, (taxon_id,) )  # it's already in a database!
        super().__init__( hargs )

        self.taxon_id = taxon_id

        if sql_helper is None:
            raise InstantiationError( self, DATABASE_ERROR.__str__() )

        self.database = PeptideAtlasScorer.ensure_database_present( taxon_id )

        if self.database is None:
            raise InstantiationError( self, f"A PeptideAtlas database is not available for taxon {taxon_id}." )

    @staticmethod
    def ensure_database_present( taxon_id: int ) -> Optional[TRemote]:
        with SystemMutex.create( "alacat.PeptideAtlasScorer.ensure_database_present" ):
            db_name = PeptideAtlasScorer.__get_db_name( taxon_id )
            database = sql_helper.Remote( user_name = "alacat",
                                          password_key = ("alacat", "dbpassword"),
                                          db_name = db_name )

            if PeptideAtlasScorer.__db_exists( database ):
                return database

            # Download db file
            try:
                db_file = peptide_atlas_builds.mysql_database.download( taxon_id )
            except DataError:
                return None

            assert db_file

            with debugging.standard_log.time( f"Importing {db_file} into {db_name}" ):
                database.create_database()
                database.execute_file( db_file )

                #
                # We must create the indexes we will use manually, since they're not
                # defined in the import file.
                # We also flag a `alacat_designer.initialised` value to "1" to say
                # that the whole import has run. Without this flag we assume the
                # import failed and retry next time!
                #
                with database.connect() as db:
                    with db.cursor() as cur:
                        cur.execute( 'CREATE INDEX peptide_peptide_sequence_index ON peptide (peptide_sequence)' )
                        cur.execute( 'CREATE INDEX peptide_instance_peptide_id_index ON peptide_instance (peptide_id)' )
                        cur.execute( 'CREATE TABLE alacat_designer (`key` VARCHAR(255), `value` VARCHAR(255))' )
                        cur.execute( 'INSERT INTO alacat_designer (`key`, `value`) VALUES ("initialised", "1")' )

                    db.commit()

            return database

    @staticmethod
    def __db_exists( database: "sql_helper.Remote" ) -> bool:
        if not database.db_exists():
            return False

        with database.connect() as db:
            with db.cursor() as cur:
                try:
                    cur.execute( 'SELECT `value` FROM `alacat_designer` WHERE `key`="initialised"' )
                    row0 = cur.fetchone()

                    if row0 is None:
                        value = None
                    else:
                        value = row0[0]

                except sql_helper.MySQLdb.ProgrammingError:
                    fail = True  # table does not exist
                else:
                    fail = value != "1"

            if fail:
                with debugging.standard_log.time(
                        f"Database is corrupt (probably due to a previously aborted import). I am going to drop the database `{database.db_name}` and try again. You have 10 seconds to cancel!" ):
                    import time
                    time.sleep( 10 )
                    with db.cursor() as cur:
                        cur.execute( f"DROP DATABASE {database.db_name}" )
                return False

        return True

    @staticmethod
    def __get_db_name( taxon_id: int ) -> str:
        db_name = f"alacat_peptide_atlas_db_{taxon_id}"
        return db_name

    def _on_query_all( self, subjects: Sequence[Peptide] ) -> Iterable[RawScore[Peptide]]:
        mapping = { subject.sequence: subject for subject in subjects }
        found = set()
        all_ = set( subjects )

        # TODO: Only really need to yield for one DB since they're all the same!

        with self.database.connect() as db:
            with db.cursor() as cur:
                cur.execute( self.QUERY, [list( mapping.keys() )] )

                for row in cur:
                    sequence = row[0]
                    peptide = mapping[sequence]
                    found.add( peptide )

                    yield self._score( value = row[4], key = self.COL_N_OBSERVATIONS, subject = peptide, input = peptide )
                    yield self._score( value = row[5], key = self.COL_N_GENOME_LOCATIONS, subject = peptide, input = peptide )
                    yield self._score( value = row[6], key = self.COL_N_PROTEIN_SAMPLES, subject = peptide, input = peptide )
                    yield self._score( value = row[7], key = self.COL_PROTEOTYPIC_SCORE, subject = peptide, input = peptide )
                    yield self._score( value = row[1], key = self.COL_ISOELECTRIC_POINT, subject = peptide, input = peptide )
                    yield self._score( value = row[2], key = self.COL_MOLECULAR_WEIGHT, subject = peptide, input = peptide )
                    yield self._score( value = row[3], key = self.COL_REL_HYDROPHOBICITY, subject = peptide, input = peptide )

        missed = all_ - found

        for peptide in missed:
            yield self._score( value = self.NOT_FOUND, key = self.COL_N_OBSERVATIONS, subject = peptide, input = peptide )
            yield self._score( value = self.NOT_FOUND, key = self.COL_N_GENOME_LOCATIONS, subject = peptide, input = peptide )
            yield self._score( value = self.NOT_FOUND, key = self.COL_N_PROTEIN_SAMPLES, subject = peptide, input = peptide )
            yield self._score( value = self.NOT_FOUND, key = self.COL_PROTEOTYPIC_SCORE, subject = peptide, input = peptide )
            yield self._score( value = self.NOT_FOUND, key = self.COL_ISOELECTRIC_POINT, subject = peptide, input = peptide )
            yield self._score( value = self.NOT_FOUND, key = self.COL_MOLECULAR_WEIGHT, subject = peptide, input = peptide )
            yield self._score( value = self.NOT_FOUND, key = self.COL_REL_HYDROPHOBICITY, subject = peptide, input = peptide )
