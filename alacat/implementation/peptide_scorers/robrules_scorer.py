from typing import cast, Dict, Iterable, List, Sequence, Set

from alacat.model import HArgs, Assessors, Peptide, PeptideScorer, HColumn, TScoreResult, ETerm, EWeight, RawScore


class RobRulesScorer( PeptideScorer ):
    """
    Overview
    --------
    
    When enabled, this scorer asserts certain string-based no-go peptide rules, the "Rob rules".
    
    Scores
    ------
    
    For the amino-acid checks, the scores are counts of how many violations
    occur. The ideal score is 0. Non-zero scores indicate the peptide contains
    the specified subsequence and may not make a suitable Qbrick. Note that the
    "GLUTAMINE_START" rule only looks at one residue and so only two possible
    values are allowed (true or false).
    
    * C count
        Number of C/cysteine/cys.                  
    * M count
        Number of M/methionine/met. 
    *  H count,
        Number of H/histidine/his. 
    * NG count
        Number of NG (N/asparagine/asn, G/glycine/gly). 
    * DG count
        Number of DG (D/aspartic-acid/asp, G/glycine/gly). 
    * Q start
        Peptide starts with Q/glutamine/gln. 
                                
    
    For the linker checks, the scores are strings, "N" indicating the N-terminal
    violates the rule, and "C" indicating the C-terminal violates the rule.
    The ideal value for such scores is blank (""). Non-blank scores indicate the
    amino acid will need to be substituted in the Qbrick meaning the environment
    of the peptide in the Qconcat will be less similar to the environment in the
    original protein.
    
    * R in linker
        R/arginine/arg in linker. 
    * K in linker
        K/lysine/lys in linker. 
    * Linker missing
        No linker (terminal peptide) or linker unknown.
        
    Source
    ------
    
    Email from Rob B:
    
        The problem is one of charge state, and distribution of signal over multiple charge states.
        
        So, a ‘normal’ peptide: doubly charged.. [M+2H]++
        Add a histidine, we now have [M+2H]++ and [M+3H]+++ which will, at best, split the signal
        Add more histidines and it gets worse
        
        Ideally, we’d have rules that say: 
        
        Eliminate H
        Of all [M+2H]++ peptides,
        Eliminate M
        Eliminate NG, NV
        Eliminate n-term Q
        
        Are there any left?
        
        If not, then relax rules
        Add back M
        Add back n-term Q
        (note I do not want to add back NG…)
        
        Allow one more H
        
        Of all [M+3H]+++ peptides….
        Etc
    """

    # First relaxation
    SK_METHIONINES = HColumn( "M count",
                              assessor = Assessors.Equal( 0 ),
                              weight = EWeight.HIGH )
    SK_GLUTAMINE_START = HColumn( "Q start",
                                  assessor = Assessors.Equal( False ),
                                  weight = EWeight.HIGH )
    SK_HISTIDINES = HColumn( "H count",
                             assessor = Assessors.Equal( 0 ),
                             rank_order = -1,  # smallest first
                             rank_assessor = Assessors.Percentile( 0 ),  # i.e. only top
                             weight = EWeight.NORMAL
                             )

    # Do not relax
    SK_ASPARAGINE_GLYCINE = HColumn( "NG count",
                                     assessor = Assessors.Equal( 0 ),
                                     weight = EWeight.HIGH )
    SK_ASPARAGINE_VALINE = HColumn( "NV count",
                                    assessor = Assessors.Equal( 0 ),
                                    weight = EWeight.HIGH )

    # Other rules
    SK_CYSTEINES = HColumn( "C count", assessor = Assessors.Equal( 0 ),
                            weight = EWeight.LOW )

    SK_ASPARTIC_ACID_GLYCINE = HColumn( "DG count", assessor = Assessors.Equal( 0 ) )

    SK_ARGININE_LINKER = HColumn( "R in linker", assessor = Assessors.Equal( "" ) )
    SK_LYSINE_LINKER = HColumn( "K in linker", assessor = Assessors.Equal( "" ) )
    SK_FULL_LINKER = HColumn( "Linker missing", assessor = Assessors.Equal( "" ) )
    SK_DP = HColumn( "DP count", assessor = Assessors.Equal( 0 ) )

    # These are not bad, but make people unhappy to see a R/K
    SK_RP = HColumn( "RP count", assessor = Assessors.Equal( 0 ), weight = EWeight.LOW )
    SK_KP = HColumn( "KP count", assessor = Assessors.Equal( 0 ), weight = EWeight.LOW )

    def __init__( self,
                  hargs: HArgs,
                  linker_size: int = 3 ):
        """
        :param hargs: 
        :param linker_size:  Size of linkers to check. 0 disables linker checking.
        """
        hargs.set_backing_data( True, (linker_size,) )
        super().__init__( hargs )
        self.linker_size = linker_size

    def _on_query( self, peptide: Peptide ) -> TScoreResult:
        yield self._score( peptide.sequence[0] == "Q", key = self.SK_GLUTAMINE_START, subject = peptide, input = peptide )
        yield self.__make_counter_score( self.SK_ASPARAGINE_GLYCINE, peptide, "NG" )
        yield self.__make_counter_score( self.SK_ASPARTIC_ACID_GLYCINE, peptide, "DG" )
        yield self.__make_counter_score( self.SK_CYSTEINES, peptide, "C" )
        yield self.__make_counter_score( self.SK_METHIONINES, peptide, "M" )
        yield self.__make_counter_score( self.SK_HISTIDINES, peptide, "H" )

        yield self.__make_counter_score( self.SK_DP, peptide, "DP" )
        yield self.__make_counter_score( self.SK_RP, peptide, "RP" )
        yield self.__make_counter_score( self.SK_KP, peptide, "KP" )

        if self.linker_size:
            n = peptide.get_flank( ETerm.N, self.linker_size, False )
            c = peptide.get_flank( ETerm.C, self.linker_size, False )

            if n:
                n = n[:-1]

            yield self.__make_linker_score( peptide, self.SK_ARGININE_LINKER, "R" in n, "R" in c )
            yield self.__make_linker_score( peptide, self.SK_LYSINE_LINKER, "K" in n, "K" in c )
            yield self.__make_linker_score( peptide, self.SK_FULL_LINKER, len( n ) != self.linker_size - 1, len( c ) != self.linker_size )

    def __make_counter_score( self, key: HColumn, peptide: Peptide, symbol: str ):
        count = peptide.sequence.count( symbol )
        return self._score( count, key = key, subject = peptide, input = peptide )

    def __make_linker_score( self, peptide: Peptide, key, n: bool, c: bool ):
        if n:
            if c:
                r = "NC"
            else:
                r = "N"
        elif c:
            r = "C"
        else:
            r = ""

        return self._score( r, key = key, subject = peptide, input = peptide )


class AdvancedRobRulesScorer( PeptideScorer ):
    """
    Overview
    --------
    
    When enabled, this scorer asserts a sequence of "Rob rules" designed to isolate a set of
    "good" peptides.
    
    Only the rank of the score is assessed (not the actual value), hence we pick from the set
    of "best" peptides.
    
    Scores
    ------
    
    One score is output, which follows the following protocol for each peptide:
    
    To all but these are assigned SCORE 0:
    
        H, M, NG, NV, n-term Q
    
    Of the remainder, to all but these are assigned SCORE 1:
    
        H > 1, NG, NV
    
    Of the remainder, to all but these are assigned SCORE 3 (then, 4, etc...):
    
        H > 2, NG, NV
        
    To the remainder, to all are assigned SCORE 100.
    """
    __slots__ = ()
    SK_ROBRULES = HColumn( "EliminationChain",
                           weight = EWeight.VERY_HIGH,
                           assessor = Assessors.Indifferent(),
                           rank_order = -1,
                           rank_assessor = Assessors.Percentile( 0 ) )
    _BACKING_VERSION = 2

    def __init__( self,
                  hargs: HArgs ):
        hargs.set_backing_data( True, (self._BACKING_VERSION,) )
        super().__init__( hargs )

    def _on_query_all( self, subjects_: Sequence[Peptide] ) -> Iterable[RawScore[Peptide]]:
        o = { }

        subjects = set( subjects_ )

        x: Peptide
        self.__push( o,
                     subjects,
                     lambda x: cast( Peptide, x ).sequence.__contains__( "H" ),
                     lambda x: cast( Peptide, x ).sequence.__contains__( "M" ),
                     lambda x: cast( Peptide, x ).sequence.__contains__( "NG" ),
                     lambda x: cast( Peptide, x ).sequence.__contains__( "NV" ),
                     lambda x: cast( Peptide, x ).sequence.startswith( "Q" ) )

        self.__push( o,
                     subjects,
                     lambda x: cast( Peptide, x ).sequence.count( "H" ) <= 1,
                     lambda x: cast( Peptide, x ).sequence.__contains__( "NG" ),
                     lambda x: cast( Peptide, x ).sequence.__contains__( "NV" ) )

        n = 2

        while self.__push( o,
                           subjects,
                           lambda x: cast( Peptide, x ).sequence.count( "H" ) <= n,
                           lambda x: cast( Peptide, x ).sequence.__contains__( "NG" ),
                           lambda x: cast( Peptide, x ).sequence.__contains__( "NV" ) ):
            n += 1

        for score, peptides in o.items():
            for peptide in peptides:
                yield self._score( score, self.SK_ROBRULES, peptide, peptide )

        for remaining in subjects:
            yield self._score( 100, self.SK_ROBRULES, remaining, remaining )

    def __push( self, o: Dict[int, List[Peptide]], peptides: Set[Peptide], *rules ) -> bool:
        passing = [peptide for peptide in peptides if not any( rule( peptide ) for rule in rules )]
        peptides -= set( passing )
        o[len( o )] = passing
        return bool( passing )
