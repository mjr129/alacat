"""
This code was adapted from the DeepMsPeptide code.
The Tensorflow model remains the same.

Here, the DeepMsPeptide code is presented as a library, rather than an
executable and the Tensorflow.Keras model is wrapped in an ALACAT PeptideScorer
for compatibility with ALACAT.

See below the attribution.
"""
from typing import List, Tuple, Optional, Sequence

from alacat.model import PeptideScorer, HArgs, InstantiationError, Peptide, HColumn, Assessors, NamedFactor, EWeight, EGrade
from alacat import ex_bin, RawScore
import numpy

class __LoadLibrariesImpl:
    
    def __init__( self ):
        """
        Tensorflow takes ages to load, and spews out a load of messages, only load
        it if required.
        """
        import os
        os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # hide tensorflow warnings
        
        try:
            # noinspection PyPackageRequirements
            from tensorflow import keras as _keras
            # noinspection PyPackageRequirements
            import h5py as _h5py
        except ImportError:
            _keras = None
            _h5py = None
        
        self.keras = _keras
        self.h5py = _h5py


def _libs():
    return __LoadLibrariesImpl()


class DeepmspeptideScorer( PeptideScorer ):
    """
    Overview
    --------
    
    When enabled, this scorer uses the DeepMsPeptide model to score the peptides based on their
    estimated proteotypicality.
    
    
    Scores
    ------
    
    The score produced is a real-typed classification per peptide, with 0 indicating the class of
    non-proteotypic peptides, and 1 indicating the class of proteotypic peptides.
    The ideal value is above 0.5.
    
    
    Usage notes
    -----------
    
    Keras must be installed if you wish to use this scorer, it's rather big so it is not
    included in the program by default.
    """
    # BEGIN ATTRIBUTION
    # purpose DeepMsPeptide scores are available in AlacatDesigner if the DeepmspeptideScorer is enabled 
    # author "Guillermo Serrano Sanz et al.
    # title DeepMsPeptide
    # website https://github.com/vsegurar/DeepMSPeptide
    # article Guillermo Serrano, Elizabeth Guruceaga, Victor Segura, DeepMSPeptide: peptide detectability prediction using deep learning, Bioinformatics, Volume 36, Issue 4, 15 February 2020, Pages 1279–1280, https://doi.org/10.1093/bioinformatics/btz708
    # date-retrieved 2021-05-25
    # license-url https://github.com/vsegurar/DeepMSPeptide/blob/master/LICENSE
    # license-text MIT License
    #              Copyright (c) 2019 Guillermo Serrano Sanz
    #              
    #              Permission is hereby granted, free of charge, to any person obtaining a copy
    #              of this software and associated documentation files (the "Software"), to deal
    #              in the Software without restriction, including without limitation the rights
    #              to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    #              copies of the Software, and to permit persons to whom the Software is
    #              furnished to do so, subject to the following conditions:
    #              
    #              The above copyright notice and this permission notice shall be included in all
    #              copies or substantial portions of the Software.
    #              
    #              THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    #              IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    #              FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    #              AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    #              LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    #              OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    #              SOFTWARE.
    # END ATTRIBUTION
    
    COLUMN = HColumn( "DeepMsPeptide",
                      rank_order = 1,
                      assessor = Assessors.Min( 0.5 ),
                      weight = EWeight.LOW )
    TOO_LONG = NamedFactor( "LONG", "Too long", EGrade.ERROR )
    
    
    def __init__( self, hargs: HArgs ):
        hargs.set_backing_data( True, () )
        super().__init__( hargs )
        
        if _libs().keras is None:
            raise InstantiationError( self, "Keras for Tensorflow is not installed." )
    
    
    def _on_query_all( self, subjects: Sequence[Peptide] ) -> Tuple[RawScore[Peptide], ...]:
        sequences: List[str] = [subject.sequence for subject in subjects]
        predictions = _process( sequences )
        r = []
        
        for subject, prediction in zip( subjects, predictions ):
            if prediction is None:
                r.append( self._score( self.TOO_LONG, self.COLUMN, subject, subject ) )
                continue
            
            assert isinstance( prediction, numpy.ndarray )
            assert prediction.shape == (1,), prediction.shape
            prediction = prediction[0].item()
            r.append( self._score( prediction, self.COLUMN, subject, subject ) )
        
        return tuple( r )


MAX_LEN = 81


def __codify( sequences: List[str] ) -> Tuple[object, List[Optional[int]]]:
    aa_dict = { 'A': 1, 'R': 2, 'N': 3, 'D': 4, 'C': 5, 'Q': 6, 'E': 7, 'G': 8, 'H': 9, 'I': 10, 'L': 11, 'K': 12, 'M': 13, 'F': 14,
                'P': 15, 'O': 16, 'S': 17, 'U': 18, 'T': 19, 'W': 20, 'Y': 21, 'V': 22 }
    
    pep_codes = []
    mapping = []
    
    for sequence in sequences:
        if len( sequence ) <= MAX_LEN:
            mapping.append( len( pep_codes ) )
            pep_codes.append( [aa_dict[aa] for aa in sequence] )
        else:
            mapping.append( None )
    
    predict_data = _libs().keras.preprocessing.sequence.pad_sequences( pep_codes, value = 0, padding = 'post', maxlen = MAX_LEN )
    return predict_data, mapping


def __load_model():
    file_name = ex_bin.get_file_name( 'model_2_1D.h5.gz' )
    
    import gzip
    with gzip.GzipFile( file_name, "r" ) as gz:
        with _libs().h5py.File( gz, "r" ) as h5:
            return _libs().keras.models.load_model( h5 )


def _process( peptides: List[str] ):
    predict_data, mapping = __codify( peptides )
    model = __load_model()
    
    predictions = model.predict( predict_data )
    
    return [predictions[index] if index is not None else None for index in mapping]
