from alacat.model import HArgs, Assessors, Peptide, PeptideScorer, HColumn, TScoreResult
from alacat.utilities import peptide_helper


class PeptideMassScorer( PeptideScorer ):
    """
    Overview
    --------
    
    When enabled, the monoisotopic and average masses are output for each peptide.
    Peptide formulae are also output as a byproduct.
    
    Scores
    ------
    
    * Average mass
    * Monoisotopic mass
    * Molecular formula
    * AA composition
    
    Very large or small values may be problematic, and are detected by the default assessor on the
    monoisotopic mass column. 
    """
    title = "Mass"
    SK_AVG_MASS = HColumn( "Average mass" )
    SK_MI_MASS = HColumn( "Monoisotopic mass",
                          assessor = Assessors.MinMax( 300, 3000 ) )
    SK_MOL_FORMULA = HColumn( "Molecular formula" )
    SK_RESIDUE_FORMULA = HColumn( "AA composition" )
    
    
    def __init__( self,
                  hargs: HArgs ):
        """
        :param hargs: 
        """
        hargs.set_backing_data( True, () )
        super().__init__( hargs )
    
    
    def _on_query( self, peptide: Peptide ) -> TScoreResult:
        pep = peptide_helper.Peptide( peptide.sequence )
        
        yield self._score( subject = peptide, input = peptide, value = pep.calc_average_mass(), key = self.SK_AVG_MASS )
        yield self._score( subject = peptide, input = peptide, value = pep.calc_monoisotopic_mass(), key = self.SK_MI_MASS )
        yield self._score( subject = peptide, input = peptide, value = pep.calc_molecular_formula().to_string(), key = self.SK_MOL_FORMULA )
        yield self._score( subject = peptide, input = peptide, value = pep.to_string(), key = self.SK_RESIDUE_FORMULA )
