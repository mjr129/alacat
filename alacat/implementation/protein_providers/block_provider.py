from typing import List, Union
from alacat.model import Protein, ProteinProvider, HColumn, RawScore, HArgs, Pending, EProvided, PendingProtein
from mhelper import bio_helper


class BlockProvider( ProteinProvider ):
    """
    Overview
    --------
    
    When this provider is enabled, the user may input proteins as a single solid block of text.
    
    Each line of the text is assumed to represent a protein and is sent to the other protein providers for parsing.
    
    If this provider is not enabled, proteins must be provided as a Python list (``[]``).
    
    This provider mainly exists to support the web GUI, which is only capable of providing a single protein string.
    If disabled, the web GUI may not function correctly.
    
    Scores
    ------
    
    The score is a boolean value on a protein, indicating if the protein was produced by this provider.
    It is for information only and has no analytical purpose.
    """
    COLUMN = HColumn( "From text?" )
    
    
    def __init__( self, hargs: HArgs ):
        hargs.set_backing_data( False, ())  # Variable input
        super().__init__( hargs )
    
    
    def _on_query( self, subject_: PendingProtein ) -> List[RawScore[Union[Protein, Pending[str, Protein]]]]:
        # NOTE: We actually repeat the FASTA reading code here (from FastaProvider)
        #       it might make more sense to have such providers accept blocks 
        subject: str = subject_.input.text
        
        if "\n" not in subject:
            # Not a block!
            return []
        
        # Remove Windows CR
        subject = subject.replace( "\r", "" )
        
        # Check for JSON
        lstripped = subject.lstrip()
        
        if lstripped.startswith( "[" ) or lstripped.startswith( "{" ):
            return []
        
        # Check for FASTA
        if ">" in subject:
            # FASTA format
            r = []
            
            for accession, sequence in bio_helper.parse_fasta( text = subject ):
                r.append( self._score( True, self.COLUMN, self.state.acquire_protein( accession, sequence, None, None ), subject_ ) )
            
            return r
        
        # Must be a plain list
        r = []
        
        for line in subject.split( "\n" ):
            line = line.strip()
            
            if line:
                text_ent = self.state.acquire_text( line )
                r.append( self._score( EProvided.NORMAL, self.COLUMN, Pending( text_ent ), subject_ ) )
        
        return r
