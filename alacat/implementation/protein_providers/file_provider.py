import csv
import json as _json
import os
from enum import auto, Enum
from io import StringIO
from typing import Dict, List, Optional, Sequence, Union

from alacat.model import DbData, EProvided, HArgs, HColumn, Pending, PendingProtein, Protein, ProteinLike, \
    ProteinProvider, Text, RawScore
from mhelper import bio_helper, file_helper, io_helper
from mhelper.array_helper import index_of_first

TResult = Optional[List[RawScore[Union[Protein, Pending[Text, Protein]]]]]


class FileProvider( ProteinProvider ):
    """
    Overview
    --------
    
    When this provider is enabled, proteins/peptides may be input by the user as either a solid block of text *or* the
    name of a text file.
    
    File formats
    ~~~~~~~~~~~~
    
    The content is "sniffed" to try and determine the format, but hints can be given, either a file extension can be
    present (e.g. c:\\proteins.FASTA) or a prefix "FILE_TYPE@" can be specified before the file-name or content-block.
    
    Example::
    
        FASTA@c:\proteins
    
    Scores
    ------
    
    The score is a boolean value on a protein, indicating if the protein was produced by this provider.
    It is for information only and has no analytical purpose.
    """
    COLUMN = HColumn( "From file?")

    class EType( Enum ):
        FASTA = auto()
        TSV = auto()
        CSV = auto()
        JSON = auto()
        LIST = auto()

    __MAX_PATH = 2048
    __BAD = "\r\n\t"
    __MAPPING = { "fasta": EType.FASTA,
                  "tsv"  : EType.TSV,
                  "csv"  : EType.CSV,
                  "json" : EType.JSON,
                  "list" : EType.LIST }
    __DEFAULT_HEADERS = {
        EType.CSV : ["sequence"],  # protein sequence
        EType.TSV : [],
        EType.LIST: ["accession"],  # protein accession 
    }

    def __init__( self,
                  hargs: HArgs,
                  file_type: EType = None,
                  csv_protein_sequence_col = ("sequence", "protein"),
                  csv_protein_accession_col = ("accession", "Group"),  # group is SeaMass terminology
                  csv_peptide_sequence_col = ("peptide", "Component"),  # component is SeaMass terminology
                  csv_assume_headers: Optional[Sequence[str]] = None
                  ) -> None:
        """
        
        :param hargs:                       Inherited.
         
        :param file_type:                   File type to assume if is not explicit.
         
        :param csv_protein_sequence_col:    Column used to obtain the protein sequence.
        
        :param csv_protein_accession_col:   Column used to obtain the protein accession.
         
        :param csv_peptide_sequence_col:    Column used to obtain the peptide sequence.
         
        :param csv_assume_headers:          `False` - no headers
                                            `None`  - DEFAULT_HEADERS for file type
                                            `True`  - read headers from file
                                            `Tuple` - use these headers
        """
        hargs.set_backing_data( False,  # Variable input
                                (file_type.name if file_type is not None else "None",
                                 csv_protein_sequence_col,
                                 csv_protein_accession_col,
                                 csv_peptide_sequence_col,
                                 csv_assume_headers if csv_assume_headers is not None else "None") )
        super().__init__( hargs )
        assert file_type is None or isinstance( file_type, FileProvider.EType )
        self.file_type = file_type
        self.__current_subject = None
        self.__current_results: TResult = None

        self.csv_pro_seq = csv_protein_sequence_col
        self.csv_pro_acc = csv_protein_accession_col
        self.csv_pep_seq = csv_peptide_sequence_col
        self.csv_headers = csv_assume_headers

    def _on_query( self, subject_: PendingProtein ) -> TResult:
        subject = subject_.input.text
        file_type = self.file_type

        for key, value in self.__MAPPING.items():
            if subject.startswith( key + "@" ):
                file_type = value
                subject = subject.split( "@", 1 )[1]
                break

        # Read it if it's a filename
        if self.__is_file_name( subject ):
            subject = io_helper.read_all_text( subject )

            if file_type is None:
                ext = file_helper.get_extension( subject )[1:].lower()
                file_type = self.__MAPPING.get( ext, None )

        # Sniff the content if necessary
        if file_type is None:
            file_type = self.__sniff( subject )

            if file_type is None:
                return None

        # Read the content
        self.__current_subject = subject_
        self.__current_results = []

        if file_type == FileProvider.EType.FASTA:
            self.__load_fasta( subject )
        elif file_type == FileProvider.EType.JSON:
            self.__load_json( subject )
        elif file_type in (FileProvider.EType.LIST, FileProvider.EType.TSV, FileProvider.EType.CSV):
            self.__load_tsv( subject, file_type )
        else:
            assert False

        return self.__current_results

    @staticmethod
    def __sniff( text: str ) -> Optional[EType]:
        text = text.lstrip()

        if text.startswith( ">" ):
            return FileProvider.EType.FASTA
        elif text.startswith( "[" ):
            return FileProvider.EType.JSON
        elif text.startswith( "{" ):
            return FileProvider.EType.JSON
        elif "\t" in text:
            return FileProvider.EType.CSV
        elif "\t" in text:
            return FileProvider.EType.TSV
        elif "\n" in text:
            return FileProvider.EType.LIST
        else:
            return None

    def __add_protein( self, protein: Protein ) -> None:
        self.__current_results.append( self._score( EProvided.NORMAL,
                                                    self.COLUMN,
                                                    protein,
                                                    self.__current_subject ) )

    def __load_fasta( self, text: str ) -> None:
        for accession, sequence in bio_helper.parse_fasta( text = text ):
            protein = self.state.acquire_protein( accession, sequence, None, None )
            self.__add_protein( protein )

    def __load_json( self, text: str ) -> None:
        try:
            json = _json.loads( text )
        except _json.JSONDecodeError:
            return

        if isinstance( json, dict ):
            json = json,

        for entity in json:
            db_data = DbData( entity )
            protein = self.state.find_or_create( db_data, Protein, None )

            if protein is None:
                return

            self.__add_protein( protein )

    def __load_tsv( self, text: str, file_type: EType ) -> None:
        default_headers: Sequence[str]
        headers: Sequence[str]
        user_n: int = 0
        protein_map: Dict[str, ProteinLike] = { }

        with StringIO( text ) as fin:
            rd = csv.reader( fin, delimiter = "," if file_type == FileProvider.EType.CSV else "\t" )

            if self.csv_headers is False:
                headers = []
            elif self.csv_headers is None:
                headers = self.__DEFAULT_HEADERS[file_type]
            elif self.csv_headers is True:
                headers = next( rd )
            else:
                headers = self.csv_headers

            pro_seq_col: Optional[int] = index_of_first( headers, self.csv_pro_seq.__contains__ )
            pro_acc_col: Optional[int] = index_of_first( headers, self.csv_pro_acc.__contains__ )
            pep_seq_col: Optional[int] = index_of_first( headers, self.csv_pep_seq.__contains__ )

            for index, row in enumerate( rd ):
                pro_seq: Optional[str] = row[pro_seq_col] if pro_seq_col is not None else None
                pro_acc: Optional[str] = row[pro_acc_col] if pro_acc_col is not None else None
                pep_seq: Optional[str] = row[pep_seq_col] if pep_seq_col is not None else None

                if not pro_acc:
                    if not pro_seq:
                        raise ValueError( "Require a protein accession and/or sequence." )

                    user_n += 1
                    pro_acc = f"USER{user_n}"

                protein: Optional[ProteinLike] = protein_map.get( pro_acc )

                if protein is None:
                    if not pro_seq:
                        # Must lookup protein sequence
                        text = self.state.acquire_text( pro_acc )
                        protein = PendingProtein( text )
                    else:
                        protein = self.state.acquire_protein( pro_acc, pro_seq, pro_acc, -1 )

                    protein_map[pro_acc] = protein

                if pep_seq:
                    self.state.pending_peptides.setdefault( protein, [] ).append( pep_seq )

        for protein in protein_map.values():
            self.__add_protein( protein )

    def __load_list( self, text: str ) -> None:
        pass

    def __is_file_name( self, subject ):
        return (len( subject ) <= self.__MAX_PATH
                and not any( x in self.__BAD for x in subject )
                and os.path.isfile( subject ))

    def _on_query_file( self, subject, file_name, file_type ) -> Optional[List[RawScore[ProteinLike]]]:
        raise NotImplementedError( "abstract" )
