from typing import Optional

from alacat.model import HArgs, InstantiationError, Protein, ProteinProvider, HColumn, RawScore, PendingProtein, ProteinLike, DbData
from mhelper import array_helper


class SequenceProvider( ProteinProvider ):
    """
    Overview
    --------
    
    When this provider is enabled, proteins can be provided in JSON format.
    
    JSON format is used internally by the application and the user is not expected to craft such data manually.
    
    JSON format is used in the web GUI.
    If this provider is disabled, the web GUI may not function correctly.
    
    Scores
    ------ 
    
    The score is a boolean value on a protein, indicating if the protein was produced by this provider.
    It is for information only and has no analytical purpose.
    """
    COLUMN = HColumn( "From sequence?" )
    __AMINO_ACIDS = frozenset( "RKWDENQHSTYACGILMFPV" )
    
    
    
    def __init__( self,
                  hargs: HArgs,
                  organism: Optional[int] = None ):
        hargs.set_backing_data( False, (organism,))  # Input is output so it seems pointless!
        super().__init__( hargs )
        
        if organism == -1:
            if not self.state.organisms:
                organism = 0
            else:
                organism = array_helper.single_or_none( self.state.organisms, -1 )
                
                if organism is None:
                    raise InstantiationError( self, "The organism of the sequences cannot be inferred because the model specifies multiple organisms." )
        
        self.organism = organism
        self.count = 0
    
    
    def _on_query( self, subject_: PendingProtein ) -> Optional[RawScore[ProteinLike]]:
        subject: str = subject_.input.text
        
        try:
            data = DbData.from_json( subject )
        except Exception:
            return 
        
        if isinstance( data, dict ):
            data = data,
        
        for data_ in data:
            parsed = Protein.parse_dbdata( data_ )
            
            if parsed:
                protein = self.state.acquire_protein( parsed.accession,
                                                      parsed.sequence,
                                                      parsed.title,
                                                      parsed.organism )
                
                yield self._score( True, self.COLUMN, protein, subject_ )
