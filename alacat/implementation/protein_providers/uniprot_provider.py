from typing import Dict, FrozenSet, List, Optional, Sequence

from alacat.model import HArgs, Protein, ProteinProvider, HColumn, RawScore, PendingProtein, ProteinLike, EProvided, ModelState
from mhelper.bio_requests import uniprot_accessions, uniprot_proteins
from mhelper import exception_helper
from mhelper.bio_requests.uniprot_accessions import EAccessionType


class UniprotProvider( ProteinProvider ):
    """
    Overview
    --------
    
    When enabled, the user may input proteins using database accessions:
    
    * Uniprot accession
    * Uniprot ID
    * Ensembl genome protein ID
    
    The database accessions supported may be changed in the API by setting the constructor parameter.
    Any accession convertible by Uniprot is supported.
    In the web interface, no configuration is possible and only the default values are supported.
    
    Scores
    ------
    
    The score is a boolean value on a protein, indicating if the protein was produced by this provider.
    It is for information only and has no analytical purpose. 
    """
    COLUMN = HColumn( "From uniprot?" )
    __VALID_ACCESSIONS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789._"
    __slots__ = "accession_types",
    __DEFAULT_ACCESSIONS_ACCEPTED = (EAccessionType.UniProtKB_AC,
                                     EAccessionType.Ensembl_Genomes_Protein,
                                     EAccessionType.UniProtKB_AC_ID)
    __BACKING_VERSION = 2

    def __init__( self,
                  hargs: HArgs,
                  from_at: Sequence[uniprot_accessions.EAccessionType] = __DEFAULT_ACCESSIONS_ACCEPTED ):
        from_at_set = frozenset( from_at )
        hargs.set_backing_data( True, (self.__BACKING_VERSION, sorted( x.value for x in from_at_set )) )
        super().__init__( hargs )
        self.accession_types: FrozenSet[uniprot_accessions.EAccessionType] = from_at_set

    def __translate_accession( self, accession: str ) -> Sequence[str]:
        if not (6 <= len( accession ) <= 30):  # TODO: Actual range
            return ()

        if not all( x in self.__VALID_ACCESSIONS for x in accession ):
            return ()

        for act in self.accession_types:
            if act == EAccessionType.UniProtKB_AC and uniprot_accessions.check_accession( accession ):
                return accession

            try:
                uniprot_accessions_: Optional[Dict[str, List[str]]] = uniprot_accessions.convert_accessions( act, EAccessionType.UniProtKB_AC, accession )
            except Exception as ex:
                self.state.warnings.append( f"{ex.__class__.__name__} trying to convert from {act!r} accession to {EAccessionType.UniProtKB_AC!r} accession: {ex}." )
            else:
                if uniprot_accessions_:
                    assert len( uniprot_accessions_ ) == 1
                    uniprot_accessions_2: List[str] = uniprot_accessions_[accession]
                    return uniprot_accessions_2

        return ()

    def _on_query( self, subject_: PendingProtein ) -> Optional[RawScore[ProteinLike]]:
        subject: str = subject_.input.text

        protein = self.get_uniprot( self.state, subject, subject )

        if protein is None:
            uniprot_accessions: Sequence[str] = self.__translate_accession( subject )

            for uniprot_accession in uniprot_accessions:
                protein = self.get_uniprot( self.state, uniprot_accession, subject )

                if protein is not None:
                    break
            else:
                return None

        assert protein is not None
        return self._score( EProvided.NORMAL, self.COLUMN, protein, subject_ )

    @staticmethod
    def get_uniprot( state: ModelState, uniprot_accession: str, original_accession: str ) -> Optional[Protein]:
        if not uniprot_accessions.check_accession( uniprot_accession ):
            return None

        try:
            uniprot = uniprot_proteins.uniprot_proteins.parse( uniprot_accession )
        except exception_helper.NotFoundError:
            return None

        assert uniprot

        # The following DOES NOT WORK because it doesn't take into account the taxonomic tree
        # if organisms is not None:
        #     if uniprot.organism != organism.value:
        #         raise ValueError(f"Protein organism '{uniprot.organism}' does not match model organism '{organism.value}'.")
        #         # return None

        protein: Protein = state.acquire_protein( uniprot.accession,
                                                  uniprot.sequence,
                                                  uniprot.name,
                                                  uniprot.organism )

        if original_accession != uniprot_accession:
            state.register_alternate_accession( protein, original_accession )

        return protein
