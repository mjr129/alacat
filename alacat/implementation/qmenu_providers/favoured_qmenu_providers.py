from typing import FrozenSet, Optional, Sequence
import collections.abc

import alacat.model as model
from alacat.implementation.generic_providers.prespecified_provider import PrespecifiedProvider
from mhelper import exception_helper


class PrespecifiedQmenuProvider( PrespecifiedProvider[Sequence[model.Qbrick],
                                                      model.Qmenu],
                                 model.QmenuProvider ):
    """
    Overview
    --------
    
    When enabled this provider allows qmenus to be entered into the workflow before the actual
    qmenu-producing stage.
    
    Such qmenus include qmenus entered manually.
    
    Disabling this provider will result in an error if qmenus are prespecified in the initial model. 
      
    The base class, `PrespecifiedProvider` providers specific implementation details.
    
    Scores
    ------
    
    The score is a boolean value on a qmenu, indicating if the qmenu was produced by this provider.
    It is for information only and has no analytical purpose.
    """
    t_parent = collections.abc.Sequence
    t_parent_key = frozenset
    t_child = model.Qmenu
    
    
    def _on_get_parent_key( self, parent: Sequence[model.Qbrick] ) -> FrozenSet[model.Qbrick]:
        return frozenset( parent )
    
    
    def _on_get_coupled_state( self ):
        return self.state.pending_qmenus
    
    
    def _on_get_favoured_state( self ):
        return self.state.model.qmenus
    
    
    def _on_find_parent_key( self, line : model.DbData ) -> Optional[FrozenSet[model.Qbrick]]:
        parsed = model.Qmenu.parse_dbdata( line )
        qbricks_lns = [qbrick_ln for qconcat_ln in parsed.qconcat_hashes for qbrick_ln in model.Qconcat.parse_dbdata( qconcat_ln ).qbrick_hashes]
        qbricks = [self.state.find( qbricks_ln, model.Qbrick ) for qbricks_ln in qbricks_lns]
        return frozenset( qbricks )
    
    
    def _on_construct_child( self,
                             parent: Sequence[model.Qbrick],
                             line: model.DbData
                             ) -> model.Qmenu:
        if isinstance(line, model.DbData):
            qconcats = model.Qmenu.parse_dbdata( line )
            qconcat_obj = []
            
            for qconcat in qconcats:
                qbricks = [self.state.find( qbrick_ln ) for qbrick_ln in model.Qconcat.parse_dbdata( qconcat ).qbrick_hashes]
                qconcat_obj.append( self.state.acquire_qconcat( qbricks ) )
            
            return self.state.acquire_qmenu( qconcat_obj )
        else:
            raise exception_helper.type_error( "line", line, model.DbData, info = "Qmenus must be specified in JSON format.", err_class = model.PipelineError )