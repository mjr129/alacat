from alacat.model import EWeight, HArgs, HColumn, Qmenu, QmenuScorer, RawScore


class BasicAlacatScorer( QmenuScorer ):
    """
    Overview
    --------
    
    When enabled a meaningless test value is produced for each `Qmenu`.
    
    Scores
    ------
    
    The output value has no purpose, please ignore it.
    """
    COLUMN = HColumn( "Test value",
                      weight = EWeight.VERY_LOW )

    def __init__( self, hargs: HArgs ):
        hargs.set_backing_data( False, () )  # Test only
        super().__init__( hargs )

    def _on_query( self, subject: Qmenu ) -> RawScore[Qmenu]:
        return self._score( 1, self.COLUMN, subject, subject )
