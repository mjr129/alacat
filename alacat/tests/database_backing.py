import alacat


model = alacat.Model()

ms = alacat.ModelState( model )
ha = alacat.HArgs( ms, (), "unp" )

upr = alacat.UniprotProvider( ha )

subs = [alacat.Pending( ms.acquire_text( "P02768" ))]

q = upr.query_all( subs )

print(len(q))

for vs in q:
    print(vs)
