"""
Invokes the Alacat toolchain.

* This executable uses a static model with limited functionality.
* The Python library and web-interface are more feature rich.
* Results are saved in binary format and must be viewed in Python or the web
  interface.
  
.. tip::

    Parameter documentation is not repeated here, see the documentation on the
    `alacat.Model` object for parameter details.

Examples
--------
    
Digest a protein and propose a suitable QBrick::
    
    alacat P02768 --out results.pkl.gz
"""
import sys
from typing import Sequence, Optional

import alacat
from mhelper import arg_parser, exception_hooks


def cli_main():
    """
    ``alacat --cli [...]` entry-point.
    """
    return arg_parser.reflect( __main )


def __main( protein_ids: Optional[Sequence[str]],
            out: str,
            quiet: bool = False,
            organisms: Sequence[int] = (),  # taxon ID
            disable: Sequence[str] = (),
            noerr: bool = False,
            nosql: bool = False
            ):
    """
    :param protein_ids: Protein input, e.g.
    
                        * Absolute sequence
                        * Uniprot accession
                        * Path to FASTA file
                        * Path to TSV file.
                        
                        The formats accepted are dictated by the available `ProteinProvider`s on
                        the `Model`.
    :param out:         Save results to this file.
    
                        * The extension ".pkl" is usual but not mandatory.
                        * Adding a ".gz" extension will compress the results.
                        
    :param quiet:       When set, debugging messages are not printed.
    :param organisms:   Sets the `Model.organisms` field.
                        Specify one or more NCBI taxonomic IDs.
    :param disable:     Disable one or more `Model.handler`s by specifying their class name(s).
    :param noerr:       Suppresses display of error messages.
    :param nosql:       Sets `Model.sql_backing` to `NONE` (as opposed to `READ_WRITE`).
    """
    exception_hooks.install_error_hooks( exception_hooks.EHook.CONSOLE )

    if not quiet:
        alacat.enable_diagnostics()

    # print( f"{protein_ids=} {report=} {using=} {verbose=} {to=} {organisms=}" )
    # exit()

    # Read protein IDs
    if not protein_ids:
        raise arg_parser.CliError( "No proteins specified." )

    # Make
    my_model = alacat.Model.explicit_init( name = f"Cli",
                                           organisms = organisms,
                                           digestion = alacat.EDigestion.TRYPSIN,
                                           handlers = alacat.HandlerFactoryCollection.create_defaults(),
                                           proteins = protein_ids,
                                           peptides = (),
                                           qblocks = (),
                                           qmenus = (),
                                           qbrick_size = 2,
                                           alacat_size = 10000,
                                           stage = alacat.EStage.ALL,
                                           mandate = True,
                                           partition_rankings = True,
                                           random_seed = 1,
                                           min_peptide_length = 5,
                                           sql_backing = alacat.ESqlBacking.NONE if nosql else alacat.ESqlBacking.READ_WRITE,
                                           qblock_size = 1,
                                           fix_accessions = False )

    nh = []
    disable = set( disable )
    disable2 = set( disable )

    for h in my_model.handlers:
        if h.klass.__qualname__ in disable:
            if h.klass.__qualname__ in disable2:
                disable2.remove( h.klass.__qualname__ )
            continue

        nh.append( h )

    if disable2:
        raise RuntimeError( f"Unrecognised handlers: {disable2}" )

    my_model.handlers = nh

    # Exact pipeline
    try:
        pipeline: alacat.Pipeline = my_model.exact()
    except Exception:
        if noerr:
            print( "ERROR MESSAGE SUPPRESSED." )
            exit( 1 )

        raise

    # Print reports
    if out:
        pipeline.save( out )
        print( f"Results saved to: {out}", file = sys.stderr )

    print( "OK" )
    return 0
