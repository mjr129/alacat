from mhelper import specific_locations

def get_data_root():
    return specific_locations.get_application_directory( "rusilowicz", "alacat" )


def get_gui_models_root():
    return specific_locations.get_application_directory( "rusilowicz", "alacat", "gui_models" )