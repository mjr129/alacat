"""
Used for running release and development modes side-by-side, each instance will
get its own data directories, backing database and error report folder.

Instances will share the same mhelper.bio_requests module, which includes
datasets from external sources and web-caches.

Usage
-----

Set `ALACAT_ISOLATION` to provide a unique name for the instance.
"""
from functools import lru_cache


@lru_cache
def get_directory():
    from mhelper import specific_locations
    return specific_locations.get_application_directory( "rusilowicz", "alacat" )
