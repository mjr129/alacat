from typing import Optional

import alacat
import hatmul.authorisation as _authorisation
import hatmul.forms
import hatmul.resources
import hatmul.master
import hatmul.master.flask_protocol
import hatmul.authorisation
import os
from . import gui_pipelines, formatting
from . import screens
from . import bg_tasks
from . import users_db


def create_app():  # ALACAT
    """
    This is the flask entry point.
    """
    from alacat.paths import get_data_root
    from mhelper import exception_recorder as er
    er.targets.log = er.DirectoryErrorRecorder( os.path.join( get_data_root(), "gui_errors" ) )
    return AlacatHatmulProtocol( get_data_root() ).create_application()


class AlacatHatmulProtocol( hatmul.master.flask_protocol.FlaskHatmulProtocol ):
    favicon = hatmul.forms.Resource( "/alacat_static/favicon.svg" )


    def __init__( self, working_directory: str ):
        _ = screens
        alacat_static_dir: str = os.path.join( os.path.dirname( __file__ ), "static" )

        super().__init__( directory = working_directory,
                          application_title = alacat.__title__,
                          application_version = alacat.__version__,
                          bg_task_server = bg_tasks.get_bgts( working_directory ),
                          login_mode = hatmul.authorisation.ELoginMode.SESSION,
                          static_map = { "/alacat_static": alacat_static_dir } )


    def validate_credentials( self,
                              authorisation: _authorisation.BasicAuthorisation
                              ) -> object:  # ~~> DenyAccess
        # Override - to provide our own users DB
        user_id = users_db.check_user( authorisation.username, authorisation.password )

        if not user_id:
            raise _authorisation.DenyAccess( "Bad username or password." )

        return user_id


    def validate_user( self, user_id: int ) -> _authorisation.UserValidation:
        # Override - to provide our own users DB
        info = super().validate_user( user_id )

        if user_id:
            info.user_name = users_db.get_user_name( user_id )

            if not info.user_name:
                raise _authorisation.DenyAccess( "User ID does not exist." )

        return info


    def get_resource_file( self, target: hatmul.forms.Resource ) -> str:
        # Override - to replace the FAVICON with our own
        if target is hatmul.forms.Resources.COMMON_FAVICON:
            return self.get_resource_file( self.favicon )

        return super().get_resource_file( target )


    def widget_render_hook( self, output: hatmul.forms.Output ) -> None:
        # Override - 1. to add our own CSS to all pages
        #            2. to add the "return to results index" button to the title bar
        state = output.state

        if isinstance( state, hatmul.forms.TitleBar.State ):
            self.__title_bar_hook( output, state )
        elif isinstance( state, hatmul.forms.Page.State ):
            self.__page_hook( output, state )
        elif isinstance( state, hatmul.forms.DebuggingSection.State ):
            self.__dbg_hook( output, state )

        return super().widget_render_hook( output )


    def __page_hook( self, output: hatmul.forms.Output, state: hatmul.forms.Page.State ):
        """
        add our own CSS to all pages
        """
        csrf = output.get_screen().get_csrf()
        from alacat.gui.screens import AddToCartScreen
        add_to_cart_url = AddToCartScreen.get_url( self )

        state.css_body_style.update( {
            "--in_basket_image"    : f"url({self.app_prefix}/alacat_static/in_basket.svg)",
            "--not_in_basket_image": f"url({self.app_prefix}/alacat_static/not_in_basket.svg)",
            "--loading24_image"    : f"url({self.app_prefix}/static/images/loading24.gif)" } )
        state.css_body_attrs["data-add_to_cart_url"] = add_to_cart_url
        state.css_body_attrs["data-add_to_cart_csrf"] = csrf

        state.css.add( formatting.AlacatResources.EXTRA_CSS )
        state.scripts.add( formatting.AlacatResources.EXTRA_JS )


    def __title_bar_hook( self, output: hatmul.forms.Output, state: hatmul.forms.TitleBar.State ):
        """
        add the "return to results index" button to the title bar
        """
        gm: Optional[gui_pipelines.GuiModel]

        if isinstance( output.get_screen(), formatting.IModelScreen ):
            try:
                formatting.gui_model_from_output( output )
            except Exception:  # Don't care why, it doesn't load, the page itself should display an error
                pass
            else:
                state.centre = formatting.ReturnToModelLink(), formatting.GoToCartLink()
        else:
            state.centre = formatting.HistoryScreenLink()


    def __dbg_hook( self, _: hatmul.forms.Output, state: hatmul.forms.DebuggingSection.State ) -> None:
        state.sections.append(
                hatmul.forms.Section( "AlacatDesigner",
                                      hatmul.forms.Table( [["session_uid", gui_pipelines._get_session_uid().__str__()]] ) ) )


    def display_exception( self, ex: object ) -> "hatmul.forms.ErrorDisplay":
        if isinstance( ex, alacat.PipelineError ):
            return hatmul.forms.ErrorDisplay( True, False )

        return super().display_exception( ex )
