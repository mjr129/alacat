function alacat_application()
{
    function on_added_to_cart(data)
    {
        console.log(`Add to cart succeeded: ${JSON.stringify(data)}`)

        if (!("entities" in data)) {
            console.log(`Add to cart failed.`)
            return;
        }

        const entities = data["entities"];
        const status = data["status"];
        // noinspection JSUnusedLocalSymbols
        const change = data["change"];
        const query = data["query"]

        const cart_count = document.getElementById("alacat_cart_count");
        
        // Flash the cart box and update the count label
        if (cart_count)
        {
            cart_count.classList.remove("alacat_cart_updated_anim");
            void cart_count.offsetWidth;
            cart_count.classList.add("alacat_cart_updated_anim")
            cart_count.innerHTML = `${entities}`;
        }

        // Set the clicked button back to normal
        update_query_areas(query, status ? "alacat_is_in_cart" : "alacat_is_not_in_cart");

    }

    /**
     * Find all the "add to cart" checkboxes for `query_id` and update their statuses accordingly.
     * @param query_id
     * @param klass
     */
    function update_query_areas(query_id, klass)
    {
        const query_areas = document.querySelectorAll(`[data-alacat_cart_area="${query_id}"]`)

        for (const query_area of query_areas) {
            query_area.classList.remove("alacat_is_in_cart");
            query_area.classList.remove("alacat_is_not_in_cart");
            query_area.classList.remove("alacat_unknown_in_cart");
            query_area.classList.add(klass);
        }
    }

    /** Add to cart failed, this will happen due to JSON parse failure if the server returns e.g. "500. INTERNAL ERROR" */
    function on_added_to_cart_failed(error, info = null)
    {
        console.log(`Add to cart failed:\nERROR:\n${error}\nINFO:\n${info}`)
        alert("Add to cart failed.")
        const cart_box = document.getElementById("alacat_cart_box");

        if (cart_box === null) {
            location.reload();
            return;
        }

        cart_box.classList.remove("alacat_cart_add_anim");
        cart_box.classList.remove("alacat_cart_already_anim");
        cart_box.classList.remove("run-alacat_cart_fail_anim");
        void cart_box.offsetWidth;
        cart_box.classList.add("alacat_cart_fail_amim");
    }

    function on_element_click(e)
    {
        const item = e.getAttribute(`data-alacat_add_to_cart`);
        const my_area = e.closest(`.alacat_cart_area`);

        if (item) {
            // Get the CSRF
            // We need this because the server enforces check this for *all* POST  
            const csrf_e = document.body.getAttribute("data-add_to_cart_url")
            const add_to_cart_url = document.body.getAttribute("data-add_to_cart_url")

            if (csrf_e === null || add_to_cart_url === null) {
                console.log("Cart elements encountered without URL and CSRF.");
                alert("Insufficient information to process this request. The source document may be an excerpt not actually present on the server.");
                return;
            }

            const csrf = csrf_e.value

            // Get the model and entity accessions
            const [model, entity] = item.split('/', 2);

            if (!entity) {
                return;
            }

            // Determine the type of change we will perform
            let change = 0;

            if (my_area.classList.contains("alacat_is_in_cart")) {
                // Remove
                change = -1
            } else if (my_area.classList.contains("alacat_is_not_in_cart")) {
                // Add
                change = 1
            } else {   // Unknown, probably clicked again while still sending first request
                alert("Cannot issue a new request for this entity until the previous request is complete.");
                return;
            }


            update_query_areas(item, "alacat_unknown_in_cart");


            // Make the actual request via POST
            const formData = new FormData();
            formData.append("results", model);
            formData.append("subject", entity);
            formData.append("change", `${change}`);
            formData.append("csrf", `${csrf}`);

            console.log(`Add to cart: ${model} ... ${entity}`)

            fetch(add_to_cart_url,
                {
                    method: "POST",
                    body: formData
                })
                .then(function (response)
                {
                    response.json()
                        .then(on_added_to_cart)
                        .catch(function (error)
                            {
                                on_added_to_cart_failed(error, response);
                            }
                        )
                }).catch(on_added_to_cart_failed)
        }
    }

    function on_document_click(event)
    {
        console.log("ALACAT: I have received a document click event.");

        let e = event.target;

        while (e !== null) {
            on_element_click(e);
            e = e.parentElement;
        }
    }

    document.addEventListener("click", on_document_click);
}

alacat_application();


