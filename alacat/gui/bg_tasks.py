import os
from mhelper import bg_task_helper


class __Config( bg_task_helper.FileBasedBgtsConfig ):
    def initialise( self ):
        import alacat
        alacat.enable_diagnostics()


__bg_app = None
__bg_app_wd = None


def get_bgts(working_directory : str): # ALACAT
    global __bg_app
    global __bg_app_wd

    if __bg_app is None:
        __bg_app_wd = os.path.join( working_directory, "bgts" )
        config = __Config( working_directory = __bg_app_wd )
        __bg_app = bg_task_helper.Bgts( config )

    assert os.path.join( working_directory, "bgts" ) == __bg_app_wd, "Change in working_directory."
    return __bg_app
