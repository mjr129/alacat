from typing import Any, Optional, Tuple, Type

import alacat
from hatmul import forms, screens
from mhelper import exception_helper, reflection_helper
from mhelper.array_helper import single_or_none
from mhelper.special_types import NOT_PROVIDED


class RuleFieldWidget( forms.InputField ):
    _s_to_assessor_map = { klass.__name__: klass
                           for klass in reflection_helper.get_subclasses( alacat.Assessors.GuiCompatibleAssessor )
                           if not reflection_helper.is_explicit_abstract( klass ) }
    
    
    def __init__( self,
                  *args,
                  column: alacat.HColumnBase,
                  factory_gui_key: str,
                  **kwargs ):
        id = f"{factory_gui_key}::{column.name}"
        super().__init__( id = id, *args, **kwargs )
        self.column: alacat.HColumnBase = column
    
    
    def format_request( self, value: Any ) -> str:
        raise exception_helper.NotSupportedError( "This class uses multiple field IDs and does not support this method." )
    
    
    def on_render_field( self, output: forms.Output ) -> forms.TRenderable:
        # Request
        rq = output.get_request()
        
        # Default name
        default_name = self.column.name
        default_name = forms.escape( default_name )
        
        # Name
        name: str = self.read_name( rq )
        name = forms.escape( name )
        
        if name == default_name:
            name = ""
        
        # Assessor (marker)
        assessor: Type[alacat.Assessor] = self.read_assessor( rq )
        
        # Assessor parameters
        parameters: str = ", ".join( self.parameter_to_string( x ) for x in self.read_parameters( rq ) )
        parameters = forms.escape( parameters )
        
        # Assessor weight
        weight: int = self.read_weight( rq )
        
        # Available assessors
        assessor_options = "\n".join( f'<option value="{klass.__name__}" '
                                      f'{"selected" if assessor is klass else ""}>'
                                      f'{klass.class_title}'
                                      f'</option>'
                                      for klass
                                      in self._s_to_assessor_map.values() )
        
        # Available priorities
        weight_options = "\n".join( f'<option value="{v}" '
                                      f'{"selected" if weight == v else ""}>'
                                      f'{k}'
                                      f'</option>'
                                      for k, v in alacat.EWeight.to_dict().items() )
        
        # Result
        return f"""
                <div style='display: flex;'>
                    <div style='flex-grow: 1; flex-basis: 0;'>
                        <select class="{forms.Css.hatmul_select}" name="{self.weight_field_id}">
                            {weight_options}
                        </select>
                    </div>
                    <div style='flex-grow: 1; flex-basis: 0;'>
                        <select class="{forms.Css.hatmul_select}" name="{self.assessor_field_id}">
                            {assessor_options}
                        </select>
                    </div>
                    <div style='flex-grow: 1; flex-basis: 0;'>
                        <input class="{forms.Css.input_styled}" type="text" name="{self.parameters_field_id}" value="{parameters}" placeholder="No parameters"/>
                    </div>
                    <div style='flex-grow: 1; flex-basis: 0;'>
                        <input class="{forms.Css.input_styled}" type="text" name="{self.name_field_id}" value="{name}" placeholder="{default_name}"/>
                    </div>
                </div>
                """
    
    
    def read_column_override( self, request: screens.RequestInfo ) -> Optional[alacat.ColumnOverride]:
        assessor_class: Type[Any] = self.read_assessor( request )
        parameters: Tuple[object] = self.read_parameters( request )
        weight: int = self.read_weight( request )
        name: str = self.read_name( request )
        
        try:
            # noinspection PyArgumentList
            assessor: alacat.Assessor = assessor_class( *parameters )
        except Exception as ex:
            raise forms.InputError( "Invalid parameters for class.", widget = self ) from ex
        
        weight = weight if weight != self.column.default_weight else None
        assessor = assessor if assessor != self.column.default_assessor else None
        parameters = parameters if parameters != self.get_default_assessor_parameters() else None
        name = name if name and name != self.column.name else None
        
        if weight is not None or assessor is not None or name is not None or parameters is not None:
            return alacat.ColumnOverride( self.column, weight, assessor, name )
        else:
            return None
    
    
    def get_default_assessor_parameters( self ):
        da = self.column.default_assessor
        
        if not isinstance( da, alacat.Assessors.GuiCompatibleAssessor ):
            raise RuntimeError( f"The assessor must derives from {alacat.Assessors.GuiCompatibleAssessor.__qualname__} in order to be compatible with the GUI." )
        
        return da.parameters
    
    
    def read_name( self, request: screens.RequestInfo ) -> str:
        #
        # Use user selection
        #
        selected = single_or_none( self.get_form_request( request = request ).getl( self.name_field_id ) )
        
        if selected:
            return selected
        
        #
        # Use default
        #
        return self.column.name
    
    
    def read_assessor( self, request: screens.RequestInfo ) -> Type[alacat.Assessor]:
        #
        # Use user selection
        #
        selected = self.get_form_request( request = request ).get1( self.assessor_field_id, None )
        
        if selected is not None:
            try:
                return self._s_to_assessor_map[selected]
            except ValueError as ex:
                raise forms.InputError( str( ex ), widget = self ) from ex
        
        #
        # Use default
        #
        return self.column.default_assessor.__class__
    
    
    def read_weight( self, request: screens.RequestInfo ) -> int:
        #
        # Use user selection
        #
        selected = self.get_form_request( request = request ).get1( self.weight_field_id, None )
        
        if selected is not None:
            try:
                return int( selected )
            except ValueError as ex:
                raise forms.InputError( str( ex ), widget = self ) from ex
        
        #
        # Use default
        #
        return self.column.default_weight
    
    
    def read_parameters( self, request: screens.RequestInfo ) -> Tuple[object, ...]:
        #
        # Use user selection
        #
        selected = self.get_form_request( request = request ).get1( self.parameters_field_id )
        
        if selected is not None:
            if not selected.strip():
                return ()
            else:
                strings = [x.strip() for x in selected.split( "," )]
                return tuple( self.string_to_parameter( string, self ) for string in strings )
        
        #
        # Use default
        #
        return self.get_default_assessor_parameters()
    
    
    @staticmethod
    def string_to_parameter( string: str, widget: Optional["RuleFieldWidget"] ) -> object:
        value = alacat.RawScore.STRING_TO_VALUE_MAP.get( string, NOT_PROVIDED )
        
        if value is not NOT_PROVIDED:
            return value
        
        try:
            return float( string )
        except ValueError as ex:
            raise forms.InputError( f"Cannot coerce the string {string!r} to a number or any predefined value ({list( alacat.RawScore.STRING_TO_VALUE_MAP.keys() )}).", widget = widget ) from ex
    
    
    @staticmethod
    def parameter_to_string( dv ) -> str:
        if isinstance( dv, float ) or isinstance( dv, int ):
            return str( dv )
        
        string = alacat.RawScore.VALUE_TO_STRING_MAP.get( dv, None )
        
        if string is not None:
            return string
        
        raise RuntimeError( f"A parameter, `{dv!r}`, was provided but it isn't coercible to a string. This is either due to a bad {alacat.Assessors.GuiCompatibleAssessor.__qualname__} implementation or {RuleFieldWidget.__qualname__} should have been updated to include a string representation for this value in its lookup table." )
    
    
    @property
    def assessor_field_id( self ):
        return f"{self.field_id}_assessor"
    
    
    @property
    def name_field_id( self ):
        return f"{self.field_id}_name"
    
    
    @property
    def parameters_field_id( self ):
        return f"{self.field_id}_parameter"
    
    
    @property
    def weight_field_id( self ):
        return f"{self.field_id}_weight"
