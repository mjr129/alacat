from typing import Iterable

import alacat
from alacat.gui.gui_pipelines import GuiModel
from hatmul import forms
from mhelper.exception_helper import safe_cast


class SequenceWidget( forms.Widget ):
    __slots__ = "entity",

    def __init__( self, entity: alacat.Entity ):
        super().__init__()
        self.entity = entity


    def on_render( self, output: forms.Output ) -> None:
        entity = self.entity

        if not isinstance( entity, alacat.AminoAcidSequence ):
            output.write( "Not an AminoAcidSequence" )
            return

        output.write( "<p><span class='alacat_sequence alacat_aa'>" )

        if len( entity.sequence ) > 2000:
            output.write( entity.sequence )
        elif isinstance( entity, alacat.Peptide ):
            self.__write_seqs( output, entity.protein.map_subsequences( [entity] ) )
        else:
            self.__write_sequence( output, entity )

        output.write( "</span></p>" )

        return


    @staticmethod
    def can_handle( entity ) -> bool:
        """
        Can this object render the entity.
        
        Note there is no concrete rule for this, for instance `alacat.Peptides`
        have a sequence but `alacat.Qmenus` don't, so we use their `Qconcat`\s.
        We don't support any arbitrary `alacat.AminoAcidSequence`.
        """
        return any( isinstance( entity, x )
                    for x in
                    (alacat.Protein,
                     alacat.Peptide,
                     alacat.Qbrick,
                     alacat.LinkedPeptide,
                     alacat.Qconcat) )


    def __write_sequence( self,
                          output: forms.Output,
                          subject: alacat.AminoAcidSequence
                          ) -> None:
        from alacat.gui.formatting import get_link_for
        url = forms.escape( get_link_for( output.get_screen(), subject ) )

        output.write( "<span class='alacat_ss'>" )
        output.write( f"<a class='alacat_sst' href='{url}' title='{subject.__class__.__name__}'>[</a>" )

        if isinstance( subject, alacat.Protein ):
            from alacat.gui.formatting import IModelScreen
            screen_: IModelScreen = safe_cast( "screen", output.get_screen(), IModelScreen )
            gm : GuiModel = screen_.get_gui_model()
            peptides = [x for x in gm.pipeline.candidate_peptides if x.protein is subject]
            self.__write_seqs( output, subject.map_subsequences( peptides ) )
        elif isinstance( subject, alacat.Peptide ):
            output.write( f"<a href='{url}' title='{len( subject.sequence )}-AA peptide'>{subject.sequence}</a>" )
        elif isinstance( subject, alacat.Qbrick ):
            self.__write_seqs( output, subject.linked_peptides )
        elif isinstance( subject, alacat.LinkedPeptide ):
            self.__write_seqs( output, [subject.n_link, subject.peptide, subject.c_link] )
        elif isinstance( subject, alacat.Qconcat ):
            self.__write_seqs( output, subject.qbricks )
        elif isinstance( subject, alacat.Qmenu ):
            self.__write_seqs( output, subject.qconcats )
        elif isinstance( subject, alacat.Qblock ):
            self.__write_seqs( output, subject.qbricks )

        output.write( f"<a class='alacat_sst' url='{url}'>]</a>" )
        output.write( "</span>" )


    def __write_seqs( self,
                      output: forms.Output,
                      subjects: Iterable[alacat.AminoAcidSequence] ):
        for subject in subjects:
            if isinstance( subject, str ):
                output.write( f"{subject}" )
            else:
                self.__write_sequence( output, subject )
