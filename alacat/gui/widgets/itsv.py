

class ITsv:
    def to_tsv( self ) -> str:
        raise NotImplementedError( "abstract" )