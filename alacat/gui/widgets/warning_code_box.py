from typing import Dict, Set, Union

import alacat
from alacat.gui.widgets.itsv import ITsv
from alacat.gui.gui_pipelines import GuiModel
from hatmul import forms
from mhelper import array_helper


class WarningCodeBox( forms.Widget, ITsv ):
    """
    Short text with a link to a score.
    """

    class WarningsTable:
        def __init__( self ):
            self.columns: Dict[alacat.Column, Set[object]] = { }
            self.__codes: Dict[alacat.Column, str] = array_helper.WriteOnceDict()
            self.__completed: bool = False

        def complete( self ) -> None:
            assert not self.__completed
            self.__completed = True

            for key, columns in array_helper.group_by( self.columns, key = lambda x: x.accession[0].lower() ).items():
                columns = list( columns )

                if len( columns ) == 1:
                    self.__codes[columns[0]] = key
                else:
                    self.__codes.update( { column: f"{key}{index}" for index, column in enumerate( columns ) } )

        def append( self, column: alacat.Column, subject: object ) -> None:
            assert not self.__completed
            self.columns.setdefault( column, set() ).add( subject )

        def get_code( self, column: alacat.Column ) -> str:
            assert self.__completed
            return self.__codes[column]

    def __init__( self,
                  warnings_table: WarningsTable,
                  model: GuiModel,
                  x: Union[alacat.Score,
                           alacat.Column] ):
        super().__init__()
        self.model = model
        self.warnings_table = warnings_table

        if isinstance( x, alacat.Column ):
            self.column = x
            self.subject = None
            warnings_table.get_code( self.column )  # test
        else:
            self.column = x.column
            self.subject = x.subject
            warnings_table.append( self.column, self.subject )

    def to_tsv( self ) -> str:
        code = self.warnings_table.get_code( self.column )
        return f"[{code}]"

    def on_render( self, output: forms.Output ) -> forms.TRenderable:
        from alacat.gui.formatting import get_link_for
        code = self.warnings_table.get_code( self.column )

        url = forms.escape( get_link_for( output.get_screen(), self.column ) )
        acc = forms.escape( self.column.accession )
        colour = "#{0:06X}".format( int.from_bytes( self.column.to_dbdata().to_hash()[:3], "little" ) )
        ali = forms.escape( code )
        klass = forms.escape( self.column.__class__.__name__ )
        return f"<a style='background: {colour}' class='alacat_score_code' href='{url}' title='&quot;{acc}&quot;: Click to inspect this {klass}.'>{ali}</span>"
