from typing import cast, Iterable, Iterator, List, Tuple, Type

import alacat
from alacat.gui.widgets.assessment_field import RuleFieldWidget
from hatmul import forms, screens


class HandlersFieldsWidget( forms.Widget ):
    def __init__( self ):
        super().__init__()

    def on_render( self, output: forms.Output ) -> None:
        help_ = [
            forms.Paragraph( "The four fields correspond to the weight, assessment, parameters and column name." ),
            forms.Html( """<p style="font-weight: bold;">WEIGHT</p>""" ),
            forms.Html( """<p style="font-family: monospace;">Python: help(alacat.EWeight)</p>""" ),
            forms.RestructuredText( alacat.EWeight.__doc__ ),
            forms.Html( """<p style="font-weight: bold;">ASSESSMENT</p>""" ),
            forms.Html( """<p style="font-family: monospace;">Python: help(alacat.Assessor)</p>""" ),
            forms.RestructuredText( alacat.Assessor.__doc__ ),
            ([forms.Html( f"""<p style="font-family: monospace;">Python: help(alacat.Assessors.{klass.__name__})</p>""" ),
              forms.RestructuredText( klass.__doc__ )] for klass in RuleFieldWidget._s_to_assessor_map.values()),
            forms.Html( """<p style="font-weight: bold;">PARAMETERS</p>""" ),
            forms.Paragraph(
                    "If the assessment requires parameters then they are specified here. Multiple parameters should be separated with a comma. Only numeric parameters and a small set of fixed values are supported in the GUI." ),
            forms.Html( """<p style="font-weight: bold;">NAME</p>""" ),
            forms.Paragraph(
                    "This field can be used to rename the column. If no name is specified the column is not renamed."),
            forms.Html( """<p style="font-weight: bold;">GRADES (output, for information only)</p>""" ),
            forms.Html( """<p style="font-family: monospace;">Python: help(alacat.Assessor)</p>""" ),
            forms.RestructuredText( alacat.EGrade.__doc__ ),
        ]

        intro = [forms.HelpButton( help_, forms.EHelpButton.DROPDOWN )]

        output.append( intro )

        field_sets: List[Tuple[forms.FieldSet, Type]] = \
            [(forms.FieldSet( "Protein providers" ), alacat.ProteinProvider),
             (forms.FieldSet( "Peptide providers" ), alacat.PeptideProvider),
             (forms.FieldSet( "Peptide scorers" ), alacat.PeptideScorer),
             (forms.FieldSet( "Qbrick providers" ), alacat.QblockProvider),
             (forms.FieldSet( "Qblock scorers" ), alacat.QblockScorer),
             (forms.FieldSet( "Qmenu providers" ), alacat.QmenuProvider),
             (forms.FieldSet( "Qmenu scorers" ), alacat.QmenuScorer),
             ]

        for field_set, type_ in field_sets:
            field_set.append( forms.RestructuredText( type_.__doc__, forms.ERestructuredText.DOC ) )

        for factory, gui_key in self.__iter_factories():
            klass: Type[alacat.Handler] = cast( Type[alacat.Handler],
                                                factory.klass )  # seems unnecessary but IntelliJ requires the cast

            columns: Iterable[alacat.HColumnBase] = klass.find_overridable_columns()

            assert columns, f"`Handler`\s usable through the GUI must expose their columns. See `{klass.find_overridable_columns.__qualname__}`."

            fields = []

            fields.append( forms.RestructuredText( factory.klass.__doc__, forms.ERestructuredText.DOC ) )
            fields.append( forms.Html( "<p>&nbsp;</p>" ) )

            for column in columns:
                title = column.name
                help = f"This input sets the assessment and weight of the column."

                if "Rank: " in title:
                    
                    title = title.replace( "Rank: ", "#" )
                    help += " This is a rank column, scores produced are the ranks of the values in the non-ranked column. Values are ordered such that more quantotypic values receive lower ranks."

                fields.append( RuleFieldWidget(
                        title = title,
                        factory_gui_key = gui_key,
                        column = column,
                        help = help ) )

            for field_set, ft in field_sets:
                if issubclass( factory.klass, ft ):
                    field_set.append( forms.FieldSet( f"{factory.klass.__name__}", fields ) )
                    break
            else:
                assert False

        for field_set, _ in field_sets:
            output.append( field_set )

    @staticmethod
    def read_handlers( request: screens.RequestInfo ) -> alacat.HandlerFactoryCollection:
        handlers = []

        for factory, gui_key in HandlersFieldsWidget.__iter_factories():
            columns: List[alacat.HColumnBase] = factory.klass.find_overridable_columns()

            # Read all the columns
            overrides: List[alacat.ColumnOverride] = []

            for column in columns:
                af = RuleFieldWidget( factory_gui_key = gui_key, column = column )
                override = af.read_column_override( request )

                if override is not None:
                    overrides.append( override )

            if overrides and not any(
                    x.weight if x.weight is not None else x.column.default_weight for x in overrides ):
                continue

            handlers.append( alacat.HandlerFactory(
                    klass = factory.klass,
                    kwargs = factory.kwargs,
                    column_overrides = overrides,
            ) )

        return alacat.HandlerFactoryCollection( handlers )

    @staticmethod
    def __iter_factories() -> Iterator[Tuple[alacat.HandlerFactory, str]]:
        for index, factory in enumerate( alacat.HandlerFactoryCollection.create_defaults() ):
            factory_gui_key = f"{factory.klass.__name__}[{index}]"

            yield factory, factory_gui_key
