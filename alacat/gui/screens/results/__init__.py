from .scr_final_report import FinalReportScreen
from .scr_model_details import AlacatModelDetailsScreen
from .scr_model_index import ModelIndexScreen
from .scr_pipeline_details import PipelineDetailsScreen
from .scr_warnings import PipelineWarningsScreen
from .scr_everything import EverythingScreen
from .scr_proteins import ProteinsScreen
from .scr_scores_table import ScoresTableScreen
