from typing import List, Optional

from alacat.gui import formatting
from hatmul import forms, screens
from alacat.gui.gui_pipelines import RESULTS_FIELD_ID


class FinalReportScreen( screens.BasicScreen, formatting.IModelScreen ):
    """
    The final selection, presented as a table.
    
    Most of the columns are obvious, the main ones of interest are "choice" and "warnings".
    
    
    Qmenu, Qconcat, Qblock, Qbrick, Protein, Peptide
    ------------------------------------------------
    
    The selected entity. You can click through to see detailed information and compare to other
    entities.
    
    
    NLink, CLink, NNat, CNat
    ------------------------
    
    The linkers. Link is the suggested qconcat linker. Nat is the native linker in the protein.
    
    
    Choice
    ------
    
    "Choice" is a list of three values (for the peptide, the qbrick and the qconcat), showing who
    thinks they are a good idea: the user (u), the system (s) or system-equivalent (e).
          
    In the final choice, "u" will always override "s", so we have these possibilities:
    
    * "u" is an entity that is in the input, but the software wouldn't have chosen by itself.
    * "su" is an entity that is in the input, and the software would also have chosen. 
    * "eu" is an entity that is in the input, and the software wouldn't have have chosen, but that
      it thinks is an equally good choice.
    * "s" is where the input didn't contain a suggestion, so the system has chosen by itself. 
      
      
    Warnings
    --------
    
    "warnings" are the reasons the software doesn't like this peptide, qbrick, or qconcat.
    
    These are the scores for the output columns, where the score is thought to be less-than ideal. 
    
    Each one is referenced by a short ID to the table below, but the IDs can be clicked to quickly
    get more information.
    
    
    Implementation details
    ----------------------
    
    No form is supplied to select the result set, this parameter must be supplied by the caller.
    """
    __view__ = screens.ScreenData( url = f"/<{RESULTS_FIELD_ID}>/report",
                                   index = screens.EIndex.TERTIARY,
                                   folder = "Results" )

    class __myform( forms.AutoForm ):
        tsv = forms.BooleanField( id = "tsv",
                                  title = "TSV",
                                  help = "Get results in TSV format.",
                                  default = False )
        nolegend = forms.BooleanField( id = "nolegend",
                                       title = "Hide legend",
                                       help = "Hide legend.",
                                       default = False )

    def on_call( self ):
        gm = self.get_gui_model()
        sections = []

        # Read form
        f = self.__myform
        r = self.request

        opt_tsv = f.tsv.read_bool( r, default = False )
        opt_legend = not f.nolegend.read_bool( r, default = False )

        # Define the table rows
        rows: List[formatting.CompositionRow] = []

        for qmenu in gm.pipeline.selected_menus:
            for qconcat in qmenu.qconcats:
                for qbrick in qconcat.qbricks:
                    for linked_peptide in qbrick.linked_peptides:
                        rows.append( formatting.CompositionRow( qmenu, qconcat, qbrick, linked_peptide ) )

        # Create the table proper
        table = formatting.CompositionTable( gm, rows, extended = True, warnings_legend = opt_legend )

        # Page - TSV
        if opt_tsv:
            import flask
            return flask.Response( formatting.to_tsv( table.rows ), mimetype = "text/tab-separated-values" )

        sec_table = forms.Section( "" )
        sec_table.append( table.to_table( hscroll = True, vscroll = False, wrap = False ) )
        sections.append( sec_table )

        # Form section
        sec_form = self.__make_form()
        sections.append( sec_form )

        # Help section
        sec_help = forms.Section( "Help", forms.RestructuredText( self.__doc__ ), collapsed = True )
        sections.append( sec_help )

        # Page
        return forms.Page( content = forms.Report( [formatting.ModelLink( gm.id ), "Alacat Report"],
                                                   sections ) )

    def __make_form( self ):
        return forms.Section( "View options", self.__myform.create_region(), collapsed = True )

    def __format_alt_linker( self, nalink, nlink ):
        if not nalink:
            nalink = "∅"
        elif nalink == nlink:
            nalink = ""
        return nalink
