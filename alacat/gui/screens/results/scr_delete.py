from alacat.gui import gui_pipelines, formatting
from hatmul import screens, forms
from alacat.gui.gui_pipelines import RESULTS_FIELD_ID

class DeleteScreen( screens.FormScreen, formatting.IModelScreen ):
    """
    Deletes the results.
    
    No form is supplied to select the result set, this parameter must be supplied by the caller.
    """
    __view__ = screens.ScreenData( url = f"/<{RESULTS_FIELD_ID}>/delete",
                                   method = "post",
                                   index = screens.EIndex.TERTIARY,
                                   folder="Results")
    
    
    class __form__:
        title = forms.AutoTitle( form_title = "Delete results",
                                 form_help = "Click the button below to delete the results. This action cannot be undone.",
                                 form_submit_image = formatting.AlacatResources.DELETE )
    
    
    def on_call( self ):
        results_set = gui_pipelines.get_store().get_results( self.get_route_arg( gui_pipelines.RESULTS_FIELD_ID ) )
        exists = results_set is not None
        
        if exists:
            return self.show()
        else:
            return self.show( over = forms.Report( "Report", forms.Section( "", "Model does not exist." ) ) )
    
    
    def on_response( self ) -> "forms.Page":
        store = gui_pipelines.get_store()
        results_set = store.get_results( self.get_route_arg( gui_pipelines.RESULTS_FIELD_ID ) )
        results_set.delete_all()
        results_set = store.get_results( self.get_route_arg( gui_pipelines.RESULTS_FIELD_ID ) )
        exists = results_set is not None
        
        if exists:
            return self.show( over = forms.Report( "Report", forms.Section( "", "Failed to delete, something went wrong." ) ) )
        else:
            return self.show( over = forms.Report( "Report", forms.Section( "", "Successfully deleted" ) ) )
