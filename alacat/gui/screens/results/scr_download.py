from alacat.gui import gui_pipelines, formatting
from hatmul import forms, screens
from alacat.gui.gui_pipelines import RESULTS_FIELD_ID

class DownloadScreen( screens.BasicScreen, formatting.IModelScreen ):
    """
    Downloads the results.
    
    No form is supplied to select the result set, this parameter must be supplied by the caller.
    """
    __view__ = screens.ScreenData( url = f"/<{RESULTS_FIELD_ID}>/download",
                                   index = screens.EIndex.TERTIARY,
                                   folder="Results")
    
    
    def on_call( self ):
        results_set: gui_pipelines.AlacatResultsStore.ResultsSet = gui_pipelines.get_store().get_results( self.request.route_args.get( gui_pipelines.RESULTS_FIELD_ID ) )
        
        if results_set is None:
            raise forms.InputError("Bad ID.", gui_pipelines.RESULTS_FIELD_ID)
        
        fn = results_set.get_pipeline_file_name()
        
        import flask
        return flask.send_file( fn, as_attachment = True )
