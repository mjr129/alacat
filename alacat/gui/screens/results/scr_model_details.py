from hatmul import forms, screens
from alacat.gui import formatting, gui_pipelines
import re
import alacat
from mhelper import string_helper
from alacat.gui.gui_pipelines import RESULTS_FIELD_ID

class AlacatModelDetailsScreen( screens.BasicScreen, formatting.IModelScreen ):
    """
    Shows the code necessary to define the pipeline parameters.
    
    No form is supplied to select the result set, this parameter must be supplied by the caller.
    """
    __view__ = screens.ScreenData( url = f"/<{RESULTS_FIELD_ID}>/model",
                                   index = screens.EIndex.TERTIARY,
                                   folder="Results")
    
    
    def on_call( self ):
        gm: gui_pipelines.GuiModel = self.get_gui_model()
        model: alacat.Model = gm.pipeline.model
        
        python = "\n".join( ("from alacat import <STUFF>",
                             "",
                             string_helper.indent_value( f"model : Model = {model!r}" ),
                             "",
                             "results : Pipeline = model.exact()") )
        import alacat as ALACAT
        
        needed = set()
        
        for mat in reversed( list( re.finditer( r"[^\w.']([A-Z]\w+)", python ) ) ):
            t = mat.group( 1 )
            
            if t in ALACAT.__dict__:
                needed.add( t )
        
        python = python.replace( "<STUFF>", ", ".join( sorted( needed ) ), 1 )
        
        return forms.Page(
                content = forms.Report(
                        title = [formatting.ModelLink( gm.id ), "Model details"],
                        content = [forms.Section( "",
                                                  [forms.HelpBlock( "The following code represents the input data. You can execute this in Python by installing the Alacat package and running the code." ),
                                                   forms.HorizontalRule(),
                                                   forms.PreBox( python, use_pre = True, scroll = False )] )] ) )
