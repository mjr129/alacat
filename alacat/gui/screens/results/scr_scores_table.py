"""

"""
from typing import Any, Callable, Dict, Iterable, List, Optional, Sequence, Set, Tuple, Type
from hatmul import forms, screens
from alacat.gui import formatting, gui_pipelines
import alacat
from mhelper import array_helper
from alacat.gui.gui_pipelines import RESULTS_FIELD_ID

TSubject = Any


class ScoresTableScreen( screens.BasicScreen, formatting.IModelScreen ):
    """
    Shows the full table of scores.
    
    No form is supplied to select the results set or scores table, these
    parameters must be supplied by the caller.
    """
    __view__ = screens.ScreenData( url = f"/<{RESULTS_FIELD_ID}>/tables/<table>",
                                   index = screens.EIndex.TERTIARY,
                                   folder = "Results" )

    def on_call( self ):
        gm: gui_pipelines.GuiModel = self.get_gui_model()
        klass_name: str = self.get_route_arg( "table" )
        table: alacat.ScoresTable = gm.pipeline.find_table_by_class( klass_name )
        columns: Sequence[alacat.Column] = table.columns
        klass: Type = table.subjects[0].__class__
        subjects: List[TSubject] = table.subject_order
        ranks: Dict[TSubject, int] = table.subject_to_rank
        gui = self.__ReadForm( self.request )

        subjects, proteins = self.group_by_protein( klass, subjects, ranks, gui.is_unlimited )

        #
        # Add protein column (if useful)
        #
        cols: List[List[str]] = []
        num_row_headers: int = 1
        s_title: Optional[str]

        if issubclass( klass, alacat.ComesFromProtein ):
            num_row_headers += 1

            cols.append( ["", "", "Subject", "Protein",
                          *[(formatting.EntityLink( subject.protein )
                             if previous is None or subject.protein is not previous.protein else "")
                            for previous, subject
                            in array_helper.lagged_iterate( subjects, head = True )]] )
            s_title = None
        else:
            s_title = "Subject"

        #
        # Add subject column
        #
        if klass is alacat.Peptide:
            # (special case for peptides - show whole sequence)
            cols.append( ["", "", s_title, klass_name,
                          *[formatting.EntityLink( subject, subject.abv_sequence ) for subject in subjects]] )
        else:
            cols.append( ["", "", s_title, klass_name,
                          *[formatting.EntityLink( subject ) for subject in subjects]] )

        infos: List[alacat.EntitySummary] = [gm.pipeline.summarise( subject ) for subject in subjects]

        #
        # Add rank column
        #
        cols.append( ["", "", "", "Rank",
                      *[formatting.SelectionRank( info ) if info else None for info in infos]] )
        cols.append( ["", "", "", "Score",
                      *[formatting.WeightWidget( info.total_points, total = True ) if info else None for info in infos]] )

        #
        # Add columns for the *actual* columns in the matrix being viewed
        #
        columns_sorted: Sequence[alacat.Column] = sorted( columns, key = _sort_order )
        last_handler = None

        for column in columns_sorted:
            is_assessed = not isinstance( column.assessor, alacat.Assessors.Indifferent )

            # Ignore non-assessed columns if assessed is checked
            if gui.is_assessed_filtered and not is_assessed:
                continue

            handler = column.handler

            cols.append( [formatting.EntityLink( handler ) if handler is not last_handler else None,
                          formatting.RuleWidget( column.assessor, short = True ) if is_assessed else "",
                          formatting.WeightWidget( column.weight ) if is_assessed else "",
                          formatting.EntityLink( column ),
                          *[formatting.ScoreWidget( table.by_subject[subject][column] ) for subject in subjects]] )

            last_handler = handler

        #
        # The last set of columns are to aid in excel filtering, they can be turned off
        #
        if not gui.no_technical_data:
            for selector in [alacat.ESelectors.FINAL, alacat.ESelectors.SYSTEM, alacat.ESelectors.USER]:
                cols.append( ["", "", "", selector.name,
                              *[str( "1" if ((info.selection & selector) == selector) else "" ) if info else None for info in infos]] )

        rows = array_helper.transpose( cols )

        if gui.tsv:
            import flask
            return flask.Response( formatting.to_tsv( rows ), mimetype = "text/tab-separated-values" )

        return forms.Page(
                content = forms.Report(
                        title = [formatting.ModelLink( gm.id ), f"Scores table: {klass_name}"],
                        content = [forms.Section( "",
                                                  [self.__get_intro( subjects, table ),
                                                   forms.Table( rows = rows,
                                                                row_headers = num_row_headers,
                                                                col_headers = 4,
                                                                wrap = False )] ),
                                   self.__make_form( klass_name, proteins ),
                                   forms.Section( "Help", self.__get_legend(), collapsed = True )
                                   ] ) )

    def __get_intro( self, subjects, table ):
        intro = [forms.Paragraph(
                "Table is {:,} columns and {:,} rows, of which {:,} rows are shown.".format( table.columns.__len__(),
                                                                                             table.by_subject.__len__(),
                                                                                             subjects.__len__() ) )]
        return intro


    def __get_legend( self ):
        from alacat.gui.screens import PipelineWarningsScreen
        gm = self.get_gui_model()
        legend = forms.Html( """
        <p>
            The table above shows all of the scores.
        </p>
        
        <p>
            <strong>The columns</strong><br/>
            
            The <em>columns</em> represent the individual scoring criteria.
            
            The scoring criteria (column) headers comprise four values:
            
            <ul>
                <li>The name of the class or program that generated the score.</li>
                <li>The <em>rule</em> applied to the score. If available, otherwise this value is blank. This is used to determine if the score signifies &quot;quantotypicness&quot;.</li>
                <li>The <em>priority</em> of the rule, as chosen by the user.</li>
                <li>The name of the actual scoring metric.</li>
            </ul>
        </p>
        
        <p>
            <strong>The rows</strong><br/>
            
            The <em>rows</em> represent the subjects being scored: peptides, qbricks or qconcats.
            
            The subject (row) headers comprise three or four values:
            
            <ul>
                <li>The protein the subject originates from. If available, otherwise this column is missing.</li>
                <li>The subject itself.</li>
                <li>The rank of the subject (how quantotypic the subject is), in comparison to the other subjects.
                    (Hover the mouse over the number for more details.)</li>
                <li>The total number of points awarded to the subject.
                    Points are awarded for every <em>rule</em> that is passed.
                    The number of points is determined by the <em>priority</em>.
                    The final value indicates how quantotypic the subject is <em>in isolation</em>: the <em>rank</em> column compares this subject against others by discounting points awarded by non-comparable metrics.</li> 
            </ul>
        </p>
        
        <p>
            <strong>The matrix</strong><br/>
            
            The rest of the table comprises the values of the scores.
            These are formatted using the following legend:
        
            <ul>
                <li><span class="alacat_red">Red</span> - Assessment failed for high priority column.</li>
                <li><span class="alacat_amber">Amber</span> - Assessment failed for low priority column.</li>
                <li><span class="alacat_pass">Green</span> - Assessment passed.</li>
                <li><span class="alacat_info">Blue</span> - No assessment / for information only.</li>
                <li><span class="alacat_error">Magenta</span> - Error acquiring data / see <a href="{}">warnings</a>.</li>
            </ul>
        </p>
        
        <p>
            <strong>More help</strong><br/>
        
            <ul>
                <li>If the table appears small, hover over it with your mouse and click the &quot;pop-out&quot; icon which appears to its right.</li>
                <li>Open the &quot;options&quot; section, check TSV and click &quot;OK&quot; to download the table in TSV format.
                <li>You can click the table to navigate through to the subjects and metrics.</li>
                <li>Click the flag icons that appear when you hover to put a flag a particular subject. The flagged subjects can then be investigated further by clicking the big flag at the top of the page.</li>
            </uk>
        </p>                  
        """.format(
                PipelineWarningsScreen.get_url( self.protocol, **{ screens.LongTaskScreen.RESULTS_FIELD_ID: gm.id } ) ) )
        return legend

    def __make_form( self, klass_name, proteins: Optional[Iterable[alacat.Protein]] ):
        form = forms.Form(
                method = "get",
                content = [forms.BooleanField( id = "tsv",
                                               title = "TSV",
                                               help = "Download results in TSV format. Selecting this option implies `unlimited`.",
                                               default = False ),
                           forms.BooleanField( id = "assessed",
                                               title = "Assessed",
                                               help = "Only show columns that have rules and priorities present.",
                                               default = False ),
                           forms.BooleanField( id = "unlimited",
                                               title = "Unlimited",
                                               help = "Show all data. "
                                                      "For large queries, this may take some time to load.",
                                               default = False ),
                           forms.BooleanField( id = "notech",
                                               title = "Quiet",
                                               help = "Suppress display of technical information.",
                                               default = False )] )

        if proteins:
            form.content.append( forms.ComboBoxField( title = "Filter",
                                                      help = "Filter the view to a single protein",
                                                      source = { "": "--All--",
                                                                 **{
                                                                     protein.accession: f"{protein.accession} ({protein.title})"
                                                                     for protein in proteins } },
                                                      default = "",
                                                      sorted = True,
                                                      id = "filter" ) )

        if klass_name == alacat.Peptide.__name__:
            id = gui_pipelines.get_results_id( self )
            from alacat.gui.screens import ProteinsScreen
            form.append( forms.Html( "Alternatively you can view the list of <a href='{}'>individual proteins</a>.".format(
                    ProteinsScreen.get_url( self.protocol, **{ RESULTS_FIELD_ID: id } ) ) ) )

        return forms.Section( "View options", form, collapsed = True )


    class __ReadForm:
        def __init__( self, rq: screens.RequestInfo ):
            self.is_assessed_filtered: bool = bool( int( rq.get_data.get1( "assessed", "0" ) ) )
            self.no_technical_data: bool = bool( int( rq.get_data.get1( "notech", "0" ) ) )
            self.tsv: bool = bool( int( rq.get_data.get1( "tsv", "0" ) ) )
            self.is_unlimited: bool = bool( int( rq.get_data.get1( "unlimited", "0" ) ) ) or self.tsv

    def group_by_protein( self, klass: Type, subjects, ranks: Dict[TSubject, int], is_unlimited: bool ):
        GRAND_LIMIT = 500
        gm: gui_pipelines.GuiModel = self.get_gui_model()

        proteins: Optional[Set[alacat.Protein]]

        if issubclass( klass, alacat.ComesFromProtein ):
            #
            # Group by protein
            #
            proteins = set( subject.protein for subject in subjects )
            filter = self.request.get_data.get1( "filter", "" )

            # OPTIONAL: Apply protein filter
            if filter:
                subjects = [x for x in subjects if x.protein.accession == filter]

            # OPTIONAL: Apply limit on a per-protein basis
            if not is_unlimited:
                subjects_by_protein = array_helper.box( subjects, lambda x: x.protein )
                subjects_by_protein = [sorted( box, key = ranks.get ) for box in subjects_by_protein.values()]
                by_protein_limit = max( GRAND_LIMIT // len( subjects_by_protein ), 5 )

                subjects = []

                for box in subjects_by_protein:
                    if len( subjects ) > GRAND_LIMIT:
                        break

                    for subject in _apply_limit( box, gm.pipeline, by_protein_limit ):
                        subjects.append( subject )

            # Sort peptides by protein
            subjects = sorted( subjects, key = lambda x: (x.protein.accession, ranks[x]) )

        else:
            #
            # Do not group by protein
            #
            proteins = None

            if not is_unlimited:
                subjects = _apply_limit( subjects, gm.pipeline, GRAND_LIMIT )

        return subjects, proteins


def _apply_limit( subjects, pipeline: alacat.Pipeline, n: int ):
    """
    Limits the list of `subjects` to `n` items.
    More items may be included if they have been selected.
    """
    r = []

    for index, subject in enumerate( subjects ):
        if index < n or pipeline.summarise_selection( subject ) != alacat.ESelectors.NONE:
            r.append( subject )

    return r


def _sort_order( column: alacat.Column ) -> Tuple[str, int, str]:
    return column.handler.accession, column.display_order, column.accession
