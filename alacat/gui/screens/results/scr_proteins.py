from typing import List

import alacat
from alacat.gui import formatting
from hatmul import forms, screens

from .scr_scores_table import ScoresTableScreen
from alacat.gui.gui_pipelines import RESULTS_FIELD_ID


class ProteinsScreen( screens.BasicScreen, formatting.IModelScreen ):
    """
    Shows the full list of proteins.
    
    No form is supplied to select the result set, this parameter must be supplied by the caller.
    """
    __view__ = screens.ScreenData( url = f"/<{RESULTS_FIELD_ID}>/proteins",
                                   index = screens.EIndex.TERTIARY,
                                   folder="Results")
    
    
    def on_call( self ):
        gm = self.get_gui_model()
        
        rows: List[forms.TRenderable] = [["Protein", "Peptides"]]
        
        for protein in sorted( gm.pipeline.proteins, key = alacat.Protein.accession.fget ):
            rows.append( [formatting.EntityLink( protein, f"{protein.accession} ({protein.title})" ),
                          str( sum( 1 for peptide in gm.pipeline.candidate_peptides if peptide.protein is protein ) )] )
        
        htm_help = ("<p>"
                     "This screen lists the proteins in your model. "
                     "Click a protein to view the peptides for that protein, and which peptides are considered quantotypic. "
                     "Alternatively, you can view all peptide scores in a table on the <a href='{}'>scores screen</a>."
                     "</p>"
                     .format( ScoresTableScreen.get_url( self.protocol, **{ RESULTS_FIELD_ID: gm.id, "table": alacat.Peptide.__name__ } ) ))
        ren_help = forms.HelpBlock( forms.Html( htm_help ) )
        
        return forms.Page(
                content = forms.Report( title = [formatting.ModelLink( gm.id ), "Alacat Report"],
                                        content = forms.Section( "",
                                                                 [ren_help,
                                                                  forms.Table( rows, col_headers = 1 )] ) ) )
