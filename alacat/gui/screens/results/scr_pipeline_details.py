from functools import partial
import os
from hatmul import forms, screens
from alacat.gui import formatting, gui_pipelines
import alacat
from .scr_scores_table import ScoresTableScreen
from alacat.gui import screens as _screens
from mhelper import string_helper
from alacat.gui.gui_pipelines import RESULTS_FIELD_ID


class PipelineDetailsScreen( screens.BasicScreen, formatting.IModelScreen ):
    """
    Shows the technical information associated with executing a pipeline.
    
    No form is supplied to select the result set, this parameter must be supplied by the caller.
    """
    __view__ = screens.ScreenData( url = f"/<{RESULTS_FIELD_ID}>/pipeline" ,
                                   index = screens.EIndex.TERTIARY,
                                   folder="Results")
    
    
    def on_call( self ):
        gm = self.get_gui_model()
        
        from alacat.gui.screens import PipelineWarningsScreen
        
        return forms.Page(
                content = forms.Report(
                        title = [formatting.ModelLink( gm.id ), "Pipeline details"],
                        content = [self.__make_scores_table_links_section( gm ),
                                   self.__make_profiling_section( gm ),
                                   PipelineWarningsScreen.make_warnings_section( gm, "Warnings" ),
                                   self.__make_details_section( gm )] ) )
    
    
    def __make_profiling_section( self, gm: gui_pipelines.GuiModel ):
        data = list( gm.pipeline.state.profiling )
        sums = { }
        ns = { }
        
        for name, value in data:
            elements = tuple( name.split( "|" ) )
            
            for element in elements:
                sums[element] = sums.get( element, 0 ) + value
                ns[element] = ns.get( element, 0 ) + 1
        
        rows = [["Profile", "Time", "n"]]
        
        for element, value in sorted( sums.items(), key = lambda x: x[1], reverse = True ):
            n = ns[element]
            rows.append( [element, string_helper.timedelta_to_string( value ), str( n )] )
        
        results = gm.results_set
        f0 = results.get_pipeline_file_name()
        f1 = results.get_cart_file_name()
        s0 = string_helper.format_size( os.path.getsize( f0 ) ) if os.path.isfile( f0 ) else "empty"
        s1 = string_helper.format_size( os.path.getsize( f1 ) ) if os.path.isfile( f1 ) else "empty"
        spar = forms.Paragraph( f"Results are {s0} and the current cart is {s1}." )
        
        return forms.Section( "Profiling",
                              [spar,
                               forms.Table( rows, col_headers = 1 )],
                              collapsed = True )
    
    
    def __make_scores_table_links_section( self, gm ):
        u = partial( ScoresTableScreen.get_url, self.protocol, **{ RESULTS_FIELD_ID: gm.id } )
        
        options = forms.TileGrid( forms.ETileGrid.INDEX_TILES )
        
        options.append( forms.ButtonTile( url = u( table = alacat.Protein.__name__ ),
                                          text = "Protein scores table",
                                          icon = formatting.AlacatResources.SCORES_TABLE ) )
        options.append( forms.ButtonTile( url = u( table = alacat.Peptide.__name__ ),
                                          text = "Peptide scores table",
                                          icon = formatting.AlacatResources.SCORES_TABLE ) )
        options.append( forms.ButtonTile( url = u( table = alacat.Qblock.__name__ ),
                                          text = "Qblock scores table",
                                          icon = formatting.AlacatResources.SCORES_TABLE ) )
        options.append( forms.ButtonTile( url = u( table = alacat.Qmenu.__name__ ),
                                          text = "Qmenu scores table",
                                          icon = formatting.AlacatResources.SCORES_TABLE ) )
        
        options_section = forms.Section( "", [
            forms.HelpBlock( forms.Html( "Metrics are produced for entities during their respective <em>provision</em> and <em>scoring</em> phases. These are subsequently ranked in order to eliminate non-quantotypic entities from the pipeline. Note that proteins are never eliminated and thus have <em>provider</em>, but no <em>scorer</em>, metrics, which are not ranked." ) ),
            options] )
        
        return options_section
    
    
    def __make_details_section( self, gm ):
        pipeline: alacat.Pipeline = gm.pipeline
        
        rows = []
        rows.append( ["Organisms", "User-specified", formatting.TaxonWidget( pipeline.model.organisms )] )
        rows.append( ["", "Resolved", formatting.TaxonWidget( pipeline.state.organisms )] )
        rows.append( ["Proteins", "User-specified", formatting.poppinlist(  pipeline.model.proteins )] )
        rows.append( ["", "Resolved", formatting.poppinlist(  pipeline.proteins )] )
        rows.append( ["Peptides", "Candidate", formatting.poppinlist(  pipeline.candidate_peptides )] )
        rows.append( ["", "Scores", f"{pipeline.peptide_scores.shape} matrix"] )
        rows.append( ["", "User-favoured", formatting.poppinlist(  pipeline.favoured_peptides )] )
        rows.append( ["", "System-favoured", formatting.poppinlist(  pipeline.selected_peptides )] )
        rows.append( ["", "Selected", formatting.poppinlist(  pipeline.selected_peptides )] )
        rows.append( ["Qblocks", "Candidate", formatting.poppinlist(  pipeline.candidate_qblocks )] )
        rows.append( ["", "Scores", f"{pipeline.qblock_scores.shape} matrix"] )
        rows.append( ["", "User-favoured", formatting.poppinlist(  pipeline.favoured_qblocks )] )
        rows.append( ["", "System-favoured", formatting.poppinlist(  pipeline.system_selected_qblocks )] )
        rows.append( ["", "Selected", formatting.poppinlist(  pipeline.selected_qblocks )] )
        rows.append( ["Qmenus", "Candidate", formatting.poppinlist(  pipeline.candidate_menus )] )
        rows.append( ["", "Scores", f"{pipeline.qmenu_scores.shape} matrix"] )
        rows.append( ["", "User-favoured", formatting.poppinlist(  pipeline.favoured_menus )] )
        rows.append( ["", "System-favoured", formatting.poppinlist(  pipeline.system_selected_menus )] )
        rows.append( ["", "Selected", formatting.poppinlist(  pipeline.selected_menus )] )
        rows.append( ["Pipeline", "Warnings", formatting.poppinlist(  pipeline.warnings )] )
        
        details_section = forms.Section(
                title = "Details",
                content = [
                    forms.TileGrid( style = forms.TileGrid.INDEX_TILES,
                                    content = [forms.ButtonTile( url = _screens.EverythingScreen.get_url( self.protocol,
                                                                                                          **{ RESULTS_FIELD_ID: gm.id } ),
                                                                 text = "All entities",
                                                                 icon = formatting.AlacatResources.SCORES_TABLE )] ),
                    forms.Table( rows = rows, row_headers = 2 )
                    ],
                collapsed = True )
        
        return details_section
