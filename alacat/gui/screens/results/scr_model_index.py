from typing import Optional
from hatmul import forms, screens
from alacat.gui import formatting, gui_pipelines
import alacat
from .scr_model_details import AlacatModelDetailsScreen
from .scr_pipeline_details import PipelineDetailsScreen
from .scr_scores_table import ScoresTableScreen
from .scr_final_report import FinalReportScreen
from .scr_warnings import PipelineWarningsScreen
from .scr_delete import DeleteScreen
from .scr_download import DownloadScreen
from mhelper.enum_helper import get_enum_title
from mhelper.string_helper import array_to_string
from mhelper.bio_requests.ncbi_taxonomy import get_taxonomy
from alacat.gui.gui_pipelines import RESULTS_FIELD_ID


class ModelIndexScreen( screens.BasicScreen, formatting.IModelScreen ):
    """
    The main results screen shown after executing the pipeline.
    
    No form is supplied to select the result set, this parameter must be supplied by the caller.
    """
    __view__ = screens.ScreenData( url = f"/<{RESULTS_FIELD_ID}>/index",
                                   scenario = screens.EScenario.MODEL,
                                   index = screens.EIndex.TERTIARY,
                                   folder = "Results" )

    def on_call( self ):
        from alacat.gui.screens.scr_pending import ViewLogScreen

        gm = self.get_gui_model()

        menu = forms.TileGrid( forms.ETileGrid.INDEX_TILES )
        menu.append( forms.ButtonTile( url = FinalReportScreen.get_url( self.protocol, **{ RESULTS_FIELD_ID: gm.id } ),
                                       text = "Final report",
                                       hover_text = f"Show the final report",
                                       icon = formatting.AlacatResources.FINAL_REPORT ) )
        menu.append( forms.ButtonTile( url = ScoresTableScreen.get_url( self.protocol, **{ RESULTS_FIELD_ID: gm.id,
                                                                                           "table"         : "Peptide" } ),
                                       text = "Peptide picker",
                                       hover_text = "Peptide scores table (Consequence style)",
                                       icon = formatting.AlacatResources.PEPTIDE_PICKER ) )

        options = forms.TileGrid( forms.ETileGrid.INDEX_TILES )
        options.append( forms.ButtonTile( url = AlacatModelDetailsScreen.get_url( self.protocol, **{ RESULTS_FIELD_ID: gm.id } ),
                                          text = "Review parameters",
                                          hover_text = f"Review the original request",
                                          icon = formatting.AlacatResources.MODEL ) )
        options.append( forms.ButtonTile( url = PipelineDetailsScreen.get_url( self.protocol, **{ RESULTS_FIELD_ID: gm.id } ),
                                          text = "Technical details",
                                          hover_text = f"Show result tables",
                                          icon = formatting.AlacatResources.MODEL ) )
        options.append( forms.ButtonTile( url = self.get_task_url(),
                                          text = "Original query",
                                          icon = formatting.AlacatResources.MODEL ) )
        options.append( forms.ButtonTile( url = ViewLogScreen.get_url( self.protocol, **{ RESULTS_FIELD_ID: gm.id } ),
                                          text = "Execution log",
                                          icon = formatting.AlacatResources.MODEL ) )
        options.append( forms.ButtonTile( url = PipelineWarningsScreen.get_url( self.protocol, **{ RESULTS_FIELD_ID: gm.id } ),
                                          text = "See warnings",
                                          icon = formatting.AlacatResources.WARNING ) )
        options.append( forms.ButtonTile( url = self.get_download_url(),
                                          text = "Download results",
                                          icon = formatting.AlacatResources.DOWNLOAD ) )
        options.append( forms.ButtonTile( url = DeleteScreen.get_url( self.protocol, **{ RESULTS_FIELD_ID: gm.id } ),
                                          text = "Delete results",
                                          icon = formatting.AlacatResources.DELETE ) )

        organism = array_to_string( (get_taxonomy().get_name( x ) for x in gm.pipeline.state.organisms if x != -1),
                                    limit = 3,
                                    empty = "",
                                    ellipsis = "et alia" )
        digestion = get_enum_title( gm.pipeline.model.digestion )
        title = gm.pipeline.model.name

        if organism:
            organism = forms.escape( organism )
            organism_html = f"{title} - <i>{organism}</i> {digestion} model"
            organism_text = f"{title} - {organism} {digestion} model"
        else:
            organism_html = f"{title} - {digestion} model of unknown organism"
            organism_text = f"{title} - {digestion} model of unknown organism"

        ren_version_warning = self.__get_version_warning()

        return forms.Page(
                title = organism_text,
                content = forms.Report(
                        title = organism_html,
                        content = [forms.Section( "", [ren_version_warning,
                                                       forms.Paragraph(
                                                               "Your pipeline is complete, the report has been generated:" ),
                                                       menu] ),
                                   forms.Section( "Advanced",
                                                  options,
                                                  collapsed = True )]
                ) )

    def get_download_url( self ):
        gm = self.get_gui_model()
        download_url = DownloadScreen.get_url( self.protocol, **{ RESULTS_FIELD_ID: gm.id } )
        return download_url

    def __get_version_warning( self ) -> forms.TRenderable:
        gm = self.get_gui_model()

        if gm.pipeline.model.alacat_version == alacat.__version__:
            return None

        return forms.Warning( forms.Html(
                f"This pipeline was executed for {alacat.__title__} version {gm.pipeline.model.alacat_version} "
                f"but the current {alacat.__title__} version is {alacat.__version__}. "
                f"While data is usually compatible there may be problems in viewing some results. "
                f"If this is the case you can <a href='{self.get_download_url()}'>download your results</a> or "
                f"<a href='{self.get_task_url()}'>go back and run your query again</a>." ) )

    def get_task_url( self ):
        gm = self.get_gui_model()
        from alacat.gui.screens.scr_main import AlacatMainScreen, AlacatResultsStore
        from alacat.gui.screens.scr_pending import PendingScreen
        rs: AlacatResultsStore.ResultsSet = AlacatMainScreen.get_results_store().get_results( gm.id )
        td: Optional[AlacatResultsStore.TaskData] = rs.read_task_data()
        if td is not None:
            task_url = PendingScreen.get_url( self.protocol, **{ PendingScreen.TASK_ROUTE_ARG: td.task_id } )
        else:
            task_url = ""
        return task_url
