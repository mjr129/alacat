from typing import Type

from hatmul import forms, screens
from alacat.gui import formatting, gui_pipelines
from alacat.gui.gui_pipelines import RESULTS_FIELD_ID
import alacat


class EverythingScreen( screens.BasicScreen, formatting.IModelScreen ):
    """
    Shows all the entities present in a pipeline.
    
    No form is supplied to select the result set, this parameter must be supplied by the caller.
    """
    __view__ = screens.ScreenData( url = f"/<{RESULTS_FIELD_ID}>/all",
                                   index = screens.EIndex.TERTIARY,
                                   folder = "Results" )


    def on_call( self ):
        gm: gui_pipelines.GuiModel = self.get_gui_model()

        rows = [["Class", "Accession"]]

        all = sorted( gm.pipeline.state.all_entities, key = lambda x: (*reversed( [x.__name__ for x in x.__class__.mro()] ), x.accession) )
        last_klass = None

        for entity in all:
            klass = self.__get_class( entity )

            if klass != last_klass:
                last_klass = klass
                klass_name = forms.escape( klass.__name__ )
            else:
                klass_name = ""

            rows.append( [klass_name,
                          formatting.EntityLink( entity )] )

        return forms.Page( "Everything",
                           forms.Report( [formatting.ModelLink( gm.id ), "Everything"],
                                         forms.Section( "",
                                                        forms.Table( rows, col_headers = 1, scroll = False ) ) ) )

    @staticmethod
    def __get_class( x: object ) -> Type:
        for b in (alacat.ProteinProvider, alacat.PeptideProvider, alacat.QblockProvider, alacat.QmenuProvider,
                  alacat.PeptideScorer, alacat.QblockScorer, alacat.QmenuScorer):
            if isinstance( x, b ):
                return b

        return type( x )
