from hatmul import forms, screens
from alacat.gui import formatting, gui_pipelines
from alacat.gui.gui_pipelines import RESULTS_FIELD_ID


class PipelineWarningsScreen( screens.BasicScreen, formatting.IModelScreen ):
    """
    Shows warning messages that were issued during the course of the pipeline.
    
    No form is supplied to select the result set, this parameter must be supplied by the caller.
    """
    __view__ = screens.ScreenData( url = f"/<{RESULTS_FIELD_ID}>/warnings",
                                   index = screens.EIndex.TERTIARY,
                                   folder = "Results" )


    def on_call( self ):
        gm: gui_pipelines.GuiModel = self.get_gui_model()

        return forms.Page(
                content = forms.Report(
                        title = [formatting.ModelLink( gm.id ), "Pipeline warnings"],
                        content = self.make_warnings_section( gm, "" ) ) )


    @staticmethod
    def make_warnings_section( gm, title: str ):
        r = []

        for warning in gm.pipeline.warnings:
            r.append( forms.Escaped( type( warning ).__name__ ) )
            r.append( forms.PreBox( str( warning ), use_pre = True ) )
            r.append( forms.HorizontalRule() )

        warnings_section = forms.Section( title, r, collapsed = bool( title ) )
        return warnings_section
