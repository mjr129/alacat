from hatmul import screens
from alacat.gui import gui_pipelines, formatting
import json
import alacat


class AddToCartScreen( screens.BasicScreen, formatting.IModelScreen ):
    """
    Handles add-to-cart AJAX requests.
    
    The parameters must be supplied by the caller:
        
        * The result set ID
        * The subject ID
        * Whether to add or remove the subject
    """
    __view__ = screens.ScreenData(
            url = "/add_to_cart",
            index=screens.EIndex.TERTIARY,
            folder = "Special"
            )
    
    
    def on_call( self ):
        # Read arguments (always POST)
        form = self.request.post_data
        model_id: str = form.get1f( gui_pipelines.RESULTS_FIELD_ID )
        entity_id: str = form.get1f( gui_pipelines.SUBJECT_FIELD_ID )
        intended_change: bool = True if int( form.get1f( "change" ) ) > 0 else False
        
        # Check model
        if not model_id:
            return json.dumps( { "error": f"missing_parameter({gui_pipelines.RESULTS_FIELD_ID})" } )
        
        # Check entity
        if not entity_id:
            return json.dumps( { "error": f"missing_parameter({gui_pipelines.SUBJECT_FIELD_ID})" } )
        
        gm : gui_pipelines.GuiModel = self.get_gui_model()
        cart: gui_pipelines.Cart = gm.cart
        model: alacat.Pipeline = gm.pipeline
        
        if not model.find( entity_id ):
            return json.dumps( { "error": f"invalid_entity({model_id}::{entity_id})" } )
        
        # Execute change
        num_before: int = len( cart.entities )
        
        if intended_change:
            cart.entities.add( entity_id )
        else:
            if entity_id in cart.entities:
                cart.entities.remove( entity_id )
        
        num_after: int = len( cart.entities )
        results_set: gui_pipelines.AlacatResultsStore.ResultsSet = gui_pipelines.get_store().get_results( model_id )
        results_set.save_cart( cart )
        
        # Return status to JS
        return json.dumps( { "entities": num_after,
                             "status"  : intended_change,
                             "change"  : -1 if num_after < num_before else 1 if num_after > num_before else 0,
                             "query"   : f"{model_id}/{entity_id}" } )
