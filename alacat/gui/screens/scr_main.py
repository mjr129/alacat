import re
from typing import List, Literal, Sequence

import alacat
from alacat.gui import formatting, gui_pipelines
from hatmul import forms, screens
from alacat.gui.gui_pipelines import AlacatResultsStore, RESULTS_FIELD_ID
from .examples import get_examples
import logging
from mhelper.documentation_helper import Documentation


class AlacatSiteMapScreen( screens.SiteMapScreen ):
    __view__ = screens.ScreenData(
            url = "/sitemap",
            method = "post",
            index = screens.EIndex.SECONDARY,
            folder = "Help" )


# noinspection PyAbstractClass
class MainScreenBase( screens.LongTaskScreen ):
    __abstract__ = True

    @classmethod
    def get_results_store( cls ) -> "gui_pipelines.AlacatResultsStore":
        # Must be overridden since we derive from this class
        return gui_pipelines.get_store()

    # noinspection PyMethodOverriding
    @staticmethod
    def on_process( data: AlacatResultsStore.ExecutionData, model: alacat.Model ) -> Literal[True]:
        #
        # User submitted a request, its been parsed into a model, run the model
        #

        #
        # Log the model's creation to a file
        #
        results: gui_pipelines.AlacatResultsStore.ResultsSet = data.results
        log_fp = results.get_creation_log_fp()
        orig_state = MainScreenBase.__engage_loggers( log_fp )
        alacat.standard_log( f"(WEB PORTAL ATTACHES TO LOG)" )

        try:
            pipeline: alacat.Pipeline = model.exact()
        except Exception as ex:
            alacat.standard_log( f"AN ERROR OCCURRED ({type( ex ).__name__})." )
            raise
        finally:
            alacat.standard_log( f"(WEB PORTAL DETACHES FROM LOG)" )
            MainScreenBase.__disengage_loggers( orig_state )

        results_set: AlacatResultsStore.ResultsSet = data.results
        results_set.save_pipeline( pipeline )
        return True

    @staticmethod
    def __engage_loggers( log_fp ):
        state = []
        handler = logging.FileHandler( log_fp, encoding = "utf8" )
        handler.setLevel( logging.DEBUG )
        
        for logger_ in alacat.standard_logs.get_loggers():
            orig_level = logger_.level
            logger_.setLevel( logging.DEBUG )
            logger_.addHandler( handler )
            state.append( (logger_, orig_level) )
            
        return state, handler

    @staticmethod
    def __disengage_loggers( state ):
        handler = state[1]
        for logger_, orig_level in state[0]:
            logger_.removeHandler( handler )
            logger_.setLevel( orig_level )

    def rs_get_url( self, results_id: str ) -> str:
        from alacat.gui.screens.results.scr_model_index import ModelIndexScreen
        return ModelIndexScreen.get_url( self.protocol, **{ RESULTS_FIELD_ID: results_id } )

    def on_view_results( self, results: Literal[True] ) -> "forms.Page":
        raise RuntimeError( "Unexpected call. `rs_get_url` does not redirect here" )


class AlacatMainScreen( MainScreenBase ):
    """
    Main Alacat query submission screen.
    """
    __view__ = screens.ScreenData(
            url = "/",
            scenario = screens.EScenario.PRIMARY,
            method = "post",
            folder = "New",
            index = screens.EIndex.PRIMARY )

    def on_call( self ) -> "forms.Page":
        #
        # User just got here, show the form
        #
        form = forms.Form( method = "post" )

        form.append( forms.HelpSection( _Introduction() ) )
        form.append( forms.Section( "Help", _MoreIntroduction(), collapsed = True ) )
        form.append( forms.Section( "Examples", self.__make_examples(), collapsed = True ) )
        form.append( forms.Section( "Options", [_Controls.protein] ) )
        form.append(
                forms.Section( "Constraints", [_Controls.peptides, _Controls.qblocks, _Controls.qmenus], collapsed = True ) )
        form.append( forms.Section( "Advanced",
                                    [_Controls.title, _Controls.qblock_size, _Controls.digestion, _Controls.organism, _Controls.mandate, _Controls.regenerate, _Controls.sql_backing, _Controls.dry],
                                    collapsed = True ) )
        form.append( forms.Section( "Handlers",
                                    [_Controls.columns],
                                    collapsed = True ) )

        return forms.Page( "", forms.Report( "Alacat", form ) )

    def __make_examples( self ):
        #
        # Add the examples
        #
        examples = []
        for ex in get_examples():
            data = forms.InputField.field_to_data( ex.values, submit = False )
            data["example"] = ex.name
            url = self.get_url( self.protocol )
            li = forms.PostLink( ex.name, url, data ), forms.Html( f"<p>{ex.doc}</p>" )
            examples.append( li )

        example_block = forms.Html( ['Try an example: ', forms.UnorderedList( examples )] )
        return example_block

    def on_parse_form( self ) -> "forms.TRenderable":
        #
        # User submitted the form, read the input
        #
        rq = self.request
        title: str = _Controls.title.read_str( rq ) or "Untitled"
        organisms: Sequence[int] = _parse_organisms( _Controls.organism.read_str( rq ) )
        digestion: alacat.EDigestion = _Controls.digestion.read_value( rq )
        protein: str = _Controls.protein.read_text( rq )
        peptides: str = _Controls.peptides.read_text( rq )
        qblocks: str = _Controls.qblocks.read_text( rq )
        qmenus: str = _Controls.qmenus.read_text( rq )
        regenerate: bool = _Controls.regenerate.read_bool( rq )
        mandate: bool = _Controls.mandate.read_bool( rq )
        dry: bool = _Controls.dry.read_bool( rq )
        handlers = formatting.HandlersFieldsWidget.read_handlers( rq )
        protein_input = (protein,) if protein else ()
        peptides_input = (peptides,) if peptides else ()
        qblocks_input = (qblocks,) if qblocks else ()
        qmenus_input = (qmenus,) if qmenus else ()
        qblock_size = _Controls.qblock_size.read_int( rq )
        sql_backing: alacat.ESqlBacking = _Controls.sql_backing.read_value( rq )

        # Describe the parameters
        model = alacat.Model.explicit_init( name = title,
                                            organisms = organisms,
                                            digestion = digestion,
                                            handlers = handlers,
                                            proteins = protein_input,
                                            peptides = peptides_input,
                                            qblocks = qblocks_input,
                                            qmenus = qmenus_input,
                                            mandate = mandate,
                                            qblock_size = qblock_size,
                                            sql_backing = sql_backing,
                                            alacat_size = 100,
                                            min_peptide_length = None,
                                            partition_rankings = True,
                                            qbrick_size = 2,
                                            random_seed = 1,
                                            stage = alacat.EStage.ALL,
                                            fix_accessions = False)

        if dry:
            return forms.Page( "", forms.Report( "", forms.Section( "", forms.PreBox( (repr( model )) ) ) ) )

        if regenerate:
            self.delete_results( model )

        return self.begin_processing( model )


__numbers = re.compile( "[0-9]+" )


def _parse_organisms( text: str ):
    return tuple( int( x ) for x in __numbers.findall( text ) )


def _split( text ) -> List[str]:
    return [x for x in text.split( " " ) if x]


class _Introduction( forms.Widget ):
    def on_render( self, output: forms.Output ) -> None:
        from alacat.gui.screens import ConsequenceScreen
        consequence_url = ConsequenceScreen.get_url( output.get_protocol() )
        output.write( f"""
    <span style="font-variant: small-caps">
    {alacat.__title__}
    </span>
    is a consensus tool which uses a variety of simulations and empirical data to identify 'quantotypic' peptides.
    From these peptides it can also help you to construct QConcats for your chosen protein(s).
    A simplified "Consequence-like" interface <a href='{consequence_url}'>is provided here</a>. 
""" )


class _MoreIntroduction( forms.Widget ):
    workflow_page = forms.Resource( "alacat_static/workflow.pptx.png" )

    def on_render( self, output: forms.Output ) -> None:
        from alacat.gui.screens import AlacatAboutScreen
        about_url = AlacatAboutScreen.get_url( output.get_protocol() )
        workflow_url = self.workflow_page.get_url( output.get_protocol() )

        output.write( f"""
<p> 
    <b>Basic configuration: </b>
    In the boxes below you should specify your proteins of interest.
    You can tell the program to focus on specific peptides and Qconcats, or leave these boxes blank
    and the program will suggest them for you.
</p>
<p> 
    <b>Note: </b>
    This web GUI is interactive and will provide additional context if you click or hover over
    items with your mouse.
</p>
<p>
    <b>Advanced configuration: </b>
    Individual analyses can be turned on and off depending on whether you are not interested in their output, but for more thorough customisation it is recommended to use the Python library instead of the web interface.
</p>
<p>
    <b>Method details: </b>
    <em><a href="{workflow_url}">See the workflow diagram here.</a></em><br/>
    {alacat.__title__} is a consensus tool that uses a variety of "scoring methods" to determine the suitability of
    peptides, qbricks, etc. 
    The list of individual scoring methods can be viewed in the advanced section, below, and links to the literature are
    provided for relevant methods on the 
    <a href='{about_url}'>
        about
    </a>
    screen.
</p>
<p>
    <b>API: </b>
    {alacat.__title__} is available to 
    <a href='http://bitbucket.org/mjr129/alacat'>
        download
    </a>
    as a web client, desktop application, command line utility and Python library.
    It can be downloaded to your local computer from 
    <a href='https://pypi.org/'>
        pypi.org
    </a> 
    using
    <span style='font-family: monospace;'>
        python -m pip install alacat
    </span>.

    The web GUI is designed for modern web browsers in a desktop environment.    
    Old/mobile/esoteric browsers are not supported.
</p>
""" )


model_doc = { k: forms.RestructuredText( v ) for k, v in Documentation( alacat.Model.__init__.__doc__ ).get_domain( "param" ).items() }


class _Controls:
    digestion = forms.ComboBoxField(
            "Digestion",
            id = "digestion",
            help = model_doc["digestion"],
            source = alacat.EDigestion,
            element = "select",
            default = str( alacat.EDigestion.TRYPSIN.value ) )

    protein = forms.TextAreaField(
            "Protein",
            id = "protein",
            rows = 8,
            help = model_doc["proteins"] )

    peptides = forms.TextAreaField(
            "Peptides",
            id = "peptides",
            help = model_doc["peptides"] )
    qblocks = forms.TextAreaField(
            "Qblocks",
            id = "qblocks",
            help = model_doc["qblocks"] )
    qmenus = forms.TextAreaField(
            "Qmenus",
            id = "qmenus",
            help = model_doc["qmenus"] )

    title = forms.TextField( "Title",
                             id = "title",
                             help = model_doc["name"] )

    organism = forms.EditableComboBoxField(
            "Organism(s)",
            id = "organism",
            help = model_doc["organisms"],
            options = ["9606 -- Homo sapiens, human",
                       "4932 -- Saccharomyces cerevisiae, yeast",
                       "9606, 4932 -- Human and yeast",
                       "942147 -- Spongiforma squarepantsii"] )

    mandate = forms.BooleanField( title = "Mandate",
                                  id = "mandate",
                                  default = True,
                                  help = model_doc["mandate"] )
    regenerate = forms.BooleanField( title = "Reset",
                                     id = "regenerate",
                                     help = "Regenerate the results, even if they already exist on the system." )
    dry = forms.BooleanField( "Dry run",
                              id = "dry",
                              help = "Shows the parameters but does not execute the workflow. You can use this to review your parameters before proceeding, or to copy the resulting parameter set for use in Python." )

    qblock_size = forms.IntegerField( "Num qblocks",
                                      id = "qblock_size",
                                      help = model_doc["qblock_size"],
                                      default = 1 )

    sql_backing = forms.ComboBoxField( "SQL",
                                       id = "sql",
                                       help = model_doc["sql_backing"],
                                       element = "select",
                                       source = alacat.ESqlBacking,
                                       default = alacat.ESqlBacking.READ_WRITE )

    columns = formatting.HandlersFieldsWidget()
