from alacat import debugging
from hatmul import forms, screens


class AlacatMaintenanceScreen( screens.BasicScreen ):
    __view__ = screens.ScreenData( url = "/maintenance",
                                   folder="Special")
    
    
    def on_call( self ):
        i = self.request.get_data.get1( "i", "" )
        
        if i:
            return debugging.get_paths()[int( i )].get_size()
        
        rows = []
        
        for inf in debugging.get_paths():
            rows.append( [inf.name,
                          inf.source,
                          inf.path,
                          forms.DynamicLoader( AlacatMaintenanceScreen.get_url( self.protocol, i = inf.index ),
                                               element = forms.EDynamicLoader.HIDDEN )] )
        
        return forms.Page( "Maintenance",
                           forms.Report( "Maintenance",
                                         forms.Section( "", forms.Table( rows ) ) ) )
