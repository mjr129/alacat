from .scr_view_entity_base import ViewEntityBaseScreen
from hatmul import screens, forms
import alacat
from alacat.gui.gui_pipelines import RESULTS_FIELD_ID


class ViewTextScreen( ViewEntityBaseScreen ):
    """
    Online viewer for `Qmenu`.
    
    No form is supplied to select the results set or entity, these parameters must be supplied by the caller.
    """
    __view__ = screens.ScreenData(
            title = "View text",
            url = f"/<{RESULTS_FIELD_ID}>/text/<subject>",
            record = alacat.Text,
            field_id = "subject",
            request_format = lambda x: x.accession,
            index = screens.EIndex.TERTIARY,
            folder = "Entities",
    )


    def get_introduction( self ) -> forms.TRenderable:
        subject: alacat.Text = self.subject
        return forms.ReadOnlyTextArea( subject.text, rows = 10 )

    def get_composition( self ) -> forms.TRenderable:
        return None
