import inspect

from .scr_view_entity_base import ViewEntityBaseScreen
from hatmul import screens, forms
import alacat
from alacat.gui import formatting
from alacat.gui.gui_pipelines import RESULTS_FIELD_ID


class ViewColumnScreen( ViewEntityBaseScreen ):
    """
    Online viewer for `Column`.
    
    No form is supplied to select the results set or entity, these parameters must be supplied by the caller.
    """
    __view__ = screens.ScreenData( title = "View Column",
                                   url = f"/<{RESULTS_FIELD_ID}>/columns/<subject>",
                                   record = alacat.Column,
                                   field_id = "subject",
                                   request_format = lambda x: x.accession,
                                   index=screens.EIndex.TERTIARY,
    folder = "Entities")

    def get_introduction( self ) -> forms.TRenderable:
        gm = self.get_gui_model()
        col: alacat.Column = self.subject

        #
        # Introduction
        #
        list_of_tables_html = self.list_tables( gm, col )

        

        table = forms.Table(
                [["Column name", str( col.accession )],
                 ["Appears in table", list_of_tables_html],
                 ["Produced by handler", formatting.EntityLink( col.handler, col.handler.accession )],
                 ["Rule", formatting.RuleWidget( col.assessor )],
                 ["Scoring weight", formatting.WeightWidget( col.weight )]], row_headers = 1 )

        return [forms.HelpBlock( forms.Html( "<p>This is a column in a results table, see the documentation below for details.</p>" ) ), table]

    def get_composition( self ) -> forms.TRenderable:
        return None

    def get_extra( self ) -> forms.TRenderable:
        col: alacat.Column = self.subject
        
        if inspect.isroutine( col.assessor ):
            rst_assessor = col.assessor.__doc__ or "The rule is not documented."
        else:
            rst_assessor = col.assessor.__class__.__doc__ or "The rule is not documented."

        rst_handler = col.handler.__doc__ or "The handler is not documented."

        gui_handler_doc = forms.HelpBlock( forms.RestructuredText( rst_handler ) )
        gui_assessor_doc = forms.HelpBlock( forms.RestructuredText( rst_assessor ) )

        return [forms.Section( "Handler documentation", gui_handler_doc, collapsed = True ),
                forms.Section( "Rule documentation", gui_assessor_doc, collapsed = True )]
