from .scr_view_entity_base import ViewEntityBaseScreen, CompositionRow
from alacat.gui.gui_pipelines import RESULTS_FIELD_ID
from hatmul import screens, forms
import alacat
from alacat.gui import formatting


class ViewLinkedPeptideScreen( ViewEntityBaseScreen ):
    """
    Online viewer for `LinkedPeptide`.
    
    No form is supplied to select the results set or entity, these parameters must be supplied by the caller.
    """
    __view__ = screens.ScreenData(
            title = "View Qbrick",
            url = f"/<{RESULTS_FIELD_ID}>/qpeptides/<subject>",
            record = alacat.LinkedPeptide,
            field_id = "subject",
            request_format = lambda x: x.accession ,
            index=screens.EIndex.TERTIARY,
    folder = "Entities")
    
    
    def get_introduction( self ) -> forms.TRenderable:
        qpeptide: alacat.LinkedPeptide = self.subject
        
        pep_f = forms.Html( [f"<span class='alacat_peptide_line'>",
                             f"<span style='opacity: 50%'>",
                             formatting.Linker( qpeptide, "n" ),
                             "</span><span class='alacat_aa'>",
                             forms.escape( qpeptide.peptide.sequence ),
                             "</span><span style='opacity: 50%'>",
                             formatting.Linker( qpeptide, "c" ),
                             "</span>",
                             "</span>"] )
        
        return [forms.Paragraph( pep_f ),
                forms.Paragraph( ["This qpeptide originates from the peptide ", 
                                  formatting.EntityLink( qpeptide.peptide ),
                                  " in the protein ",
                                  formatting.EntityLink( qpeptide.protein )] )]
    
    
    def get_composition( self ) -> forms.TRenderable:
        return self.make_composition_table( [CompositionRow( None, None, None, self.subject )] ).to_table()
