from typing import List

from .scr_view_entity_base import ViewEntityBaseScreen
from hatmul import screens, forms
import alacat
from alacat.gui import formatting
from alacat.gui.gui_pipelines import RESULTS_FIELD_ID


class ViewHandlerScreen( ViewEntityBaseScreen ):
    """
    Online viewer for `Handler`.
    
    No form is supplied to select the results set or entity, these parameters must be supplied by the caller.
    """
    __view__ = screens.ScreenData( title = "View Handler",
                                   url = f"/<{RESULTS_FIELD_ID}>/handlers/<subject>",
                                   record = alacat.Handler,
                                   field_id = "subject",
                                   request_format = lambda x: x.accession,
                                   index = screens.EIndex.TERTIARY,
                                   folder = "Entities" )

    def get_introduction( self ) -> forms.TRenderable:
        # Find the table for the handler
        gm = self.get_gui_model()
        handler: alacat.Handler = self.subject

        #
        # Introduction
        #
        ts = self.list_tables( gm, handler )

        if ts:
            par = forms.Paragraph( [f"This handler appears in the ", ts, " table."] )
        else:
            from alacat.gui.screens.results.scr_model_details import AlacatModelDetailsScreen
            from alacat.gui.screens.results.scr_warnings import PipelineWarningsScreen
            modsm = AlacatModelDetailsScreen.get_url( self.protocol,
                                                      **{ RESULTS_FIELD_ID: gm.id } )
            warsc = PipelineWarningsScreen.get_url( self.protocol,
                                                    **{ RESULTS_FIELD_ID: gm.id } )
            par = forms.Paragraph( forms.Html( f"This handler doesn't appear in any tables, the handler was never required, <a href='{modsm}'>all of its outputs have been suppressed</a>, or the it <a href='{warsc}'>failed to initialise</a>." ) )

        return [
            forms.Section(
                    title = "",
                    content = [forms.PreBox( str( handler ) ), par] )]

    def get_extra( self ) -> forms.TRenderable:
        handler = self.subject
        doc_handler = handler.__doc__ or "The handler is not documented."
        return forms.Section( "Documentation", forms.HelpBlock( forms.RestructuredText( doc_handler ) ), collapsed = True )

    def get_composition( self ) -> forms.TRenderable:
        gm = self.get_gui_model()
        handler = self.subject
        my_columns = set( column for table in gm.pipeline.find_tables( handler ) for column in table.columns if column.handler is handler )

        if not my_columns:
            return forms.Paragraph( "This handler produced no output columns." )

        rows: List[List[forms.TRenderable]] = [["Column"], *[[formatting.EntityLink( x, x.accession )] for x in my_columns]]
        return forms.Table( rows = rows, col_headers = 1 )
