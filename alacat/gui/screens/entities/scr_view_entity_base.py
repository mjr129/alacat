import sys
from typing import List, Optional

import alacat as _alacat
from alacat.gui import formatting, gui_pipelines, widgets
from alacat.gui.formatting import CompositionRow
from alacat.gui.screens.results import scr_scores_table
from hatmul import forms, screens
from mhelper import array_helper
from alacat.gui.gui_pipelines import RESULTS_FIELD_ID, SUBJECT_FIELD_ID


class Constants:
    RED_LIGHT = '<span style="color:#cd4547;">⬤</span>'
    AMBER_LIGHT = '<span style="color:#faa252;">⬤</span>'
    GREEN_LIGHT = '<span style="color:#47b7a7;">⬤</span>'
    MAGENTA_LIGHT = '<span style="color:#fb1ef6">⬤</span>'
    BLUE_LIGHT = '<span style="color:#29c3ff;">⬤</span>'


class ViewEntityBaseScreen( screens.BasicScreen, formatting.IModelScreen ):
    __abstract__ = True

    def __init__( self ):
        super().__init__()
        self.subject = None

    def on_call( self ):
        self.subject = self.get_subject()
        klass = self.subject.__class__.__name__
        scores_listing, scores = self.get_scores_listing()

        content = []

        content.append( forms.Section(
                title = "",
                content = [
                    self.get_introduction(),
                ] ) )

        content.append( self.get_extra() )

        content.append( self.get_final_position_table() )

        composition = self.get_composition()

        if composition:
            composition = [forms.HelpButton( f"This section shows the composition of the {klass}, regardless of whether this was chosen to form part of the final report." ),
                           composition]

            content.append( forms.Section(
                    title = "Composition",
                    content = composition,
                    collapsed = True ) )

        content.append( scores_listing )

        content.append( self.__get_sequence() )

        content.append( self.get_id_report() )

        return forms.Page(
                content = forms.Report(
                        title = [formatting.ModelLink( self.get_gui_model().id ), self.get_title()],
                        content = content ) )

    def get_title( self ):
        return f"{self.subject.__class__.__name__}: {self.subject.accession}"

    def get_metric_summary( self,
                            gm: gui_pipelines.GuiModel,
                            klass_name: str,
                            scores: List[_alacat.Score],
                            ss_scores: List[_alacat.Score],
                            summary: _alacat.EntitySummary
                            ) -> Optional[forms.UnorderedList]:
        """
        Small <ul> of metrics that appears above the table.
        
        :param gm: 
        :param klass_name: 
        :param scores: 
        :param ss_scores: 
        :param summary: 
        :return: 
        """
        if not scores:
            return None

        lst = forms.UnorderedList()

        lst.append( ["Overall rank: ", formatting.SelectionRank( summary )] )

        if len( scores ) != len( ss_scores ):
            lst.append( f"Showing the first {len( ss_scores ):,} of {len( scores ):,} scores for this {klass_name.lower()}." )
        else:
            lst.append( f"Showing all {len( ss_scores ):,} scores for this {klass_name.lower()}." )

        full_url = forms.escape( scr_scores_table.ScoresTableScreen.get_url( self.protocol, **{ RESULTS_FIELD_ID: gm.id, "table": klass_name } ) )

        grade: _alacat.EGrade

        if summary is not None:
            lst.append( forms.Html( f"<span>Total score: {summary.total_points:,}</span>" ) )

        lst.append( forms.Link( full_url, f"See also: all {klass_name.lower()}s" ) )

        return lst

    def get_introduction( self ) -> forms.TRenderable:
        raise NotImplementedError( "abstract" )

    def get_composition( self ) -> forms.TRenderable:
        raise NotImplementedError( "abstract" )

    def get_extra( self ) -> forms.TRenderable:
        return None

    def get_final_position_table( self ):
        gm: gui_pipelines.GuiModel = self.get_gui_model()
        subject: _alacat.Entity = self.subject
        klass = subject.__class__.__name__

        if isinstance( subject, _alacat.Column ) or isinstance( subject, _alacat.Handler ) or isinstance( subject, _alacat.Text ):
            return None

        rows = []

        for qmenu in gm.pipeline.selected_menus:
            if subject.__class__ is _alacat.Qmenu and qmenu is not subject:
                continue

            for qconcat in qmenu.qconcats:
                if subject.__class__ is _alacat.Qconcat and qconcat is not subject:
                    continue

                for qbrick in qconcat.qbricks:
                    if subject.__class__ is _alacat.Qbrick and qbrick is not subject:
                        continue

                    if subject.__class__ is _alacat.Protein and qbrick.protein is not subject:
                        continue

                    for linked_peptide in qbrick.linked_peptides:
                        if subject.__class__ is _alacat.LinkedPeptide and linked_peptide is not subject:
                            continue

                        peptide = linked_peptide.peptide

                        if subject.__class__ is _alacat.Peptide and peptide is not subject:
                            continue

                        rows.append( CompositionRow( qmenu, qconcat, qbrick, linked_peptide ) )

        if not rows:
            return None

        return forms.Section( "Final position",
                              [forms.HelpButton( f"This section shows the context this {klass} has been placed within in the final report. If this {klass} was not selected for processing, this section is not shown." ),
                               formatting.CompositionTable( gm, rows ).to_table()],
                              collapsed = True )

    def get_scores_listing( self ):
        gm = self.get_gui_model()
        subject = self.subject

        # Get the table
        if isinstance( subject, _alacat.Handler ):
            return None, None

        table: _alacat.ScoresTable = gm.pipeline.find_table( subject )

        if table is None:
            return None, None

        klass_name = table.subjects[0].__class__.__name__
        summary = gm.pipeline.summarise( subject )

        ren_table, all_scores, scores = self.__render_scores_listing_table( table )
        ren_help = f"This table shows the scores for this {klass_name}. These include the raw metric, grade applied by the assessor, and the priority of the assessment. Click on the headings for more details."
        ren_summary: forms.TRenderable = self.get_metric_summary( gm, klass_name, all_scores, scores, summary )
        ren_title = ["Scores ", formatting.SelectionRank( summary )]
        ren_content = [forms.Subsection( "Summary", ren_summary ),
                       forms.Subsection( "Table", ren_table )]

        return forms.Section( ren_title,
                              ren_content,
                              collapsed = True,
                              help = ren_help ), all_scores

    def __render_scores_listing_table( self, table: _alacat.ScoresTable ):
        """
        Actual scores table.
        
        :param table: 
        :return: 
        """
        gm = self.get_gui_model()
        subject = self.subject

        #
        # FORMAT THE TABLE
        #
        DISPLAY_LIMIT = 200
        cols: List[List[forms.TRenderable]] = []
        all_scores: List[_alacat.Score]  # All the scores
        scores: List[_alacat.Score]  # The scores we actually display
        fmt = array_helper.RepeatBlanker()
        num_row_headers: int

        if isinstance( subject, _alacat.Column ):
            # We're displaying a column, i.e. source from a transposed view
            all_scores = [x for x in table.all_scores if x.column is subject]
            cfp = isinstance( all_scores[0].subject, _alacat.ComesFromProtein )

            scores = sorted( all_scores, key = self.__scores_order )[:DISPLAY_LIMIT]

            if cfp:
                # PROTEIN column
                cols.append( [["Protein"],
                              *[fmt( formatting.EntityLink( score.subject.protein ), score.subject.protein ) for score in scores]] )
                num_row_headers = 2
            else:
                num_row_headers = 1

            # SUBJECT column 
            cols.append( ["Subject", *[fmt( formatting.EntityLink( score.subject ), score.subject ) for score in scores]] )

            cols.append( ["Rank", *[formatting.SelectionRank( gm.pipeline.summarise( score.subject ) ) for score in scores]] )
        else:
            # We're displaying a row, i.e. source normally
            all_scores = [x for x in table.all_scores if x.subject is subject]
            scores = sorted( all_scores, key = lambda x: (x.origin.accession, x.column.accession)[:DISPLAY_LIMIT] )

            # HANDLER column
            cols.append( [["Handler"], *[fmt( formatting.EntityLink( score.origin ), score.origin ) for score in scores]] )

            # METRIC column
            cols.append( [["Metric"], *[fmt( formatting.EntityLink( score.column ), score.column ) for score in scores]] )

            num_row_headers = 2
        # VALUE, GRADE and WEIGHT columns
        cols.append( [["Value"], *[formatting.ScoreWidget( score ) for score in scores]] )
        cols.append( [["Grade"], *[formatting.GradeMarker( score ) for score in scores]] )
        cols.append( [["Weight"], *[formatting.WeightWidget( score.column.weight ) for score in scores]] )
        ren_table: forms.TRenderable = forms.Table( cols = cols,
                                                    row_headers = num_row_headers,
                                                    col_headers = 1 )
        return ren_table, all_scores, scores

    def __scores_order( self, score: _alacat.Score ):
        if isinstance( score.subject, _alacat.ComesFromProtein ):
            protein_str = score.subject.protein.accession
        else:
            protein_str = ""

        grade_int = { _alacat.EGrade.PASS : 1,
                      _alacat.EGrade.FAIL : 0,
                      _alacat.EGrade.INFO : 2,
                      _alacat.EGrade.ERROR: 3 }[score.grade]

        subject_str = score.subject.accession

        return protein_str, grade_int, subject_str

    def __get_sequence( self ) -> forms.TRenderable:
        if not widgets.SequenceWidget.can_handle( self.subject ):
            return None

        return forms.DynamicLoader(
                url = AjaxSequenceScreen.get_url( self.protocol,
                                                  **{ RESULTS_FIELD_ID: self.get_route_arg( RESULTS_FIELD_ID ),
                                                      SUBJECT_FIELD_ID: self.get_route_arg( SUBJECT_FIELD_ID ) } ),
                section = "Sequence" )

    def get_id_report( self ):
        """
        Creates the section that describes the IDs of the entity.
        """
        subject: _alacat.Entity = self.subject

        mro = []

        for klass in subject.__class__.mro():
            mro.append( sys.modules[klass.__module__].__name__ + "." + klass.__qualname__ )

        rows = []
        rows.append( ["class", forms.UnorderedList( mro )] )
        rows.append( ["accession", subject.accession] )
        rows.append( ["dbdata::json", subject.to_dbdata().to_json()] )
        rows.append( ["dbdata::shash", subject.to_dbdata().to_shash()] )

        hlp = forms.RestructuredText( """
        This section shows the identifiers used internally.
        
        You can copy and paste these if you wish to reuse parts of your workflow in the Python API.
        To use existing entities in new workflow in the GUI, it is easier to use the cart instead.
        
        class
            The Python class of the entity.
            
        accession
            The ID of this entity.
            This can be used to retrieve the entity from the current pipeline.
            
        dbdata::json
            The JSON code for the entity.
            This can be used order to pass the entity into a new pipeline or to identify the entity in any pipeline. 
            
        dbdata::shash
            The hash of the JSON code.
            This can be used in order to identify the entity in any pipeline.
        """ )

        return forms.Section( title = "Identifiers",
                              content = [forms.Table( rows = rows, row_headers = 1 )],
                              collapsed = True,
                              help = hlp )

    def make_composition_table( self, rows ) -> formatting.CompositionTable:
        return formatting.CompositionTable( self.get_gui_model(), rows, False )

    def list_tables( self, gm, handler ) -> Optional[forms.TRenderable]:
        tables = gm.pipeline.find_tables( handler )

        if not tables:
            return None

        ts = []
        table: _alacat.ScoresTable
        for table, is_first, is_last in array_helper.when_first_or_last( tables ):
            if not is_first:
                if is_last:
                    ts.append( " and " )
                else:
                    ts.append( ", " )

            ts.append( forms.Html( "<a href='{}'>{}</a>".format(
                    scr_scores_table.ScoresTableScreen.get_url( self.protocol,
                                                                **{ RESULTS_FIELD_ID: gm.id,
                                                                    "table"         : table.t_subject.__name__ } ),
                    forms.escape( formatting.get_table_name( gm.pipeline, table ) ) ) ) )
        return ts


class AjaxSequenceScreen( screens.BasicScreen, formatting.IModelScreen ):
    __view__ = screens.ScreenData( url = f"/ajax/sequence/<{RESULTS_FIELD_ID}>/<subject>" )

    def on_call( self ):
        subject = self.get_subject()
        

        if isinstance( subject, _alacat.AminoAcidSequence ):
            fasta_ = forms.escape( f">{subject.accession}\n{subject.sequence}" )
            
            if isinstance(subject, _alacat.Peptide):
                protein_fasta_ = forms.escape( f">{subject.protein.accession}\n{subject.protein.sequence}" )
                
                gui_fasta = forms.TileGrid( forms.ETileGrid.ACTION_TILES,
                                     [   forms.ButtonTile(
                                                text = "Protein FASTA",
                                                exhtml = f"{forms.Data.hatmul_data_clipboard}='{protein_fasta_}'",
                                                icon = formatting.AlacatResources.DOWNLOAD ),
                                            forms.ButtonTile(
                                                text = "Peptide FASTA",
                                                exhtml = f"{forms.Data.hatmul_data_clipboard}='{fasta_}'",
                                                icon = formatting.AlacatResources.DOWNLOAD )])
            else:
                gui_fasta = forms.TileGrid( forms.ETileGrid.ACTION_TILES,
                                        forms.ButtonTile(
                                                text = "Copy FASTA",
                                                exhtml = f"{forms.Data.hatmul_data_clipboard}='{fasta_}'",
                                                icon = formatting.AlacatResources.DOWNLOAD ) )
        else:
            gui_fasta = None

        gui_sequence = widgets.SequenceWidget( subject )
        gui_area = forms.ScrollArea( gui_sequence )

        return forms.Region( (gui_area, gui_fasta) )
