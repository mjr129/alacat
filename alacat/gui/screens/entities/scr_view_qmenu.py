from typing import List

from .scr_view_entity_base import ViewEntityBaseScreen, CompositionRow
from hatmul import screens, forms
import alacat
from alacat.gui import formatting
from alacat.gui.gui_pipelines import RESULTS_FIELD_ID

class ViewQmenuScreen( ViewEntityBaseScreen ):
    """
    Online viewer for `Qmenu`.
    
    No form is supplied to select the results set or entity, these parameters must be supplied by the caller.
    """
    __view__ = screens.ScreenData(
            title = "View Qmenu",
            url = f"/<{RESULTS_FIELD_ID}>/qmenus/<subject>",
            record = alacat.Qmenu,
            field_id = "subject",
            request_format = lambda x: x.accession,
            index=screens.EIndex.TERTIARY,
            folder = "Entities",
    )
    
    
    def get_introduction( self ) -> forms.TRenderable:
        menu: alacat.Qmenu = self.subject
        num_qconcats = len( menu.qconcats )
        num_qbricks = sum( 1 for qconcat in menu.qconcats for _ in qconcat.qbricks )
        num_peptides = sum( 1 for qconcat in menu.qconcats for qbrick in qconcat.qbricks for _ in qbrick.peptides )
        num_proteins = len( set( qbrick.protein for qconcat in menu.qconcats for qbrick in qconcat.qbricks ) )
        
        return [forms.Paragraph(
                ["This qmenu comprises ",
                 formatting.Code( num_qconcats ),
                 " qconcats, ",
                 formatting.Code( num_qbricks ),
                 " qbricks, ",
                 formatting.Code( num_peptides ),
                 " peptides or ",
                 formatting.Code( num_proteins ),
                 " proteins."] )
            ]
    
    
    def get_composition( self ):
        menu = self.subject
        rows: List[CompositionRow] = []
        
        for qconcat in menu.qconcats:
            for qbrick in qconcat.qbricks:
                for qpeptide in qbrick.linked_peptides:
                    rows.append( CompositionRow( None, qconcat, qbrick, qpeptide ) )
        
        return self.make_composition_table( rows ).to_table()
