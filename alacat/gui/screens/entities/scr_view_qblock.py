from typing import List

from .scr_view_entity_base import ViewEntityBaseScreen, CompositionRow
from hatmul import screens, forms
import alacat
from alacat.gui import formatting
from alacat.gui.gui_pipelines import RESULTS_FIELD_ID

class ViewQblockScreen( ViewEntityBaseScreen ):
    """
    Online viewer for `Qblock`.
    
    No form is supplied to select the results set or entity, these parameters must be supplied by the caller.
    """
    __view__ = screens.ScreenData(
            title = "View Qblock",
            url = f"/<{RESULTS_FIELD_ID}>/qblocks/<subject>",
            record = alacat.Qblock,
            field_id = "subject",
            request_format = lambda x: x.accession ,
            index=screens.EIndex.TERTIARY,
    folder = "Entities")
    
    
    def get_introduction( self ) -> forms.TRenderable:
        qblock: alacat.Qblock = self.subject
        
        return ["Qblock for protein ",
                formatting.EntityLink( qblock.protein ),
                ", comprising ",
                formatting.Code( qblock.qbricks.__len__() ),
                " qbricks."]
    
    
    def get_composition( self ) -> forms.TRenderable:
        qblock: alacat.Qblock = self.subject
        rows: List[CompositionRow] = []
        
        for qbrick in qblock.qbricks:
            for lp in qbrick.linked_peptides:
                rows.append( CompositionRow( None, None, qbrick, lp ) )
        
        return self.make_composition_table( rows ).to_table()
