from .scr_view_boundcolumn import ViewColumnScreen
from .scr_view_handler import ViewHandlerScreen 
from .scr_view_peptide import ViewPeptideScreen 
from .scr_view_protein import ViewProteinScreen 
from .scr_view_qblock import ViewQblockScreen 
from .scr_view_qbrick import ViewQbrickScreen 
from .scr_view_qconcat import ViewQconcatScreen 
from .scr_view_qmenu import ViewQmenuScreen
from .scr_view_linked_peptide import ViewLinkedPeptideScreen 
from .scr_view_text import ViewTextScreen