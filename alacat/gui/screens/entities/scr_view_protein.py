from typing import List

from .scr_view_entity_base import ViewEntityBaseScreen
from hatmul import screens, forms
import alacat
from alacat.gui import formatting
from alacat.gui.screens.scr_compare import ComparePeptidesScreen
from mhelper import array_helper
from alacat.gui.gui_pipelines import RESULTS_FIELD_ID, SUBJECT_FIELD_ID


class ViewProteinScreen( ViewEntityBaseScreen ):
    """
    Online viewer for `Protein`.
    
    No form is supplied to select the results set or entity, these parameters must be supplied by the caller.
    """
    __view__ = screens.ScreenData(
            title = "View Protein",
            url = f"/<{RESULTS_FIELD_ID}>/proteins/<subject>",
            record = alacat.Protein,
            field_id = "subject",
            request_format = lambda x: x.accession ,
            index=screens.EIndex.TERTIARY,
    folder = "Entities")

    def get_introduction( self ) -> forms.TRenderable:
        gm = self.get_gui_model()
        protein: alacat.Protein = self.subject

        n_peptides = sum( x.protein is protein for x in gm.pipeline.candidate_peptides )
        s_peptides = sum( x.protein is protein for x in gm.pipeline.selected_peptides )
        n_qbricks = sum( x.protein is protein for x in gm.pipeline.candidate_qblocks )
        s_qbricks = sum( x.protein is protein for x in gm.pipeline.selected_qblocks )

        msg = [[formatting.Code( n_peptides ),
                " peptides were evaluated for this protein, out of which ",
                formatting.Code( s_peptides ),
                " were selected for further processing into qbricks."],
               [formatting.Code( n_qbricks ),
                " qblock permutations were evaluated for these peptides, out of which ",
                formatting.Code( s_qbricks ),
                " were selected for further processing into qconcats."]]

        return [formatting.Code( protein.title ),
                " in ",
                formatting.TaxonWidget( protein.organism ),
                forms.UnorderedList( msg )]

    def get_composition( self ) -> forms.TRenderable:
        return None

    def get_extra( self ) -> forms.TRenderable:
        gm = self.get_gui_model()
        return [self.add_peptide_listing( gm.pipeline.peptide_scores ),
                self.add_peptide_listing( gm.pipeline.qblock_scores )]

    def add_peptide_listing( self, fst: alacat.ScoresTable[alacat.ComesFromProtein] ):
        MAX_DISPLAY = 50

        gm = self.get_gui_model()
        protein: alacat.Protein = self.subject
        klass_ = type( fst.subjects[0] )
        klass = klass_.__name__

        cols: List[List[forms.TRenderable]] = []

        mine_ = [subject for subject in fst.subject_order if subject.protein is protein]
        mine = mine_[:MAX_DISPLAY]
        subs: List[alacat.EntitySummary] = [gm.pipeline.summarise( subject ) for subject in mine]

        cols.append( ["Compare",
                      *[forms.Html( f'<input name="{SUBJECT_FIELD_ID}" value="{subject.accession}" type="checkbox"/>' )
                        for subject in mine]] )

        cols.append( [klass,
                      *[formatting.EntityLink( subject, subject.sequence if isinstance( subject, alacat.AminoAcidSequence ) else subject.accession )
                        for subject in mine]] )

        cols.append( ["Rank",
                      *[[formatting.SelectionRank( sum )]
                        for sum in subs]] )

        rows = array_helper.transpose( cols )

        form = forms.Form( method = "post",
                           submit_caption = "Compare selected",
                           submit_image = formatting.AlacatResources.COMPARE,
                           action = ComparePeptidesScreen.get_url( self.protocol, **{ RESULTS_FIELD_ID: gm.id } ) )
        form.append( forms.Paragraph( "Showing {:,} out of {:,} {}s".format( len( mine ), len( mine_ ), klass ) ) )
        form.append( forms.Table( rows = rows, col_headers = 1, vscroll = False ) )
        return forms.Section( f"{klass}s",
                              form,
                              collapsed = True,
                              help = f"The {klass}s are sorted by rank, which is derived from how many successful tests they passed. This is affected by the metrics assessed, the assessments on these metrics and the priorities assigned to these assessments. Some metrics take into account the other competing {klass}s, and so will vary depending on the context." )
