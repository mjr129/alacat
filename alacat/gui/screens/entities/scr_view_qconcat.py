from typing import List

from .scr_view_entity_base import ViewEntityBaseScreen, CompositionRow
from hatmul import screens, forms
import alacat  as _alacat
from alacat.gui import formatting
from alacat.gui.gui_pipelines import RESULTS_FIELD_ID

class ViewQconcatScreen( ViewEntityBaseScreen ):
    """
    Online viewer for `Qconcat`.
    
    No form is supplied to select the results set or entity, these parameters must be supplied by the caller.
    """
    __view__ = screens.ScreenData(
            title = "View Qconcat",
            url = f"/<{RESULTS_FIELD_ID}>/qconcats/<subject>",
            record = _alacat.Qconcat,
            field_id = "subject",
            request_format = lambda x: x.accession,
            index=screens.EIndex.TERTIARY ,
    folder = "Entities")
    
    
    def get_introduction( self ) -> forms.TRenderable:
        qconcat: _alacat.Qconcat = self.subject
        
        num_qbricks = len( qconcat.qbricks )
        num_peptides = sum( len( qbrick.peptides ) for qbrick in qconcat.qbricks )
        num_proteins = len( set( qbrick.protein for qbrick in qconcat.qbricks ) )
        
        return [f"This qconcat comprises ",
                formatting.Code( num_qbricks ),
                " qbricks, ",
                formatting.Code( num_peptides ),
                " peptides or ",
                formatting.Code( num_proteins ),
                " proteins."
                ]
    
    
    def get_composition( self ) -> forms.TRenderable:
        qconcat: _alacat.Qconcat = self.subject
        rows: List[CompositionRow] = []
        
        for qbrick in qconcat.qbricks:
            for linked_peptide in qbrick.linked_peptides:
                rows.append( CompositionRow( None, qconcat, qbrick, linked_peptide ) )
        
        return self.make_composition_table( rows ).to_table()
