from typing import List

from .scr_view_entity_base import ViewEntityBaseScreen, CompositionRow
from hatmul import screens, forms
import alacat
from alacat.gui import formatting
from alacat.gui.gui_pipelines import RESULTS_FIELD_ID

class ViewQbrickScreen( ViewEntityBaseScreen ):
    """
    Online viewer for `Qbrick`.
    
    No form is supplied to select the results set or entity, these parameters must be supplied by the caller.
    """
    __view__ = screens.ScreenData(
            title = "View Qbrick",
            url = f"/<{RESULTS_FIELD_ID}>/qbricks/<subject>",
            record = alacat.Qbrick,
            field_id = "subject",
            request_format = lambda x: x.accession,
            index=screens.EIndex.TERTIARY ,
    folder = "Entities"                   )
    
    
    def get_introduction( self ) -> forms.TRenderable:
        qbrick: alacat.Qbrick = self.subject
        
        return ["Qbrick for protein ",
                formatting.EntityLink( qbrick.protein, qbrick.protein.accession ),
                ", comprising ",
                formatting.Code( len( qbrick.peptides ) ),
                " peptides."]
    
    
    def get_composition( self ) -> forms.TRenderable:
        qbrick: alacat.Qbrick = self.subject
        rows: List[CompositionRow] = []
        
        for linked_peptide in qbrick.linked_peptides:
            rows.append( CompositionRow( None, None, qbrick, linked_peptide ) )
        
        return self.make_composition_table( rows ).to_table()
