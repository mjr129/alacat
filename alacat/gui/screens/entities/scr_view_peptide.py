from .scr_view_entity_base import ViewEntityBaseScreen
from hatmul import screens, forms
import alacat
from alacat.gui import formatting
from alacat.gui.gui_pipelines import RESULTS_FIELD_ID

class ViewPeptideScreen( ViewEntityBaseScreen ):
    """
    Online viewer for `Peptide`.
    
    No form is supplied to select the results set or entity, these parameters must be supplied by the caller.
    """
    __view__ = screens.ScreenData(
            title = "View Peptide",
            url = f"/<{RESULTS_FIELD_ID}>/peptides/<subject>",
            record = alacat.Peptide,
            field_id = "subject",
            request_format = lambda x: x.accession ,
            index=screens.EIndex.TERTIARY,
    folder = "Entities")
    
    
    def get_introduction( self ) -> forms.TRenderable:
        peptide: alacat.Peptide = self.subject
        
        n = peptide.get_flank( alacat.ETerm.N )
        c = peptide.get_flank( alacat.ETerm.C )
        pep_f = forms.Html( ["<span class='alacat_peptide_line'>",
                             f"<span style='opacity: 50%'>",
                             forms.escape( n ),
                             "✂</span><span>",
                             forms.escape( peptide.sequence ),
                             "</span><span style='opacity: 50%'>✂",
                             forms.escape( c ),
                             "</span>",
                             "</span>"] )
        
        return [forms.Paragraph( pep_f ), forms.Paragraph( [formatting.Code( peptide.sequence.__len__() ),
                                                            " sites in protein ",
                                                            formatting.EntityLink( peptide.protein )] )]
    
    
    def get_composition( self ) -> forms.TRenderable:
        return None
