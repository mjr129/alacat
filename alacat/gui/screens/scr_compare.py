from typing import Iterable, Sequence

import alacat as ALACAT
from hatmul import forms, screens
from mhelper import string_helper

from alacat.gui import gui_pipelines, formatting
from alacat.gui.formatting import EntityLink
from alacat.gui.gui_pipelines import RESULTS_FIELD_ID, SUBJECT_FIELD_ID


class EView:
    MODEL = "model"
    QBRICKS = "qbricks"
    PROTEINS = "proteins"
    PIPELINE = "pipeline"
    REPORT = "report"
    INDEX = "index"


class ComparePeptidesScreen( screens.BasicScreen, formatting.IModelScreen ):
    """
    Compares the results from two or more entities.
    
    No form is supplied to select the results set or entities, these parameters must be supplied by the caller.
    """
    __view__ = screens.ScreenData(
            url = f"/<{RESULTS_FIELD_ID}>/compare",
            method = "get",
            index = screens.EIndex.TERTIARY,
            folder = "Results" )


    def on_call( self ) -> "forms.Page":
        #
        # Read form
        #
        compare_strs = self.request.post_data.getl( SUBJECT_FIELD_ID )

        if not compare_strs:
            raise forms.InputError( "At least one entity must be selected.", widget = SUBJECT_FIELD_ID )

        #
        # Load model
        #
        gui_model: gui_pipelines.GuiModel = self.get_gui_model()
        pipeline: ALACAT.Pipeline = gui_model.pipeline

        #
        # Get entities
        #
        entities = []

        for compare_str in compare_strs:
            ent = pipeline.find( compare_str )

            if ent is None:
                raise forms.InputError( f"Cannot find the entity with the accession {compare_str!r}.", widget = SUBJECT_FIELD_ID )

            entities.append( ent )

        if not entities:
            raise forms.InputError( "No entities resolved.", widget = SUBJECT_FIELD_ID )

        #
        # Find table
        #
        table = pipeline.find_table( entities )

        if not table:
            raise forms.InputError( "These entities do not share a common scores table.", widget = "compare" )

        #
        # Get scores for entities
        #
        score_lists = [table.by_subject[ent] for ent in entities]
        keys = { key for score_list in score_lists for key in score_list }
        differences = { key for key in keys if len( set( score_list[key].grade for score_list in score_lists ) ) != 1 }
        all_others = keys - differences
        is_multi = len( entities ) > 1

        #
        # Format report
        #
        report = forms.Report( [formatting.ModelLink( gui_model.id ), f"Compare {len( entities )} entities"] )

        # Main section
        # - list of selected entities
        section = forms.Section( "" )
        report.append( section )

        lst = [["Entity", "Rank"]]

        for peptide in entities:
            summary = pipeline.summarise( peptide )
            lst.append( [EntityLink( peptide ), formatting.SelectionRank( summary )] )

        section.append( forms.Table( lst, col_headers = 1 ) )

        # Differences section
        # - table of differences
        report.append( _make_scores_table( table, entities, differences, "Differences" ) )

        if is_multi and not differences:
            report.append( forms.Section( "Differences", "No differences." ) )

        # Similarities sections
        # - tables of common pass/fail/info/error grades, in turn
        for symbol in ALACAT.EGrade:
            matching = [key for key in all_others if score_lists[0][key].grade == symbol]

            if is_multi:
                name = "Common " + symbol.name.lower()
            else:
                name = string_helper.capitalise_first( symbol.name, True )

            report.append( _make_scores_table( table, entities, matching, name ) )

        return forms.Page( content = report )


def _make_scores_table( table: ALACAT.ScoresTable,
                        entities: Sequence[ALACAT.Entity],
                        keys: Iterable[ALACAT.Column],
                        title: str ):
    if not keys:
        return

    score_lists = [table.by_subject[entity] for entity in entities]

    r = []

    r.append( ["Column", "Rule", "Weight", *(EntityLink( entity ) for entity in entities)] )

    keys = sorted( keys, key = lambda x: x.accession )

    for key in keys:
        r.append( [EntityLink( key, key.accession ),
                   formatting.RuleWidget( key.assessor ),
                   formatting.WeightWidget( key.weight ),
                   *(formatting.ScoreWidget( score_list[key] ) for score_list in score_lists)] )

    t1 = forms.Table( r, section = title, col_headers = 1, row_headers = 1 )
    return t1
