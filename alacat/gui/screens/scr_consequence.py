import alacat
from hatmul import screens, forms
from .scr_main import MainScreenBase, AlacatMainScreen


class ConsequenceScreen( MainScreenBase ):
    """
    Makes Alacat look like Consequence.
    """
    __view__ = screens.ScreenData(
            url = "/consequence",
            method = "post",
            index = screens.EIndex.SECONDARY,
    folder="New")
    
    
    class __form__:
        __region__ = forms.Html
        
        protein = forms.TextAreaField( "FASTA", rows = 10 )
    
    
    def on_create_form( self ) -> "forms.Form":
        form = super().on_create_form()
        form.append( forms.AutoTitle( form_help = intro ) )
        return form
    
    
    def on_parse_form( self ) -> forms.TRenderable:
        fasta = self.__form__.protein.read_text( self.request )
        
        model = alacat.Model( name = "ConsequenceMode",
                              organisms = (),
                              digestion = alacat.EDigestion.TRYPSIN,
                              handlers = alacat.HandlerFactoryCollection.create_defaults(),
                              proteins = (fasta,),
                              peptides = (),
                              qblocks = (),
                              qmenus = (),
                              mandate = True )
        
        return self.begin_processing( model )


# noinspection SpellCheckingInspection
example_protein = """>A6NKT7
MSCSKAYGERYVASVQGSAPSPRKKSTRGFYFAKLYYEAKEYDLAKKYICTYINVREMDP
RAHRFLGLLYELEENTEKAVECYRRSVELNPTQKDLVLKIAELLCKNDVTDGRAEYWVER
AAKLFPGSPAIYKLKEQLLDCEGEDGWNKLFDLIQSELYVRPDDVHVNIRLVELYRSTKR
LKDAVARCHEAERNIALRSSLEWNSCVVQTLKEYLESLQCLESDKSDWRATNTDLLLAYA
NLMLLTLSTRDVQESRELLESFDSALQSAKSSLGGNDELSATFLEMKGHFYMHAGSLLLK
MGQHGNNVQWRALSELAALCYLIAFQVPRPKIKLIKGEAGQNLLEMMACDRLSQSGHMLL
NLSRGKQDFLKVVVETFANKSGQSALYDALFSSQSPKDTSFLGSDDIGNIDVQEPELEDL
ARYDVGAIRAHNGSLQHLTWLGLQWNSLPALPGIRKWLKQLFHHLPQETSRLETNAPESI
CILDLEVFLLGVVYTSHLQLKEKCNSHHSSYQPLCLPLPVCKQLCTERQKSWWDAVCTLI
HRKAVPGNSAKLRLLVQHEINTLRAQEKHGLQPALLVHWAKCLQKMGSGLNSFYDQREYI
GRSVHYWKKVLPLLKIIKKKNSIPEPIDPLFKHFHSVDIQASEIVEYEEDAHVTFAILDA
VNGNIEDAMTAFESIKSVVSYWNLALIFHRKAEDIANDALSPEEQEECKNYLRKTRGYLI
KILDDSDSNLSVVKKLPVPLESVKEMLKSVMQELENYSEGGPLYKNGSLRNADSEIKHST
PSPTKYSLSPSKSYKYSPKTPPRWAEDQNSLLKMIRQEVKAIKEEMQELKLNSSKSASHH
RWPTENYGPDSVPDGYQGSQTFHGAPLTVATTGPSVYYSQSPAYNSQYLLRPAANVTPTK
GSSNTEFKSTKEGFSIPVSADGFKFGISEPGNQEKKSEKPLENDTGLQAQDISGRKKGRG
VIFGQTSSTFTFADVAKSTSGEGFQFGKKDLNFKGFSGAGEKLFSSQYGKMANKANTSGD
FEKDDDAYKTEDSDDIHFEPVVQMPEKVELVTGEEGEKVLYSQGVKLFRFDAEVSQWKER
GLGNLKILKNEVNGKVRMLMQREQVLKVCANHWITTTMNLKPLSGSDRAWMWSASDFSDG
DAKLERLAAKFKTPELAEEFKQKFEECQRLLLDIPLQTPHKLVDTGRAAKLIQRAEEMKS
GLKDFKTFLTNDQTKVAEEENKGSGTGAAGASDTTIKPNAENTGPTLEWDNYDLREDALD
DSVSSSSVHASPLASSPVRKNLFHFDESTTGSNFSFKSALSLSKSPAKLNQSGTSVGTDE
ESDVTQEEERDGQYFEPVVPLPDLVEVSSGEENEQVVFSHRAEFYRYDKDVGQWKERGIG
DIKILQNYDNKHVRILMRRDQVLKLCANHRITPDMSLQNMKGTERVWVWTACDFADGERK
VEHLAVRFKLQDVADSFKKIFDEAKTAQEKDSLITPHVSRSSTPRESPCGKIAVAVLEET
TRERTDVIQGDDVADAASEVEVSSTSETTTKAVVSPPKFVFGSESVKRIFSSEKSKPFAF
GNSSATGSLFGFSFNAPLKSNNSETSSVAQSGSESKVEPKKCELSKNSDIEQSSDSKVKN
LSASFPTEESSINYTFKTPEKEPPLWYAEFTKEELVQKLSSTTKSADHLNGLLREIEATN
AVLMEQIKLLKSEIRRLERNQEQEVSAANVEHLKNVLLQFIFLKPGSERERLLPVINTML
QLSLEEKGKLAAVAQGEE"""

intro = forms.Html( forms.make_renderable(
        """Simple evaluation of peptides.
    
        This interface runs exactly the same routine as {}, but looks
        more like <a href='http://king.smith.man.ac.uk/CONSeQuence/'>Consequence</a>.
        
        Paste your own protein sequence(s) in Fasta format or {}.""",
        forms.Link( forms.PageUrl( AlacatMainScreen ), alacat.__title__ ),
        forms.PostLink( "load some example data",
                        forms.PageUrl( ConsequenceScreen ),
                        { "protein"         : example_protein,
                          forms.SUBMIT_FIELD: "0" } ) ) )
