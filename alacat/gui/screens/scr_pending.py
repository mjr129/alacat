from typing import Optional

from mhelper.bg_task_helper import TaskQuery
from hatmul import screens, forms
from alacat.gui.gui_pipelines import RESULTS_FIELD_ID
from alacat.gui import gui_pipelines
from mhelper import io_helper


class PendingScreen( screens.TaskScreen ):
    __view__ = screens.ScreenData( url = "/pending",
                                   scenario = screens.EScenario.TASK,
                                   index = screens.EIndex.TERTIARY,
                                   folder = "Special" )


    def on_get_intro_text( self, task: TaskQuery ) -> forms.TRenderable:
        # Get the task ID from the submission
        # (it should be the first argument to the function of the task).
        arg0 : Optional[screens.LongTaskScreen.ExecutionData] = next( iter( task.request.arguments ), None )

        if isinstance( arg0, screens.LongTaskScreen.ExecutionData ):
            results_id = arg0.results_id
            log_url = ViewLogScreen.get_url( self.protocol, **{ RESULTS_FIELD_ID: results_id } )
        else:
            # Don't mess up if we weren't able to get the task ID
            log_url = ""

        text = ("This page tracks the progress of your task. "
                "You can bookmark it and return later. "
                "Anyone with the link can view the results. "
                f"<a href={log_url}>You can check the execution logs here</a>. "
                f"Execution time varies from a couple of seconds for a protein which has been seen before, "
                f"to several hours if a new or updated proteome requires downloading.")

        return forms.HelpBlock( forms.Html( text ) ), forms.Html( "<br/>" ), super().on_get_intro_text( task )


class ViewLogScreen( screens.BasicScreen ):
    """
    Shows the logs recorded during pipeline execution.
    
    This page can be viewed during the pipeline execution itself, though
    requires a manual refresh in order to update.
    """
    __view__ = screens.ScreenData( url = f"/<{RESULTS_FIELD_ID}>/log",
                                   index = screens.EIndex.TERTIARY,
                                   folder = "Results" )


    def on_call( self ):
        results_id: str = self.get_route_arg( RESULTS_FIELD_ID )
        results: Optional[gui_pipelines.AlacatResultsStore.ResultsSet] = gui_pipelines.get_store().get_results( results_id )
        creation_log_fp: str = results.get_creation_log_fp()

        log_text = io_helper.read_all_text( creation_log_fp, default = "" )

        return forms.Page( "", forms.Report( "", forms.Section( "", forms.ReadOnlyTextArea( log_text, rows = 20 ) ) ) )
