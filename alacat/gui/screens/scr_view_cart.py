import json
from collections import Counter
from dataclasses import dataclass
from itertools import chain
from typing import List, Sequence, Set

import alacat
from hatmul import forms, screens

from alacat.gui import formatting, gui_pipelines
from alacat.gui.screens.scr_compare import ComparePeptidesScreen
from alacat.gui.gui_pipelines import RESULTS_FIELD_ID, SUBJECT_FIELD_ID


class _Fake( alacat.Entity ):
    """
    Silly object used to give us the "flag" symbol without having to specify
    an actual object or mirror existing code. Note that JS-side a blank 
    accession is rejected without warning, so the user can't actually click the
    flag.
    """

    def to_dbdata( self ) -> alacat.DbData:
        return alacat.DbData()

    @property
    def accession( self ) -> str:
        return ""


class ViewCartScreen( screens.BasicScreen, formatting.IModelScreen ):
    """
    Allows the user to view the flagged items and create a new model from them
    if desired.
    
    The legacy name "cart" is sometimes used, which followed Uniprot's "protein
    cart" terminology, but that makes it sound like the user is buying
    something, and they're not.
    """
    __view__ = screens.ScreenData(
            url = f"/<{RESULTS_FIELD_ID}>/cart",
            index = screens.EIndex.TERTIARY,
            folder = "Results"
    )

    def on_call( self ) -> "forms.Page":
        gui_model: gui_pipelines.GuiModel = self.get_gui_model()
        report = forms.Report( "Flags" )
        compare_url: str = ComparePeptidesScreen.get_url( self.protocol,
                                                          **{ RESULTS_FIELD_ID: gui_model.id } )
        checkout_url = CheckoutNewScreen.get_url( self.protocol,
                                                  **{ RESULTS_FIELD_ID: gui_model.id } )
        submit_buttons = [forms.ButtonTile( text = "Compare",
                                            formaction = compare_url,
                                            element = forms.ButtonTile.EElement.SUBMIT,

                                            icon = formatting.AlacatResources.COMPARE,
                                            hover_text = "Compare the selected entities.",
                                            ),
                          forms.ButtonTile( text = "New query",
                                            formaction = checkout_url,
                                            element = forms.ButtonTile.EElement.SUBMIT,
                                            icon = formatting.AlacatResources.IN_BASKET,
                                            hover_text = "Create a new query, detailed below." )]

        form = forms.Form( method = "post",
                           content = [self.__make_table( gui_model )],
                           submit_button = submit_buttons )
        report.append( forms.Section( title = "",
                                      content = [forms.HelpBlock(
                                              "You can flag items to bookmark them for later use. Once you have flagged some items they will appear on this page. You can inspect or compare the flagged items, or create a whole new query using just these items." ),
                                          form] ) )
        return forms.Page( "Flags", report )

    def __make_table( self, gui_model: gui_pipelines.GuiModel ) -> forms.TRenderable:
        """
        Makes a table of the cart entities.
        
        The entities are tagged with `SUBJECT_FIELD_ID`. This allows them to be
        used both on the `ComparePeptidesScreen` and the `CheckoutNewScreen`.
        (The results ID is a routing parameter and isn't included in the form).
        """
        if not gui_model.cart.entities:
            return [forms.Paragraph( forms.HelpBlock( [forms.Html( "Hover over objects with the mouse to show the flag button. "
                                                                   "Try it here: " ),
                                                       formatting.EntityLink( gui_model.pipeline.selected_menus[0] ),
                                                       ". Refresh the page to see your changes!"] ) )]

        rows = [["Class", "Count", "ID", "Compare"]]

        ss = [gui_model.pipeline.find( entity_id ) for entity_id in gui_model.cart.entities]
        ss = list( sorted( ss, key = lambda x: f"{x.__class__.__name__}:{x.accession}" ) )
        cnt = Counter( [entity.__class__.__name__ for entity in ss] )
        lcn = None
        for entity in ss:
            cn = entity.__class__.__name__

            if cn != lcn:
                dcn = cn
                count = cnt[cn]
            else:
                dcn = ""
                count = ""

            accession = forms.escape( entity.accession )

            rows.append( [dcn,
                          str( count ),
                          formatting.EntityLink( entity, entity.accession ),
                          forms.Html( f'<input type="checkbox" name="{SUBJECT_FIELD_ID}" value="{accession}"/>' )
                          ] )

            lcn = cn

        tbl = forms.Table( rows = rows, col_headers = 1 )
        return tbl


class CheckoutNewScreen( screens.BasicScreen, formatting.IModelScreen ):
    """
    Handles creating a new model from the cart.
    """
    __view__ = screens.ScreenData(
            url = f"/<{RESULTS_FIELD_ID}>/checkout"
    )

    def on_call( self ):
        gui_model: gui_pipelines.GuiModel = self.get_gui_model()
        selected_ids: Sequence[str] = self.request.post_data.getl( SUBJECT_FIELD_ID )
        proposed: _NewFromCart = _NewFromCart.create_parameters( gui_model, set( selected_ids ) )

        return forms.Page(
                "", forms.Report(
                        "", [forms.Section( "", self.__make_table( proposed ) ),
                             forms.Section( "Explanation", [
                                 forms.HelpBlock( "This section describes the logic used to create the above query." ),
                                 forms.UnorderedList( proposed.info )],
                                            collapsed = True )] ) )

    def __make_table( self, params: "_NewFromCart" ):
        from alacat.gui.screens.scr_main import AlacatMainScreen, _Controls
        pid = _Controls.protein.field_id
        eid = _Controls.peptides.field_id
        bid = _Controls.qblocks.field_id
        mid = _Controls.qmenus.field_id

        mod_rows = []

        tx = """<textarea wrap="off" rows="4" cols="80">{1}</textarea><input type="hidden" name="{0}" value="{1}" />"""
        mod_rows.append( ["Proteins", forms.Html( tx.format( pid, forms.escape( params.proteins ) ) )] )
        mod_rows.append( ["Peptides", forms.Html( tx.format( eid, forms.escape( params.peptides ) ) )] )
        mod_rows.append( ["Qblocks", forms.Html( tx.format( bid, forms.escape( params.qblocks ) ) )] )
        mod_rows.append( ["Menus", forms.Html( tx.format( mid, forms.escape( params.menus ) ) )] )
        mod_rows.append( forms.Html( f'''<input type="hidden" name="{forms.SUBMIT_FIELD}" value="0" />''' ) )

        return forms.Form(
                help = "This is a preview of the query that will be created by the 'Checkout' button. Your entry will be represented in JSON, regardless of how you entered it before, but this won't affect the results! Open the 'Explanation' section below to see how this query was generated.",
                method = "post",
                action = AlacatMainScreen.get_url( self.protocol ),
                content = forms.Table( rows = mod_rows, row_headers = 1, vscroll = False, js = False ),
                submit_button = forms.ButtonTile( element = forms.ButtonTile.EElement.SUBMIT,
                                                  text = "OK",
                                                  icon = forms.Resources.ACCEPT_BUTTON_ICON,
                                                  css_class = forms.ButtonTile.CssClasses.BUTTON_TILE ) )


@dataclass
class _NewFromCart:
    """
    Set of parameters used to create a new model from the cart.
    """
    gui_model: gui_pipelines.GuiModel
    proteins: str
    peptides: str
    qblocks: str
    menus: str
    none_unused: bool
    info: List[forms.TRenderable]

    @staticmethod
    def create_parameters( gm: gui_pipelines.GuiModel,
                           selected_ids: Set[str],
                           ) -> "_NewFromCart":
        messages = []
        el = lambda x: formatting.EntityLink( x, x.accession )
        n = lambda x: formatting.Code( len( x ) )

        # Get selected entities
        if not selected_ids:
            selected_ids = gm.cart.entities

        entities = [gm.pipeline.find( entity_id ) for entity_id in selected_ids]

        # If the user has specified x, use them, otherwise use the original x

        # Proteins
        proteins: List[alacat.Protein] = [x for x in entities if isinstance( x, alacat.Protein )]

        if proteins:
            messages.append( ["Using the ", n( proteins ), " proteins in the cart"] )
        else:
            proteins = list( gm.pipeline.proteins )

            if proteins:
                messages.append( ["No proteins in cart - using the ", n( proteins ), " from the original query"] )
            else:
                messages.append( ["No proteins in cart and none in the original query - leaving system to decide"] )

        # Peptides
        all_peptides: List[alacat.Peptide] = []

        for protein in proteins:
            peptides = [x for x in entities if isinstance( x, alacat.Peptide ) and x.protein is protein]

            if peptides:
                messages.append( ["Using the ",
                                  formatting.Code( len( peptides ) ),
                                  " ",
                                  el( protein ),
                                  " peptides in the cart"] )
            else:
                peptides = [x for x in gm.pipeline.favoured_peptides if x.protein is protein]

                if peptides:
                    messages.append( [f"No ",
                                      el( protein ),
                                      " peptides in cart - using the ",
                                      n( peptides ),
                                      " from the original query"] )
                else:
                    messages.append( [f"No ",
                                      el( protein ),
                                      " peptides in cart and none in the original query - leaving system to decide"] )

            all_peptides.extend( peptides )

        # Qbricks
        all_qbricks: List[alacat.Qbrick] = []

        for protein in proteins:
            qblocks = [x for x in entities if isinstance( x, alacat.Qblock ) and x.protein is protein]

            if qblocks:
                messages.append( ["Using the {len( qblocks )} ", el( protein ), " qbricks in the cart"] )
            else:
                qblocks = [x for x in gm.pipeline.favoured_qblocks if x.protein is protein]

                if qblocks:
                    messages.append( ["No ", el( protein ), " qblocks in cart - using the ", n( qblocks ),
                                      " from the original query"] )
                else:
                    messages.append( ["No ", el( protein ),
                                      " qblocks in cart and none in the original query - leaving system to decide"] )

            all_qbricks.extend( qblocks )

        # Menus
        menus: List[alacat.Qmenu] = [x for x in entities if isinstance( x, alacat.Qmenu )]

        if menus:
            messages.append( ["Using the ", n( menus ), " menus in the cart"] )
        else:
            menus = list( gm.pipeline.favoured_menus )

            if menus:
                messages.append( ["No menus in cart - using the ", n( menus ), " from the original query"] )
            else:
                messages.append( "No menus in cart and none in the original query - leaving system to decide" )

        entitity_set: Set[alacat.Entity] = set( entities )

        for x in chain( proteins, all_peptides, all_qbricks, menus ):
            if x in entitity_set:
                entitity_set.remove( x )

        if entitity_set:
            ff = [formatting.EntityLink( x, x.accession ) for x in entitity_set]
            messages.append( ["Ignoring the following items, they have nothing to do with the new model: ", *ff] )
        else:
            messages.append( ["There are no unused items in the cart."] )

        def jd( x ):
            if x:
                return json.dumps( x, indent = 4 )
            else:
                return ""

        return _NewFromCart( gm,
                             jd( [x.to_dbdata().to_dict() for x in proteins] ),
                             jd( [x.to_dbdata().to_dict() for x in all_peptides] ),
                             jd( [x.to_dbdata().to_dict() for x in all_qbricks] ),
                             jd( [x.to_dbdata().to_dict() for x in menus] ),
                             not bool( entitity_set ),
                             messages )
