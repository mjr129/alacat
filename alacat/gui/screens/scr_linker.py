from alacat import EDigestion
from hatmul import screens, forms
from alacat.utilities.linker_fixer_wrapper import propose_linker


class ProposeLinkerScreen( screens.FormScreen ):
    """
    Enter the peptide sequence, and the N (leading) and C (trailing) sequences,
    and this tool will propose suitable linking sequences.
    """

    class __form__:
        e = forms.ComboBoxField( "Enzyme", source = EDigestion )
        n = forms.TextField( "N flank" )
        p = forms.TextField( "Peptide" )
        c = forms.TextField( "C flank" )

    def on_response( self ) -> "forms.Page":
        form = self.__form__
        data = self.request

        e = form.e.read_value( data )
        n = form.n.read_str( data )
        p = form.p.read_str( data )
        c = form.c.read_str( data )

        prop_n = propose_linker( e, n, p, False )
        prop_l = propose_linker( e, c, p, True )

        table = [["Actual N", n],
                 ["Proposed N", prop_n],
                 ["Peptide", p],
                 ["Actual C", c],
                 ["Proposed C", prop_l]]

        report = forms.Report( "Report", forms.Table( table, section = "" ) )

        return self.show( after = report )
