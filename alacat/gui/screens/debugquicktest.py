from time import sleep

from hatmul import screens, forms
import alacat


class TestTaskScreen( screens.FormScreen ):
    __view__ = screens.ScreenData(
            url = "/testtask" )
    
    
    class __form__:
        succeed = forms.ComboBoxField( "Succeed", source =
        [
            "succeed",
            "error",
            "critical",
            "bad_submit",
            ] )
    
    
    def on_response( self ) -> "forms.Page":
        succeed = self.__form__.succeed.read_text( self.request )
        
        if succeed == "bad_submit":
            task_id = self.protocol.get_bg_task_server().submit_task( test_task, 1234 )
        else:
            task_id = self.protocol.get_bg_task_server().submit_task( test_task, [succeed] )
            
        return forms.Redirect( self.master.find_screen( screens.EScenario.TASK ).get_url( self.protocol, task = task_id ) )


def test_task( succeed: str ):
    sleep( 5 )
    
    if succeed == "succeed":
        return 1
    elif succeed == "error":
        raise alacat.PipelineError( "a pipeline error" )
    elif succeed == "critical":
        raise Exception( "a critical error" )
    
    assert False
