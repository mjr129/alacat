from hatmul import screens, forms
from alacat.gui.gui_pipelines import RESULTS_FIELD_ID
from alacat.gui import gui_pipelines
from alacat.gui.screens.results.scr_model_index import ModelIndexScreen


class HistoryScreen( screens.BasicScreen ):
    __view__ = screens.ScreenData( url = "/history",
                                   index = screens.EIndex.PRIMARY,
                                   folder = "Users" )

    def on_call( self ):
        if self.auth_result.user_id is None:
            from alacat.gui.screens import AlacatLoginScreen
            login_url = AlacatLoginScreen.get_url( self.protocol )
            return forms.Page( "", forms.Report( "", forms.Section( "", forms.Html( f"<p>You must be <a href='{login_url}'>logged in</a> to use this feature.</p>" ) ) ) )

        history = gui_pipelines.retrieve_history( self.auth_result.user_id )

        from alacat.gui.screens import AlacatMainScreen
        main_url = AlacatMainScreen.get_url( self.protocol )

        if not history:
            return forms.Page( "", forms.Report( "", forms.Section( "", forms.Html( f"<p>You currently have no history stored. <a href='{main_url}'>Make a new analysis.</a></p>" ) ) ) )

        lst = forms.UnorderedList()
        
        for id, title in history:
            url = ModelIndexScreen.get_url( self.protocol, **{ RESULTS_FIELD_ID: id } )
            lst.append( forms.Link( url, f"{id} - {title}" ) )

        lst.append( forms.Link( main_url, f"Make a new analysis!" ) )

        return forms.Page( "", forms.Report( "", forms.Section( "", lst ) ) )
