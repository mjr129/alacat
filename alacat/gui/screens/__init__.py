from .scr_about import AlacatAboutScreen, AlacatLoginScreen, AlacatLogoutScreen, AlacatNewUserScreen 
from .scr_add_to_cart import AddToCartScreen 
from .scr_compare import ComparePeptidesScreen 
from .scr_main import AlacatMainScreen
from .scr_consequence import ConsequenceScreen
from .scr_pending import PendingScreen 
from .scr_view_cart import ViewCartScreen 
from .scr_history import HistoryScreen
from .entities import *
from .results import *
from .scr_maintenance import AlacatMaintenanceScreen
