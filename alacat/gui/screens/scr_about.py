from typing import Optional

from hatmul import screens, forms
from alacat.gui import users_db


class AlacatReportBugScreen( screens.BugReportScreen ):
    __view__ = screens.ScreenData( url = "/bug",
                                   title = "Report a bug",
                                   index = screens.EIndex.SECONDARY,
                                   method = "post",
                                   folder = "Help" )


class AlacatNewUserScreen( screens.NewUserScreen ):
    __view__ = screens.ScreenData( url = "/new_user",
                                   title = "New user",
                                   folder = "Users",
                                   scenario = screens.EScenario.NEW_USER,
                                   index = screens.EIndex.PRIMARY,
                                   method = "post" )


    def register_user( self, name: str, password: str ) -> bool:
        if not users_db.register_user( name, password ):
            raise forms.InputError( "Cannot register. The username is not valid or may be in use.", widget = self.__form__.name )

        return True


    def on_create_form( self ) -> "forms.Form":
        form = super().on_create_form()
        form.append( forms.AutoTitle(
                form_help = "Register a new user here. "
                            "Logging on allows you to keep your flagged entities, "
                            "even when using different browsers or computers. "
                            "You can still create models without logging on." ) )
        return form


class AlacatLoginScreen( screens.LoginScreen ):
    __view__ = screens.ScreenData( url = "/login",
                                   title = "Login",
                                   folder = "Users",
                                   scenario = screens.EScenario.LOGIN,
                                   index = screens.EIndex.PRIMARY,
                                   method = "post" )


class AlacatLogoutScreen( screens.LogoutScreen ):
    __view__ = screens.ScreenData( url = "/logout",
                                   title = "Logout",
                                   folder = "Users",
                                   scenario = screens.EScenario.LOGOUT,
                                   index = screens.EIndex.PRIMARY )


class AlacatExitScreen( screens.BasicScreen ):
    """
    Returns to the server root.
    """
    __view__ = screens.ScreenData( url = "help/exit",
                                   folder = "Help",
                                   title = "Exit",
                                   index = screens.EIndex.SECONDARY,
                                   scenario = screens.EScenario.EXIT )

    def on_call( self ):
        import flask
        return flask.redirect( "/" )


class AlacatAboutScreen( screens.AboutScreen ):
    __view__ = screens.ScreenData( url = "/about",
                                   folder = "Help",
                                   scenario = screens.EScenario.ABOUT,
                                   index = screens.EIndex.SECONDARY )


    def on_get_credits( self ) -> Optional["forms.TRenderable"]:
        return [super().on_get_credits()] + [
            forms.HorizontalRule(),
            self.make_credits( "CONSeQuence",
                               "https://dx.doi.org/10.1074%2Fmcp.M110.003384",
                               "http://king.smith.man.ac.uk/CONSeQuence/" ),
            self.make_credits( "DeepMsPeptide",
                               "https://doi.org/10.1093/bioinformatics/btz708",
                               "https://github.com/vsegurar/DeepMSPeptide" ),
            self.make_credits( "Google Scholar",
                               "",
                               "https://scholar.google.com/",
                               "https://scholar.google.com/intl/en/scholar/images/1x/scholar_logo_64dp.png" ),
            self.make_credits( "McPred",
                               "https://dx.doi.org/10.1089%2Fomi.2011.0156",
                               "http://king.smith.man.ac.uk/mcpred/" ),
            self.make_credits( "Peptide Atlas",
                               "https://doi.org/10.1093/nar/gkj040",
                               "http://www.peptideatlas.org/" ),
            self.make_credits( "Peptide Sieve",
                               "https://dx.doi.org/10.1038%2Fnbt.1524",
                               "http://tools.proteomecenter.org/wiki/index.php?title=Software:PeptideSieve" ),
            self.make_credits( "PPA",
                               "https://dx.doi.org/10.1074%2Fmcp.M114.044321",
                               "http://software.steenlab.org/rc4/PPA.php" ),
            self.make_credits( "Yolanda",
                               "",
                               "https://bitbucket.org/mjr129/yolanda" ),
        ]


    def on_get_app_credits( self ):
        title = self.protocol.get_resource_text( forms.Resources.APPLICATION_TITLE )

        return [super().on_get_app_credits(),
                forms.Html( f"""
                    <p style='text-align: center'>
                    Martin Rusilowicz 2020.<br/>
                    {title} is <em>free, open source software</em>, licensed under the <a href="https://www.gnu.org/licenses/agpl-3.0.en.html">GNU Affero General Public License Version 3</a>.
                    </p>
                    """ )]


    def make_credits( self, name, doi, url, img = "" ):
        f = forms.Html()
        f.append( "\n\n<!--ABOUTSTART-->\n\n" )
        f.append( "<p style='text-align: center;'>" )

        if img:
            f.append( f"<img width='64px' src='{img}' alt='credit icon' /><br/>" )

        f.append( forms.escape( name ) )
        f.append( "<br/>" )

        if doi:
            f.append( forms.ButtonTile( url = doi,
                                        icon = forms.Resources.ARTICLE_BUTTON_ICON,
                                        css_class = forms.ButtonTile.CssClasses.ICON_BUTTON ) )

        if url:
            f.append( forms.ButtonTile( url = url,
                                        icon = forms.Resources.WEBSITE_BUTTON_ICON,
                                        css_class = forms.ButtonTile.CssClasses.ICON_BUTTON ) )

        f.append( "</p>" )

        f.append( "<p style='text-align: center;'>" )
        f.append( "* * *" )
        f.append( "</p>" )

        f.append( "\n\n<!--ABOUTEND-->\n\n" )
        return f
