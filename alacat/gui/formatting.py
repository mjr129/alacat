import inspect
from dataclasses import dataclass
from enum import Enum
from functools import partial
from itertools import islice
from typing import Callable, cast, Collection, Dict, Iterable, List, Optional, Tuple, Type, TypeVar, Union

import alacat
from hatmul import forms, screens
from mhelper import array_helper, exception_helper, string_helper
from mhelper.bio_requests import ncbi_taxonomy

from .gui_pipelines import GuiModel, RESULTS_FIELD_ID, Cart, SUBJECT_FIELD_ID
from . import gui_pipelines
from .widgets import RuleFieldWidget, HandlersFieldsWidget, WarningCodeBox, ITsv

_ = RuleFieldWidget, HandlersFieldsWidget  # re-export

T = TypeVar( "T" )


class IModelScreen:
    """
    Mixin for Alacat screens (webpages) that are bound to a GUI model.
    """
    __gui_model = None
    __subject = None


    def get_cart( self ) -> Cart:
        """
        Gets the user's cart.
        """
        return self.get_gui_model().cart

    def get_gui_model( self ) -> GuiModel:
        """
        Gets the model being investigated.
        """
        if self.__gui_model is None:
            screen_: screens.BaseScreen = cast( screens.BaseScreen, self )
            self.__gui_model = gui_pipelines.load( screen_ )

        return self.__gui_model

    def get_subject( self ) -> alacat.Entity:
        """
        Loads a GUI model and selects an entity from it.
        """
        if self.__subject is None:
            gui_model = self.get_gui_model()
            screen_: screens.BaseScreen = cast( screens.BaseScreen, self )
            subject_name: str = screen_.get_route_arg( SUBJECT_FIELD_ID )
            self.__subject = gui_model.pipeline.find( subject_name )

        return self.__subject


class EntityLink( forms.Widget, ITsv ):
    """
    A clickable link to an entity, with optional shopping cart.
    """

    def __init__( self, item: alacat.Entity, text: forms.TRenderable = None, cart: bool = True ):
        super().__init__()
        self.item = item
        self.text = text
        self.cart = cart

    def to_tsv( self ):
        if self.text:
            return str( self.text )
        else:
            return str( self.item.accession )

    def on_render( self, output: forms.Output ) -> forms.TRenderable:
        gm: GuiModel = gui_model_from_output( output )
        text = self.text

        if text is None:
            text = self.item.accession

        if self.item.__class__ is alacat.Protein:
            # Hack for removing "sp|" from the front of Uniprot IDs during display
            text = text.split("|", 1)[-1]

        text_partial = string_helper.max_width( text )

        tx_partial_escaped = output.render( text_partial, True )
        tx_full_escaped = output.render( text, True )
        ac = forms.escape( self.item.accession )
        url = forms.escape( get_link_for( output.get_screen(), self.item ) )
        klass_name = forms.escape( self.item.__class__.__name__ )
        py_class = f"Click to inspect this {klass_name}: {tx_full_escaped}"

        if isinstance( self.item, alacat.Column ) or isinstance( self.item, alacat.Handler ):
            klass = "alacat_technical"
        elif hasattr( self.item, "sequence" ) and text == getattr( self.item, "sequence" ):
            klass = "alacat_aa"
        else:
            klass = ""

        if not self.cart:
            return f"<a class='{klass}' title='{py_class}' href='{url}'>{tx_partial_escaped}</a>"

        is_in_cart = ac in gm.cart.entities
        alacat_is_in_cart = 'alacat_is_in_cart' if is_in_cart else 'alacat_is_not_in_cart'
        id = forms.escape( gm.id )

        return (f"<span class='alacat_cart_area {alacat_is_in_cart}' data-alacat_cart_area='{id}/{ac}'>"
                f"<a title='{py_class}' class='{klass}' href='{url}'>{tx_partial_escaped}</a>"
                f"<span class='alacat_cart_outer'>"
                f"<span title='Click to flag this {klass_name} for your reference' data-alacat_add_to_cart='{id}/{ac}' class='alacat_cart'>"
                f"</span>"
                f"</span>"
                f"</span>")


class GradeMarker( forms.Widget ):
    """
    Formats a grade in HTML.
    """

    def __init__( self, x ):
        super().__init__()
        self.x = x

    def on_render( self, output: forms.Output ) -> forms.TRenderable:
        return _map_symbol( _k_map_symbol_to_html, self.x )


class TaxonWidget( forms.Widget ):
    """
    Displays a taxon as a name and link.
    """

    def __init__( self, x: Union[int, Iterable[int]] ):
        super().__init__()
        self.x: Tuple[int, ...] = (x,) if isinstance( x, int ) else tuple( x )

    def on_render( self, output: forms.Output ) -> forms.TRenderable:
        y: Tuple[int, ...] = self.x

        if not y:
            return "None"

        return ", ".join( f"<a href='{ncbi_taxonomy.get_url( x )}'><i>{ncbi_taxonomy.get_taxonomy().get_name( x ) if x != -1 else 'Unknown'}</i></a>" for x in y )


class ScoreWidget( forms.Widget, ITsv ):
    """
    Shows a score's value, formatted by grade and including its source as a
    tooltip.
    """
    __slots__ = "score",

    def __init__( self, score: alacat.Score ):
        super().__init__()
        self.score = score

    def to_tsv( self ):
        return self.__get_param_str().replace( "\n", "<br/>" )

    def on_render( self, output: forms.Output ) -> forms.TRenderable:
        param: alacat.Score = self.score
        val_repr: str = string_helper.max_width( repr( param.value ) )
        val_wid: forms.TRenderable = self.__get_param_html()

        tool_tip: str = forms.escape( f"{param.value.__class__.__name__}: {val_repr} #{param.grade.name}" )
        css_class: str = _map_symbol( _k_map_symbol_to_css, param )

        output.write( f"<span class='{css_class}' title='{tool_tip}'>" )
        output.append( val_wid, False )
        output.write( "</span>" )

        return None

    def __get_param_html( self ) -> forms.TRenderable:
        # This is a hack to add highlighting to the N and C flanking sequences in HTML
        html_formatter: Optional[Callable[["alacat.Score"], object]] = self.score.column.specification.default_html_formatter

        if html_formatter is not None:
            return cast( forms.TRenderable, html_formatter( self.score ) )

        val_str: str = self.__get_param_str()

        val_wid = forms.Escaped( val_str, lf = True )

        if "\n" in val_str or len( val_str ) > 20:
            val_wid = forms.ClickToShow( "", val_wid )

        return val_wid


    def __get_param_str( self ) -> str:
        v: object = self.score.value

        if isinstance( v, Enum ):
            return string_helper.object_to_string( v )  # flag or enum
        elif isinstance( v, int ):
            return f"{v:,}"
        elif isinstance( v, float ) and 0 <= v <= 1:
            return f"{v:.2f}"
        else:
            return str( v ) or "--"


def get_link_for( screen: screens.BaseScreen, x: alacat.Entity ):
    screen_ = exception_helper.safe_cast( "screen", screen, IModelScreen )
    gm = screen_.get_gui_model()
    urls = screen.master.list_urls( x, route_args = { RESULTS_FIELD_ID: gm.id, forms.SUBMIT_FIELD: "0" } )

    try:
        url = array_helper.single( urls ).url
    except KeyError:
        raise ValueError( f"No handler found for {x.__class__.__name__!r}." )

    return url


_k_map_symbol_to_html: Dict[alacat.EGrade, str] = {
    alacat.EGrade.PASS :
        "<span class='alacat_grade_box_pass' title='Pass: Meets requirement'>P</span>",
    alacat.EGrade.FAIL :
        "<span class='alacat_grade_box_red' title='Fail: Did not meet a requirement'>F</span>",
    alacat.EGrade.INFO :
        "<span class='alacat_grade_box_info' title='Information: Not assessed; no requirement; information only'>I</span>",
    alacat.EGrade.ERROR:
        "<span class='alacat_grade_box_error' title='Error: Could not assess; problem obtaining value'>E</span>" }

_k_map_symbol_to_css: Dict[alacat.EGrade, str] = {
    alacat.EGrade.FAIL : "alacat_red",
    alacat.EGrade.PASS : "alacat_pass",
    alacat.EGrade.INFO : "alacat_info",
    alacat.EGrade.ERROR: "alacat_error" }


def _map_symbol( m: Dict[alacat.EGrade, str],
                 s: Union[alacat.EGrade, alacat.Score] ) -> str:
    if isinstance( s, alacat.Score ):
        s = s.grade

    if not isinstance( s, alacat.EGrade ):
        raise exception_helper.type_error( "s", s, [alacat.EGrade, alacat.Score] )

    r = m.get( s )

    if r is not None:
        return r

    return m[s[0]]


class SelectionRank( forms.Widget, ITsv ):
    allow_debug = False

    def __init__( self, summary: Optional[alacat.EntitySummary], no_star = False, no_rank = False ):
        super().__init__()
        assert summary is None or isinstance( summary, alacat.EntitySummary )
        self.summary = summary
        self.no_rank = no_rank
        self.no_star = no_star

    def __render_rank( self, output: forms.Output ):
        if self.no_rank:
            return

        s = self.summary

        output.write( f"<div class='alacat_rank'>{s.rank}</div>" )

    def __render_message( self ):
        klass = self.summary.entity.__class__.__name__

        message_map = {
            # selected, system likes, user likes
            (False, False, False):
                f"This {klass} was not selected for further processing. The system did not consider it appropriate and the user did not favour it.",
            (False, False, True) :  # Impossible in MANDATED mode
                f"This {klass} was not selected for further processing. While the user favoured this {klass} the user also favoured {klass}s the system felt were more appropriate.",
            (False, True, False) :
                f"This {klass} was not selected. The system considered it appropriate but this choice was overridden by the user.",
            (False, True, True)  :  # Impossible!
                f"This {klass} was not selected for further processing. This is probably a mistake, since both the system and user considered it appropriate.",
            (True, False, False) :
                f"This {klass} was selected for further processing. This is probably a mistake, since neither the system or user considered it appropriate.",
            (True, False, True)  :
                f"This {klass} was selected for further processing. The system did not find this {klass} appropriate but this choice was overridden by the user.",
            (True, True, False)  :
                f"This {klass} was selected for further processing. The system found this {klass} an appropriate choice.",
            (True, True, True)   :
                f"This {klass} was selected for further processing. Both the user and system favoured this {klass}.",
        }

        msg = message_map[(bool( self.summary.selection & alacat.ESelectors.FINAL ),
                           bool( self.summary.selection & alacat.ESelectors.SYSTEM ),
                           bool( self.summary.selection & alacat.ESelectors.USER ))]

        if self.summary.same_ranked != 0:
            msg += f" Note that some choices made by the system may be arbitrary: {self.summary.same_ranked} out of {self.summary.ranked_out_of - 1} other {klass}s ranked equal to this one."
        else:
            msg += f" Note that some choices made by the system may be arbitrary, although in this case none of the {self.summary.ranked_out_of - 1} other {klass}s ranked equal to this one."

        msg += f" Score: {self.summary.total_points}"

        return msg

    def __render_selection( self, output: forms.Output ):
        selection = self.summary.selection

        output.write( "<div class='alacat_selection_block'>" )

        if selection & alacat.ESelectors.SYSTEM:
            output.write( "<span>" )
            output.append( forms.FlatImage( AlacatResources.SYSTEM ) )
            output.write( "</span>" )

        if selection & alacat.ESelectors.USER:
            output.write( "<span>" )
            output.append( forms.FlatImage( AlacatResources.USER ) )
            output.write( "</span>" )

        if selection & alacat.ESelectors.FINAL and not self.no_star:
            output.write( "<span>" )
            output.append( forms.FlatImage( AlacatResources.SELECTED ) )
            output.write( "</span>" )

        output.write( "</div>" )

    def on_render( self, output: forms.Output ) -> None:
        if self.summary is None:
            return

        output.write( f"<span title='{self.__render_message()}'>" )
        self.__render_rank( output )
        self.__render_selection( output )
        output.write( "</span>" )
        return None

    def to_tsv( self ):
        if self.summary is None:
            return ""

        selection = self.summary.selection
        S = "S" if selection & alacat.ESelectors.SYSTEM else ""
        U = "U" if selection & alacat.ESelectors.USER else ""
        F = "F" if selection & alacat.ESelectors.FINAL and not self.no_star else ""
        return f"{self.summary.rank}{S}{U}{F}"


def poppinlist( rows: Collection[object] ):
    n = len( rows )

    if n == 0:
        return "None"

    rows: List[forms.TRenderable]

    if isinstance( array_helper.first_or_error( rows ), alacat.Entity ):
        rows = [EntityLink( cast( alacat.Entity, x ), str( x ) ) for x in islice( rows, 100 )]
    else:
        rows = [str( x ) for x in rows]

    if n == 1:
        return rows[0]

    if len( rows ) == 100:
        rows.append( "..." )

    return forms.ClickToShow( content = forms.UnorderedList( rows, style = forms.EList.COMPACT ),
                              title = f"{n:,} records",
                              mode = forms.EClickToShow.BUTTON )


class ModelLink( forms.Widget ):
    def __init__( self, id ):
        super().__init__()
        self.id = id

    def on_render( self, output: forms.Output ) -> forms.TRenderable:
        from alacat.gui.screens.results.scr_model_index import ModelIndexScreen
        url = forms.escape( ModelIndexScreen.get_url( output.get_protocol(), **{ RESULTS_FIELD_ID: self.id } ) )

        return forms.TileGrid( style = forms.TileGrid.HEADING_TILES,
                               content = forms.ButtonTile( url = url,
                                                           icon = AlacatResources.MODEL,
                                                           text = "Back to results" ) )


def get_table_name( pipeline, x ):
    for k in dir( pipeline ):
        if not k.startswith( "_" ) and getattr( pipeline, k ) is x:
            return k

    return "??unknown-table-name??"


class Linker( forms.Widget, ITsv ):
    def __init__( self, subject: Union[alacat.LinkedPeptide, alacat.Peptide], terminal: str ):
        super().__init__()
        self.subject: alacat.LinkedPeptide = subject
        self.terminal = terminal

    def to_tsv( self ) -> str:
        if isinstance( self.subject, alacat.LinkedPeptide ):
            if self.terminal == "c":
                lnk = self.subject.c_link
            elif self.terminal == "n":
                lnk = self.subject.n_link
            else:
                raise ValueError( "terminal unknown." )

            return lnk.ljust( 3, "-" )
        elif isinstance( self.subject, alacat.Peptide ):
            if self.terminal == "c":
                return self.subject.c_flank
            elif self.terminal == "n":
                return self.subject.n_flank
            else:
                raise ValueError( "terminal unknown." )
        else:
            raise exception_helper.type_error( "self.subject", self.subject, [alacat.LinkedPeptide, alacat.Peptide] )

    def on_render( self, output: forms.Output ) -> forms.TRenderable:
        if isinstance( self.subject, alacat.LinkedPeptide ):
            if self.terminal == "c":
                nat = self.subject.peptide.c_flank
                lnk = self.subject.c_link
            elif self.terminal == "n":
                nat = self.subject.peptide.n_flank
                lnk = self.subject.n_link
            else:
                raise ValueError( "terminal unknown." )

            sz = max( len( nat ), len( lnk ) )
            lnk_f = lnk.ljust( sz, "-" )
            nat_f = nat.ljust( sz, "-" )

            html = []
            for l, n in zip( lnk_f, nat_f ):
                if l == n:
                    html.append( f"<span class='alacat_linker_nat'>{l}</span>" )
                else:
                    html.append( f"<span class='alacat_linker_syn'>{l}</span>" )

            html = "".join( html )
            term_name = "carboxyl-terminus" if self.terminal == "c" else "amino-terminus"
            caption = forms.escape( f"Natural {term_name}: {nat!r}, suggested {term_name}: {lnk!r}" )
            html = f"<span class='alacat_aa' title='{caption}'>{html}</span>"

            output.append( EntityLink( self.subject, forms.Html( html ) ) )
        elif isinstance( self.subject, alacat.Peptide ):
            if self.terminal == "c":
                nat = self.subject.c_flank
            elif self.terminal == "n":
                nat = self.subject.n_flank
            else:
                raise ValueError( "terminal unknown." )

            nat = forms.escape( nat )
            html = f"<span class='alacat_aa alacat_natural'>{nat}</span>"

            output.append( forms.Html( html ) )
        else:
            raise exception_helper.type_error( "self.subject", self.subject, [alacat.LinkedPeptide, alacat.Peptide] )

        return None


@dataclass
class CompositionRow:
    qmenu: Optional[alacat.Qmenu]
    qconcat: Optional[alacat.Qconcat]
    qbrick: Optional[alacat.Qbrick]
    linked_peptide: Optional[alacat.LinkedPeptide]


def find_qblock( gm: GuiModel, qbrick: alacat.Qbrick ) -> alacat.Qblock:
    """
    Finds the qblock for a given qbrick.
    """
    x = [qblock for qblock in gm.pipeline.selected_qblocks if qbrick in qblock.qbricks]
    return array_helper.single( x, None )


def _fmt_choice( gm: GuiModel, entity: alacat.Entity ):
    assert isinstance( entity, alacat.Entity )
    return SelectionRank( gm.pipeline.summarise( entity ), no_star = True, no_rank = True )


def __add_selection_letters( summary: alacat.EntitySummary ) -> str:
    r = []

    if alacat.ESelectors.SYSTEM in summary.selection:
        r.append( "s" )

    if alacat.ESelectors.USER in summary.selection:
        if alacat.ESelectors.SYSTEM not in summary.selection and summary.is_equivalent:
            r.append( "e" )

        r.append( "u" )

    return "".join( r )


def _fmt_warnings( gm: GuiModel, wtable: WarningCodeBox.WarningsTable, row: CompositionRow, qblock: Optional[alacat.Qblock] ):
    warns: List[forms.TRenderable] = []

    warns.extend( WarningCodeBox( wtable, gm, x )
                  for x in gm.pipeline.peptide_scores.get_failures( row.linked_peptide.peptide ) )
    warns.extend( WarningCodeBox( wtable, gm, x )
                  for x in gm.pipeline.qblock_scores.get_failures( qblock ) )
    warns.extend( WarningCodeBox( wtable, gm, x )
                  for x in gm.pipeline.qmenu_scores.get_failures( row.qmenu ) )

    return warns


class AssessorWidget( forms.Widget, ITsv ):
    def __init__( self, assessor: alacat.AssessorDelegate ):
        super().__init__()
        self.assessor = assessor

    def to_tsv( self ) -> str:
        return f"{self.assessor}"

    def on_render( self, output: forms.Output ) -> forms.TRenderable:
        output.write( f"<code>" )
        output.write( forms.escape( self.assessor ) )
        output.write( "</code>" )
        return None


class CompositionTable:
    __slots__ = "short_tables", "extended", "warnings_table", "rows"


    def __init__( self,
                  gm: GuiModel,
                  rows: List["CompositionRow"],
                  extended: bool = False,
                  warnings_legend: bool = False ):
        self.short_tables: Dict[Type, Dict[alacat.Entity, int]] = { alacat.Qmenu  : { },
                                                                    alacat.Qconcat: { },
                                                                    alacat.Qblock : { },
                                                                    alacat.Qbrick : { } }
        self.extended = extended
        cols: List[List[forms.TRenderable]] = []
        _hc = partial( forms.Table.Cell, is_header = True )

        # QMenu column
        if any( row.qmenu for row in rows ):
            cols.append( [_hc( "Qmenu" ),
                          *[self.get_text( row.qmenu )
                            if row.qmenu is not None and (prev is None or row.qmenu is not prev.qmenu) else None
                            for prev, row
                            in array_helper.lagged_iterate( rows, head = True )]] )

        # QConcat column
        if any( row.qconcat for row in rows ):
            cols.append( [_hc( "Qconcat" ),
                          *[self.get_text( row.qconcat )
                            if prev is None or row.qconcat is not prev.qconcat else None
                            for prev, row
                            in array_helper.lagged_iterate( rows, head = True )]] )

        # QBlock column
        qblocks = [find_qblock( gm, row.qbrick ) for row in rows]

        if any( qblocks ):
            cols.append( [_hc( "Qblock" ),
                          *[self.get_text( qblock )
                            if qblock is not None and prev is not qblock else None
                            for prev, qblock
                            in array_helper.lagged_iterate( qblocks, head = True )]] )

        # QBrick column
        if any( row.qbrick for row in rows ):
            cols.append( [_hc( "Qbrick" ),
                          *[self.get_text( row.qbrick )
                            if row.qbrick is not None and (prev is None or row.qbrick is not prev.qbrick) else None
                            for prev, row
                            in array_helper.lagged_iterate( rows, head = True )]] )

        # Peptide columns (Protein, Protein Title, NLink, NNat, Peptide, CNat, CLink)
        if any( row.linked_peptide for row in rows ):
            cols.append( [_hc( "Protein" ),
                          *[EntityLink( row.linked_peptide.protein )
                            if row.linked_peptide is not None and (prev is None or row.linked_peptide.protein is not prev.linked_peptide.protein) else None
                            for prev, row
                            in array_helper.lagged_iterate( rows, head = True )]] )

            if extended:
                cols.append( [_hc( "Protein Title" ),
                              *[EntityLink( row.linked_peptide.protein, text = row.linked_peptide.protein.title )
                                if row.linked_peptide is not None and (prev is None or row.linked_peptide.protein is not prev.linked_peptide.protein) else None
                                for prev, row
                                in array_helper.lagged_iterate( rows, head = True )]] )

            if not extended:
                cols.append( [_hc( "NLink" ),
                              *[Linker( row.linked_peptide, "n" )
                                if row.linked_peptide is not None else None
                                for row in rows]] )

            cols.append( [_hc( "NNat" ),
                          *[Linker( row.linked_peptide.peptide, "n" )
                            if row.linked_peptide is not None else None
                            for row in rows]] )

            cols.append( [_hc( "Peptide" ),
                          *[EntityLink( row.linked_peptide.peptide, row.linked_peptide.peptide.sequence )
                            if row.linked_peptide is not None else None
                            for row in rows]] )

            cols.append( [_hc( "CNat" ),
                          *[Linker( row.linked_peptide.peptide, "c" )
                            if row.linked_peptide is not None else None
                            for row in rows]] )

            if not extended:
                cols.append( [_hc( "CLink" ),
                              *[Linker( row.linked_peptide, "c" )
                                if row.linked_peptide is not None else None
                                for row in rows]] )

        # Extended columns (P-rank, B-rank, M-rank)
        if extended:
            cols.append( [_hc( "P-rank" ),
                          *[_fmt_choice( gm, row.linked_peptide.peptide )
                            for row in rows]] )

            cols.append( [_hc( "B-rank" ),
                          *[_fmt_choice( gm, qblock )
                            for row, qblock in zip( rows, qblocks )]] )

            cols.append( [_hc( "M-rank" ),
                          *[_fmt_choice( gm, row.qmenu )
                            for row in rows]] )

            warnings_table: Optional[WarningCodeBox.WarningsTable] = WarningCodeBox.WarningsTable()

            cols.append( [_hc( "Warnings" ),
                          *[_fmt_warnings( gm, warnings_table, row, qblock )
                            for row, qblock in zip( rows, qblocks )]] )

            warnings_table.complete()
        else:
            warnings_table = None

        self.rows = array_helper.transpose( cols )
        self.warnings_table = warnings_table

        if warnings_legend:
            self.__add_warnings_legend( gm )


    def get_text( self, entity: alacat.Entity ):
        if not self.extended:
            return None

        dct: Dict[alacat.Entity, int] = self.short_tables[entity.__class__]
        r = dct.setdefault( entity, len( dct ) + 1 )
        return EntityLink( entity, text = str( r ) )


    def to_table( self, **kwargs ):
        return forms.Table( self.rows, **kwargs )

    def __add_warnings_legend( self, gm: GuiModel ):
        self.rows.append( forms.Table.TitleRow( "Warnings summary" ) )
        self.rows.append( forms.Table.Row( ["Legend", "Num. violations", "Column", "Rule"], is_header = True ) )
        warnings_cols = sorted( self.warnings_table.columns, key = lambda x: -len( self.warnings_table.columns[x] ) )

        for column in warnings_cols:
            count = len( self.warnings_table.columns[column] )
            self.rows.append( [WarningCodeBox( self.warnings_table, gm, column ),
                               f"{count:,}",
                               EntityLink( column, column.accession ),
                               AssessorWidget( column.assessor )] )

        self.rows.insert( 0, [forms.Table.Cell( gm.id, is_header = True )] )


class Code( forms.Widget ):
    def __init__( self, e ):
        super().__init__()
        self.e = e

    def on_render( self, output: forms.Output ) -> forms.TRenderable:
        name = forms.escape( self.e )
        return f"<span class='alacat_code'>{name}</span>"


class AlacatResources:
    FINAL_REPORT = forms.Resource( "/alacat_static/final_report.svg" )
    PEPTIDE_PICKER = forms.Resource( "/alacat_static/peptide_picker.svg" )
    QMENU = forms.Resource( "/alacat_static/qmenu.svg" )
    MODEL = forms.Resource( "/alacat_static/model.svg" )
    SCORES_TABLE = forms.Resource( "/alacat_static/scores_table.svg" )
    WARNING = forms.Resource( "/alacat_static/warning.svg" )
    DELETE = forms.Resource( "/alacat_static/delete.svg" )
    NONE = forms.Resource( "/alacat_static/none.svg" )
    SYSTEM = forms.Resource( "/alacat_static/system.svg" )
    USER = forms.Resource( "/alacat_static/user.svg" )
    SELECTED = forms.Resource( "/alacat_static/selected.svg" )
    COMPARE = forms.Resource( "/alacat_static/compare.svg" )
    IN_BASKET = forms.Resource( "/alacat_static/in_basket.svg" )
    DOWNLOAD = forms.Resource( "/alacat_static/download.svg" )
    NOT_IN_BASKET = forms.Resource( "/alacat_static/not_in_basket.svg" )
    EXTRA_CSS = forms.Resource( "/alacat_static/extra.css" )
    EXTRA_JS = forms.Resource( "/alacat_static/extra.js" )


class ReturnToModelLink( forms.Widget ):
    def __init__( self ):
        super().__init__()

    def on_render( self, output: forms.Output ) -> None:
        from alacat.gui import screens
        gm = gui_model_from_output( output )

        url2: str = screens.ModelIndexScreen.get_url( output.get_protocol(), **{ RESULTS_FIELD_ID: gm.id } )

        model_img = output.render( forms.FlatImage( AlacatResources.MODEL ) )

        output.write( f"""
                <div class='alacat_return_to_model'>
                    <a href='{url2}'>
                        {model_img}
                        Results
                    </a>
                </div>
                """ )


class HistoryScreenLink( forms.Widget ):
    def __init__( self ):
        super().__init__()

    def on_render( self, output: forms.Output ) -> None:
        from alacat.gui import screens

        url2: str = screens.HistoryScreen.get_url( output.get_protocol() )

        model_img = output.render( forms.FlatImage( AlacatResources.USER ) )

        output.write( f"""
                <div class='alacat_return_to_model'>
                    <a href='{url2}'>
                        {model_img}
                        Results history
                    </a>
                </div>
                """ )


class GoToCartLink( forms.Widget ):
    def __init__( self, ):
        super().__init__()

    def on_render( self, output: forms.Output ) -> None:
        from alacat.gui import screens

        gm = gui_model_from_output( output )
        n: str = gm.cart.entities.__len__().__str__()
        url: str = screens.ViewCartScreen.data.get_url( output.get_protocol(), **{ RESULTS_FIELD_ID  : gm.id,
                                                                                   forms.SUBMIT_FIELD: 0 } )
        in_basket = output.render( forms.FlatImage( AlacatResources.IN_BASKET ) )

        output.write( f"""
                <div class='alacat_cart_box'>
                    <a href='{url}'>
                        <span id='alacat_cart_box'>
                                {in_basket}
                                <span id='alacat_cart_count'>
                                    {n}
                                </span>
                        </span>
                    </a>
                </div>""" )


class RuleWidget( forms.Widget, ITsv ):
    def __init__( self, assessor: object, short: bool = False ):
        super().__init__()
        self.assessor = assessor
        self.short: bool = short

    def to_tsv( self ):
        if inspect.isroutine( self.assessor ):
            return self.assessor.__name__
        else:
            if self.short:
                return str( self.assessor )
            else:
                return f"{self.assessor.__class__.__name__} ({self.assessor})"

    def on_render( self, output: forms.Output ) -> None:
        if inspect.isroutine( self.assessor ):
            output.append( Code( self.assessor.__name__ ) )
        else:
            if self.short:
                output.append( Code( string_helper.max_width( str( self.assessor ), 20 ) ) )
            else:
                output.append( Code( self.assessor.__class__.__name__ ) )
                output.append( forms.Escaped( " (" ) )
                output.append( Code( self.assessor ) )
                output.append( forms.Escaped( ")" ) )


class WeightWidget( forms.Widget, ITsv ):
    _map = {
        alacat.EWeight.DISABLED : "Off",
        alacat.EWeight.VERY_HIGH: "VHIGH",
        alacat.EWeight.HIGH     : "HIGH",
        alacat.EWeight.NORMAL   : "NORM",
        alacat.EWeight.LOW      : "LOW",
        alacat.EWeight.VERY_LOW : "VLOW",
    }
    __slots__ = "weight", "total"

    def __init__( self, weight: int, total: bool = False ):
        super().__init__()
        self.weight = weight
        self.total = total

    def to_tsv( self ) -> str:
        return self.__value_str()

    def __value_str( self ):
        if self.total:
            return f"{self.weight:,}"

        name = self._map.get( self.weight )

        if name:
            return name
        else:
            return f"{self.weight:,}"

    def on_render( self, output: forms.Output ) -> None:
        output.append( Code( self.__value_str() ) )


def gui_model_from_output( output ):
    gm = exception_helper.safe_cast( "screen", output.get_screen(), IModelScreen ).get_gui_model()
    return gm


def to_tsv( rows ):
    r = []
    for ir, row in enumerate( rows ):
        if ir != 0:
            r.append( "\n" )

        for ic, cell in enumerate( row ):
            if ic != 0:
                r.append( "\t" )

            r.append( __cell_to_tsv( cell ) )

    return "".join( r )


def __cell_to_tsv( cell ) -> str:
    if isinstance( cell, forms.Table.Cell ):
        cell = "".join( __cell_to_tsv( x ) for x in cell.content.children )

    if isinstance( cell, list ):
        cell = "".join( __cell_to_tsv( x ) for x in cell )

    if isinstance( cell, ITsv ):
        cell = cell.to_tsv()

    if cell is None:
        return ""

    assert isinstance( cell, str ), "Probably need to convert this to ITsv"
    return cell
