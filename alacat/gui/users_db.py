import datetime
from functools import lru_cache
from typing import Optional

from alacat.utilities.mysql_backed_cache import AlacatBacking, ESqlBacking

# noinspection PyUnresolvedReferences,PyPackageRequirements
import MySQLdb


@lru_cache()
def __ensure_users_table_exists():
    sql = """
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `data` text NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_id_uindex` (`id`),
  UNIQUE KEY `users_username_uindex` (`name`)
);
"""
    bk = AlacatBacking( None, ESqlBacking.NONE )

    try:
        with bk.connection.cursor() as cursor:
            cursor: MySQLdb.cursors.Cursor
            cursor.execute( """SHOW TABLES LIKE 'users'""" )

            row = cursor.fetchall()

            if not row:
                cursor.execute( sql )
                cursor.fetchall()
                bk.connection.commit()
    finally:
        bk.close()


@lru_cache
def get_user_name( user_id: int ) -> Optional[str]:
    __ensure_users_table_exists()

    bk = AlacatBacking( None, ESqlBacking.NONE )

    try:
        with bk.connection.cursor() as cursor:
            cursor: MySQLdb.cursors.Cursor
            sql = "SELECT `name` FROM `users` WHERE `id` = %s"
            args = [user_id]
            cursor.execute( sql, args )

            row = cursor.fetchone()

            if not row:
                return None

            user_name = row[0]

            return user_name
    finally:
        bk.close()


def register_user( name, password ):
    __ensure_users_table_exists()

    # noinspection PyPackageRequirements
    from werkzeug import security

    bk = AlacatBacking( None, ESqlBacking.NONE )
    updated = datetime.datetime.now().strftime( '%Y-%m-%d %H:%M:%S' )
    password_hash = security.generate_password_hash( password )
    data = ""

    try:
        with bk.connection.cursor() as cursor:
            sql = "INSERT INTO `users` (`name`, `password`, `data`, `updated`) VALUES (%s, %s, %s, %s)"
            args = (name, password_hash, data, updated)

            try:
                cursor.execute( sql, args )
            except MySQLdb.IntegrityError:
                # Duplicate
                return False

            bk.connection.commit()
    finally:
        bk.close()

    return True


def check_user( name, password ) -> Optional[int]:
    __ensure_users_table_exists()

    # noinspection PyPackageRequirements
    from werkzeug import security
    updated = datetime.datetime.now().strftime( '%Y-%m-%d %H:%M:%S' )

    bk = AlacatBacking( None, ESqlBacking.NONE )

    try:
        with bk.connection.cursor() as cursor:
            cursor: MySQLdb.cursors.Cursor
            sql = "SELECT `id`, `password` FROM `users` WHERE `name` = %s"
            args = [name]
            cursor.execute( sql, args )

            row = cursor.fetchone()

            if not row:
                return None

            user_id = row[0]
            password_hash = row[1]

            if not security.check_password_hash( password_hash, password ):
                return False

            sql = "UPDATE `users` SET `updated` = %s WHERE `id` = %s"
            args = [updated, user_id]
            cursor.execute( sql, args )

            bk.connection.commit()

            return user_id
    finally:
        bk.close()
