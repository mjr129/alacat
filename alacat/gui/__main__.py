from alacat.gui import app

def gui_main():
    app.create_app().run_gui()
    
if __name__=="__main__":
    gui_main()