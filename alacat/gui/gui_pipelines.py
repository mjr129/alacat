import os
from dataclasses import dataclass
from functools import lru_cache
from typing import List, Optional, Tuple
from uuid import uuid4

import alacat
from hatmul import forms, screens
from mhelper import io_helper, lock_helper
from hatmul.screens import LongTaskScreen, ResultsStore

from .cart import Cart


RESULTS_FIELD_ID = LongTaskScreen.RESULTS_FIELD_ID
SUBJECT_FIELD_ID = "subject"
_CART_FILE_MAGIC = "ALACAT_CART", 1
_CART_RESULTS_SUBSET = "cart-{}.pkl.gz"
_PIPELINE_RESULTS_SUBSET = "pipeline.pkl.gz"


class AlacatResultsStore( ResultsStore ):


    def __init__( self ):
        super().__init__( "alacat_gui_models" )


    class ResultsSet( ResultsStore.ResultsSet ):
        def get_pipeline_file_name( self ) -> str:
            return super().get_path( _PIPELINE_RESULTS_SUBSET )


        def get_cart_file_name( self ) -> str:
            user_id = _get_session_uid()
            return super().get_path( _CART_RESULTS_SUBSET.format( user_id ) )


        def load_pipeline( self ) -> alacat.Pipeline:
            fn = self.get_pipeline_file_name()
            return _lru_load( fn )


        def save_pipeline( self, results: alacat.Pipeline ):
            fn = self.get_pipeline_file_name()
            results.save( fn )


        def load_cart( self ) -> Cart:
            fn = self.get_cart_file_name()

            if fn and os.path.exists( fn ):
                with io_helper.SequentialReader( fn, default = None, delete_on_failure = True ) as reader:
                    reader.read_magic( _CART_FILE_MAGIC )
                    r = reader.read_pickle( Cart, Cart() )

                    if r is not None:
                        return r

            return Cart()


        def delete_cart( self ) -> bool:
            fn = self.get_cart_file_name()

            if not fn or not os.path.isfile( fn ):
                return False

            os.remove( fn )
            return True


        def save_cart( self, cart: Cart ):
            fn = self.get_cart_file_name()

            with io_helper.SequentialWriter( fn ) as writer:
                writer.write_magic( _CART_FILE_MAGIC )
                writer.write_pickle( cart )

        def get_creation_log_fp( self ):
            return self.get_path( "creation_log.txt" )


@dataclass
class GuiModel:
    """
    Encapsulates the pipeline and cart, used throughout the GUI.
    """
    id: str
    pipeline: alacat.Pipeline
    cart: Cart
    store: AlacatResultsStore
    results_set: AlacatResultsStore.ResultsSet


_g_store: Optional[AlacatResultsStore] = None


def get_store() -> AlacatResultsStore:
    """
    Retrieves the AlacatModelStore singleton instance.
    """
    global _g_store

    if _g_store is None:
        _g_store = AlacatResultsStore()

    return _g_store


def load( screen: screens.BaseScreen, results_id: Optional[str] = None ) -> GuiModel:
    """
    Loads the GuiModel from a results ID.
     
    :param screen:          `Screen` instance that we are viewing 
    :param results_id:      ID of the results. If this is `None` the ID is
                            pulled from the `Request`.
    :return:                GuiModel.
    :exception InputError: Bad ID or missing `Request` data. 
    """
    if not results_id:
        results_id = get_results_id( screen )

    if not results_id:
        raise forms.InputError( "No model specified", widget = RESULTS_FIELD_ID )

    store: AlacatResultsStore = get_store()
    results_set: Optional[AlacatResultsStore.ResultsSet] = store.get_results( results_id )

    if results_set is None:
        raise forms.InputError( "Failed to load results", widget = RESULTS_FIELD_ID )

    user_id: object = screen.auth_result.user_id
    gm = GuiModel( results_id, results_set.load_pipeline(), results_set.load_cart(), store, results_set )
    __store_in_history( user_id, gm.id, gm.pipeline.model.name )
    return gm


@lru_cache()
def __get_history_folder() -> str:
    dir_ = os.path.join( alacat.paths.get_data_root(), "gui_user_history" )
    os.makedirs( dir_, exist_ok = True )
    return dir_


def __get_history_file( user_id ) -> Tuple[str, lock_helper.FileLock]:
    fp = os.path.join( __get_history_folder(), f"{user_id}.lst" )
    return fp, lock_helper.FileLock( f"{fp}.lock" )


def __store_in_history( user_id: object, results_id: str, results_title: str ):
    if user_id is None or not results_id:
        return

    kv = f"{results_id}\t{results_title}"

    fp, lock = __get_history_file( user_id )

    with lock:
        if os.path.isfile( fp ):
            lines = io_helper.read_all_lines( fp )
        else:
            lines = []

        if lines and lines[0] == kv:
            return

        if kv in lines:
            lines.remove( kv )

        lines.insert( 0, kv )
        io_helper.write_all_text( fp, lines )


def retrieve_history( user_id: object ) -> List[List[str]]:
    if user_id is None:
        return []

    fp, lock = __get_history_file( user_id )

    with lock:
        if not os.path.isfile( fp ):
            return []

        return [x.split( "\t", 1 ) for x in io_helper.read_all_lines( fp ) if x]


def get_results_id( screen ):
    results_id = screen.get_route_arg( RESULTS_FIELD_ID, None )

    if not results_id:
        results_id = screen.request.post_data.get1( RESULTS_FIELD_ID )

    return results_id


def _get_session_uid() -> Optional[str]:
    """
    Generates an ID for the user's cart.
    """
    import flask

    session_uid = flask.session.get( "session_uid" )

    if session_uid is None:
        session_uid = str( uuid4() )
        flask.session["session_uid"] = session_uid

    return session_uid


@lru_cache( maxsize = 4 )
def _lru_load( fn: str ) -> alacat.Pipeline:
    try:
        return alacat.Pipeline.load( fn )
    except Exception as ex:
        raise forms.GeneralError( "Could not load the pipeline. It may have been deleted or created using a different version of the software." ) from ex
