"""
Reporting functions for Alacats.

These are a guide only, and this list may change.

The functions typically take a Pipeline and produce some sort of report.
"""
import sys
from dataclasses import dataclass
from functools import partial
from typing import Callable, Sequence, List, Dict, Union, FrozenSet, Set, Tuple, Optional
from itertools import chain

from mhelper import ansi, string_helper, utf_table, exception_helper, array_helper
from mhelper.string_helper import array_to_string

from alacat.pipeline import Pipeline
from alacat.model import EGrade, ScoresTable, Peptide, ESubsequence, Score, PeptideCollection, ColumnOverride, BadValue, EStage, Column, Qmenu, Qconcat, Qbrick

from hatmul import forms, master


__hatmul_renderer = master.Renderers.Simple()


class __ReportFunction:
    __slots__ = "function", "stage", "full_name", "short_name", "doc", "names"
    
    
    def __init__( self, function, stage: EStage, *, name = None, doc = None ):
        self.function = function
        self.stage = stage
        self.full_name = name or function.__name__
        self.short_name = "".join( x[0] if x not in ("protein", "proteins") else "P" for x in self.full_name.split( "_" ) )
        self.doc = doc or function.__doc__ or "Not documented"
        self.names = self.full_name, self.short_name
        
        assert not any( set.intersection( set( x.names ), set( self.names ) ) for x in available ), f"Name in use ({self.names}) in {available}"
    
    
    def __repr__( self ):
        return f"{self.names}"


class __ReportFunctionCollection( List[__ReportFunction] ):
    __slots__ = ()
    
    
    def __getitem__( self, item ) -> "__ReportFunction":
        for x in self:
            if item in x.names:
                return x
        
        raise KeyError( f"There is no report with the name {item!r}. Your options are: {', '.join( y for x in self for y in x.names )}" )


available = __ReportFunctionCollection()
"""
List of all available report functions.
See `__ReportFunctionCollection`.
"""


def __report( *args, **kwargs ):
    """
    Internal decorator that adds the function to the `available` list.
    
    :param s:   Stage of pipeline required before this report can be generated.
    """
    
    
    def dec( f ):
        available.append( __ReportFunction( f, *args, **kwargs ) )
        return f
    
    
    return dec


@__report( EStage.ALL )
def execution_summary_ansi( p: Pipeline ) -> str:
    """
    A short summary of the results from the various stages of execution of the pipeline.
    
    Format: ANSI
    """
    r = []
    r.append( ansi.BOLD + "===EXECUTION SUMMARY===" + ansi.RESET )
    r.append( "factories          : {}".format( __get_length( p.factories ) ) )
    r.append( "warnings           : {}".format( __get_length( p.warnings ) ) )
    r.append( "proteins           : {}".format( __get_length( p.proteins ) ) )
    r.append( "candidate_peptides : {}".format( __get_length( p.candidate_peptides ) ) )
    r.append( "peptide_scores     : {}".format( __get_length( p.peptide_scores ) ) )
    r.append( "selected_peptides  : {}".format( __get_length( p.selected_peptides ) ) )
    r.append( "candidate_qblocks  : {}".format( __get_length( p.candidate_qblocks ) ) )
    r.append( "qblock_scores      : {}".format( __get_length( p.qblock_scores ) ) )
    r.append( "candidate_menus    : {}".format( __get_length( p.candidate_menus ) ) )
    r.append( "alacat_scores      : {}".format( __get_length( p.qmenu_scores ) ) )
    r.append( "selected_menus     : {}".format( __get_length( p.selected_menus ) ) )
    r.append( "" )
    
    return "\n".join( r )


def __get_length( x : object ) -> object:
    if x is None:
        return "NA"
    
    if isinstance( x, ScoresTable ):
        return x.shape
    
    return x.__len__()




@__report( EStage.ALL )
def big_list_ansi( p: Pipeline, mode: str = "fpdqa" ) -> str:
    """
    A list of everything but the scores.
    """
    
    r = []
    
    for c in mode:
        if c == "f":
            for x in p.factories:
                s = __big_list_ansi__fs( True )
                r.append( f"{s} Factory: {x}" )
        elif c == "p":
            for x in p.proteins:
                s = __big_list_ansi__fs( True )
                r.append( f"{s} Protein: {x}" )
        elif c == "d":
            for x in p.candidate_peptides:
                if p.selected_peptides:
                    s = __big_list_ansi__fs( x in p.selected_peptides )
                else:
                    s = ""
                r.append( f"{s} Peptide: {x}" )
        elif c == "q":
            for x in p.candidate_qblocks:
                s = __big_list_ansi__fs( x in p.selected_qblocks )
                r.append( f"{s} Qbrick: {x}" )
        elif c == "a":
            for x in p.selected_menus:
                s = __big_list_ansi__fs( True )
                r.append( f"{s} Qmenu: {x}" )
                r.append( selected_menu_ansi( p ) )
        else:
            raise exception_helper.SwitchError( "mode[i]", c )
    
    return "\n".join( r )


def __big_list_ansi__fs( x ):
    if x:
        return "[X]"
    else:
        return "[ ]"


qbricks = __report( EStage.SCORE_QBRICKS, name = "qbricks" )( partial( big_list_ansi, mode = "q" ) )
digest = __report( EStage.MAKE_PEPTIDES, name = "digest" )( partial( big_list_ansi, mode = "d" ) )
peptides = __report( EStage.MAKE_PEPTIDES, name = "peptides" )( partial( big_list_ansi, mode = "d" ) )
alacats = __report( EStage.SELECT_QMENUS, name = "alacats" )( partial( big_list_ansi, mode = "a" ) )
proteins = __report( EStage.MAKE_PROTEINS, name = "proteins" )( partial( big_list_ansi, mode = "p" ) )


@__report( EStage.SELECT_QMENUS )
def selected_menu_ansi( p: Pipeline ) -> str:
    r = []
    
    for menu in p.selected_menus:
        assert isinstance(menu, Qmenu)
        
        for qconcat in menu.qconcats:
            assert isinstance(qconcat, Qconcat)
            r.append( "===ALACAT===\n" )
            
            for qbrick in qconcat.qbricks:
                assert isinstance(qbrick, Qbrick)
                
                for linked_peptide in qbrick.linked_peptides:
                    r.append( ansi.FORE_BLUE )
                    r.append(linked_peptide.n_link)
                    r.append( ansi.FORE_YELLOW )
                    r.append(linked_peptide.peptide.sequence)
                    r.append( ansi.FORE_CYAN )
                    r.append(linked_peptide.c_link)
                    r.append( ansi.RESET )
    
    return "".join( r )


@__report( EStage.MAKE_PROTEINS )
def selected_proteins_ansi( p: Pipeline ) -> str:
    """
    A list of the selected proteins, including their titles, accessions and sequences.
    
    Format: ANSI
    """
    self = p.proteins
    
    r = []
    
    for protein in self:
        r.append( f"{protein.accession} = {ansi.BOLD}{protein.title}{ansi.BOLD_OFF}" )
        r.append( protein.sequence )
        r.append( "" )
    
    r.append( f"{len( self )} proteins." )
    r.append( "" )
    
    return "\n".join( r )


@__report( EStage.MAKE_PEPTIDES )
def candidate_peptides_ansi( p: Pipeline ) -> str:
    """
    A list of the candidate peptides, sorted by protein.
    
    Format: ANSI
    """
    self = p.candidate_peptides
    r = []
    
    for protein, peptides in self.map().items():
        r.append( f"{protein.accession} = {ansi.BOLD}{protein.title}{ansi.BOLD_OFF}" )
        
        for peptide in peptides:
            r.append( str( peptide ) )
    
    r.append( "" )
    return "\n".join( r )


@__report( EStage.SCORE_PEPTIDES )
def peptide_scores_ansi( p: Pipeline ) -> str:
    """
    All candidate peptides and their scores, in long list format. 
    
    Format: ANSI 
    """
    self = p.peptide_scores
    
    r = []
    
    # noinspection PyTypeChecker
    tl = list( EGrade )
    
    tls = " ".join( f'{__map_symbol_to_ansi( (t, 0) )} ' for t in tl )
    r.append( f"{'PEPTIDE':<30} {tls}" )
    
    for peptide in self.subject_order:
        counter = self.get_subject_counts( peptide )
        tls = " ".join( f'{counter[t]:<4}' for t in tl )
        ps = string_helper.fix_width( peptide.sequence, 30, trim = 0 )
        r.append( f"{ps} {tls}" )
    
    return "\n".join( r )


@__report( EStage.SCORE_PEPTIDES )
def peptide_scores_flags_ansi( p: Pipeline ) -> str:
    """
    A table showing the flag (red/amber/green) for all peptides and all scores.
    
    Format: TSV
    """
    self = p.peptide_scores
    r = []
    
    r.append( "\t".join( str( x ) for x in chain( ["PEPTIDE"],
                                                  self.columns,
                                                  ["TOTAL_GREEN"] ) ) )
    
    for peptide, passes in zip( self.by_subject, self.matrix ):
        r.append( "\t".join( str( x ) for x in chain( [peptide],
                                                      (pass_.grade for pass_ in passes),
                                                      [sum( pass_.grade == EGrade.PASS for pass_ in passes )] ) ) )
    
    r.append( "" )
    
    return "\n".join( r )


@dataclass
class __ScoredPeptide:
    """
    Uses by the `peptide_scores_*` functions, describes a peptide and its
    scores.
    """
    peptide: Peptide
    scores: Dict[ColumnOverride, Score[Peptide]]
    rank: int
    filter: bool
    your_ref: Optional[object]
    my_ref: bool


_UPeptide = Union[Peptide, str]
_TSelect = Union[None,
                 Set[_UPeptide],
                 FrozenSet[_UPeptide],
                 Dict[_UPeptide, object],
                 List[_UPeptide],
                 Tuple[_UPeptide, ...]]


# noinspection PyPackageRequirements
@__report( EStage.SCORE_PEPTIDES )
def peptide_scores_pandas( *args, **kwargs ):
    """
    A table showing the value for all peptides and all scores.
    
    Format: Pandas
    
    :param args:        See `peptide_scores_tsv`.
    :param kwargs:      See `peptide_scores_tsv`. 
    :return:            Pandas dataframe.
    """
    tsv = peptide_scores_tsv( *args, **kwargs )
    
    import pandas
    from io import StringIO
    
    with StringIO( tsv ) as fin:
        df = pandas.read_csv( fin, sep = "\t", header = 0, index_col = 0 )
    
    return df


@__report( EStage.SCORE_PEPTIDES )
def peptide_scores_tsv( p: Pipeline,
                        *,
                        mode: int = 3,
                        select: _TSelect = None,
                        value_formatter: Callable[[object], str] = None,
                        filter: Sequence[Callable[[__ScoredPeptide], bool]] = (),
                        filter_column: bool = False ) -> str:
    """
    A table showing the value for all peptides and all scores.
    
    Format: TSV
    
    :param p:               Pipeline 
    :param mode:            `1` --> Value column only
                            `2` --> Grade column only
                            `3` --> Separate value and grade columns
                            `4` --> Combined value and grade column 
    :param select:          Defines a column "your_ref".
                            `set` --> (your_ref is boolean in/out)
                            `list` --> (your_ref is 1-offset index or blank)
                            `dict` --> (your ref is lookup or blank)
                            Peptides may be listed as a `Peptide` or a string.
                            If they are a string, and they are *not* in the
                            results, they will be listed at the *end* of the
                            table with the comment: "missing".
    :param value_formatter: How to format values.
                            `None` --> Uses a simple formatter which excludes
                                       error messages and missing data.
                            `repr` --> displays all values
                            ...    --> ...
    :param filter:          A tuple of one or more filters on displayed
                            peptides.
                            If you attempt to filter out a value in `select` an
                            error is raised.
    :param filter_column:   When set, includes a column for the `filter`, rather
                            than stripping results from the table.
    :return:                The report. 
    """
    if value_formatter is None:
        value_formatter = __peptide_scores_tsv__simple_format
    
    PEPTIDE_COL_NAME = "peptide"
    YOUR_REF_COL_NAME = "your_ref"
    SELECTED_COL_NAME = "my_ref"
    FILTER_COL_NAME = "filter"
    RANK_COL_NAME = "rank"
    PROT_AC_COL_NAME = "prot_accession"
    PROT_TITLE_COL_NAME = "prot_name"
    FLAGS_COL_NAME = "{}_flags"
    OWNER_COL_NAME = "refs"
    
    OWNER_BOTH = "BOTH"
    OWNER_ME = "MINE"
    OWNER_YOU = "YOURS"
    OWNER_MISSING = "MISSING"
    
    scores = p.peptide_scores
    keys: Tuple[Column, ...] = scores.columns
    r = []
    
    pep_ranks = p.peptide_scores.subject_to_rank
    your_ref = __YourRef( select, p.candidate_peptides )
    
    columns = []
    
    # Column: Protein accession, title
    columns.append( (PROT_AC_COL_NAME, lambda sp: str( sp.peptide.protein.accession )) )
    columns.append( (PROT_TITLE_COL_NAME, lambda sp: str( sp.peptide.protein.title )) )
    
    # Column: Peptide sequence
    columns.append( (PEPTIDE_COL_NAME, lambda sp: sp.peptide.sequence) )
    
    # Column: Your reference
    if your_ref.any:
        columns.append( (YOUR_REF_COL_NAME, lambda sp: str( sp.your_ref ) if sp.your_ref is not None else "") )
    
    # Column: My reference
    columns.append( (SELECTED_COL_NAME, lambda sp: "1" if sp.my_ref else "0") )
    
    # Column: Your filter
    if filter and filter_column:
        columns.append( (FILTER_COL_NAME, lambda sp: "" if sp.filter else "0") )
    
    # Column: Who references
    if your_ref.any:
        columns.append( (OWNER_COL_NAME, lambda sp:
        (OWNER_BOTH if sp.peptide in your_ref.your_refs else OWNER_ME)
        if sp.peptide in p.selected_peptides else
        (OWNER_YOU if sp.peptide in your_ref.your_refs else "")) )
    
    # Column: Overall rank
    columns.append( (RANK_COL_NAME, lambda sp: sp.rank) )
    
    # Columns: Grade counts
    for tl in EGrade:
        columns.append( (FLAGS_COL_NAME.format( tl.name ), (lambda tl_:
                                                            lambda sp: scores.get_subject_counts( sp.peptide ).get( tl_, "" )
                                                            )( tl )) )
    
    # Column: Scores
    for key in keys:
        exception_helper.safe_cast( "key", key, Column )
        if mode == 4:
            # ... combined value and grade column
            columns.append( (f"{key.__accession}", (lambda key_:
                                                    lambda sp:
                                                    f"({sp.scores[key_].grade.name[0]}) {value_formatter( sp.scores[key_].value )}"
                                                    )( key )) )
        else:
            # ... value column
            if mode & 1:
                columns.append( (f"{key.__accession}", (lambda key_:
                                                        lambda sp:
                                                        value_formatter( sp.scores[key_].value )
                                                        )( key )) )
            # ... grade column
            if mode & 2:
                columns.append( (f"{key.__accession}.grade", (lambda key_:
                                                              lambda sp: f"{sp.scores[key_].grade.name[0]}"
                                                              )( key )) )
    
    r.append( '\t'.join( column_name for column_name, column_fn in columns ) )
    
    subjects = scores.subject_order
    
    skipped = 0
    
    for subject in subjects:
        sub_scores = scores.by_subject[subject]
        sp = __ScoredPeptide( peptide = subject,
                              scores = sub_scores,
                              rank = pep_ranks[subject],
                              filter = True,  # set below
                              your_ref = your_ref.your_refs.get( subject, None ),
                              my_ref = subject in p.selected_peptides )
        
        if not all( f( sp ) for f in filter ):
            if sp in your_ref.select:
                raise ValueError( "A selected value has been filtered out. This is probably a mistake." )
            
            if filter_column:
                sp.filter = False
            else:
                skipped += 1
                continue
        
        r.append( "\t".join( str( column_fn( sp ) ) for column_name, column_fn in columns ) )
    
    for missing_seq, missing_key in your_ref.missing_refs.items():
        r.append( "\t".join( missing_seq if column_name == PEPTIDE_COL_NAME else
                             missing_key if column_name == YOUR_REF_COL_NAME else
                             OWNER_MISSING if column_name == OWNER_COL_NAME else
                             ""
                             for column_name, column_fn in columns ) )
    
    if skipped:
        r.append( f"# ...and {skipped} others not shown." )
    
    txt = "\n".join( r )
    
    return txt


def __peptide_scores_tsv__simple_format( value: object ) -> object:
    """
    Used to exclude missing data and error messages from the scores output.
    """
    if value is None or isinstance( value, BadValue ):
        return ""
    elif value is True:
        return "1"
    elif value is False:
        return "0"
    else:
        return str( value )


@__report( EStage.SCORE_PEPTIDES )
def peptide_scores_html( p: Pipeline,
                         select: _TSelect
                         ) -> str:
    your_ref = __YourRef( select, p.candidate_peptides )
    all_peptides = set( p.candidate_peptides )
    your_peptides = set( your_ref.your_refs )
    my_peptides = set( p.selected_peptides )
    both_peptides = set.intersection( your_peptides, my_peptides )
    my_peptides_only = my_peptides - your_peptides
    your_peptides_only = your_peptides - my_peptides
    page = forms.Page()
    
    rep = forms.Report( "Summary" )
    sec = forms.Section( "" )
    sec.append( f"Out of {len( all_peptides )} peptides, you chose {len( your_ref.select )} and I chose {len( my_peptides )}." )
    rep.append( sec )
    page.append( rep )
    
    for peptide in both_peptides:
        report = __scores_report( p, (peptide,), "Our choice", ("Our choice",), "We both chose this peptide." )
        page.append( report )
    
    while True:
        top = __peptide_scores_html__pick_top( p, my_peptides_only, your_peptides_only )
        
        if top is None:
            break
        
        ranks = { p.peptide_scores.subject_to_rank[peptide] for peptide in top }
        
        if len( ranks ) == 1:
            message = "I have given these peptides the same rank; there is no reason I didn't choose the same as you."
        else:
            message = "I have given these peptides different ranks; I selected the lower ranking peptide."
        
        report = __scores_report( p, top, "Comparison", ("My choice", "Your choice"), message )
        page.append( report )
    
    for peptide in your_peptides_only:
        report = __scores_report( p, (peptide,), "Your choice", ("Your choice",), "Only you chose this peptide because you selected more peptides than me." )
        page.append( report )
    
    for peptide in your_ref.missing_refs:
        report = forms.Report( f"Your choice (?) - {peptide}" )
        report.append( forms.Section( "", "I didn't come across this peptide in my search. Check that this is a real peptide and that you have told me the correct enzyme." ) )
        print( f"MISSING: {peptide}", file = sys.stderr )
    
    for peptide in my_peptides_only:
        report = __scores_report( p, (peptide,), "My choice", ("My choice",), "Only I chose this peptide because I selected more peptides than you." )
        page.append( report )
    
    # Warnings
    if p.warnings:
        report = forms.Report( "Warnings" )
        page.append( report )
        
        section = forms.Section()
        report.append( section )
        
        ul = forms.UnorderedList()
        section.append( ul )
        
        for error in p.warnings:
            if hasattr( error, "_repr_html_" ):
                txt = forms.ListItem( error._repr_html_() )
            else:
                txt = forms.ListItem( "<pre>{}</pre>".format( forms.escape( error ) ) )
            
            ul.append( txt )
    
    return __hatmul_renderer.render( page )


def __peptide_scores_html__pick_top( p: Pipeline, s1: Set[Peptide], s2: Set[Peptide] ) -> Optional[Tuple[Peptide, Peptide]]:
    p1 = array_helper.first_or_none( x for x in p.peptide_scores.subject_order if x in s1 )
    p2 = array_helper.first_or_none( x for x in p.peptide_scores.subject_order if x in s2 )
    
    if p1 is None or p2 is None:
        return None
    
    s1.remove( p1 )
    s2.remove( p2 )
    
    return p1, p2


class __YourRef:
    def __init__( self, select: _TSelect, candidate_peptides: PeptideCollection ):
        self.select: _TSelect = select
        self.candidate_peptides: PeptideCollection = candidate_peptides
        self.your_refs: Dict[Peptide, str] = { }
        self.missing_refs: Dict[str, str] = { }
        
        if select is None:
            pass
        elif isinstance( select, dict ):
            for k, v in select.items():
                self.add( k, v )
        elif isinstance( select, set ) or isinstance( select, frozenset ):
            for k in select:
                self.add( k, "1" )
        elif isinstance( select, list ) or isinstance( select, tuple ):
            for i, k in enumerate( select ):
                self.add( k, f"#{i}" )
        else:
            raise exception_helper.type_error( "select", select, Union[None, dict, set, frozenset, list, tuple] )
        
        self.any: bool = bool( select )
    
    
    def add( self, sequence: _UPeptide, key: str ) -> Optional[Peptide]:
        if isinstance( sequence, str ):
            try:
                peptide = self.candidate_peptides.find( (sequence,) )[0]
            except KeyError:
                self.missing_refs[sequence] = key
                peptide = None
            else:
                self.your_refs[peptide] = key
            
            return peptide
        elif isinstance( sequence, Peptide ):
            return sequence
        else:
            raise exception_helper.type_error( "sequence", sequence, Union[Peptide, str] )


@__report( EStage.SCORE_PEPTIDES )
def selected_peptide_ansi( p: Pipeline ) -> str:
    """
    Shows information on the selected peptides, their scores, flags and descriptions.
    
    Format: ANSI
    """
    self = p.peptide_scores
    subjects = p.selected_peptides
    peptides = subjects
    
    r = []
    
    for peptide in peptides:
        r.append( "Peptide:" )
        r.append( ansi.BOLD + peptide.sequence + ansi.RESET )
        r.append( "" )
        
        r.append( "Characteristics:" )
        r.append( __selected_peptide_ansi__characteristics( self, peptide ) )
        r.append( "" )
        
        r.append( "Summary:" )
        r.append( __selected_peptide_ansi__descriptive( self, peptide ) )
        
        r.append( "" )
        r.append( __selected_peptide_ansi__symbols( self, peptide ) )
        
        r.append( "" )
    
    return "\n".join( r )


def __selected_peptide_ansi__characteristics( self: ScoresTable[Peptide], peptide: Peptide ) -> str:
    peptide_index = self.subjects.index( peptide )
    peptide_scores = self.by_subject[peptide].values()
    filters_passed = self.matrix[peptide_index]
    passes = { }
    
    for passed, key in zip( filters_passed, self.columns ):
        passes[key] = passed
    
    tbl = [None, ["PASSED", "SCORE", "VALUE"], None]
    for score in peptide_scores:
        passed = passes[score.column]
        passed_str = "" if passed else "FAIL"
        
        if isinstance( score.value, BadValue ):
            val_str = f"(NA -- {score.value})"
        else:
            val_str = repr( score.value )
        
        tbl.append( [passed_str, score.column, val_str] )
    
    tbl.append( None )
    return utf_table.TextTable.from_table( tbl ).to_string()


def __selected_peptide_ansi__descriptive( self: ScoresTable[Peptide], peptide: Peptide ) -> str:
    rx = []
    
    # rx.append( f"    {'RULE':<30} TRL DESCRIPTION" )
    
    my_results = self.by_subject[peptide].values()
    so = None
    for result in my_results:
        nso = result.score.origin.__class__.__name__
        
        if nso != so:
            rx.append( f"{nso:<34} {ansi.FORE_BRIGHT_BLACK}TRL DESCRIPTION{ansi.RESET}" )
            so = nso
        
        nm = f"{result.score.column}"
        
        rx.append( f"    {nm:<30} {__map_symbol_to_ansi( (result.grade, result.column.priority) )} {result.message}" )
    
    txt2 = "\n".join( rx )
    return txt2


def __selected_peptide_ansi__symbols( self: ScoresTable[Peptide], peptide: Peptide ) -> str:
    counter = self.get_subject_counts( peptide )
    r = []
    r.append( f"GREEN lights: {counter[EGrade.PASS]}" )
    r.append( f"RED lights  : {counter[EGrade.FAIL]}" )
    r.append( f"Errors      : {counter[EGrade.ERROR]}" )
    r.append( f"Comments    : {counter[EGrade.INFO]}" )
    
    return "\n".join( r )


@__report( EStage.NONE )
def warnings_ansi( p: Pipeline ) -> str:
    """
    Warnings, in ANSI format.
    """
    r = []
    
    r.append( ansi.BOLD + "===WARNINGS===" + ansi.RESET )
    
    if p.warnings:
        r.append( "The workflow has completed with the following warnings:" )
        r.append( "" )
        
        for i, warning in enumerate( p.warnings ):
            r.append( string_helper.bullet( f"{i}. ", str( warning ) ) )
        
        r.append( "" )
        r.append( "Please check your configuration:" )
        r.append( f" * Model: {p.model}" )
    else:
        r.append( "The workflow has completed with no errors or warnings." )
    
    return "\n".join( r )


__TMap = Dict[Union[
                  EGrade,
                  Tuple[EGrade,
                        object]],
              str]

__k_map_symbol_to_ansi: __TMap = { EGrade.PASS     : "(" + ansi.BK + ansi.FBG + "●" + ansi.X + ")",
                                   (EGrade.FAIL, 0): "(" + ansi.BK + ansi.FBR + "●" + ansi.X + ")",  # RED - priority 0
                                   EGrade.FAIL     : "(" + ansi.BK + ansi.FBY + "●" + ansi.X + ")",  # AMBER - any other priority
                                   EGrade.INFO     : "(" + ansi.BK + ansi.FBK + "●" + ansi.X + ")",
                                   EGrade.ERROR    : "(" + ansi.BK + ansi.FBM + "▲" + ansi.X + ")" }

__TMapSymbol = Union[Score, Tuple[EGrade, int]]


def __map_symbol( m: __TMap,
                  s: __TMapSymbol ) -> str:
    if isinstance( s, Score ):
        s = (s.grade, s.column.priority)
    
    r = m.get( s )
    
    if r is not None:
        return r
    
    return m[s[0]]


def __map_symbol_to_ansi( s: __TMapSymbol ):
    return __map_symbol( __k_map_symbol_to_ansi, s )
