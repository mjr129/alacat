from typing import List, Sequence, Set, Type
import os

import alacat


def main():
    alacat.standard_logs.attach_to_console()
    file_name = os.path.join( alacat.paths.get_data_root(), "evaluation_of_k562_peptides/results2022.pickle" )
    os.makedirs( os.path.dirname( file_name ), exist_ok = True )
    results: alacat.Pipeline

    if os.path.isfile( file_name ):
        print( "Already exists" )
    else:
        in_file_name : str = "/home/mjr/mnt/exdata/k562/seamass-2021-09-10/list_of_proteins_with_peptides_in_peptidoform.txt"

        with open(in_file_name) as fin:
            PROTEIN_LIST_STR: str = ",".join(x.strip() for x in fin.readlines())

        PROTEIN_LIST: List[str] = PROTEIN_LIST_STR.split( "," )
        WITHDRAW_HANDLERS: Set[Type[alacat.Handler]] = { alacat.GoogleSearchScorer, alacat.YolandaScorer, alacat.DoesItFlyScorer }

        orig_handlers: Sequence[alacat.HandlerFactory] = alacat.HandlerFactoryCollection.create_defaults()
        handlers: Sequence[alacat.HandlerFactory] = [x for x in orig_handlers if x.klass not in WITHDRAW_HANDLERS]
        print( f"{len( orig_handlers )} --> {len( handlers )} handlers" )

        model: alacat.Model = alacat.Model.explicit_init( name = "All K562 SeaMass proteins 2022",
                                                          organisms = [9606],
                                                          digestion = alacat.EDigestion.TRYPSIN,
                                                          handlers = handlers,
                                                          proteins = PROTEIN_LIST,
                                                          peptides = (),
                                                          qblocks = (),
                                                          qmenus = (),
                                                          qbrick_size = 2,
                                                          alacat_size = 100,
                                                          stage = alacat.EStage.SCORE_PEPTIDES,
                                                          mandate = False,
                                                          partition_rankings = True,
                                                          random_seed = 1,
                                                          min_peptide_length = 5,
                                                          sql_backing = True,
                                                          qblock_size = 1,
                                                          fix_accessions = False)

        results = model.exact()
        results.save( file_name )
        print( f"Saved to: {file_name}" )


if __name__ == "__main__":
    main()
