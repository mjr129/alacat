"""
This module hooks in to the ALACAT Pipeline and transforms it into an SQL-backed
database - rather than calculating or downloading values every time, values are
stored and retrieved from the database.

Only if the database has never seen the data before are the new values
calculated.

The backing feature can be disabled by the `Model`.
If MySQL is not available, this feature is always disabled and a warning is
issued if it is requested.
"""
import datetime
from itertools import chain
import warnings
import os
from alacat.model import Handler, Entity, RawScore, ModelState, AnyScoreType, Pending, HColumn, NamedFactor, \
    DbData, debugging, ESqlBacking, EGrade, PipelineError
from typing import Dict, List, Optional, Sequence, Set, Tuple, Union
from mhelper import exception_helper
from mhelper.exception_helper import safe_cast

from mhelper import sql_helper, specific_locations, io_helper, string_helper
from mhelper.sql_helper import MySQLdb

dblog = debugging.backingdb_log
AnyEntity = Union[Entity, Pending]


class AlacatBacking:
    """
    Manages the database for a single run of the pipeline.
    
    The connection is upheld for the lifetime of the workflow, at which point
    `close` should be called.
    
    The `AlacatBacking` is responsible for deciding whether or not to cache
    values to the database and an `AlacatBacking` instance always exists for the
    pipeline, even if it is not connected.
    
    See module comments for details.
    
    The database has three tables:
    
    Entities
    --------
    
    Maps entity database IDs to JSON and hashes.
    
    * The json is deterministic and can be used to reconstruct the entity.
    * The hash can be calculated from the JSON.
    
    Retrieval:
    
        hash --LOOKUP--> json --PARSE--> entity
        
    Storage:
    
        entity --PARSE--> json --STORE--> hash+json
        
    Entities include "row-values" - proteins, peptides, qbricks, etc, as well as "column-values" - handlers, columns, etc.
    
    Scores
    ------
    
    Tabulates scores for row-values against the column-values.
    
    Presence
    --------
    
    Indicates whether scores for row-values against the column-values exist.
    This is needed because they might exist, there may just not be any values in `scores`.
    
    Users
    -----
    
    An additional table, "users" is maintained in the same database, that holds the web interface users.
    This is unrelated from the actual backing store.
    """
    __slots__ = "__mode", "__db_name", "__state", "__database", "connection"

    def __init__( self, state: Optional[ModelState], mode: ESqlBacking ):
        """
        :param state:   The pipeline to which we shall attach. 
        """
        assert not state or mode != ESqlBacking.NONE, "Cannot construct with mode NONE when a state is specified."

        self.__mode = mode
        self.__db_name = self.__get_database_name()
        self.__state: Optional[ModelState] = state
        self.__database = sql_helper.Remote( user_name = "alacat",
                                             password_key = ("alacat", "dbpassword"),
                                             db_name = self.__db_name )

        assert not state or state.model.sql_backing, "This is probably a mistake."

        if not self.__database.db_exists():
            self.__initialise_database()

        self.connection: MySQLdb._mysql.connection = self.__database.connect().__enter__()

    def get_db_directory( self ) -> str:
        return self.__database.get_directory()

    @staticmethod
    def __get_database_name():
        db_name_file_name = specific_locations.get_application_file_name( "rusilowicz", "alacat", "db_name.txt" )

        if os.path.isfile( db_name_file_name ):
            db_name = io_helper.read_all_text( db_name_file_name ).strip()
        else:
            random = string_helper.get_random_varname( 8 )
            db_name = f"alacat_backing_{random}"
            io_helper.write_all_text( db_name_file_name, db_name )
            warnings.warn( f"The database name is not specified at {db_name_file_name} so a new database name, {db_name}, has been generated for you. Please edit the file if you would like to change it." )

        return db_name

    def close( self ):
        """
        Closes the connection to MySQL.
        """
        if self.connection:
            self.connection.close()

    def __initialise_database( self ):
        """
        Constructs the database and tables if they do not already exist. 
        """

        self.__database.create_database()

        with self.__database.connect() as connection:
            with connection.cursor() as cursor:
                src = f"""
USE {self.__db_name};

create table presence
(
    id int AUTO_INCREMENT,
    input_id int not null,
    handler_id int not null,
    updated DATETIME not null,
    constraint presence_pk
        primary key (id),
    constraint presence_2col_key
        unique (input_id, handler_id)
);

CREATE TABLE entities
(
    id INT AUTO_INCREMENT,
    accession BINARY(32) NOT NULL,
    data TEXT NOT NULL,
    updated DATETIME NOT NULL,
    CONSTRAINT entities_pk
        PRIMARY KEY (id),
	CONSTRAINT entities_accession_key
        UNIQUE (accession)
);

create unique index entities_accession_uindex
    on entities (accession);

create unique index entities_id_uindex
    on entities (id);

create table scores
(
    id int auto_increment,
    input_id int not null,
    output_id int not null,
    column_id int not null,
    handler_id int not null,
    value text not null,
    constraint scores_pk
        primary key (id),
    constraint scores_input_id_fk
        foreign key (input_id) references entities (`id`),
    constraint scores_output_id_fk
        foreign key (output_id) references entities (`id`),
    constraint scores_column_id_fk
        foreign key (column_id) references entities (`id`),
    constraint scores_handler_id_fk
        foreign key (handler_id) references entities (`id`),
	constraint scores_4col_key
        unique (input_id, output_id, column_id, handler_id)
);

create unique index scores_4col_uindex
    on scores (input_id, output_id, column_id, handler_id);

create unique index scores_id_uindex
    on scores (id);
    """

                cursor: MySQLdb.cursors.Cursor
                cursor.execute( src, [] )

            connection.commit()

    def save( self,
              handler: Handler,
              inputs: Sequence[AnyEntity],
              exclude: Set[AnyEntity],
              scores: Sequence[RawScore]
              ) -> None:
        """
        Saves or updates a set of scores to the backing store (if enabled).
        
        :param handler:     Handler the scores are from
        :param inputs:      The things that were queried.
        :param exclude:     A set of `inputs` to exclude from being stored
        :param scores:      Scores to save
        """
        if not self.__database:
            return

        if self.__mode not in (ESqlBacking.WRITE, ESqlBacking.READ_WRITE):
            return

        with dblog.action( "SAVE {}".format( handler.__class__.__name__ ) ):
            #
            # Create the list of entities we'll reference
            #
            input_ents: Set[Entity] = { self.__extract_entity( input ) for input in inputs }
            outputs: Set[Entity] = set( score.subject for score in scores )
            columns: Set[Entity] = { x for x in handler.bound_columns if not x.is_derived }

            # Filter based on `exclude`
            dblog( "Before {} input, {} score", len( input_ents ), len( scores ) )
            input_ents -= { self.__extract_entity( input ) for input in exclude }
            scores = [score for score in scores if score.input not in exclude]
            dblog( "Filtered {}, {}", len( input_ents ), len( scores ) )

            if not input_ents:
                return

            assert all( score.origin is handler for score in scores )
            assert all( x.column.specification in columns for x in scores )

            #
            # Map the entities to DB IDs 
            #
            all_ents: Sequence[Entity] = tuple( chain( input_ents, outputs, [handler], columns ) )
            mapping = self.__map_entities( all_ents, create = True )
            updated = datetime.datetime.now().strftime( '%Y-%m-%d %H:%M:%S' )
            handler_id = mapping[handler]

            with dblog.time( f"Saving {len( scores )} scores to DB." ):
                with self.connection.cursor() as cursor:
                    statements = []
                    arguments = []

                    for input in input_ents:
                        input_id = mapping[input]

                        statements.append(
                                """REPLACE INTO `presence` (`input_id`, `handler_id`, `updated`) VALUES (%s, %s, %s);""" )
                        arguments.extend( (input_id, handler_id, updated) )

                    cursor.execute( "\n".join( statements ), arguments )

                    if scores:
                        statements = []
                        arguments = []

                        for score in scores:
                            output_id = mapping[score.subject]
                            input_id = mapping[self.__extract_entity( score.input )]
                            column_id = mapping[score.column.specification]
                            value = self.__value_to_string( score.value )

                            assert isinstance( input_id, int ) and input_id
                            assert isinstance( output_id, int ) and output_id
                            assert isinstance( column_id, int ) and column_id
                            assert isinstance( handler_id, int ) and handler_id

                            statements.append(
                                    """REPLACE INTO `scores` (`input_id`, `output_id`, `column_id`, `handler_id`, `value`) VALUES (%s, %s, %s, %s, %s);""" )
                            arguments.extend( (input_id, output_id, column_id, handler_id, value) )

                        cursor.execute( "\n".join( statements ), arguments )

                self.connection.commit()

    def __extract_entity( self, unknown: AnyEntity ) -> Entity:
        if isinstance( unknown, Pending ):
            return exception_helper.safe_cast( "unknown.input", unknown.input, Entity )
        elif isinstance( unknown, Entity ):
            return unknown
        else:
            raise exception_helper.type_error( "unknown", unknown, [Pending, Entity] )

    def __map_entities( self,
                        entities: Sequence[Entity],
                        create: bool = False,
                        check: bool = False
                        ) -> Dict[Entity, int]:
        """
        Maps entities to their database IDs.
        
        :param entities:    Entities to query 
        :param create:      Whether to create records if they do not exist 
        :param check:       Whether to raise an error if they do not exist
        :return:            Mapping from entities to IDs. 
        """
        cursor: MySQLdb.cursors.Cursor

        a2e = { self.__entity_to_accession( entity ): entity for entity in entities }
        e2i = { entity: None for entity in entities }

        with dblog.time( f"Mapping {len( entities )} entities from DB." ):
            with self.connection.cursor() as cursor:
                cursor.execute(
                        """
                        SELECT `entities`.`accession`,
                               `entities`.`id`
                        FROM `entities`
                        WHERE `accession` IN %s
                        """, [list( a2e )] )

                for accession, id in cursor:
                    e = a2e[accession]
                    e2i[e] = id

        if create or check:
            updated = datetime.datetime.now().strftime( '%Y-%m-%d %H:%M:%S' )

            to_create = [entity for entity, id in e2i.items() if not id]

            if to_create:
                if check:
                    raise RuntimeError( "Expected to exist." )

                with dblog.time( f"Creating {len( entities )} new entities in DB." ):
                    statements = []
                    arguments = []

                    for entity in to_create:
                        ac = self.__entity_to_accession( entity )
                        cm = self.__entity_to_data( entity )

                        statements.append(
                                "INSERT IGNORE INTO `entities` (`accession`, `data`, `updated`) VALUES (%s, %s, %s);" )
                        arguments.extend( (ac, cm, updated) )

                    with self.connection.cursor() as cursor:
                        cursor.execute( "\n".join( statements ), arguments )

                    self.connection.commit()

                    extra = self.__map_entities( to_create )
                    e2i.update( extra )

        return e2i

    def load( self,
              handler: Handler,
              subjects: Sequence[AnyEntity]
              ) -> Tuple[Sequence[RawScore], Sequence[AnyEntity]]:
        """
        Loads a set of scores from the backing store (if enabled).
        
        :param handler:     Handler who's scores should be loaded 
        :param subjects:    Subjects who's scores should be loaded 
        :return:            The result is a tuple of:
        
                                * The retrieved scores
                                * The `subjects` for which we have *not* been
                                  able to load scores (i.e. subjects that still
                                  need scoring because they are not in the
                                  backing store).
                                  
                            If the backing store is not enabled, the result will
                            naturally be a tuple of zero scores and all
                            subjects.
        """
        if not self.__database:
            return (), subjects

        if self.__mode not in (ESqlBacking.READ, ESqlBacking.READ_WRITE):
            return (), subjects

        with dblog.time( f"LOAD {handler.__class__.__name__}" ):
            dblog( "Input, n = {}", len( subjects ) )

            #
            # Unwrap any `Pending` (if present), we want the actual `Entity`
            #
            input_ents: Sequence[Entity]
            e_to_orig: Optional[Dict[Entity, Pending]]

            if isinstance( subjects[0], Pending ):
                e_to_orig = { subject.input: subject for subject in subjects }
                input_ents = list( e_to_orig.keys() )
            else:
                e_to_orig = None
                input_ents = subjects

            #
            # Map our subjects, handler and columns to Database IDs
            #
            columns = [x for x in handler.bound_columns.keys() if not x.is_derived]
            all_ents: Sequence[Entity] = tuple( chain( input_ents,
                                                       [handler],
                                                       columns ) )
            e_to_db: Dict[Entity, int] = self.__map_entities( all_ents )
            db_to_e: Dict[int, Entity] = { v: k for k, v in e_to_db.items() }

            #
            # Filter the subjects
            # Only include the subjects the database actually has knowledge of
            #
            handler_id = e_to_db[handler]

            if handler_id is None:
                dblog( "Never seen this handler before." )
                return (), subjects

            input_ids = [e_to_db[input] for input in input_ents]
            input_ids = [x for x in input_ids if x is not None]

            if not input_ids:
                dblog( "Never seen any of these subjects before." )
                return (), subjects

            dblog( "With IDs, n = {}", len( subjects ) )

            column_ids = [e_to_db[column] for column in columns]

            if not all( column_ids ):
                # Will need to rerun anyway, so no point in retrieving results for the columns we have seen
                dblog( "Not seen at least one column before." )
                return (), subjects

            with dblog.time( f"Checking presence matrix" ):
                with self.connection.cursor() as cursor:
                    cursor: MySQLdb.cursors.Cursor
                    cursor.execute( """
                        SELECT `presence`.`input_id`
                        FROM `presence`
                        WHERE `presence`.`handler_id` = %s
                        AND `presence`.`input_id` IN %s""", [handler_id, input_ids] )

                    present_ids = [x[0] for x in cursor]

            dblog( "Present, n = {}", len( present_ids ) )

            #
            # Find all the things!
            #
            r: List[RawScore] = []  # Results

            if present_ids:
                resolved: Set[AnyEntity] = set()  # For info only

                with dblog.time(
                        f"Loading {handler.accession!r} scores for {len( present_ids )} subjects from DB." ) as log_time:
                    with self.connection.cursor() as cursor:
                        cursor: MySQLdb.cursors.Cursor
                        cursor.execute( """
                        SELECT `scores`.`input_id`,
                               `scores`.`column_id`,
                               `scores`.`value`,
                               `entities`.`data` AS `output_str`
                        FROM `scores`
                        LEFT JOIN
                            `entities` ON `entities`.`id` = `scores`.`output_id` 
                        WHERE `scores`.`input_id` IN %s
                        AND `scores`.`column_id` IN %s
                        AND `scores`.`handler_id` = %s  
                        
                        """, [present_ids, column_ids, handler_id] )

                        dblog( f"Cursor {cursor.rowcount} rows" )

                        for input_id, column_id, value_str, output_str in cursor:
                            input_inst = safe_cast( "input", db_to_e[input_id], Entity )
                            column = safe_cast( "column", db_to_e[column_id], HColumn )
                            value = self.__string_to_value( value_str )
                            output = self.__data_to_entity( output_str )

                            if e_to_orig is not None:
                                pending = e_to_orig[input_inst]
                            else:
                                pending = input_inst

                            r.append( handler._score( value, column, output, pending ) )

                    log_time.result = f"{len( resolved )} hits"

            #
            # Calculate remaining
            #
            found = set()

            for present_id in present_ids:
                present_entity = db_to_e[present_id]

                if e_to_orig is not None:
                    pending = e_to_orig[present_entity]
                else:
                    pending = present_entity

                found.add( pending )

            remaining = list( set( subjects ) - found )
            dblog( "{} scores, {} inputs remaining", len( r ), len( remaining ) )

            return r, remaining

    def __value_to_string( self, value: AnyScoreType ) -> str:
        """
        Converts a value from a score into the string we will store in the database.
        """
        # int, float, str, bool, "NamedFactor", "Rank", Enum
        if isinstance( value, bool ):
            return RawScore.VALUE_TO_STRING_MAP[value]
        elif isinstance( value, int ):
            return f"I:{value}"
        elif isinstance( value, float ):
            return f"F:{value}"
        elif isinstance( value, str ):
            return f"S:{value}"
        elif isinstance( value, NamedFactor ):
            return f"L:{value.code}:{value.name}:{value.grade.name}"

        return RawScore.VALUE_TO_STRING_MAP[value]

    def __string_to_value( self, value: str ) -> AnyScoreType:
        """
        Converts a value from the database into it's original value suitable for use as a score.
        """
        r = RawScore.STRING_TO_VALUE_MAP.get( value )

        if r is not None:
            return r

        ct: str
        pfx, ct = value.split( ":", 1 )

        if pfx == "I":
            return int( ct )
        elif pfx == "F":
            return float( ct )
        elif pfx == "S":
            return value[2:]
        elif pfx == "L":
            if ct.count( ":" ) == 1:
                # Old style
                name, grade = ct.split( ":", 1 )
                code = name
            else:
                # New style
                code, name, grade = ct.split( ":", 2 )

            return NamedFactor( code, name, EGrade[grade] )
        else:
            raise PipelineError( "A string-serialised value in the AlacatDesigner backing store cannot be converted back to its original value. This may be due to presence of obsolete data in the database, if so try running the model in SQL write-only mode." )

    def __entity_to_accession( self, value: Entity ) -> bytes:
        """
        Gets the *database accession* for an entity.
        """
        return value.to_dbdata().to_hash()

    def __entity_to_data( self, entity: Entity ) -> str:
        """
        Gets the data required to restore an entity. 
        """
        return entity.to_dbdata().to_json()

    def __data_to_entity( self, value: str ) -> Entity:
        """
        Restores an entity from its data.
        """
        db_data = DbData.from_json( value )
        return self.__state.find_or_create( db_data )
