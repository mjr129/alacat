import re
from collections import Counter
from typing import Type, TypeVar, Generic, ClassVar, Union, Iterable

from mhelper import exception_helper, utf_helper
from mhelper.array_helper import WriteOnceDict


class _Massed:
    __slots__ = "name", "symbol", "monoisotopic_mass", "average_mass"
    
    
    def __init__( self, name, symbol, monoisotopic_mass, average_mass ):
        self.name = name
        self.symbol = symbol
        self.monoisotopic_mass = monoisotopic_mass
        self.average_mass = average_mass
    
    
    def __repr__( self ):
        return f"{type( self ).__name__}[ {self.symbol!r} ]"
    
    
    def __str__( self ):
        return self.name
    
    
    def __eq__( self, other ):
        if type( other ) == type( self ):
            return False
        
        return other.symbol == self.symbol
    
    
    def __hash__( self ):
        return hash( self.symbol )


T = TypeVar( "T" )
TMassed = TypeVar( "TMassed", bound = _Massed )


class _Formula( Generic[TMassed] ):
    __slots__ = "data",
    item_class: ClassVar[TMassed]
    fixed_monoisotopic_mass = 0
    fixed_average_mass = 0
    _rx1 = re.compile( "([A-Z][a-z]*|[0-9]+)" )
    
    
    def __init__( self, *args, **kwargs ):
        """
        !CONSTRUCTOR
        
        e.g.::
        
            "H20"
            "Hydrogen2Oxygen",
            "H2", "O"
            h = 2, o = 1
            { "H": 2, "O": 1 }
            { Element["H"] : 2, Element["O"] : 1 }
        
        :param args:        Any of:
        
                            String(s).
                            The string may specify a single or multiple items, in the form::
                            
                                XXX000XXX000XXX000...
                            
                            Where ``XXX`` is a `TMassed` identifier and ``000`` is its count.
                            If the count is not specified, 1 is assumed.
                            ``XXX`` must be "Camel Cased" to facilitate delimitation.
                            
                            `TMassed`(s).
                            Counts are assumed to be 1.
                            
                            Dict(s).
                            Mapping a `TMassed` or its identifier (`str`) to the count.
        :param kwargs:      Each mapping a `TMassed` identifier (`str`) to the count.
        :except KeyError:   Failed mapping.
        """
        data = WriteOnceDict()
        pending = Counter()
        last = None
        
        for arg in args:
            if isinstance( arg, dict ):
                for key, value in arg.items():
                    data[self.translate( key )] = value
            elif isinstance( arg, str ):
                for part in self._rx1.findall( arg ):  # type:str
                    if part.isdigit():
                        assert pending[last] == 1
                        del pending[last]
                        count = int( part )
                        if count:
                            data[last] = count
                    else:
                        last = self.translate( part )
                        pending[last] += 1
        
        for item, count in pending.items():
            data[item] = count
        
        for arg, count in kwargs.items():
            item = self.translate( arg )
            if count:
                data[item] = count
        
        self.data = dict( data )
    
    
    @classmethod
    def sum( cls : Type[T], formulae: Iterable["_Formula[TMassed]"] ) -> T:
        c = Counter()
        
        for formula in formulae:
            c.update( formula.data )
        
        return cls( c )
    
    
    def __mul__( self, other ):
        return type( self )( { item: count * other for item, count in self.data.items() } )
    
    
    @classmethod
    def translate( cls, key: Union[TMassed, str] ) -> T:
        if isinstance( key, cls.item_class ):
            return key
        elif isinstance( key, str ):
            return cls.item_class[key]
        else:
            raise exception_helper.type_error( "key", key, Union[cls.item_class, str] )
    
    
    def __add__( self: T, other: T ) -> T:
        cls = type( self )
        keys = set.union( set( self.data ), set( other.data ) )
        
        return cls( **{ key: self.data.get( key, 0 ) + other.data.get( key, 0 ) for key in keys } )
    
    
    def __repr__( self ) -> str:
        f = ", ".join( f"{key} = {value}" for key, value in self.data.items() )
        return f"{type( self ).__name__}({f})"
    
    
    def __str__( self ) -> str:
        return self.to_string( "1" )
    
    
    def to_string( self, mode: str = "1" ):
        r = []
        
        is_symbols = "1" in mode
        is_subscript = "s" in mode
        
        if is_symbols:
            for key, value in self.data.items():
                if value == 1:
                    r.append( key.symbol )
                else:
                    r.append( key.symbol )
                    
                    if is_subscript:
                        r.append( utf_helper.subscript( str( value ) ) )
                    else:
                        r.append( str( value ) )
            
            return "".join( r )
        else:
            for key, value in self.data.items():
                if value == 1:
                    r.append( f"'{key.name}'" )
                else:
                    r.append( f"'{key.name}' * {value}" )
            
            return ", ".join( r )
    
    
    def calc_monoisotopic_mass( self ) -> float:
        return sum( key.monoisotopic_mass * value for key, value in self.data.items() ) + self.fixed_monoisotopic_mass
    
    
    def calc_average_mass( self ) -> float:
        return sum( key.average_mass * value for key, value in self.data.items() ) + self.fixed_average_mass


class AminoAcidType( type ):
    __slots__ = ()
    
    # noinspection SpellCheckingInspection
    _symbols = { *"ARNDCEQGHILKMFPSTWYVU" }
    _map = { }
    
    
    def __getitem__( self, item: str ) -> "AminoAcid":
        return AminoAcidType._map[item.lower()]


class AminoAcid( _Massed, metaclass = AminoAcidType ):
    def __init__( self, name, symbol, three_letter_code, monoisotopic_mass, average_mass, *, c, h, o, n, s, se = 0 ):
        """
        !CONSTRUCTOR
        """
        super().__init__( name, symbol, monoisotopic_mass, average_mass )
        self.three_letter_code = three_letter_code
        self.formula = Molecule( c = c, h = h, o = o, n = n, s = s, se = se )
        
        AminoAcidType._map[symbol.lower()] = self
        AminoAcidType._map[three_letter_code.lower()] = self
        AminoAcidType._map[name.lower()] = self


class ElementType( type ):
    # __slots__ = "_map",
    
    
    def __init__( cls, name, bases, dct ):
        super().__init__( name, bases, dct )
        cls._map = { }
    
    
    def __getitem__( cls, item: str ):
        map = cls._map
        
        try:
            return map[item.lower()]
        except KeyError:
            raise KeyError( f"There is no item with the value {item!r} in the {cls.__name__} set: {set( map )!r}." )


class Element( _Massed, metaclass = ElementType ):
    __slots__ = ()
    
    
    def __init__( self, name, symbol, monoisotopic_mass, average_mass ):
        super().__init__( name, symbol, monoisotopic_mass, average_mass )
        Element._map[name.lower()] = self
        Element._map[symbol.lower()] = self


class Molecule( _Formula[Element] ):
    __slots__ = ()
    
    item_class = Element


Element( "Hydrogen", "H", 1.007276, 1.007 )
Element( "Oxygen", "O", 15.994915, 15.999 )
Element( "Carbon", "C", 12.0, 12.011 )
Element( "Nitrogen", "N", 14.003074, 14.007 )
Element( "Sulfur", "S", 31.972071, 32.065 )
Element( "Selenium", "Se", 79.9165196, 78.96 )

AminoAcid( "Alanine", "A", "Ala", 71.03711, 71.0788, c = 3, h = 5, o = 1, n = 1, s = 0 )  # AA.A
AminoAcid( "Arginine", "R", "Arg", 156.10111, 156.1875, c = 6, h = 12, o = 1, n = 4, s = 0 )  # AA.R
AminoAcid( "Asparagine", "N", "Asn", 114.04293, 114.1038, c = 4, h = 6, o = 2, n = 2, s = 0 )  # AA.N
AminoAcid( "Aspartic acid", "D", "asp", 115.02694, 115.0886, c = 4, h = 5, o = 3, n = 1, s = 0 )  # AA.D
AminoAcid( "Cysteine", "C", "Cys", 103.00919, 103.1388, c = 3, h = 5, o = 1, n = 1, s = 1 )  # AA.C
AminoAcid( "Glutamic acid", "E", "glu", 129.04259, 129.1155, c = 5, h = 7, o = 3, n = 1, s = 0 )  # AA.E
AminoAcid( "Glutamine", "Q", "Gln", 128.05858, 128.1307, c = 5, h = 8, o = 2, n = 2, s = 0 )  # AA.Q
AminoAcid( "Glycine", "G", "Gly", 7.02146, 57.0519, c = 2, h = 3, o = 1, n = 1, s = 0 )  # AA.G
AminoAcid( "Histidine", "H", "His", 137.05891, 137.1411, c = 6, h = 7, o = 1, n = 3, s = 0 )  # AA.H
AminoAcid( "Isoleucine", "I", "Ile", 113.08406, 113.1594, c = 6, h = 11, o = 1, n = 1, s = 0 )  # AA.I
AminoAcid( "Leucine", "L", "Leu", 113.08406, 113.1594, c = 6, h = 11, o = 1, n = 1, s = 0 )  # AA.L
AminoAcid( "Lysine", "K", "Lys", 128.09496, 128.1741, c = 6, h = 12, o = 1, n = 2, s = 0 )  # AA.K
AminoAcid( "Methionine", "M", "Met", 131.04049, 131.1926, c = 5, h = 9, o = 1, n = 1, s = 1 )  # AA.M
AminoAcid( "Phenylalanine", "F", "Phe", 147.06841, 147.1766, c = 9, h = 9, o = 1, n = 1, s = 0 )  # AA.F
AminoAcid( "Proline", "P", "Pro", 97.05276, 97.1167, c = 5, h = 7, o = 1, n = 1, s = 0 )  # AA.P
AminoAcid( "Serine", "S", "Ser", 87.03203, 87.0782, c = 3, h = 5, o = 2, n = 1, s = 0 )  # AA.S
AminoAcid( "Threonine", "T", "Thr", 101.04768, 101.1051, c = 4, h = 7, o = 2, n = 1, s = 0 )  # AA.T
AminoAcid( "Tryptophan", "W", "Trp", 186.07931, 186.2132, c = 11, h = 10, o = 1, n = 2, s = 0 )  # AA.W
AminoAcid( "Tyrosine", "Y", "Tyr", 163.06333, 163.1760, c = 9, h = 9, o = 2, n = 1, s = 0 )  # AA.Y
AminoAcid( "Valine", "V", "Val", 99.06841, 99.1326, c = 5, h = 9, o = 1, n = 1, s = 0 )  # AA.V

AminoAcid( "Selenocysteine", "U", "Sec", 168.964203, 168.053, c = 3, h = 5, o = 1, n = 1, s = 0, se = 1 )
AminoAcid( "Pyrrolysine", "O", "Pyl", 255.158292, 255.313, c = 12, h = 21, n = 3, o = 3, s = 0 )


class Peptide( _Formula[AminoAcid] ):
    __slots__ = ()
    item_class = AminoAcid
    # H at the N-terminus and OH at the C-terminus
    __termini = Molecule( "OH2" )
    fixed_average_mass = __termini.calc_average_mass()
    fixed_monoisotopic_mass = __termini.calc_monoisotopic_mass()
    
    
    @staticmethod
    def is_sequence( sequence: str ) -> bool:
        return all( c in AminoAcidType._symbols for c in sequence )
    
    
    @staticmethod
    def assert_is_sequence( sequence: str, permitted: str = "" ) -> None:
        for x in sequence:
            if x not in AminoAcidType._symbols and x not in permitted:
                raise ValueError( f"Bad amino-acid sequence, '{x}' is unmapped in '{sequence}'." )
    
    
    def calc_molecular_formula( self ) -> Molecule:
        return Molecule.sum( aa.formula * count for aa, count in self.data.items() )
