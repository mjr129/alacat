"""
Wraps `linker_fixer.propose_linker`, but uses a member of Alacat's `EDigest` enum instead.
"""
from typing import Optional

from .linker_fixer import Enzymes as _Enz, propose_linker as _propose_linker
from alacat.model.configuration import EDigestion as _Dig

__all__ = "propose_linker",


def propose_linker( digest: _Dig, link: str, peptide: str, cterm: bool ) -> Optional[str]:
    """
    See `linker_fixer.propose_linker`.
    """
    enzyme = _dig_to_rx[digest]
    r= _propose_linker( enzyme, link, peptide, cterm )
    return r


_dig_to_rx = { _Dig.OTHER                        : None,
               _Dig.INHERIT                      : None,
               _Dig.CUSTOM                       : None,
               _Dig.CUSTOM_NO_LINKERS            : None,
               _Dig.LYS_C_P                      : _Enz.LYS_C_P,
               _Dig.CHYMOTRYPSIN_P               : _Enz.CHYMOTRYPSIN_P,
               _Dig.PEPSINA                      : _Enz.PEPSINA,
               _Dig.TRYPSIN_P                    : _Enz.TRYPSIN_P,
               _Dig.C_N_BR                       : _Enz.C_N_BR,
               _Dig.ARG_C_P                      : _Enz.ARG_C_P,
               _Dig.CYANOGEN_BROMIDE             : _Enz.CYANOGEN_BROMIDE,
               _Dig.IODOSOBENZOATE               : _Enz.IODOSOBENZOATE,
               _Dig.STAPHYLOCOCCAL_PROTEASE_D    : _Enz.STAPHYLOCOCCAL_PROTEASE_D,
               _Dig.CLOSTRIPAIN_P                : _Enz.CLOSTRIPAIN_P,
               _Dig.ALPHA_LYTIC_PROTEASE         : _Enz.ALPHA_LYTIC_PROTEASE,
               _Dig.TWO_IODOBENZOATE             : _Enz.TWO_IODOBENZOATE,
               _Dig.PROLINE_ENDOPEPTIDASE_HKR    : _Enz.PROLINE_ENDOPEPTIDASE_HKR,
               _Dig.GLUTAMYL_ENDOPEPTIDASE       : _Enz.GLUTAMYL_ENDOPEPTIDASE,
               _Dig.TRYP_CHYMO                   : _Enz.TRYP_CHYMO,
               _Dig.V8_DE                        : _Enz.V8_DE,
               _Dig.V8_E                         : _Enz.V8_E,
               _Dig.LYS_C                        : _Enz.LYS_C,
               _Dig.ARG_C                        : _Enz.ARG_C,
               _Dig.TRYPSIN                      : _Enz.TRYPSIN,
               _Dig.CHYMOTRYPSIN                 : _Enz.CHYMOTRYPSIN,
               _Dig.LEUKOCYTE_ELASTASE           : _Enz.LEUKOCYTE_ELASTASE,
               _Dig.PEPSINA_P                    : _Enz.PEPSINA_P,
               _Dig.ELASTASE_TRYPSIN_CHYMOTRYPSIN: _Enz.ELASTASE_TRYPSIN_CHYMOTRYPSIN,
               _Dig.GLU_C_P                      : _Enz.GLU_C_P,
               _Dig.PROLINE_ENDOPEPTIDASE        : _Enz.PROLINE_ENDOPEPTIDASE,
               _Dig.ASP_N_AMBIC                  : _Enz.ASP_N_AMBIC,
               _Dig.ASP_N                        : _Enz.ASP_N,
               _Dig.ASP_N_B                      : _Enz.ASP_N_B,
               _Dig.LYS_N                        : _Enz.LYS_N,
               _Dig.FORMIC_ACID                  : _Enz.FORMIC_ACID,
               }
