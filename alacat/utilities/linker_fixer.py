"""
Peptide linker fixer.
Proposes a suitable replacement linker from a peptide's sequence.

Usage
-----

Can be used stand alone::

    from alacat.utilities.linker_fixer import propose_linker, Enzymes 
    
    linker = propose_linker( Enzymes.TRYPSIN, "NNNN", "NNN" )
    print( "Suggested linker: " + linker )

Can be used as an executable, which runs the test cases::

    python -m alacat.utilities.linker_fixer
"""
# INCLUDE_IN_README
from dataclasses import dataclass
import re
from typing import Optional

__all__ = "propose_linker", "Enzymes"


@dataclass
class Enzyme:
    """
    :cvar regex:             Regular expression that searches for the breakpoint
    :cvar breakpoint:        Handle to insert should an actual breakpoint not be present.
                             Must match the regex and breakpoint_size. 
    :cvar inert:             Amino acid to use to blank with should an actual breakpoint need to be removed
    :cvar breakpoint_size:   Size of the handle.
                             Tells us how much to cut out when adding or removing breakpoints.
                             Negative size for handles that occur before the breakpoint.
                             Positive size for handles that occur after the breakpoint.
    :cvar dual:              Set if the handle produces two breakpoints. 
    :cvar proline_inhibitor: Set if a trailing proline inhibits the enzyme.
    :cvar name:              Name.
                             If this `Enzyme` is a member of `Enzymes`, this is the name of the
                             field and is set automatically.
    """
    regex: re.Pattern
    breakpoint: str
    inert: str
    breakpoint_size: int
    dual: bool
    proline_inhibitor: bool
    name: str = ""


def propose_linker( enzyme: Enzyme, link: str, peptide: str, cterm: bool ) -> Optional[str]:
    """
    Proposes a suitable leading (N) linker.
    The existing linker may already be good.         
    """
    assert isinstance( enzyme, Enzyme )
    SIZE = 3

    if not link:
        if enzyme.breakpoint_size < 0:
            # Break before, cterm contains the breakpoint
            if cterm:
                link = enzyme.breakpoint
        else:
            # Break after, nterm contains the breakpoint
            if not cterm:
                link = enzyme.breakpoint

    if len( link ) < SIZE:
        link = __form( (enzyme.inert * (SIZE - len( link ))), link, cterm )

    # Special case for proline inhibitors
    if enzyme.proline_inhibitor:
        if cterm:
            if link.startswith( "P" ):
                link = enzyme.inert + link[1:]
        else:
            if peptide.startswith( "P" ):
                return None

    rx = enzyme.regex
    visited = set()
    xbp = len( peptide ) if cterm else SIZE

    while True:
        # Are we going round in circles?
        if link in visited:
            return None  # Cannot resolve this linker. Check whether the protein is inhibiting the enzyme."

        visited.add( link )

        if len( visited ) > 1000:
            raise RuntimeError( "Infinite recursion." )

            # Look for the breakpoint in our sequence
        sequence = __form( link, peptide, cterm )
        mat = __rxsearch( rx, sequence, cterm )
        breakpoint = mat.start( 0 ) if mat else None

        # No breakpoint, we can try and introduce one
        if breakpoint is None or __breakpoint_in_protein( breakpoint, xbp, cterm ):
            link = __introduce_breakpoint( enzyme, link, cterm )
            continue

        # Breakpoint in right place that's great 
        if breakpoint == xbp:
            return link
        elif __size_adjacent( enzyme, breakpoint, cterm, xbp ):
            test_link = __remove_breakpoint( enzyme, peptide, link, breakpoint, cterm )
            test_match = __rxsearch( rx, __form( test_link, peptide, cterm ), cterm )
            if test_match and test_match.start( 0 ) == xbp:
                return test_link
            else:
                return link

        # Early breakpoint, try to remove it 
        link = __remove_breakpoint( enzyme, peptide, link, breakpoint, cterm )


__AMINO_ACIDS = "ARKWDENQHSTYCGILMFPV"


def _enzyme( rx: str, manual_breakpoint = None, dual: bool = False ):
    if manual_breakpoint is None:
        manual_breakpoint = next( iter( x for x in rx if x in __AMINO_ACIDS ) )

    lbp = len( manual_breakpoint )

    return Enzyme(
            regex = re.compile( rx ),
            breakpoint = manual_breakpoint,
            inert = next( iter( x for x in __AMINO_ACIDS if x not in rx ) ),
            breakpoint_size = -lbp if rx.startswith( "(?<=" ) else lbp,
            dual = dual,
            proline_inhibitor = "(?!P)" in rx )


# BEGIN ATTRIBUTION
# PURPOSE The enzyme regular expressions used in the above code were taken from OpenMS
# TITLE OpenMS -- Open-Source Mass Spectrometry
# AUTHOR Uwe Schmitt
# WEBSITE https://www.openms.de/
# DATE-RETRIEVED 15/04/2021
# LICENSE-URL http://opensource.org/licenses/BSD-3-Clause
# LICENSE-TEXT 
#   OpenMS -- Open-Source Mass Spectrometry
#   
#   Copyright The OpenMS Team -- Eberhard Karls University Tuebingen,
#   ETH Zurich, and Freie Universitaet Berlin 2002-2020.
#   
#   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#   
#   1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#   
#   2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#   
#   3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
#   
#   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# END ATTRIBUTION

class Enzymes:
    """
    Enzymes.
    The enzyme regular expressions were taken from OpenMS.
    """

    # Cut after 1
    LYS_C_P = _enzyme( "(?<=[KX])" )
    CHYMOTRYPSIN_P = _enzyme( "(?<=[FYWLJX])" )
    PEPSINA = _enzyme( "(?<=[FLJX])" )
    TRYPSIN_P = _enzyme( "(?<=[KRX])" )
    C_N_BR = _enzyme( "(?<=[MX])" )
    ARG_C_P = _enzyme( "(?<=[RX])" )
    CYANOGEN_BROMIDE = _enzyme( "(?<=[MX])" )
    IODOSOBENZOATE = _enzyme( "(?<=W)" )
    STAPHYLOCOCCAL_PROTEASE_D = _enzyme( "(?<=[EZX])" )
    CLOSTRIPAIN_P = _enzyme( "(?<=[RX])" )
    ALPHA_LYTIC_PROTEASE = _enzyme( "(?<=[TASVX])" )
    TWO_IODOBENZOATE = _enzyme( "(?<=[WX])" )
    PROLINE_ENDOPEPTIDASE_HKR = _enzyme( "(?<=[PX])" )
    GLUTAMYL_ENDOPEPTIDASE = _enzyme( "(?<=[DBEZX])" )

    # Cut after 1, not before P
    TRYP_CHYMO = _enzyme( "(?<=[FYWLJKRX])(?!P)" )
    V8_DE = _enzyme( "(?<=[DBEZX])(?!P)" )
    V8_E = _enzyme( "(?<=[EZX])(?!P)" )
    LYS_C = _enzyme( "(?<=[KX])(?!P)" )
    ARG_C = _enzyme( "(?<=[RX])(?!P)" )
    TRYPSIN = _enzyme( "(?<=[KRX])(?!P)" )
    CHYMOTRYPSIN = _enzyme( "(?<=[FYWLJX])(?!P)" )
    LEUKOCYTE_ELASTASE = _enzyme( "(?<=[ALIJVX])(?!P)" )
    PEPSINA_P = _enzyme( "(?<=[FLJX])(?!P)" )
    ELASTASE_TRYPSIN_CHYMOTRYPSIN = _enzyme( "(?<=[ALIVKRWFYX])(?!P)" )
    GLU_C_P = _enzyme( "(?<=[DBEZX])(?!P)" )

    # Cut after 2, not before P
    PROLINE_ENDOPEPTIDASE = _enzyme( "(?<=[HKRX][PX])(?!P)", manual_breakpoint = "KP" )

    # Cut before 1
    ASP_N_AMBIC = _enzyme( "(?=[DBEZX])" )
    ASP_N = _enzyme( "(?=[DBX])" )
    ASP_N_B = _enzyme( "(?=[DX])" )
    LYS_N = _enzyme( "(?=[KX])" )

    # Cut either side of 1
    FORMIC_ACID = _enzyme( "((?<=[DBX]))|((?=[DBX]))", dual = True )


def __form( first, second, reversed ):
    if reversed:
        return second + first
    else:
        return first + second


def __rxsearch( rx, sequence, cterm ):
    if cterm:
        mat = None
        for mat in rx.finditer( sequence ):
            pass
        return mat
    else:
        return rx.search( sequence )


def __breakpoint_in_protein( breakpoint, xbp, cterm ):
    if cterm:
        return breakpoint < xbp
    else:
        return breakpoint > xbp


def __size_adjacent( enzyme: Enzyme, breakpoint: int, cterm: bool, size: int ):
    if cterm:
        return enzyme.dual and breakpoint == size + 1
    else:
        return enzyme.dual and breakpoint == size - 1


def __introduce_breakpoint( enzyme: Enzyme, link: str, cterm: bool ) -> str:
    l = len( enzyme.breakpoint )

    if cterm:
        return enzyme.breakpoint + link[l:]
    else:
        return link[:-l] + enzyme.breakpoint


def __remove_breakpoint( enzyme: Enzyme, peptide: str, link: str, breakpoint: int, cterm: bool ):
    if cterm and enzyme.dual:
        breakpoint -= 1

    if cterm:
        breakpoint -= len( peptide )

    if enzyme.breakpoint_size < 0:
        s = breakpoint + enzyme.breakpoint_size
        e = breakpoint
    else:
        s = breakpoint
        e = breakpoint + enzyme.breakpoint_size

    return link[:s] + (enzyme.inert * abs( enzyme.breakpoint_size )) + link[e:]


def __init():
    for k, v in Enzymes.__dict__.items():
        if isinstance( v, Enzyme ):
            v.name = k


__init()


def __run_tests():
    class __TestError( Exception ):
        def __init__( self, msg ):
            super().__init__( f"Expected: [{msg}]" )

    def __test( enzyme: Enzyme, flank, peptide, *, to, cterm = False ):
        assert isinstance( enzyme, Enzyme )

        if cterm:
            f = f"C {peptide}-{flank:>3}"
        else:
            f = f"N {flank:>3}-{peptide}"

        print( f"{enzyme.name:<24} {f} --> ", end = "" )

        linker = propose_linker( enzyme, flank, peptide, cterm )

        if cterm:
            print( f"{peptide}-{linker}" )
        else:
            print( f"{linker}-{peptide}" )

        if to is None:
            if linker is not None:
                raise __TestError( to )
            return
        else:
            if linker is None:
                raise __TestError( to )

        for A, B, T in zip( flank or "   ", linker, to ):
            if T == ".":
                if A != B:
                    raise __TestError( to )
            elif T == "#":
                if A == B:
                    raise __TestError( to )
                elif B not in enzyme.inert:
                    raise __TestError( to )
            elif T == "*":
                if A == B:
                    raise __TestError( to )
                elif B not in enzyme.breakpoint:
                    raise __TestError( to )
            else:
                raise __TestError( to )

    def __ctest( digest, peptide, flank, *, to ):
        __test( digest, flank, peptide, to = to, cterm = True )

    try:
        EDig = Enzymes

        #
        # N TERMINAL
        #

        # # Test the "after X"
        # # TRYPSIN - After RK
        __test( EDig.TRYPSIN, "GGR", "NNNN", to = "..." )  # All good
        __test( EDig.TRYPSIN, "GGK", "NNNN", to = "..." )  # All good
        __test( EDig.TRYPSIN, "GKK", "NNNN", to = ".#." )  # K, should be swapped
        __test( EDig.TRYPSIN, "RKK", "NNNN", to = "##." )  # R and K, should both be swapped
        __test( EDig.TRYPSIN, "", "NNNN", to = "##*" )  # No linker, should be created
        # 
        # # Test the "after X but not before Y"
        # # LEUKOCYTE_ELASTASE - After ALIJV if not followed by P.
        __test( EDig.LEUKOCYTE_ELASTASE, "GGA", "NNNN", to = "..." )  # All good
        __test( EDig.LEUKOCYTE_ELASTASE, "GAA", "NNNN", to = ".#." )  # A, should be swapped
        __test( EDig.LEUKOCYTE_ELASTASE, "APA", "NNNN", to = "..." )  # A but ok it's followed by P
        __test( EDig.TRYPSIN, "GGG", "NNNN", to = "..*" )  # No breakpoint in the linker, should swap the last one out
        __test( EDig.LEUKOCYTE_ELASTASE, "GGG", "PNNN", to = None )  # We can never fix this, the protein itself is inhibiting the enzyme
        # 
        # # Test the "before X"
        # # ASP_N - Before D
        __test( EDig.ASP_N, "GGG", "DNNN", to = "..." )  # Good
        __test( EDig.ASP_N, "GGD", "DNNN", to = "..#" )  # Bad, there is a D at the last position, we must swap it
        __test( EDig.ASP_N, "DDD", "DNNN", to = "###" )  # Bad, it's full of D's
        __test( EDig.ASP_N, "GGG", "NNNN", to = None )  # Unfixable, the protein must begin with D

        # Test formic acid, this cuts either side of DB
        __test( EDig.FORMIC_ACID, "GGD", "NNNN", to = "..." )
        __test( EDig.FORMIC_ACID, "GGG", "DNNN", to = "..." )
        __test( EDig.FORMIC_ACID, "GDD", "NNNN", to = ".#." )
        __test( EDig.FORMIC_ACID, "GGD", "DNNN", to = "..#" )

        # Test proline_endopeptidase, this cuts after HKR then P but not before P
        __test( EDig.PROLINE_ENDOPEPTIDASE, "GKP", "NNNN", to = "..." )  # Good
        __test( EDig.PROLINE_ENDOPEPTIDASE, "GKP", "PNNN", to = None )  # Unfixable, the protein cannot begin with P

        #
        # C TERMINAL
        #
        __ctest( EDig.TRYPSIN, "NNNK", "NNN", to = "..." )  # All good
        __ctest( EDig.TRYPSIN, "NNNR", "NNN", to = "..." )  # All good
        __ctest( EDig.TRYPSIN, "NNNK", "KNN", to = "#.." )  # K, should be swapped
        __ctest( EDig.TRYPSIN, "NNNK", "KNR", to = "#.#" )  # R and K, should both be swapped
        __ctest( EDig.TRYPSIN, "NNNN", "NNN", to = None )  # We can never fix this, the protein itself is the problem
        # 
        # # Test the "after X but not before Y"
        # # LEUKOCYTE_ELASTASE - After ALIJV if not followed by P.
        __ctest( EDig.LEUKOCYTE_ELASTASE, "NNNA", "NNN", to = "..." )  # All good
        __ctest( EDig.LEUKOCYTE_ELASTASE, "NNNA", "ANN", to = "#.." )  # A, should be swapped
        __ctest( EDig.LEUKOCYTE_ELASTASE, "NNNA", "PNN", to = "#.." )  # P, should be swapped
        __ctest( EDig.LEUKOCYTE_ELASTASE, "NNNA", "NPN", to = "..." )  # P, but in OK position
        __ctest( EDig.TRYPSIN, "NNNN", "NNN", to = None )  # We can never fix this, the protein itself is the problem
        # 
        # # Test the "before X"
        # # ASP_N - Before D
        __ctest( EDig.ASP_N, "NNNN", "DNN", to = "..." )
        __ctest( EDig.ASP_N, "NNNN", "DDN", to = ".#." )
        __ctest( EDig.ASP_N, "NNNN", "DDD", to = ".##" )
        __ctest( EDig.ASP_N, "NNNN", "", to = "*##" )

        # Test formic acid, this cuts either side of DB
        __ctest( EDig.FORMIC_ACID, "NNNN", "DNN", to = "..." )
        __ctest( EDig.FORMIC_ACID, "NNND", "NNN", to = "..." )
        __ctest( EDig.FORMIC_ACID, "NNND", "DNN", to = "#.." )
        __ctest( EDig.FORMIC_ACID, "NNND", "DDN", to = "##." )
        __ctest( EDig.FORMIC_ACID, "NNNN", "NNN", to = "*.." )

        # Test proline_endopeptidase, this cuts after HKR then P but not before P
        __ctest( EDig.PROLINE_ENDOPEPTIDASE, "NNKP", "NNN", to = "..." )
        __ctest( EDig.PROLINE_ENDOPEPTIDASE, "NNKP", "PNN", to = "#.." )
        __ctest( EDig.PROLINE_ENDOPEPTIDASE, "NNKP", "KPN", to = "##." )
        __ctest( EDig.PROLINE_ENDOPEPTIDASE, "NNNN", "KPN", to = None )

        # Other tests
        print("bugfix-1")
        __ctest( EDig.TRYPSIN, "EAFAEQFLR", "GSD", to = "..." )
        __test( EDig.TRYPSIN, "PPR", "EAFAEQFLR", to = "..." )
        
        print("bugfix-2")
        __test( EDig.TRYPSIN, "IHR", "ANMLAQGSPAASK", to = "..." )
        __ctest( EDig.TRYPSIN, "EEDSTVSILPTSLPQIHR", "ANM", to = "..." )
        __ctest( EDig.TRYPSIN, "EESDDDMGFGLFD", "", to = None ) # This is right, we cannot add a C-linker
    except __TestError as ex:
        print( ex )
    else:
        print( "ALL TESTS PASSED OK" )


if __name__ == "__main__":
    __run_tests()
