from mhelper import file_helper


def get_cache_file_name( *args ):
    return file_helper.get_application_file_name( "rusilowicz", "alacat", "cache", *args )
