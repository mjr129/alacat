from alacat.model import InstantiationError, Handler, standard_log
from typing import Callable, Optional, Sequence, TypeVar


TDataSet = TypeVar( "TDataSet" )


def initialise_per_organism( handler: Handler, 
                             acquire: Callable[[int], Optional[TDataSet]]
                             ) -> Sequence[TDataSet]:
    """
    Initialises a `TDataSet` for every organism in the current model.
    
    Creates warnings for missing organisms, raises an `InstantiationError`
    if no organisms are present or if no `TDataSet` could be initialised.
    
    `TDataSet` is the type of the dataset being initialised for an individual
    organism. This *must* have a `columns` attribute containing the output 
    `HColumn`\s for that organism.
     
    :param handler: The handler we are initialising for.
     
    :param acquire: The initialisation method for a given organism.
                    Should return the initialised `TDataSet`, or `None`.
                     
    :return:        Sequence of successful results from `acquire`.
    
    :except InstantiationError: No organisms, or could not initialise a
                                `TDataSet` for any organism.
    """
    if not handler.state.organisms:
        raise InstantiationError( handler, "Requires the organism(s) to be known." )
    
    warnings = []
    results = []
    
    for taxon_id in handler.state.organisms:
        with standard_log.time( f"Instantiating for taxon {taxon_id}" ):
            dbi = acquire( taxon_id )

            if dbi:
                results.append( dbi )
                handler.bind_columns( dbi.columns )
            else:
                warnings.append( taxon_id )
    
    if not results:
        raise InstantiationError( handler, f"Could not get {handler.accession!r} data for any organism: {warnings}." )
    elif warnings:
        handler.state.warnings.append( f"Could not get {handler.accession!r} data for organism: {warnings}." )
    
    return results
