from mhelper.specific_locations import get_application_directory as _get_application_directory


def get_data_dir():
    return _get_application_directory( "rusilowicz", "alacat" )
