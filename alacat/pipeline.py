import time
from dataclasses import dataclass
from enum import Flag
from typing import cast, Dict, Iterable, List, Optional, Sequence, Type, Union

from alacat import model as m
from mhelper import array_helper, io_helper, string_helper


@dataclass
class Pipeline:
    """
    The pipeline runs and holds the results of an entire ALACAT workflow.
    
    To *run* and create a `Pipeline`, see `Pipeline.exact`.
    """
    model: "m.Model"
    state: "m.ModelState"
    factories: "m.HandlerFactoryCollection"
    warnings: "m.WarningCollection"
    proteins: Optional["m.ProteinCollection"]
    candidate_peptides: Optional["m.PeptideCollection"]
    favoured_peptides: Optional["m.PeptideCollection"]
    peptide_scores: Optional["m.ScoresTable"]
    selected_peptides: Optional["m.PeptideCollection"]
    system_selected_peptides: Optional["m.PeptideCollection"]
    candidate_qblocks: Optional["m.QblockCollection"]
    qblock_scores: Optional["m.ScoresTable"]
    favoured_qblocks: Optional["m.QblockCollection"]
    selected_qblocks: Optional["m.QblockCollection"]
    system_selected_qblocks: Optional["m.QblockCollection"]
    candidate_menus: Optional["m.QmenuCollection"]
    qmenu_scores: Optional["m.ScoresTable"]
    selected_menus: Optional["m.QmenuCollection"]
    favoured_menus: Optional["m.QmenuCollection"]
    system_selected_menus: Optional["m.QmenuCollection"]
    qblock_production_scores: Optional["m.ScoresTable"]
    qmenu_production_scores: Optional["m.ScoresTable"]
    protein_production_scores: Optional["m.ScoresTable"]
    peptide_production_scores: Optional["m.ScoresTable"]

    FILE_MAGIC = "ALACAT_PIPELINE", 1

    @staticmethod
    def exact( model: "m.Model" = None ) -> "Pipeline":
        """
        Runs the whole pipeline for the specified model.
        Usage instructions are not repeated here, please see `alacat.Model`.
        """
        start_time = time.perf_counter()
        m.standard_log( "ALACATDESIGNER PIPELINE STARTED" )
        m.standard_log( "MODEL IS {}", model.name )

        with m.ModelState( model ) as state:
            protein_accessions = [state.acquire_text( text ) for text in model.proteins]
            factories = model.handlers

            protein_production_scores: Optional[m.ScoresTable[m.Protein]] = None
            proteins_: Optional[m.ProteinCollection] = None
            peptide_production_scores: Optional[m.ScoresTable[m.Peptide]] = None
            candidate_peptides_: Optional[m.PeptideCollection] = None
            peptide_scores = None
            favoured_peptides = None
            selected_peptides = None
            system_selected_peptides = None
            qblock_production_scores = None
            candidate_qblocks_ = None
            qblock_scores = None
            favoured_qblocks = None
            selected_qblocks = None
            system_selected_qblocks = None
            qmenu_production_scores = None
            candidate_menus_ = None
            qmenu_scores = None
            selected_qmenus = None
            system_selected_qmenus = None
            favoured_menus = None
            stage = model.stage
            EStage = m.EStage

            m.standard_log( "Running to the {} stage.", stage )

            #
            # MAKE PROTEINS
            #
            if stage.value >= EStage.MAKE_PROTEINS.value:
                # get the proteins
                protein_production_scores = m.ProteinProviderCollection( state ).provide( protein_accessions )
                proteins_ = m.ProteinCollection( protein_production_scores.subjects )

                # add any new organisms
                state.organisms.update( frozenset( protein.organism for protein in protein_production_scores.subjects ) )

            #
            # MAKE PEPTIDES
            #
            if stage.value >= EStage.MAKE_PEPTIDES.value:
                peptide_production_scores = m.PeptideProviderCollection( state ).provide( protein_production_scores )
                candidate_peptides_ = m.PeptideCollection( peptide_production_scores.subjects )
                favoured_peptides = m.EProvided.find_favoured( model.peptides, peptide_production_scores )

            #
            # SCORE & SELECT PEPTIDES
            #
            if stage.value >= EStage.SCORE_PEPTIDES.value:
                # Score the peptides
                peptide_scores = m.PeptideScorerCollection( state ).score( peptide_production_scores )

                # Select the best 2 peptides for each protein to make our qbricks
                selected_peptides = m.PeptideCollection( peptide_scores.get_top( key = lambda x: x.protein,
                                                                                 n = model.qbrick_size * model.qblock_size,
                                                                                 favour = favoured_peptides,
                                                                                 mandate = model.mandate ) )

                system_selected_peptides = m.PeptideCollection( peptide_scores.get_top( key = lambda x: x.protein,
                                                                                        n = selected_peptides ) )

            #
            # MAKE QBLOCKS
            #
            if stage.value >= EStage.MAKE_QBRICKS.value:
                p_groups: List[List[m.Peptide]] = [list( group ) for group in array_helper.group_by( selected_peptides, lambda x: x.protein ).values()]
                qblock_production_scores = m.QblockProviderCollection( state ).provide( p_groups )
                candidate_qblocks_ = m.QblockCollection( qblock_production_scores.subjects )
                favoured_qblocks = m.EProvided.find_favoured( model.qblocks, qblock_production_scores )

            #
            # SCORE & SELECT QBLOCKS
            #
            if stage.value >= EStage.SCORE_QBRICKS.value:
                qblock_scores = m.QblockScorerCollection( state ).score( qblock_production_scores )
                selected_qblocks = m.QblockCollection( qblock_scores.get_top( lambda x: x.protein,
                                                                              n = 1,
                                                                              favour = favoured_qblocks,
                                                                              mandate = model.mandate ) )
                system_selected_qblocks = m.QblockCollection( qblock_scores.get_top( lambda x: x.protein,
                                                                                     n = selected_qblocks ) )

            #
            # MAKE QMENUS
            #
            if stage.value >= EStage.MAKE_QMENUS.value:
                # Expand qblocks to qbricks
                qm_input: List[List[m.Qbrick]] = [[qbrick for qblock in selected_qblocks for qbrick in qblock.qbricks]]
                qmenu_production_scores = m.QmenuProviderCollection( state ).provide( qm_input )
                candidate_menus_ = m.QmenuCollection( qmenu_production_scores.subjects )
                favoured_menus = m.EProvided.find_favoured( model.qmenus, qmenu_production_scores )

            #
            # SCORE & SELECT QMENUS
            #
            if stage.value >= EStage.SCORE_QMENUS.value:
                qmenu_scores = m.QmenuScorerCollection( state ).score( qmenu_production_scores )

                # Select the best alacat
                selected_qmenus = m.QmenuCollection( qmenu_scores.get_top( key = lambda x: True,
                                                                           n = 1,
                                                                           favour = favoured_menus,
                                                                           mandate = model.mandate ) )
                system_selected_qmenus = m.QmenuCollection( qmenu_scores.get_top( key = lambda x: True,
                                                                                  n = 1 ) )

            #
            # PROFILING
            #
            state.profiling.append( (f"Pipeline to {stage.name}", time.perf_counter() - start_time) )

        m.standard_log( "PIPELINE COMPLETE! Took {}", string_helper.timedelta_to_string( time.perf_counter() - start_time ) )

        # If we are calling this programmatically, the caller may wish to use
        # the results, return them now.
        return Pipeline( model = model,
                         state = state,
                         factories = factories,
                         warnings = state.warnings,
                         proteins = proteins_,
                         candidate_peptides = candidate_peptides_,
                         peptide_scores = peptide_scores,
                         selected_peptides = selected_peptides,
                         system_selected_peptides = system_selected_peptides,
                         candidate_qblocks = candidate_qblocks_,
                         qblock_production_scores = qblock_production_scores,
                         qblock_scores = qblock_scores,
                         selected_qblocks = selected_qblocks,
                         system_selected_qblocks = system_selected_qblocks,
                         candidate_menus = candidate_menus_,
                         qmenu_scores = qmenu_scores,
                         selected_menus = selected_qmenus,
                         system_selected_menus = system_selected_qmenus,
                         favoured_peptides = favoured_peptides,
                         favoured_qblocks = favoured_qblocks,
                         favoured_menus = favoured_menus,
                         qmenu_production_scores = qmenu_production_scores,
                         protein_production_scores = protein_production_scores,
                         peptide_production_scores = peptide_production_scores )

    def save( self, file_name: str ):
        with io_helper.SequentialWriter( file_name ) as writer:
            writer.write_magic( Pipeline.FILE_MAGIC )
            writer.write_pickle( self )

    @staticmethod
    def load( file_name: str ):
        with io_helper.SequentialReader( file_name ) as reader:
            reader.read_magic( Pipeline.FILE_MAGIC )
            return reader.read_pickle( Pipeline )

    def find( self, subject_str: str ) -> m.Entity:
        return self.state.find( subject_str )

    @property
    def tables( self ):
        """
        All the score tables.
        
        .. note::
        
            The order is arbitrary, but supersets of scores are guaranteed to
            come before their subsets (e.g. peptide_scores comes before
            protein_production_scores because peptide_scores includes all
            protein_production_scores). i.e. linear search by subject will
            reveal the table with the full scores table first.
        """
        return [self.peptide_scores,
                self.qblock_scores,
                self.qmenu_scores,
                self.protein_production_scores,
                self.peptide_production_scores,
                self.qblock_production_scores,
                self.qmenu_production_scores]

    def find_table( self, entities: Union[m.Entity, Iterable[m.Entity]] ) -> Optional[m.ScoresTable]:
        return array_helper.single( self.find_tables( entities ), None, array_helper.FIRST )

    def find_tables( self, entities: Union[m.Entity, Iterable[m.Entity]] ) -> Sequence[m.ScoresTable]:
        """
        Finds the tables associated with the specified entity(s).
        """
        if isinstance( entities, m.Entity ):
            entities = entities,

        if not entities:
            return ()

        r = []

        for table_ in self.tables:
            if all( self.__is_in_table( ent, table_ ) for ent in entities ):
                r.append( table_ )

        return r

    def __is_in_table( self, entity: m.Entity, table: m.ScoresTable ) -> bool:
        """
        Returns if the `entity` occurs in the specified `table`.
        """
        if isinstance( entity, m.Column ):
            return entity in table.columns
        elif isinstance( entity, m.Handler ):
            return entity in (column.handler for column in table.columns)
        else:
            return entity in table.subjects

    def find_table_by_class( self, klass: Union[str, Type[m.Entity]] ) -> Optional[m.ScoresTable]:
        """
        Returns the most suitable table for the specified `Entity` `klass`.
        
        :param klass: Class or class name. 
        """
        if is_match( klass, m.Protein ):
            return self.protein_production_scores
        elif is_match( klass, m.Peptide ):
            return self.peptide_scores
        elif is_match( klass, m.Qbrick ):
            return None
        elif is_match( klass, m.Qblock ):
            return self.qblock_scores
        elif is_match( klass, m.Qconcat ):
            return None
        elif is_match( klass, m.Qmenu ):
            return self.qmenu_scores
        else:
            raise ValueError( "Invalid class." )

    def find_selection( self, klass ) -> Optional["SelectionTables"]:
        if is_match( klass, m.Protein ):
            return None
        elif is_match( klass, m.Peptide ):
            return SelectionTables( self.candidate_peptides, self.favoured_peptides, self.system_selected_peptides, self.selected_peptides )
        elif is_match( klass, m.Qbrick ):
            return None
        elif is_match( klass, m.Qblock ):
            return SelectionTables( self.candidate_qblocks, self.favoured_qblocks, self.system_selected_qblocks, self.selected_qblocks )
        elif is_match( klass, m.Qconcat ):
            return None
        elif is_match( klass, m.Qmenu ):
            return SelectionTables( self.candidate_menus, self.favoured_menus, self.system_selected_menus, self.selected_menus )
        else:
            raise ValueError( "Invalid class." )

    def summarise( self, entity: m.Entity ) -> Optional["EntitySummary"]:
        """
        Summarises an entity, see `EntitySummary`.
        """
        if isinstance( entity, m.Column ) or isinstance( entity, m.Handler ):
            return None

        table: Optional[m.ScoresTable] = self.find_table( entity )

        if table is None:
            return None

        rank: int = table.subject_to_rank[entity]

        valid: Dict[m.Entity, int] = table.subject_to_rank

        if self.state.model.partition_rankings and isinstance( entity, m.ComesFromProtein ):
            valid = { subject_: rank_
                      for subject_, rank_
                      in valid.items()
                      if cast( m.ComesFromProtein, subject_ ).protein is cast( m.ComesFromProtein, entity ).protein }

        same_ranked = [subject_ for subject_, rank_ in valid.items() if rank_ == rank and subject_ is not entity]
        ranked_out_of = len( valid )

        mse: Optional[ESelectors] = self.summarise_selection( entity )

        if mse is None:
            return None

        is_equivalent: bool = False
        sel: Optional["SelectionTables"] = self.find_selection( entity.__class__ )

        for subject_ in sel.system:
            srank: int = table.subject_to_rank[subject_]

            if rank == srank:
                is_equivalent = True

        scores = table.get_scores( entity )

        total_score = 0

        for score in scores:
            if score.grade==m.EGrade.PASS:
                total_score += score.column.weight
                
            

        return EntitySummary( entity, rank, len( same_ranked ), ranked_out_of, mse, is_equivalent, total_score )

    def summarise_selection( self, entity: m.Entity ) -> Optional["ESelectors"]:
        sel = self.find_selection( entity.__class__ )

        if sel is None:
            return None

        mse = ESelectors.NONE

        if entity in sel.user:
            mse |= ESelectors.USER

        if entity in sel.system:
            mse |= ESelectors.SYSTEM

        if entity in sel.selected:
            mse |= ESelectors.FINAL

        return mse


@dataclass()
class SelectionTables:
    candidates: Sequence
    user: Sequence
    system: Sequence
    selected: Sequence


class ESelectors( Flag ):
    """
    :cvar NONE:     No flags
    :cvar USER:     Selected by user
    :cvar SYSTEM:   Selected by system
    :cvar FINAL:    Selected for progression in pipeline
    """
    NONE = 0
    USER = 1
    SYSTEM = 2
    FINAL = 4


@dataclass()
class EntitySummary:
    """
    :cvar rank:             Rank of entity
    :cvar same_ranked:      Number of subjects in the subset with same rank as this one
                            If the subjects originate from proteins (i.e. peptides or qblocks) *and*
                            the model is set to `partition_rankings` (which is the default) then the
                            subset includes those subjects originating from the same protein.
                            Otherwise, the subset is the set of all subjects in the table.
    :cvar ranked_out_of:    For `same_ranked`, the size of the subset.
    :cvar selection:        Flags, see `ESelectors`.
    :cvar is_equivalent:    Even if this wasn't selected by the system, was the selection by the
                            system equivalently ranked.     
    """
    entity: m.Entity
    rank: int
    same_ranked: int
    ranked_out_of: int
    selection: ESelectors
    is_equivalent: bool
    total_points: int


def is_match( a, b ):
    return a == b or a == b.__name__
