#!/usr/bin/perl
#

use warnings;
use vars qw($opt_m $opt_e $opt_n);   # do this if using strict
use Getopt::Std;    # you need this in order to use the module !
my %enzname = (
               "t" => 'trypsin',
               "k" => 'lys-c',
               "v" => 'Staph-v8',
               "r" => 'arg-c',
               "x" => 'trypsin-acetyl',
               "e" => 'glu-c'
               );
#
# options variables. Declare all the variables you want to use here.
# If you want to pass a parameter to the program, put a colon after
# the variable (single letter)
#
getopts('m:e:n');
my $enzyme   = ( defined $opt_e ) ? $opt_e : "t";
my $missed   = ( defined $opt_m ) ? $opt_m : 0;
my $ntermonly = ( defined $opt_n ) ? 1 : 0;
my %sequence = ();
my $header;

#
# error check for missed cleavages
#
$missed = int($missed);  # ensure it is an integer
$missed = 0 if ($missed < 0 || $missed > 5 );

foreach my $file ( @ARGV ) {
#
# open file
#
    open(FILE,$file) or die "error: unable to open file $file\n";
    while ( <FILE> ) {
#
# read in sequences in fasta format
#
        next if /^\s*$/;   # ignore blank lines
        if ( /^>(\S+)/ ) {
            $header = $1;
        } else {            # assume its sequences
            chomp;
            $sequence{$header} .= $_;
        }
    }
    close(FILE);
#
# loop through the sequence hash, analysing one sequence at
# a time, sorting the keys (protein headers) as we go
#
    foreach my $protein ( sort keys %sequence ) {

        if ( $sequence{$protein} =~ /[BOUJZX]/ ) {
            print "## Warning: protein $protein contains unusual amino acids\n";
        }

        my $pepcount = 0;
        @peptides = digest($sequence{$protein},$enzyme);
        for ( my $i=0; $i<@peptides; $i++ ) {
#
# deal with limit peptides first ('limit' means with no missed cleavages)
#
            $ntype = "I";   # I means internal
            $ntype = "C" if ( $i == (@peptides - 1) );  # cterminal peptide
            $ntype = "N" if ( $i == 0 );   # n terminal peptide

            $pepcount++;

            unless ( $ntermonly && $i ) {
                printf ">%s peptide %4d missed=0 %s %s \n%s\n",
                $protein, $pepcount, $ntype, $enzname{$enzyme}, $peptides[$i];
            }
#
# then add on any missed cleavages
#
            my $missed_pep_seq = $peptides[$i];
            for ( my $j=1; $j<=$missed; $j++) {
                if ( $i+$j < @peptides ) {
                   $missed_pep_seq .= $peptides[$i+$j];
                   $pepcount++;
                   $ntype = "C" if ( $i+$j == (@peptides - 1));
                   unless ( $ntermonly && $i ) {
                      printf ">%s peptide %4d missed=%d %s %s \n%s\n",
                      $protein, $pepcount, $j, $ntype, $enzname{$enzyme},
                      $missed_pep_seq;
                   }
                }
            }

        }
    }
}

sub digest {
        my ($sequence, $enzyme) = @_;
        my %specificity = (
                "t" => '([KR])([^P\n])',
                "k" => '(K)([^P\n])',
                "r" => '(R)([^P\n])',
                "v" => '([DE])([^P\n])',
                "e" => '(E)([^P\n])',
                "x" => '(R)([^P\n])'
        );

#
# we digest the sequence by replacing all the "cuttable" sites with
# a new line character. We have to do the operation twice because
# of the way Perls' regex works. It will have trouble dealing with
# examples such as "..ANMKKIASL.." where it spots the KK but and
# replaces it with "K\nK" but misses the second K.
#
# There some fancy pattern matching tricks to do this in a single line,
# but this way works too and is fast enough not to be a problem
#
        $sequence =~ s/$specificity{$enzyme}/$1\n$2/g;
        $sequence =~ s/$specificity{$enzyme}/$1\n$2/g;
        @peptides = split /\n/, $sequence;
        return @peptides;
}