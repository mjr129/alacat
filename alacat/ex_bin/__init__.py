def get_file_name( file_name ):
    import os
    return os.path.join( os.path.dirname( __file__ ), file_name )
