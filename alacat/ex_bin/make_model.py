#
# Creates the "default" DOESITFLY model used by ALACATDESIGNER
#
# This is a self contained utility script and is not imported by the ALACAT package 
#

import os
import doesitfly
from mhelper.location_helper import find_file

data_source: doesitfly.FlySource = \
    doesitfly.FlySource(
            name = "circbp",
            data_source = find_file( "circ3/seamass/output/log2_component_variances.tsv" ),
            seq_column = "Component",
            class_column = "v",
            transformation = lambda x: x,
            regression = True,
            sanitise = False )

data: doesitfly.FlyData = \
    data_source.read()

split: doesitfly.FlySplit = \
    data.holdout( 0.75 )

algorithm: doesitfly.FlyPredictor = \
    doesitfly.algorithms.SkLearnPredictors.mlp_regressor()

algorithm.train( data )

predictions: doesitfly.FlyPredictions = \
    algorithm.predict( data )

# print( predictions.get_confusion_matrix().get_metrics_table().to_text() )

file_name: str = \
    os.path.join( os.path.dirname( __file__ ), "circ3.doesitfly" )

algorithm.save( file_name )
