"""
Scores - these are produced as outputs.
"""
from collections import Counter, defaultdict
from enum import Enum
from itertools import chain
from typing import Callable, cast, Counter as _Counter, DefaultDict, Dict, Generic, Iterable, List, Optional, Sequence, Set, Tuple, Type, TypeVar, Union

from mhelper import array_helper, exception_helper, maths_helper
from mhelper.exception_helper import safe_cast

from alacat import model  # Forwardref


TSubject = TypeVar( "TSubject" )
TSubjectIn = TypeVar( "TSubjectIn" )
TSubjectOut = TypeVar( "TSubjectOut" )
TKey = TypeVar( "TKey" )

AnyScoreType = Union[int, float, str, bool, "BadValue", "Rank", Enum]  # NOTE: Ensure `_ACCEPTED_TYPES` matches this.
AnyFinalScore = Union[str, "Score[TSubject]"]


class EProvided( Enum ):
    """
    The score usually applied when a provider-type `Handler` yields an output.
    
    :cvar NONE:     This handler did not yield for the input.
    :cvar NORMAL:   This handler yielded this subject.
    :cvar FAVOURED: This handler yielded this subject.
                    The subject should be considered favoured in the pipeline.
                    See `find_favoured`.  
    """
    NONE = 0
    NORMAL = 1
    FAVOURED = 2


    @staticmethod
    def find_favoured( mandatory: object, scores: "ScoresTable[TSubject]" ) -> List[TSubject]:
        """
        Locates the subjects that are flagged with a `FAVOURED` enum member
        in their provision scores.
         
        :param mandatory: A boolean-coercible object that can be used to
                          determine if at least one favoured output is
                          expected. This is used as "the user's request" error
                          text if the operation fails. 
        :param scores:  Score table from a provision stage. 
        :return: 
        """
        favoured = [subject for subject, scores in scores.by_subject.items() if any( score.value is EProvided.FAVOURED for score in scores.values() )]

        if mandatory and not favoured:
            klass = scores.t_subject.__name__
            keys_str: str = "\n".join( f"* {x.__class__.__name__}: {x!r}" for x in sorted( str( x ) for x in frozenset( scores.columns ) ) )

            if isinstance( mandatory, list ) or isinstance( mandatory, tuple ):
                mandatory = "\n".join( f"* {x.__class__.__name__}: {x!r}" for x in mandatory )

            raise PipelineError( f"There are no `{klass}` `{EProvided.FAVOURED!s}` scores.\n"
                                 f"The user's requested {klass}(s) cannot be propagated to the final model.\n"
                                 f"Either ensure a handler producing such scores exists, or do not request specific\n"
                                 f"{klass}(s).\n"
                                 f"The keys I received are:\n"
                                 f"{keys_str}\n"
                                 f"The user's request is:\n"
                                 f"{mandatory}" )

        return favoured


class RawScore( Generic[TSubject] ):
    """
    A cell in the matrix of values written out.
    """
    __slots__ = "origin", "column", "subject", "value", "input",
    empty: "RawScore" = None

    STRING_TO_VALUE_MAP = { "False"   : False,
                            "True"    : True,
                            "None"    : None,
                            "Empty"   : "",
                            "Provided": EProvided.NORMAL }
    VALUE_TO_STRING_MAP = { v: k for k, v in STRING_TO_VALUE_MAP.items() }


    def __init__( self,
                  origin: "model.Handler",
                  key: Union["model.Column", "model.HColumn"],
                  subject: TSubject,
                  value: AnyScoreType,
                  input: Optional["model.Pending"] ):
        """
        :param origin:  Handler that produced this score 
        :param key:     Identifier to column 
        :param subject: Identifier to row 
        :param value:   Value of the score.
        """
        if value.__class__ not in _ACCEPTED_TYPES and not isinstance( value, Enum ):
            raise exception_helper.type_error( f"{key} value from {origin}", value, _ACCEPTED_TYPES.union( { Enum } ) )

        if not isinstance( key, model.Column ):
            try:
                key = origin.bound_columns[key]
            except KeyError as ex:
                raise RuntimeError( f"The `HColumn` (``{key!r}``) in the `Handler` (``{origin!r}``) has not been bound to an output column.\n"
                                    f"Ensure that `HColumn`\s are bound by calling `{model.Handler.bind_column.__qualname__}` from within the `{model.Handler.__qualname__}`\s `__init__` method.\n"
                                    f"This Handler has defined the following scores: ``{origin.bound_columns!r}``." ) from ex

        assert subject is not None, "Subject cannot be None"

        self.origin: model.Handler = safe_cast( "origin", origin, model.Handler )
        self.column: model.Column = key
        self.subject: TSubject = subject
        self.value: AnyScoreType = value
        self.input: model.Pending = input


    def __repr__( self ):
        return f"{self.origin!r} {self.column!r} {self.subject!r} {self.value!r}"


class RawScoresTable( Generic[TSubject] ):
    """
    Collection of `RawScore`\s.
    """
    __slots__ = "scores",


    def __init__( self, scores: Sequence[RawScore[TSubject]] ):
        RawScoresTable.__assert_coherent( scores )
        self.scores = scores


    @staticmethod
    def create( scores: Iterable[RawScore[TSubject]] ) -> "RawScoresTable[TSubject]":
        """
        Creates a `RawScoresTable`: 
        
        * explicitly adds any undefined `RawScore`\s (missing score-key to
          subject pairs) to complete the table.
        * generates rank-scores from rankings for any `HColumn`\s with the
          `rank` attribute set.
          
        :param scores:          Scores to add to the collection. 
        :return:                The collection. 
        """
        scores = RawScoresTable.__generate_missing( scores )
        RawScoresTable.__assert_coherent( scores )

        # Generate derived (rank) scores
        # We have two methods of calculating the ranks 
        s0: RawScore[TSubject] = scores[0]
        if isinstance( s0.subject, model.ComesFromProtein ) and s0.origin.state.model.partition_rankings:
            scores = RawScoresTable.__generate_ranks_across_partitions( scores, lambda x: x.subject.protein )
        else:
            scores = RawScoresTable.__generate_ranks_across_all( scores )

        return RawScoresTable( scores )


    @staticmethod
    def __assert_coherent( scores: Iterable[RawScore[TSubject]] ):
        """
        Checks that there are no missing values in a long-format matrix.
        
        There shouldn't be because we `__generate_missing` values!
        
        :param scores:  Matrix to check (in long format).
        """
        all_scores: Dict[Tuple[TSubject, model.Column], RawScore[TSubject]] = \
            { (score.subject, score.column): score for score in scores }
        subjects = set( score.subject for score in all_scores.values() )
        keys = set( score.column for score in all_scores.values() )

        for key in keys:
            for subject in subjects:
                if all_scores.get( (subject, key) ) is None:
                    raise RuntimeError( "The matrix is incomplete, it is missing at least the value for column `{}` and row `{}`.".format( key, subject ) )


    @staticmethod
    def __generate_missing( scores: Iterable[RawScore[TSubject]] ) -> Sequence[RawScore[TSubject]]:
        """
        Adds missing values to a long-format matrix (missing values receive the
        value `MISSING_VALUE`).
        
        :param scores:  Matrix (in long format), possibly with missing values.
        :return:        A new matrix (in long format), with no missing pairs.
        """
        matrix: Dict[Tuple[TSubject, model.Column], RawScore[TSubject]] = \
            { (score.subject, score.column): score for score in scores }
        assert matrix, "Expected at least one result, but none were generated."

        # Add missing scores
        rows: Set[TSubject] = set( score.subject for score in matrix.values() )
        columns: Set[model.Column] = set( score.column for score in matrix.values() )
        origins: Dict[model.Column, model.Handler] = { score.column: score.origin for score in matrix.values() }

        for column in columns:
            missing = []  # missing subjects

            for subject in rows:
                score = matrix.get( (subject, column) )

                if score is None:
                    missing.append( subject )

            for subject in missing:
                scorer: model.Handler = origins[column]

                matrix[subject, column] = RawScore( origin = scorer,
                                                    key = column,
                                                    subject = subject,
                                                    value = MISSING_VALUE,
                                                    input = None )
        return tuple( matrix.values() )


    @staticmethod
    def __generate_ranks_across_partitions( source: Sequence[RawScore[TSubject]],
                                            partition_fn: Callable[[RawScore[TSubject]], object]
                                            ) -> Sequence[RawScore[TSubject]]:
        """
        As `__generate_derived`, but performs within-group rankings rather
        than global rankings.
           
        :param source:          Source, as per `__generate_ranks_across_all`.
        :param partition_fn:    Grouping function, as per `array_helper.group_by`. 
        :return: 
        """
        partitions = array_helper.group_by( source, partition_fn ).values()
        results = []

        for partition in partitions:
            results.extend( RawScoresTable.__generate_ranks_across_all( partition ) )

        # Ranks may have been rejected for *some* partitions (because they don't have enough values)
        # So make sure to create values for the missing ones now
        results = RawScoresTable.__generate_missing( results )

        return results


    @staticmethod
    def __generate_ranks_across_all( source: Sequence[RawScore[TSubject]] ) -> Sequence[RawScore[TSubject]]:
        """
        Creates derived scores by sorting and ranking the scores which require
        a rank generating.
        
        :param source: Scores matrix to generate ranks for.
                       For all keys and subjects, the corresponding key-subject
                       must be present once and once only.
        """
        assert source
        rank_scores = []  # Matrix of "ranks" in long-format, out.

        #
        # Group scores by their column
        #
        by_column: DefaultDict[model.Column, List[RawScore]] = defaultdict( list )

        for score in source:
            by_column[score.column].append( score )

        #
        # Process each column in turn
        #
        for column, scores_list in by_column.items():
            rank_column: Optional[model.HColumn] = column.specification.rank_column

            if rank_column is None:
                # This column doesn't want to be ranked
                continue

            #
            # Filter out things that can't be ranked
            # (and by that I mean error messages, etc.)
            #
            f_scores_list = []

            for score in scores_list:
                assert isinstance( score, RawScore )
                v = score.value

                if isinstance( v, float ) or isinstance( v, int ):
                    f_scores_list.append( score )
                else:
                    if isinstance( v, NamedFactor ):
                        if v.grade == EGrade.FAIL:
                            rv = Rank.NONE_FAIL
                        elif v.grade == EGrade.ERROR:
                            rv = Rank.NONE_ERROR
                        else:
                            rv = Rank.NONE_NA
                    else:
                        rv = Rank.NONE_NA

                    rank_scores.append( RawScore( origin = score.origin,
                                                  key = rank_column,
                                                  subject = score.subject,
                                                  value = rv,
                                                  input = score.input ) )

            #
            # Group scores by value
            # (so same values get the same rank)
            #
            by_value: DefaultDict[object, List[RawScore]] = defaultdict( list )

            for score in f_scores_list:
                assert isinstance( score, RawScore )
                assert score.column is column

                by_value[score.value].append( score )

            # Ranks don't make sense for a list of 1 (i.e. when all items have the same value)
            # Abort now if that is the case
            if len( by_value ) == 1:
                for score in f_scores_list:
                    rank_scores.append( RawScore( origin = score.origin,
                                                  key = rank_column,
                                                  subject = score.subject,
                                                  value = Rank.NONE_ONLY,
                                                  input = score.input ) )
                continue

            #
            # Sort our list
            # (put the *best* values first, where *best* is lower or higher as stated in the column spec)
            #
            by_value_in_order = sorted( by_value.items(),
                                        key = lambda x: x[0] if RawScoresTable.__is_basic_type( x[0] ) else 0,
                                        reverse = column.specification.is_rank_reversed )

            #
            # Assign the ranks!
            # (add to `rank_scores`)
            #
            rank = 1
            num_alt = len( by_value )

            for index, (value, scores) in enumerate( by_value_in_order ):
                # Calculate quantile
                # - i.e. (R-1)/(N-1) -- is this right?
                #                       yes, this gives the proportion of entries with a *lower* rank
                #                       the proportion *higher* would also be interesting, but probably a bit of
                #                       information overload!
                quantile = (rank - 1) / num_alt

                for score in scores:
                    assert isinstance( score, RawScore )
                    rank_info = Rank( absolute = cast( float, score.value ),
                                      rank = rank,
                                      rank_of = num_alt,
                                      quantile = quantile )
                    rank_scores.append( RawScore( origin = score.origin,
                                                  key = rank_column,
                                                  subject = score.subject,
                                                  value = rank_info,
                                                  input = score.input ) )

                rank += len( scores )

        # Return the original table, with our newly crafted ranks added onto the end of it
        return tuple( chain( source, rank_scores ) )


    @staticmethod
    def __is_basic_type( x: object ) -> bool:
        return any( isinstance( x, t ) for t in [int, float] )


class EGrade( Enum ):
    """
    After values have been assigned to each subject (e.g. `Peptide`, `Qbrick`)
    and `Column`, each value is assessed by the rule of the column
    (`Column.assessor`) and given a grade (`EGrade`)
    
    Along with the weights of the columns (`Column.weight`) the grades
    (`Score.grade`) determine the final number of points
    (`EntitySummary.total_points`) each subject receives and how subjects are
    ranked against each other. See `EWeight` for calculation details.
    
    :cvar PASS:    Subject passed evaluation (i.e. hint to use this `Peptide`/`Qbrick`).
                   Subject receives points equal to the weight of the column (`Column.weight`).
                   
    :cvar FAIL:    Subject failed evaluation.
                   Subject receives no points.
                   
    :cvar INFO:    Informational message only.
                   Subject receives no points.
    
                   Unlike `FAIL`, to compare `INFO` against `PASS`/`FAIL` is considered a programmatic error.
                   i.e. `INFO` values should be consistent across all subjects for a given `Column`.
                             
    :cvar ERROR:   Subject could not be evaluated.
    
                   Metrics with this grade are *not comparable*.
                   This is because an error indicates that the value is *unknown* and thus subjects
                   cannot be compared based on this data.
                   Hence, when two individual subjects are ranked, if either subject reports an
                   ERROR for a particular column then any points from the other subject, for the
                   same column, are discounted. Note that, in rare circumstances, this caveat
                   actually makes it possible for a subject with fewer total-points to rank above a
                   subject with more total-points, when the points for only the intersection of
                   non-error columns are greater for the first subject.   
    """
    PASS = 1
    FAIL = 2
    INFO = 3
    ERROR = 4

    def __lt__( self, other ):
        return self.value < other.value


class Score( Generic[TSubject] ):
    """
    A `RawScore` with an `EGrade` attached to it.
    """

    __slots__ = "score", "grade", "message"


    def __init__( self,
                  score: "RawScore[TSubject]",
                  grade: EGrade ):
        """
        :param score:       Score from which the description originates 
        :param grade:       Traffic-light symbol      
        """
        self.score: RawScore[TSubject] = score
        self.grade = grade


    def __repr__( self ):
        return f"Score(column={self.column.specification!r}, symbol={self.grade}, ...)"


    @property
    def origin( self ) -> "model.Handler":
        """
        Handler that output the score (value, column and for providers, the subject). 
        """
        return self.score.origin


    @property
    def column( self ) -> "model.Column":
        """
        Column the score applies to.
        """
        return self.score.column


    @property
    def subject( self ) -> TSubject:
        """
        Subject the score applies to.
        """
        return self.score.subject


    @property
    def value( self ) -> Optional[object]:
        """
        Actual value of the score.
        """
        return self.score.value


class ScoresTable( Generic[TSubject] ):
    """
    Collection of `Score`\s.
    
    :ivar subject_to_rank:  Dictionary of subjects to their rank.
                            Unlike `rank_subjects` this accounts for identically ranked subjects.
    :ivar subject_order:    List of subjects in rank order.
                            If cases where subjects have the same rank, they are sorted alphabetically, by their `__str__`.
    """
    __slots__ = "t_subject", "all_scores", "by_subject", "columns", "matrix", "subjects", "subject_to_rank", "scores_rows", "subject_to_row", "subject_order"
    subjects: Tuple[TSubject, ...]
    matrix: Tuple[Tuple[Score[TSubject], ...], ...]
    t_subject: Type[TSubject]
    columns: Tuple["model.ColumnOverride", ...]
    by_subject: Dict[TSubject, Dict["model.Column", "model.Score[TSubject]"]]


    def __init__( self, scores: Iterable[Score[TSubject]], *, partition: Optional[bool] = None ):
        exception_helper.safe_cast_iter( "scores", scores, Score )

        #
        # Maintain iterable as tuple
        #
        tup: Tuple[Score[TSubject]] = tuple( scores )

        if not tup:
            raise ValueError( "Trying to create a `ScoresTable` with no scores." )

        self.all_scores: Tuple[Score[TSubject], ...] = tup

        #
        # Create a map of subject to keys to scores
        #
        by_subject: Dict[TSubject, Dict[model.Column, Score[TSubject]]] = { }

        for final_score in tup:
            d = by_subject.setdefault( final_score.subject, { } )
            assert final_score.column not in d
            d[final_score.column] = final_score

        self.by_subject: Dict[TSubject, Dict[model.Column, Score[TSubject]]] = by_subject

        #
        # Create a tuple of keys
        #
        keys: Set[model.Column] = set( score.column for score in tup )
        self.columns: Tuple[model.Column, ...] = tuple( sorted( keys, key = str ) )

        #
        # Assert all subjects possess all keys
        #
        for subject, sub_scores in self.by_subject.items():
            my_keys: Set[model.Column] = set( key for key in sub_scores )

            if my_keys != keys:
                raise ValueError( f"All subjects must be scored according to the same criteria. This is not the case for `{subject!r}`." )

        #
        # Create a tuple of subjects
        #
        self.subjects: Tuple[TSubject, ...] = tuple( self.by_subject )

        #
        # Record and assert the type of the subjects
        #
        self.t_subject: Type[TSubject] = type( self.subjects[0] )
        exception_helper.safe_cast_iter( "subjects", self.subjects, self.t_subject )

        #
        # Make a matrix of scores
        #
        matrix: List[Tuple[Score[TSubject], ...]] = []

        for subject in self.subjects:
            bs = self.by_subject[subject]
            matrix_row = []

            for key in self.columns:
                score = bs[key]
                matrix_row.append( score )

            matrix.append( tuple( matrix_row ) )

        self.matrix = tuple( matrix )
        self.scores_rows = tuple( _ScoresRow( row, subject ) for subject, row in zip( self.subjects, self.matrix ) )
        self.subject_to_row = { subject: row for subject, row in zip( self.subjects, self.scores_rows ) }

        #
        # Subject to rank
        #
        self.subject_to_rank: Dict[TSubject, int] = self.__make_subject_to_rank( self.scores_rows, partition = partition )

        #
        # Subject order (by rank)
        #
        dl = array_helper.make_dict_list( (rank_, subject_) for subject_, rank_ in self.subject_to_rank.items() )
        so = []

        for rank, subjects in sorted( dl.items(), key = lambda x: x[0] ): # LOWEST ranks first
            subjects = sorted( subjects,
                               key = lambda x: x.accession )  # NOTE: This ensures our order is deterministic, even where ranks tie
            so.extend( subjects )

        self.subject_order: List[TSubject] = so  # Highest ranked first, then alphabetically


    @staticmethod
    def __make_subject_to_rank( scores_rows: Tuple["_ScoresRow"], *, partition: Optional[bool] ) -> Dict[TSubject, int]:
        r = array_helper.WriteOnceDict()
        s0 = scores_rows[0].row[0]

        if partition is None:
            partition = s0.origin.state.model.partition_rankings

        if partition and isinstance( s0.subject, model.ComesFromProtein ):
            partition_groups = { cast( model.ComesFromProtein, x.subject ).protein for x in scores_rows }
            partition_accessor = lambda g, x: x.subject.protein is g
        else:
            partition_groups = [None]
            partition_accessor = lambda _, __: True

        for partition_group in partition_groups:
            pertinent = [x for x in scores_rows if partition_accessor( partition_group, x )]

            r.update( { pertinent[index].subject: rank
                        for index, rank
                        in enumerate( array_helper.rank_tie( pertinent, reverse = True ) ) } ) # Reverse - HIGHEST scores first

        subjects = { x.subject for x in scores_rows }
        assert all( subject in r for subject in subjects )

        r.freeze()
        return r


    def __repr__( self ):
        return f"{type( self ).__name__}[{self.t_subject.__name__}]({self.all_scores!r})"


    @property
    def shape( self ):
        """
        Shape of the matrix.
        """
        return len( self.subjects ), len( self.columns )


    @staticmethod
    def create( source: Union["RawScoresTable[TSubject]", Iterable["RawScore[TSubject]"]], *, partition: Optional[bool] = None ) -> "ScoresTable[TSubject]":
        """
        Creates a `ScoresTable` by asking the scores to describe
        themselves in the context of their rank.
        
        :param source:      A `RawScoresTable`, or an iterable of scores.
        
                            For all keys and subjects, the corresponding key-subject
                            pair must be present once and once only.
        :param partition:   When ranking, rank scores on a *per-protein* basis rather
                            than on an overall basis. This parameter is only for 
                            subjects that correspond to a protein (e.g. peptides,
                            qbricks) and is ignored if the subjects do not correspond to
                            a protein (e.g. qconcats). The default value of `None` uses
                            the `partition_rankings` value specified by the `Model`.
                            
        """
        if isinstance( source, RawScoresTable ):
            source = source.scores

        r: List[Score] = []

        for raw_score in source:
            score: Score = ScoresTable.__assess_score( raw_score )
            r.append( score )

        assert r
        return ScoresTable( r, partition = partition )

    def reassess( self, *, partition: bool ) -> "ScoresTable[TSubject]":
        """
        Re-assesses the table.
        
        This can be called if the assessors on the columns have changed.
        (Note this never happens in the program itself, which considers all
        tables immutable. It is debugging feature to trial the effects of
        different assessors without having to run the whole pipeline again).  
        """
        return self.create( [x.score for x in self.all_scores], partition = partition )


    @staticmethod
    def __assess_score( score: RawScore[TSubject] ) -> "Score[TSubject]":
        result = score.column.assessor( score.origin, score )

        if not isinstance( result, EGrade ):
            raise exception_helper.type_error( "result", result, EGrade )

        return Score( score, result )


    def get_top_cum_rank( self, key: Callable[[TSubject], TKey] ) -> Tuple[TKey, ...]:
        """
        Calculates a cumulative rank for each `key` and returns the results in
        order (lowest ranked first) 
        
        :param key:         Key function
        :return: 
        """
        return tuple( x[0] for x in self.cum_rank( key ).most_common() )


    def cum_rank( self, key: Callable[[TSubject], TKey] ) -> _Counter[TKey]:
        """
        Returns a counter for each `key` on the subjects, indicating its total
        (summed) rank.
        
        If the number of subjects for each key differs, an exception is raised.
        """
        r = defaultdict( list )

        for subject, rank in self.subject_to_rank.items():
            key_ = key( subject )
            r[key_].append( rank )

        for (key1, ranks1), (key2, ranks2) in array_helper.lagged_iterate( r.items() ):
            if len( ranks1 ) != len( ranks2 ):
                raise ValueError( f"The number of subjects for each key differs. Notably, `{key1}` has `{len( ranks1 )}` subjects and `{key2}` has `{len( ranks2 )}` subjects." )

        return Counter( { k: sum( v ) for k, v, in r.items() } )


    def get_top( self,
                 key: Callable[[TSubject], TKey] = None,
                 *,
                 n: Union[int, Iterable[TSubject]],
                 favour: Iterable[TSubject] = None,
                 mandate: bool = True ) -> Tuple[TSubject, ...]:
        """
        Gets the top `n` subjects for every given `key`
        (e.g. the top 2 peptides for each protein).
        
        .. note::
        
            To allow models to be re-run but selecting different entities by
            their "long name" this function *must* be deterministic, so even
            if two entities rank the same as must make a coherent choice.
        
        :param key:     A callable to translate a `TSubject` into its key.
                        If `None` the `TSubject` *is* the key.
        :param n:       Either - the number of results, per key *or* a previous
                        result (iterable) from `get_top` to which `n` will be
                        matched on a per-key basis.
        :param favour:  Prioritise results from this set of subjects above all others.
        :param mandate: What to do when the number of favoured values is above n for any given key.
                        `true` - multiply "n" by an integer such that favour.size <= n
                        `false` - remove the lowest values from `favour` such that favour.size <= n  
        :return: 
        """
        return self.get_top_list( self.subject_order, key = key, n = n, favour = favour, mandate = mandate )


    @staticmethod
    def get_top_list( subject_order: Iterable[TSubject],
                      key: Callable[[TSubject], TKey] = None,
                      *,
                      n: Union[int, Iterable[TSubject]],
                      favour: Iterable[TSubject] = None,
                      mandate: bool = True ) -> Tuple[TSubject, ...]:
        """
        See `get_top`.
        """
        return tuple( x
                      for lst in ScoresTable.get_top_dict( subject_order, key, n = n, favour = favour, mandate = mandate ).values()
                      for x in lst )


    @staticmethod
    def get_top_dict( subject_order: Iterable[TSubject],
                      key: Callable[[TSubject], TKey] = None,
                      *,
                      n: Union[int, Iterable[TSubject]],
                      favour: Iterable[TSubject] = None,
                      mandate: bool = True ) -> Dict[TKey, List[TSubject]]:
        """
        As `get_top`, but results sorted by key. 
        """
        if key is None:
            key = lambda _: cast( TKey, None )

        if isinstance( n, int ):
            if n <= 0:
                raise ValueError( "n must be above 0." )

            matched = None
        else:
            matched = Counter( key( subject ) for subject in n )

        key_to_subjects = defaultdict( list )

        subjects: Iterable[Tuple[TSubject, bool]]

        if favour is not None:
            favour = frozenset( favour )
            subjects = chain( tuple( (subject, True) for subject in subject_order if subject in favour ),
                              tuple( (subject, False) for subject in subject_order if subject not in favour ) )
        else:
            subjects = ((subject, False) for subject in subject_order)

        for subject, is_favoured in subjects:
            l_key = key( subject )
            l_len: int = len( key_to_subjects[l_key] )
            l_max: int = matched[l_key] if matched is not None else n

            if l_len == 0 or (l_len % l_max != 0) or (mandate and is_favoured):
                key_to_subjects[l_key].append( subject )

        assert key_to_subjects

        return key_to_subjects


    def get_subject_counts( self, subject: TSubject ) -> Dict[EGrade, int]:
        """
        Gets a dict of RED/GREEN counts for the specified peptide's flags.
        """
        my_results = self.by_subject[subject].values()
        counter = Counter( result.grade for result in my_results )
        return counter


    def _get_scores_row( self, subject: TSubject ) -> "_ScoresRow":
        return _ScoresRow( self.matrix[self.subjects.index( subject )] )


    def get_scores( self, subject: TSubject ) -> Iterable[Score[TSubject]]:
        return self.by_subject[subject].values()


    def get_failures( self, subject: TSubject ) -> Iterable[Score[TSubject]]:
        """
        Gets the failed scores (EGrade.FAIL) for a particular subject.
        """
        return [x for x in self.by_subject[subject].values() if x.grade == EGrade.FAIL]


class _ScoresRow( maths_helper.Comparable ):
    """
    Permits scores to be compared between different subjects (aka. rows or
    peptides).
    """


    def __init__( self, row: Sequence[Score], subject: object = None ):
        """
        :param row:     Sequence of scores.
                        When comparing, the scores must appear in the same
                        order. 
        :param subject: Subject of the scores.
        """
        self.row = row
        self.subject = subject
        assert [x.subject is subject for x in row]


    def compare( self, other: "_ScoresRow", dbg = lambda x: None ):
        """
        This is our primary comparison function.
        We use it to determine if a peptide is better than another. 
        """
        sn = str( self.row[0].subject )
        on = str( other.row[0].subject )
        dbg( f"Compare [{sn}] ---VS--- [{on}]" )

        assert len( self.row ) == len( other.row )

        total_self = 0
        total_other = 0

        for score_self, score_other in zip( self.row, other.row ):
            if score_self.grade in (EGrade.ERROR, EGrade.INFO) or score_other.grade == (EGrade.ERROR, EGrade.INFO):
                continue

            weight: int = score_self.column.weight

            if score_self.grade == EGrade.PASS:
                total_self += weight

            if score_other.grade == EGrade.PASS:
                total_other += weight

        if total_self > total_other:
            return 1
        elif total_self == total_other:
            return 0
        else:
            return -1


    def __str__( self ):
        return "".join( score.grade.name[0] for score in self.row )


class NamedFactor:
    """
    For categorical values returned as scores.
    """
    __slots__ = "code", "name", "grade",

    def __init__( self, code: str, name: str, grade: EGrade ):
        self.code = code
        self.name = name
        self.grade = grade

    def __str__( self ):
        return self.code


    def __repr__( self ):
        return repr( self.name )

    def __eq__( self, other ):
        if not isinstance( other, NamedFactor ):
            return None

        return other.name == self.name

    def __hash__( self ):
        return hash( self.name )


class Rank:
    """
    Holds information about a rank.
    
    :cvar NONE_ERROR:   Unrankable value; error.
    :cvar NONE_FAIL:    Unrankable value; direct failure.
    :cvar NONE_NA:      Unrankable value; not applicable; non-numeric.
    :cvar NONE_ONLY:    Unrankable value; not applicable; list of 1; first and last.
    """
    NONE_ERROR = NamedFactor( "ERR", "ERROR", EGrade.ERROR )
    NONE_FAIL = NamedFactor( "FAI", "FAIL", EGrade.FAIL )
    NONE_NA = NamedFactor( "NA", "NA", EGrade.ERROR )
    NONE_ONLY = NamedFactor( "SNG", "SINGULAR", EGrade.ERROR )

    def __init__( self, absolute: float, rank: int, rank_of: int, quantile: Optional[float] ):
        """
        :param absolute:        Absolute value 
        :param rank:            Rank 
        :param rank_of:         Number of items ranked against
        :param quantile:        Proportion of data below 
        """
        assert isinstance( rank, int )

        self.absolute = absolute
        self.rank = rank
        self.rank_of = rank_of
        self.quantile = quantile


    def __str__( self ):
        return f"Q{self.quartile}"


    def __repr__( self ):
        return f"{self.__class__.__name__}({self.rank} / {self.rank_of} = {self.quantile})"


    @property
    def quartile( self ) -> Optional[int]:
        """
        Quartile (1-4), or `None` if we have no `quantile`.
        """
        if self.quantile is None:
            return None
        elif self.quantile < 0.25:
            return 1
        elif self.quantile < 0.5:
            return 2
        elif self.quantile < 0.75:
            return 3
        else:
            return 4


MISSING_VALUE = NamedFactor( "MISS", "missing", EGrade.ERROR )
_ACCEPTED_TYPES = { int, float, str, bool, NamedFactor, Rank, Enum, EProvided }

TScoreResult = Union[RawScore[TSubject], Iterable[RawScore[TSubject]], None]


class PipelineError( Exception ):
    """
    Error raised from ALACAT.
    Such errors are safe to expose to the end user over the web interface.
    """


    def __init__( self, message, critical = False ):
        super().__init__( message )
        self.critical = critical
