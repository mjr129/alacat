from itertools import chain
from typing import Generic, Iterable, List, Sequence, Tuple, Type, Union

from alacat.model.debugging import standard_log
from alacat.model.handler_base import Handler, ModelState
from alacat.model.outputs import Score, ScoresTable, TSubject, RawScore, RawScoresTable
from alacat.model.sequences import Qblock, Qmenu
from mhelper import exception_helper, utf_table

from .sequences import Peptide


class ScorerCollectionBase( Generic[TSubject] ):
    """
    !ABSTRACT
    
    Collection of `PeptideScorer` objects.
    Typically obtained from `HandlerFactoryCollection.create_scorers`.
    
    :cvar t_scorer: This must be set to the type of scorer by the derived class.
    """
    t_scorer: Type[Handler] = None
    
    
    def __init_subclass__( cls, **kwargs ):
        if not cls.__dict__.get( "__abstract__", False ) and cls.t_scorer is None:
            raise ValueError( f"Class {cls} has no :cvar:`t_scorer` and is not explicitly marked as abstract." )
    
    
    def __init__( self, scorers: Union[ModelState, Iterable[Handler[TSubject, TSubject]]] ):
        if isinstance( scorers, ModelState ):
            scorers = scorers.instantiate( self.t_scorer )
        
        scorers_: Tuple[Handler[TSubject, TSubject], ...] = tuple( sorted( scorers, key = lambda x: x.accession ) )
        exception_helper.safe_cast_iter( "scorers", scorers_, self.t_scorer )
        self.handlers = tuple( scorers_ )
    
    
    def score( self, subjects: Union[Sequence[TSubject], ScoresTable[TSubject]] ) -> ScoresTable[TSubject]:
        """
        Scores the specified `subjects` using the handlers in the collection.
        
        :param subjects:    Subjects to score.
                            If this is `ScoresTable` the existing scores will be merged 
                            into the new set.
        :return:            `RawScore`\s. 
        """
        standard_log( utf_table.format_box( f"STAGE: {self.t_scorer.__name__} scoring" ) )
        
        #
        # Extract "subjects" and "extra scores" if we got a `ScoresTable`
        #
        extra_scores: Iterable[Score[TSubject]]
        subjects: Tuple[TSubject, ...]
        
        if isinstance( subjects, ScoresTable ):
            extra_scores = subjects.all_scores
            subjects = subjects.subjects
        else:
            subjects = tuple( subjects )
            extra_scores = tuple()
        
        if not subjects:
            raise ValueError( "The peptides collection is empty. This is probably a mistake." )
        
        #
        # Calculate scores
        #
        all_scores: List[RawScore[TSubject]] = []
        state : ModelState = self.handlers[0].state
        
        for handler in self.handlers:
            with standard_log.time( f"Scoring {len( subjects ):,} subjects using {handler}" ) as timer:
                my_scores: Sequence[RawScore[TSubject]] = handler.query_all( subjects )
                timer.result = len( my_scores )
                state.profiling.append( (f"Scoring|{handler.accession}|{handler.accession}/Score", timer.now()) )
            
            all_scores.extend( my_scores )
        
        #
        # Close the collection
        #
        for handler in self.handlers:
            handler.close()
        
        values = RawScoresTable.create( all_scores )
        finals = ScoresTable.create( values )
        
        standard_log()
        
        return ScoresTable[TSubject]( tuple( chain( extra_scores, finals.all_scores ) ) )


# noinspection PyAbstractClass
class PeptideScorer( Handler[Peptide, Peptide] ):
    """
    The peptide scorers are responsible for assigning scores (metrics) to the
    peptides.
    
    Scores are assigned into the columns of the scores matrix, with the peptides
    forming the rows. 
    
    The values of the scores may be of use in identifying whether the peptides
    are quantotypic, or may be for information only.
    
    Disabling a scorer means that those scores will not be present in the
    output. Scorers can be disabled to speed up processing if their output is
    of no use.
    """
    __abstract__ = True


class PeptideScorerCollection( ScorerCollectionBase[Peptide] ):
    """
    Collection of peptide scorers, produces a `ScoresTable`.
    """
    t_scorer = PeptideScorer


# noinspection PyAbstractClass
class QmenuScorer( Handler[Qmenu, Qmenu] ):
    """
    The `QmenuScorer`\s are to `Qconcat`\s and `Qmenu`\s as `PeptideScorer`\s are to
    `Peptide`\s. See `PeptideScorer` for more details. 
    """
    __abstract__ = True


class QmenuScorerCollection( ScorerCollectionBase[Qmenu] ):
    t_scorer = QmenuScorer


class QblockScorer( Handler[Qblock, Qblock] ):
    """
    The `QblockScorer`\s are to `Qbricks`\s and `Qblocks`\s as `PeptideScorer`\s
    are to `Peptide`\s. See `PeptideScorer` for more details.
    """
    __abstract__ = True


class QblockScorerCollection( ScorerCollectionBase[Qblock] ):
    """
    Collection of `QbrickScorer`\s.
    """
    t_scorer = QblockScorer
