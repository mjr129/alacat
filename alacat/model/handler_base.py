import inspect
import operator
import random
import time
from collections import Counter
from itertools import chain
from typing import Callable, cast, ClassVar, Counter as TCounter, Dict, Generic, Iterable, List, Optional, Sequence, \
    Set, Tuple, Type, TypeVar, Union

from alacat import model
from alacat.model.accession import Entity, DbData
from alacat.model.debugging import standard_log
from alacat.model.outputs import AnyScoreType, TScoreResult, TSubject, TSubjectIn, TSubjectOut, RawScore
from mhelper import exception_helper, reflection_helper, string_helper
from mhelper.array_helper import ordered_insert, WriteOnceDict
from mhelper.generics_helper import NestedSequence
from mhelper.special_types import TTristate, NOT_PROVIDED

AssessorDelegate = object

TAssessor = Union[str, AssessorDelegate]
TKey = TypeVar( "TKey" )
TValue = TypeVar( "TValue", bound = Entity )
TSelf = TypeVar( "TSelf" )


class Assessor:
    """
    The `Assessor`\s determine rules to denote what signifies a "good value": 
    
        When a subject is assessed, for every metric (`Score`) that passes
        (`EGrade.PASS`) the rule (`Assessor`) of its column (`Column`), the
        points of the subject are increased by the weight of the `Column`. 
        -- See `EWeight` for more details.
    
    Assessment's are mostly binary, meaning that they typically return PASS or
    FAIL, though ERROR and INFO flags are also supported internally (see
    `EGrade` for details).
    
    The *indifferent* assessment (`Assessors.indifferent`) is a special value,
    which means an assessment is not applied.
    """
    __abstract__ = True

    def __repr__( self ):
        return string_helper.auto_repr( self )

    def __call__( self, origin: "model.Handler", score: "model.Score" ) -> "model.EGrade":
        raise NotImplementedError( "abstract" )


class Assessors:
    # noinspection PyAbstractClass
    class GuiCompatibleAssessor( Assessor ):
        """
        !ABSTRACT
        
        For compatibility with the GUI only an assessor requires a `parameters`
        field, describing the ctor parameters.
        All parameters must either be floats/ints or exist in the lookup table
        described in `alacat.gui.formatting.AssessmentField`.
        These assessors should be present in the `Assessors.builtin` array so
        the GUI can actually make use of them.
        
        Custom assessors do not require this since they don't appear in the GUI.
        """
        parameters: Tuple[object]
        __abstract__ = True
        class_title: ClassVar[str] = None  # !ABSTRACT, for GUI

        def __eq__( self, other ):
            if other.__class__ != self.__class__:
                return False

            return other.parameters == self.parameters

    # noinspection PyAbstractClass
    class Compare( GuiCompatibleAssessor ):
        """
        Basic logical comparison assessor.
        
        The value must logically compare against a threshold value.
        """
        comparison: ClassVar[Callable[[object, object], bool]]  # !ABSTRACT
        symbol: str  # for GUI, !ABSTRACT
        __abstract__ = True

        def __init__( self, value ):
            self.value = value

        @property
        def parameters( self ):
            return self.value,

        def __call__( self, _, score ):
            v = score.value

            if isinstance( v, model.NamedFactor ):
                return v.grade

            if self.comparison( v, self.value ):
                return model.EGrade.PASS
            else:
                return model.EGrade.FAIL

        def __str__( self ):
            return f"Xᵢ {self.symbol} {self.value!r}"

    class NotEqual( Compare ):
        """
        Rule: Not equal to
        ------------------
        
            Xᵢ ≠ θ₁  
        
        Overview
        --------
        
        When applied to a column, only cell values that are not equal to the
        parameter are passed. Cell values equal to the parameter are failed.
        """
        comparison = operator.ne
        class_title = "≠"
        symbol = "≠"

    class Equal( Compare ):
        """
        Rule: Equal to
        --------------
        
            Xᵢ = θ₁
        
        Overview
        --------
        
        When applied to a column, only cell values that are equal to the
        parameter are passed. Cell values not equal to the parameter are failed.
        """
        comparison = operator.eq
        class_title = "="
        symbol = "="

    class Min( Compare ):
        """
        Rule: Greater than or equal to
        ------------------------------
        
            Xᵢ ≥ θ₁
        
        Overview
        --------
        
        When applied to a column, only cell values that are greater than or equal to the
        parameter are passed. Cell values less than the parameter are failed.
        """
        comparison = operator.ge
        class_title = "≥"
        symbol = "≥"

    class Max( Compare ):
        """
        Rule: Less than or equal to
        ---------------------------
        
            Xᵢ ≤ θ₁
            
        Overview
        --------
        
        When applied to a column, only cell values that are less than or equal to the
        parameter are passed. Cell values greater than the parameter are failed.
        """
        comparison = operator.le
        class_title = "≤"
        symbol = "≤"

    class MinMax( GuiCompatibleAssessor ):
        """
        Rule: In range
        --------------
        
            θ₁ ≤ Xᵢ ≤ θ₂
        
        Overview
        --------
        
        When applied to a column, only cell values that lie within the range are passed.
        Cell values outside of this range are failed.
        
        Usage notes
        -----------
        
        The range is defined by the two parameters, and includes both endpoints.
        """
        class_title = "Between"

        def __init__( self, min, max ):
            self.min = min
            self.max = max

        def __str__( self ):
            return f"{self.min!r} ≤ Xᵢ ≤ {self.max!r}"

        @property
        def parameters( self ):
            return self.min, self.max

        def __call__( self, _, score ):
            v = score.value
            
            if isinstance( v, model.NamedFactor ):
                return v.grade

            if self.min <= v <= self.max:
                return model.EGrade.PASS
            else:
                return model.EGrade.FAIL

    class Percentile( GuiCompatibleAssessor ):
        """
        Rule: First percentile rank
        ---------------------------
        
            rank(Xᵢ) / |X| ≤ θ₁
        
        Overview
        --------
        
        When applied to a column, cell values are ranked. Ranks comprising the first n% are passed,
        while all others are failed.
        
        
        Usage notes
        -----------
        
        n is set based on the parameter, which should lie in the range [0..1].
        
        percentiles are based on the number of values *below*.
        i.e. There can be a score at the 0%ile, but not at the 100%ile. 
        """
        class_title = "Rank ≤"
        value: float

        def __init__( self, value: float ):
            if not 0 <= value <= 1:
                raise ValueError( f"`value` {value} should be between 0 and 1." )

            self.value = value

        def __str__( self ):
            return f"rank(Xᵢ) / |X| ≤ {self.value!r}"

        @property
        def parameters( self ):
            return self.value,

        def __call__( self, _, score ):
            v: object = score.value

            if isinstance( v, model.NamedFactor ):
                return v.grade

            if not isinstance( v, model.Rank ):
                return model.EGrade.ERROR  # Not comparable, why do we have this assessor then?

            if v.quantile <= self.value:
                return model.EGrade.PASS
            else:
                return model.EGrade.FAIL

    class Indifferent( GuiCompatibleAssessor ):
        """
        Rule: Indifferent
        -----------------
        
        Overview
        --------
        
        When applied to a column, this rule denotes that the column is for information only.
        
        Usage notes
        -----------
        
        This rule does not pass or fail any cell values.
        All cell values are marked as `EGrade.INFO` ("Informational message only").
        """
        class_title = "Not-assessed"

        def __str__( self ):
            return "∅"

        @property
        def parameters( self ):
            return ()

        def __call__( self, _, __ ):
            return model.EGrade.INFO

    quantile = Percentile( .5 )
    indifferent = Indifferent()


class EWeight:
    """
    The weight fields (`Column.weight`) control how important the various
    scoring metrics are.
    
    When a subject (e.g. `Peptide`/`Qbrick`) is assessed, for every value
    (`Score`) that passes (`EGrade.PASS`) the rule (`Assessor`) of its column
    (`Column`), the total-points (`EntitySummary.total_points`)) of the subject
    are increased by the weight of the `Column`. I.e.::
    
        POINTS[subject] = sum(COLUMN.WEIGHT for COLUMN in COLUMNS if COLUMN[subject] is PASS)
        
    .. note::
    
        When comparing (or ranking) subjects, points are only counted from columns
        in which *both* subjects to be compared did not report an error.
        See `EGrade.ERROR` for more details.
         
    The default weights are orders of magnitude apart, which means that, for
    instance, a subject with a pass in *one* `HIGH` weighted column will always
    have a higher score than a subject with `multiple` passes in `LOW` weighted columns.
    
    * If columns are not assessed (i.e. if they have their `Column.assessor` set
      to `Assessors.indifferent`) then the `Column.priority` is immaterial.
      Setting the priority to `EPriorities.DISABLED` will still turn off the
      output for that column however.
    * The descriptions below cover the *default* meanings of the weights.
      The user is free to set column weights however they choose, including to
      values outside this range. To avoid overflow and rounding errors, any
      custom weights should be integer-typed.
    
    
    :cvar DISABLED:     Disabled.
    
                        Setting the `Column.weight` to `DISABLED` will remove
                        the `Column` from the output entirely. In the GUI, if
                        all columns are `DISABLED` for a `Handler`, the
                        `Handler` itself will be disabled.
                        
                        Note that disabling a `Column` only suppresses its
                        output, side effects, such as generating peptides, still
                        occur, providing the handler itself is not also
                        disabled.
                        
    :cvar VERY_HIGH:    Highest priority.
    
                        * By default, this is not used internally.
                         
    :cvar HIGH:         High priority.
                        
                        * By default, major constraints,
                            e.g. peptide length. NG in peptide.
                        
    :cvar NORMAL:       Normal priority.
                        "Red" priority.
                        
                        * By default, unambiguous constraints,
                            e.g. H in peptide.
                        
    :cvar LOW:          Low priority.
                        "Amber" priority
                        
                        * By default, ambiguous constraints,
                            e.g. scores derived from simulations or limited
                                 databases.
                        * Also, by default, unambiguous constraints that require human opinion,
                            e.g. C in peptide.
    
    :cvar VERY_LOW:     Lowest priority.
                        
                        * By default, unreliable, irrelevant or redundant sources.
                            e.g. PPA digestion scores for a different experiment type.
    """
    DISABLED = 0
    VERY_HIGH = 100000000
    HIGH = 1000000
    NORMAL = 10000
    LOW = 100
    VERY_LOW = 1

    @staticmethod
    def to_dict():
        return { k: v for k, v in EWeight.__dict__.items() if not k.startswith( "_" ) and isinstance( v, int ) }


class HColumnBase( Entity ):
    """
    !PUBLIC !ABSTRACT
    
    Defines something that can be used to override columns in a
    `ColumnOverride`.
    """
    TReference = Union[
        model.DbData, bytes, str, "HColumn"]  # things that can be used to reference a column at the handler level
    __slots__ = "name", "ranked_type", "rank_assessor", "tag", "doc", "default_weight", "default_assessor", "is_rank_reversed", "rank_column", "is_derived", "__default_html_formatter", "default_display_order"

    def __init__( self: TSelf,
                  name: str,
                  *,
                  tag: object = None,
                  rank_order: int = 0,
                  rank_assessor: Optional[Assessor] = None,
                  assessor: AssessorDelegate = Assessors.indifferent,
                  weight: int = EWeight.NORMAL,
                  is_derived: bool = False,
                  display_order: int = 0,
                  html_formatter: Optional[Callable[["model.Score"], object]] = None ):
        """
        .. important::
        
            All parameters must be serialisable!
        
        :param name:            Default name of the column.
        
        :param tag:             Arbitrary reference for caller.
         
        :param rank_order:      When specified, a rank column is also created, the value denotes the
                                order of rankings.
        
                                * `1`         --> Biggest first. 
                                * `-1`        --> Smallest first.
                                * `0`         --> Do not rank.
                                
        :param rank_assessor:   Assessor to use for the rank column, if present (ignored otherwise).
        
                                * `None` (default) --> Top 50% passes, bottom 50% fails.
                                
        :param assessor:        Default marking scheme.
                                Defaults to `_indifferent_assessor`.
                                
        :param weight:          Default weight of the assessor.
                                May be overridden by the user.
                                If this is `0` the column is disabled.
                                
        :param display_order:   Controls GUI display of the column.
        
                                GUI sorts columns by handler accession, display order, and column
                                accession in turn.
                                
        :param html_formatter:  Suggests how the value should be rendered onto web-pages.
        
                                The value should be:
                                * `None`, the default, which implies the standard implementation.
                                  This colours the text depending on the grade.
                                * A callable. This should input the `Score` and output HTML (`str`)
                                  or a hatmul `TRenderable`. Ensure this callable is serialisable
                                  as stated above, lambdas are not permitted.
                                
                                Note:
                                * That the formatter (currently) cannot be overridden in the
                                  `ColumnOverride`.
                                * This is a suggestion to the HTML renderer only and does not affect
                                  the console or Python implementations.
                                * The formatter outputs raw HTML and should be trusted.
        """

        self.name: str = name
        self.tag = tag
        self.default_weight = weight
        self.default_assessor: Optional[AssessorDelegate] = assessor
        self.default_display_order: int = display_order
        self.__default_html_formatter = html_formatter

        self.rank_assessor: Optional[Assessor] = rank_assessor
        self.ranked_type: Union[bool, int] = rank_order  # original rank value, see __doc__
        self.is_rank_reversed: bool = (rank_order > 0) if isinstance( rank_order, int ) else False  # defines the input to the rank column - 0..n
        self.rank_column: Optional[TSelf] = self._make_rank_column()  # actual rank column
        self.is_derived: bool = is_derived  # is this column a rank column?

    @property
    def default_html_formatter( self ) -> Optional[Callable[["model.Score"], object]]:
        """
        !PUBLIC !FINAL
        
        :return: 
        """
        try:
            return self.__default_html_formatter
        except AttributeError:
            # Handle serialised legacy objects without the field
            # TODO: To be deprecated
            return None

    def is_match( self, key: TReference ) -> bool:
        """
        !PUBLIC !VIRTUAL
        
        Tests if a `key` uniquely identifies this column at the level of the
        `Handler`.
        """
        if isinstance( key, model.DbData ):
            return key.to_hash() == self.to_dbdata().to_hash()
        elif isinstance( key, bytes ):
            return key == self.to_dbdata().to_hash()
        elif isinstance( key, str ):
            return key == self.accession
        elif isinstance( key, HColumnBase ):
            return key is self
        else:
            raise exception_helper.type_error( "key", key, self.TReference )

    def _make_rank_column( self: TSelf ) -> Optional[TSelf]: # virtual
        """
        !PUBLIC !VIRTUAL
        
        ... 
        """
        if self.ranked_type:
            if self.rank_assessor is not None:
                rank_assessor = self.rank_assessor
            elif isinstance( self.ranked_type, int ):
                rank_assessor = Assessors.quantile
            else:
                rank_assessor = Assessors.indifferent

            t_self: Type[TSelf] = type( self )
            return t_self( name = f"Rank: {self.name}",
                           tag = self.tag,
                           rank_order = 0,
                           assessor = rank_assessor,
                           weight = self.default_weight,
                           is_derived = True )
        else:
            return None

    def to_dbdata( self ) -> DbData:
        """
        !PUBLIC !ABSTRACT !OVERRIDE
        
        :return: 
        """
        raise NotImplementedError( "abstract" )

    @property
    def accession( self ) -> str:
        """
        !PUBLIC !VIRTUAL !OVERRIDE
        
        :return: 
        """
        return self.name


class HColumnGroup( HColumnBase ):
    """
    Defines a column group.
    
    Groups can be used:
     
    * to provide a override handle to the user for an otherwise dynamic column
    * to allow the user to define overrides for multiple columns simultaneously
    * to define sets of properties for multiple columns simultaneously 
    """
    __slots__ = ()

    def to_dbdata( self ) -> DbData:
        raise RuntimeError( "`HColumnGroup` does not support `to_dbdata`." )

    def __init__( self, name: str, **kwargs ):
        """
        :param name:        See `HColumnBase`
        :param group_doc:   Group documentation.
                            Currently does not actually appear in the GUI. 
        :param kwargs:      See `HColumnBase` 
        """
        super().__init__( name, **kwargs )

    @property
    def accession( self ) -> str:
        return self.name

    def new_column( self, name = "{}" ) -> "HColumn":
        """
        Generates a new column within this group.
        
        :param name:    Name format, use ``{}`` for the group name.  
        :return:        Column 
        """
        if "{}" in self.name:
            name = self.name.format( name )
        else:
            name = name.format( self.name )

        return HColumn( name = name,
                        tag = self.tag,
                        rank_order = self.ranked_type,
                        assessor = self.default_assessor,
                        weight = self.default_weight,
                        group = self )


class HColumn( HColumnBase ):
    """
    Defines the defaults for a `Column`.
    See `Column` for details.
    """
    __slots__ = "group",

    def __init__( self,
                  name: str,
                  *,
                  group: HColumnGroup = None,
                  **kwargs ):
        """
        :param name:        See `HColumnBase`
        :param group:       Group for this column.
                            When overriding, columns may also be accessed via their group. 
        :param kwargs:      See `HColumnBase` 
        """
        self.group: Optional[HColumnGroup] = group
        super().__init__( name, **kwargs )

    def is_match( self, key: HColumnBase.TReference ) -> bool:
        return super().is_match( key ) or (self.group is not None and self.group.is_match( key ))

    def _make_rank_column( self: TSelf ) -> Optional["HColumn"]:
        # Rank columns should belong to self.group.rank_column (if present)
        result: HColumn = cast( HColumn, super()._make_rank_column() )

        if result is not None and self.group and self.group.rank_column:
            result.group = self.group.rank_column

        return result

    def to_dbdata( self ) -> DbData:
        return DbData( CLASS = "HCol",
                       name = self.name )

    @property
    def accession( self ) -> str:
        return self.name

    def __str__( self ):
        return self.name

    def __repr__( self ):
        return f"{self.__class__.__qualname__}({self.name!r})"

    def iter_sub_keys( self ):
        if self.rank_column:
            return self.rank_column,
        else:
            return ()

    def derive( self, *args, **kwargs ) -> "ColumnOverride":
        return ColumnOverride( self, *args, **kwargs )


class ColumnOverride:
    """
    Defines the user-overrides for a `Column`.
    See `Column` for details. 
    """
    __slots__ = "column", "weight", "assessor", "name", "display_order"

    def __init__( self,
                  column: HColumnBase.TReference,
                  weight: Optional[int] = None,
                  assessor: Optional[AssessorDelegate] = None,
                  name: Optional[str] = None,
                  display_order: Optional[int] = None ):
        """
        The arguments (apart from the `HColumn` itself ) are all overrides.
        `None`-values indicate to use the default for the `HColumn`.
    
        Specify the `column` we are going to override as:
                            
        * `HColumn` column handle (`HColumn`)
        * `str` column name (`HColumn.name` or `HColumn.to_dbdata()`)
        * `HColumnGroup` group handle (`HColumn.group`)
        * `str` group name (`HColumn.group.accession` or `HColumn.group.to_dbdata()`)
        """
        self.column = column
        self.weight = weight
        self.assessor = assessor
        self.name = name
        self.display_order: Optional[int] = display_order

    def __repr__( self ):
        r = []

        r.append( f"column={self.column_name!r}" )

        if self.name is not None:
            r.append( f"name={self.name!r}" )

        if self.weight is not None:
            r.append( f"weight={self.weight!r}" )

        if self.assessor is not None:
            r.append( f"assessor={self.assessor!r}" )

        if self.display_order is not None:
            r.append( f"display_order={self.display_order!r}" )

        rs = ", ".join( r )

        return f"{type( self ).__name__}({rs})"

    @property
    def column_name( self ):
        column = self.column if isinstance( self.column, str ) else self.column.name
        return column


class Column( Entity ):
    """
    A column that appears in the final scores table.
    
    At execution an `HColumn`, which specifies the default properties, is
    combined with a `ColumnOverride`, which specifies the user-overrides, to
    form a `Column`. It is this `Column` to which the actual scores produced are
    registered.
    """
    __slots__ = "__accession", "specification", "name", "weight", "assessor", "handler", "__display_order"

    def __init__( self,
                  specification: HColumn,
                  overrides: ColumnOverride,
                  handler: "Handler",
                  accession: str,
                  ) -> None:
        """
        Do not call manually.
        This must be created via `ModelState.acquire_column`,
        
        :param specification:         Column specification 
        :param overrides:             Override        
        :param handler:               Owning handler 
        :param accession:             Accession 
        """
        self.__accession = accession
        self.specification: HColumn = specification
        self.name = overrides.name if overrides.name is not None else self.specification.name
        self.weight = overrides.weight if overrides.weight is not None else self.specification.default_weight
        self.assessor = overrides.assessor if overrides.assessor is not None else self.specification.default_assessor
        self.handler = handler
        self.__display_order: int = overrides.display_order if overrides.display_order is not None else self.specification.default_display_order

    @property
    def display_order( self ) -> int:
        try:
            return self.__display_order
        except AttributeError:
            # TODO: To be deprecated!!
            # Legacy columns did not have this field, for now, allow it to be missing
            # from the attributes.
            return 0

    def __repr__( self ):
        return (f"{self.__class__.__name__}("
                f"handler={self.handler.accession!r}, "
                f"column={self.specification.accession!r}, "
                f"weight={self.weight!r}, "
                f"assessor={self.assessor!r}, "
                f"accession={self.__accession!r})")

    def __str__( self ):
        return self.__accession

    def to_dbdata( self ) -> DbData:
        return DbData( CLASS = "Column",
                       handler = self.handler.to_dbdata().to_shash(),
                       column = self.specification.to_dbdata().to_shash() )

    @property
    def accession( self ) -> str:
        return self.__accession


class ModelState:
    """
    A bunch of stuff shared between different aspects of the `Pipeline`.
    
    * This class is only intended to be modified during `Pipeline` execution.
    * After the pipeline has executed, the `Pipeline` itself will contain the
      results.
    
    :ivar model:                The model.
                                By now this is for reference only and is 
                                considered immutable.
    :ivar organisms:            Organisms, this may be expanded if we source
                                proteins from an organism different to the
                                original model.
    :ivar warnings:             Warnings, any procedure may append to this list.
    :ivar pending_peptides:     These are peptides produced as a side-effect of
                                the protein-acquisition stage. They are released
                                by the `PrespecifiedPeptideProvider` during the
                                protein-digestion (peptide acquisition) stage
                                (if not available, a warning is shown).
    :ivar pending_qblocks:      As for `pending_peptides`.      
    :ivar pending_qmenus:       As for `pending_peptides`.
    :ivar profiling:            Object used to control profiling.
    :ivar rng:                  Random number generator state.
    :ivar sql_backing:          Backing data store, while running pipeline only.
    """
    __slots__ = ("model",
                 "organisms",
                 "warnings",
                 "profiling",
                 "rng",
                 "sql_backing",
                 "pending_peptides",
                 "pending_qblocks",
                 "pending_qmenus",
                 "__accessions_in_use",
                 "__text_table",
                 "__protein_table",
                 "__peptide_table",
                 "__linked_peptide_table",
                 "__qbrick_table",
                 "__qblock_table",
                 "__qconcat_table",
                 "__qmenu_table",
                 "__column_table",
                 "__handler_table",
                 "__accession_to_object",
                 "__hash_to_object")

    def __init__( self, model_: "model.Model" ):
        self.model = model_
        self.organisms: Set[int] = set( model_.organisms )
        self.warnings: WarningCollection = WarningCollection()

        try:
            from alacat.utilities import mysql_backed_cache
        except ImportError as ex:
            self.warnings.append( str( ex ) )
            mysql_backed_cache = None

        self.profiling: List[Tuple[str, float]] = []
        self.rng = random.Random( 1 )
        self.sql_backing: Optional[mysql_backed_cache.AlacatBacking] = mysql_backed_cache.AlacatBacking(
                self, model_.sql_backing ) if (model_.sql_backing != model.ESqlBacking.NONE and mysql_backed_cache is not None) else None

        self.pending_peptides: Dict[model.ProteinLike, List[str]] = { }
        self.pending_qblocks = { }
        self.pending_qmenus = { }

        self.__accessions_in_use: TCounter[str] = Counter()
        self.__text_table: Dict[str, model.Text] = { }
        self.__protein_table: Dict[str, model.Protein] = { }
        self.__peptide_table: Dict[Tuple[model.Protein, str], model.Peptide] = { }
        self.__linked_peptide_table: Dict[Tuple[str, model.Peptide, str], model.LinkedPeptide] = { }
        self.__qbrick_table: Dict[Tuple[model.LinkedPeptide, ...], model.Qbrick] = { }
        self.__qblock_table: Dict[Tuple[model.Qbrick, ...], model.Qblock] = { }
        self.__qconcat_table: Dict[Tuple[model.Qbrick, ...], model.Qconcat] = { }
        self.__qmenu_table: Dict[Tuple[model.Qconcat, ...], model.Qmenu] = { }
        self.__column_table: Dict[Tuple[model.Handler, model.HColumn], model.Column] = { }
        self.__handler_table: Dict[model.HandlerFactory, model.Handler] = { }

        self.__accession_to_object: Dict[str, model.Entity] = WriteOnceDict()
        self.__hash_to_object: Dict[bytes, model.Entity] = WriteOnceDict()

    def __enter__( self ):
        return self

    def __exit__( self, exc_type, exc_val, exc_tb ):
        self.close()

    def close( self ):
        if self.sql_backing:
            self.sql_backing.close()
            self.sql_backing = None

    @property
    def digestion( self ) -> "model.EDigestion":
        return self.model.digestion

    def instantiate( self, *args, **kwargs ) -> "HandlerCollection[THandler]":
        """
        Instantiates the handlers.
        See `HandlerFactoryCollection.instantiate`.
        """
        return self.model.handlers.instantiate( self, *args, **kwargs )

    def find( self,
              key: Union[str, "model.DbData", bytes],
              t: Type[TValue] = None
              ) -> TValue:  # ~~>KeyError
        """
        Gets an object by any of its unique keys (`str` accession, `DbData`
        internal data, or `bytes` hash of internal data).
        See `Entity`
        """

        if isinstance( key, str ):
            r = self.__accession_to_object[key]
        elif isinstance( key, model.DbData ):
            r = self.__hash_to_object[key.to_hash()]
        elif isinstance( key, bytes ):
            r = self.__hash_to_object[key]
        else:
            raise exception_helper.type_error( "key", key, [str, model.DbData, bytes] )

        if t is not None:
            assert isinstance( r, t ), "Bad type"

        return r

    def find_or_create( self,
                        db_data: "model.DbData",
                        type: Type[TValue] = None,
                        default = NOT_PROVIDED ) -> TValue:
        exception_helper.assert_type( "db_data", db_data, model.DbData )

        # EXISTING ENTITY 
        r = self.__hash_to_object.get( db_data.to_hash() )

        if r is None:
            fail = False
        elif type is None or isinstance( r, type ):
            return r
        else:  # Got but wrong type
            fail = True

        if not fail:
            # PROTEIN
            if type is None or type is model.Protein:
                r = model.Protein.from_dbdata( db_data, self )

                if r:
                    return r

            # PEPTIDE
            if type is None or type is model.Peptide:
                r = model.Peptide.from_dbdata( db_data, self )

                if r:
                    return r

            # QBLOCK
            if type is None or type is model.Qblock:
                r = model.Qblock.from_dbdata( db_data, self )

                if r:
                    return r

        # NOT FOUND
        if default is NOT_PROVIDED:
            type = type or Entity
            raise RuntimeError( f"Could not deserialise an `{type.__name__}` from its data `{db_data!r}`." )

        return cast( TValue, default )

    @property
    def all_entities( self ) -> Iterable[Entity]:
        return set( self.__hash_to_object.values() )

    @property
    def all_proteins( self ) -> Iterable["model.Protein"]:
        return self.__protein_table.values()

    def __acquire( self,
                   key: TKey,
                   table: Dict[TKey, TValue],
                   accession_template: str,
                   obj_fun: Callable[..., TValue],
                   abs_accession: TTristate = False,
                   exist_ok: bool = True,
                   ) -> TValue:
        """
        Gets a subject from the cache or creates a new one.
        
        :param key:                 Cache key.
                                    Used to access and test for equality.
         
        :param table:               Cache table
        
        :param accession_template:  Preferred accession (may be changed)
        
        :param obj_fun:             Creator function, takes the actual (final)
                                    accession as a single parameter.
                                    
        :param abs_accession:       * `True`
                                        - Use the `accession_template` verbatim
                                          if available, otherwise raise an
                                          error.
                                    * `False`
                                        - Use an incremental accession based on
                                          (but not equal to) the
                                          `accession_template`.
                                    * `None`
                                        - Use the `accession_template` verbatim
                                          if available, otherwise use an
                                          incremental accession based on (but
                                          not equal to) the `accession_template`.
                                          
        :param exist_ok:            * `True`
                                        - Return existing item(s)
                                    * `False`
                                        - Only create, do not allow get.   
                                         
        :return:                    Cached subject. 
        """
        value = table.get( key )

        if value is not None:
            if not exist_ok:
                raise ValueError( f"The accession {accession_template!r} is already in use." )

            return value

        #
        # Make the accession
        #
        if abs_accession is False:
            self.__accessions_in_use[accession_template] += 1
            accession = f"{accession_template}-{self.__accessions_in_use[accession_template]}"
        elif abs_accession is None:
            self.__accessions_in_use[accession_template] += 1

            if self.__accessions_in_use[accession_template] == 1:
                accession = accession_template
            else:
                accession = f"{accession_template}-{self.__accessions_in_use[accession_template]}"

        elif abs_accession is True:
            if self.__accessions_in_use[accession_template]:
                raise ValueError( f"The accession {accession_template!r} is already in use." )

            self.__accessions_in_use[accession_template] += 1
            accession = accession_template
        else:
            raise exception_helper.SwitchError( "abs_accession", abs_accession )

        #
        # Create the object
        #
        obj: Entity = obj_fun( accession )

        table[key] = obj

        self.__hash_to_object[obj.to_dbdata().to_hash()] = obj
        self.__accession_to_object[obj.accession] = obj

        return obj

    def acquire_text( self, text: str ) -> "model.Text":
        return self.__acquire( key = text,
                               table = self.__text_table,
                               accession_template = "Text",
                               obj_fun = lambda accession: model.Text( accession, text ) )

    def acquire_handler( self, factory: "model.HandlerFactory" ) -> "model.Handler":
        """
        Instantiates a handler, providing it with a unique `Entity` reference.
        
        :param factory:     Factory 
        :return:            Handler 
        """
        return self.__acquire(
                key = factory,
                table = self.__handler_table,
                accession_template = factory.klass.__name__,
                abs_accession = None,
                obj_fun = lambda accession: factory.instantiate_handler( self, accession ),
                exist_ok = False )

    def register_alternate_accession( self, obj: Entity, accession: str ):
        existing = self.__accession_to_object.get( accession )  # TODO: Should be more changes here
        if existing is not None:
            if existing is obj:
                return

            raise RuntimeError( "Accession in use." )
        self.__accession_to_object[accession] = obj

    def acquire_protein( self,
                         accession: str,
                         sequence: str,
                         title: Optional[str],
                         organism: Optional[int]
                         ) -> "model.Protein":
        """
        Get-or-create a protein.
        """
        #
        # Fix weird | formatted accessions, note that this isn't perfect
        #
        if self.model.fix_accessions:
            if "|" in accession:
                elements: List[str] = accession.split( "|" )
                elements.pop( 0 )  # rubbish
                accession = elements.pop( 0 ) if elements else accession.replace( "|", "?" )  # accession
                meta_data = elements.pop( 0 ) if elements else ""  # meta-data
                title = title or string_helper.regex_extract( r"\b([A-Za-z0-9_ ]+) \b", meta_data, 1, None )
                organism = organism or int( string_helper.regex_extract( r"\bOX=([0-9]+)\b", meta_data, 1, "-1" ) )

        #
        # Handle inline digests
        # Note that "/" is the peptide delimiter
        #
        if self.model.digestion == model.EDigestion.CUSTOM:
            peptides = [x for x in sequence.split( "/" ) if len( x ) > 3]  # > 3 = ignore dummy linker peptides
            sequence = sequence.replace( "/", "" )
        elif self.model.digestion == model.EDigestion.CUSTOM_NO_LINKERS:
            peptides = sequence.split( "/" )
            sequence = "AAA" + "AAA".join( peptides ) + "AAA"
        else:
            peptides = None

        protein: model.Protein = self.__acquire(
                key = accession,
                table = self.__protein_table,
                accession_template = accession,
                obj_fun = lambda accession_: model.Protein( accession_, sequence, title, organism ),
                abs_accession = True,
        )

        if protein.sequence != sequence:
            raise ValueError( "Two proteins have the same accession but different sequences." )

        if title:
            if protein.title and protein.title != title:
                raise ValueError( "Two proteins have the same accession but different titles." )

            protein.title = title

        if organism:
            if protein.organism is not None and protein.organism != organism:
                raise ValueError( "Two proteins have the same accession but different organisms." )

            protein.organism = organism

        if peptides:
            self.pending_peptides.setdefault( protein, [] ).extend( peptides )

        return protein

    def try_acquire_peptide( self,
                             sequence: str,
                             protein: Optional["model.Protein"] = None ) -> Optional["model.Peptide"]:
        if len( sequence ) < self.model.min_peptide_length:
            return None

        return self.__acquire(
                key = (protein, sequence),
                table = self.__peptide_table,
                accession_template = f"PEP-{protein.accession}-{sequence}",
                abs_accession = None,
                obj_fun = lambda accession: model.Peptide( accession = accession, sequence = sequence,
                                                           protein = protein, ) )

    def acquire_linked_peptide( self, n_link: str, peptide: "model.Peptide", c_link: str ):
        return self.__acquire(
                key = (n_link, peptide, c_link),
                table = self.__linked_peptide_table,
                accession_template = f"LPE-{peptide.protein.accession}",
                obj_fun = lambda accession: model.LinkedPeptide( accession, n_link, peptide, c_link )
        )

    def acquire_qbrick( self, linked_peptides: Iterable["model.LinkedPeptide"] ) -> "model.Qbrick":
        linked_peptides = tuple( linked_peptides )

        return self.__acquire(
                key = linked_peptides,
                table = self.__qbrick_table,
                accession_template = f"QBR-{linked_peptides[0].peptide.protein.accession}",
                obj_fun = lambda accession: model.Qbrick( accession, linked_peptides )
        )

    def acquire_qblock( self, qbricks: Iterable["model.Qbrick"] ) -> "model.Qblock":
        qbricks = tuple( qbricks )

        return self.__acquire(
                key = qbricks,
                table = self.__qblock_table,
                accession_template = f"QBL-{qbricks[0].peptides[0].protein.accession}",
                obj_fun = lambda accession: model.Qblock( accession, qbricks )
        )

    def acquire_qconcat( self, qbricks: Iterable["model.Qbrick"] ) -> "model.Qconcat":
        qbricks = tuple( qbricks )

        return self.__acquire(
                key = qbricks,
                table = self.__qconcat_table,
                accession_template = "CAT",
                obj_fun = lambda accession: model.Qconcat( accession, qbricks ),
        )

    def acquire_qmenu( self, alacats: Iterable["model.Qconcat"] ) -> "model.Qmenu":
        alacats = tuple( alacats )

        return self.__acquire(
                key = alacats,
                table = self.__qmenu_table,
                accession_template = "MNU",
                obj_fun = lambda accession: model.Qmenu( accession, alacats ),
        )

    def acquire_column( self, specification: HColumn, override: ColumnOverride,
                        handler: "model.Handler" ) -> Column:
        return self.__acquire(
                key = (handler, specification),
                table = self.__column_table,
                accession_template = override.name or specification.name,
                obj_fun = lambda accession: Column( specification, override, handler, accession ),
                abs_accession = None,
                exist_ok = False )


class HArgs:
    """
    Wraps up the arguments passed to the base `Handler.__init__`.
    
    The first three arguments are pre-set by the factory, whilst `backing_data`
    must be set by the derived class.
    
    :ivar state:            Reference to the model state.
     
    :ivar column_overrides: Assigned overrides for this handler.
    
    :ivar accession:        Assigned accession for this handler.
    """
    __slots__ = "state", "column_overrides", "accession", "__backing_enabled", "__backing_key"

    def __init__( self,
                  state: ModelState,
                  column_overrides: Sequence[ColumnOverride],
                  accession: str ):
        self.state = state
        self.column_overrides: Sequence[ColumnOverride] = column_overrides
        self.accession = accession
        self.__backing_enabled: Optional[bool] = None
        self.__backing_key: Optional[Tuple[model.DbData.TValue, ...]] = None

    def set_backing_data( self, enabled: bool, backing_key: Tuple["model.DbData.TValue", ...] ):
        """
        Specifies whether SQL backing is enabled, and how it should be
        addressed.
        
        When backing is `enabled`, the results of the handler are cached
        into/from an SQL database.
        
        When backing is disabled, scores are not cached and are always
        recalculated. This should be the case for handlers with stochastic,
        trivial or dynamic output.
        
        Backing requires a `backing_key`. The backing key should be a tuple that
        uniquely identifies an instance of a handler and any initialisation
        parameters that might affect the output. Handlers with the same backing
        key are assumed to always produce the same output for the same input.
        
        * The class name is always automatically included as part of the key and
          does not need to be specified manually.
        * The key can be an empty tuple if the handler takes no parameters that
          might affect the output and therefore always produces the same
          results from the same input.
        * They key may contain a version number to ensure that results from
          older versions of the software are ignored. 
        
        Handlers which are not backed (i.e. ``enabled = False``) still require
        a backing key - the key is used for convenience in other parts of the
        program. 
        """
        self.__backing_enabled = enabled
        self.__backing_key = backing_key

    backing_enabled = property( lambda self: self.__backing_enabled )
    backing_key: Tuple[DbData.TValue, ...] = property( lambda self: self.__backing_key )
    model = property( lambda self: self.state.model )
    warnings: "WarningCollection" = property( lambda self: self.state.warnings )
    organisms = property( lambda self: self.state.organisms )


class InstantiationError( Exception ):
    """
    Handlers may raise this exception during construction.
    
    This is a "light" error that signals that the handler is of no use given the
    specified parameters, but that the parameters are otherwise correct.
    
    For the FASTA provider may raise an `InstantiationError` if no FASTA file
    is specified, but will raise a `RuntimeError` if the specified file does
    not exist.
    
    The error may be raised, converted to a warning, or ignored, depending upon
    the instantiating factory.
    """

    def __init__( self, handler: Union["Handler", str], message: str ):
        self.message: str = message
        self.handler: str = str( handler )
        super().__init__( f"{self.handler}: {self.message}" )


class Handler( Generic[TSubjectIn, TSubjectOut], Entity ):
    """
    Objects that control aspects of the workflow.
    
    In its essence a handler converts a `TSubjectIn` to a `TSubjectOut` and
    assigns a score to the conversion.
    
    
    Scorers
    -------
    
    The "scorer" handlers assign scores to individual items.
    These scores are used to assess the subjects's suitabilities to be selected
    in the next stage of the pipeline.
    Both `TSubjectIn` and `TSubjectOut` are `TSubject`.
    
        ==================== ====================
        Scorer               TSubject
        ==================== ====================
        `PeptideScorer`      `Peptide`
        `QbrickScorer`       `Qbrick`
        `QMenuScorer`        `Qmenu`
        ==================== ====================
        
    .. hint::
    
        This workflow is controlled by `ScorerCollectionBase.score`.
        
        
    Providers
    ---------
    
    The "provider" handlers convert a parent to either a child or another
    parent. The scores may be used for assessing the subjects' suitabilities
    (as for the `Scorers` above), but in many cases are a meaningless byproduct
    of the provision and are assigned `EProvided.NORMAL`. `EProvided.FAVOURED`
    conversely overrides all other scores, selecting the subject (i.e. because
    the user says so) for continuation in the next stage of the pipeline .
    `TSubjectIn` is `Pending[TParent, TChild]` and `TSubjectOut` is either
    `TChild` or another `Pending[TParent, TChild]`.  
     
        ===================== ======================= ==========================
        Provider              TParent                 TChild
        ===================== ======================= ==========================     
        `ProteinProvider`     `str`                   `Protein`       
        `PeptideProvider`     `Protein`               `Peptide`       
        `QbrickProvider`      `Tuple[Peptide,...]`    `Qbrick`        
        `QMenuProvider`       `Tuple[Qbrick,...]`     `Qmenu`
        
    .. hint::
    
        This workflow is controlled by `ProviderCollectionBase.provide`.
    
    Lifetime
    --------
    
    * All `Handler`\s for a particular stage are constructed just prior to that
      stage.                                                                                          fi
    * `query_all` may be called multiple times during the stage.
    * After the stage is complete, `close()` is executed on all the handlers.
        * After `close` has been called the handler should not be reused.
        * Remaining information may be inspected, but `query_all` must not be
          called again.
        * Handlers cannot be reused in a new pipeline - use `HandlerFactory`
          instead.
    * Handlers should use a static `lru_cache` to cache information they 
      wish to persist between Pipelines. These may be cleared by calling
      `clear_all_lru_caches`.

    
    Implementation
    --------------
    
    The derived class must implement `_on_query` *or* `_on_query_all`.
    
    :cvar __abstract__: Marks the class as abstract.
                        See `reflection_helper.is_explicit_abstract`.
                        This stops abstract classes being picked up by the GUI.
                        !VIRTUAL !SPECIAL
    :cvar all_handlers: List of all instantiable handler classes.
                        !FINAL 
    """
    __slots__ = "__backing_enabled", "__hash_info", "__backing_withdrawn", "__accession", "__is_closed", "__column_overrides", "state", "bound_columns"
    __abstract__ = True
    all_handlers: ClassVar[List[Type["Handler"]]] = []
    by_argument: ClassVar[Dict[str, Type["Handler"]]] = WriteOnceDict()

    def __init_subclass__( cls, **kwargs ):
        if reflection_helper.is_explicit_abstract( cls ):
            return

        ordered_insert( Handler.all_handlers, cls, lambda x: x.__name__ )
        Handler.by_argument[cls.__name__] = cls

        todo: List[HColumn] = []

        for name in dir( cls ):
            value = getattr( cls, name )

            if isinstance( value, HColumn ):
                todo.append( value )

        keys = set()

        while todo:
            key = todo.pop( -1 )
            keys.add( key )

            for sk in key.iter_sub_keys():
                if sk not in keys:
                    todo.append( sk )

        cls.__score_keys = tuple( keys )

    def __init__( self, hargs: HArgs ):
        """
        Instantiates the base `Handler`.
        
        :param hargs:   Arguments common to all `Handler`\s.  
        """
        self.__backing_enabled: bool = hargs.backing_enabled and hargs.state.sql_backing is not None
        self.__hash_info: Tuple[DbData.TValue, ...] = hargs.backing_key
        self.__backing_withdrawn: Optional[Set[object]] = None
        self.__accession: str = hargs.accession
        self.__is_closed: bool = False
        self.__column_overrides: Optional[List[List[ColumnOverride, bool]]] = [[x, False] for x in
                                                                               hargs.column_overrides]
        self.state: Optional[ModelState] = hargs.state
        self.bound_columns: Optional[Dict[HColumn, Column]] = { }
        self.bind_columns( self.__find_overridable_concrete_columns() )

    def __parse_backing_data( self, backing_data ):
        try:
            exception_helper.safe_cast( "backing_data", backing_data, tuple )
            assert len( backing_data ) == 2, "backing data has wrong parameter count"
            exception_helper.safe_cast( "backing_data", backing_data[0], bool )
            exception_helper.safe_cast( "backing_data", backing_data[1], tuple )
            exception_helper.safe_cast_iter( "backing_data", backing_data[1], DbData.TValue )
        except Exception as ex:
            raise RuntimeError(
                    f"`HArgs`.`backing_data` for {self.__class__.__name__} should have been set during `__init__`. It has either not been set or has been set incorrectly." ) from ex

        return backing_data

    def __getstate__( self ) -> Dict[str, object]:
        """
        Handler instances may contain large amounts of data
        Serialisation is only performed when we are saving the pipeline *after* it has been run
        (The GUI saves the pipeline, and the user may also wish to save it)
        However, we do not require any additional state after this, so we serialise only accession
        """
        return { "accession": self.__accession,
                 "hash_info": self.__hash_info }

    def __setstate__( self, state ):
        """
        When restored the handler has already been used and we are only investigating the model.
        Hence, we restore only the base data. See `__getstate__`.
        """
        self.__accession = state["accession"]
        self.__hash_info = state["hash_info"]
        self.__is_closed = True
        self.__column_overrides = None
        self.state = None
        self.bound_columns = None

    def __repr__( self ):
        return f"{type( self ).__name__}{self.__hash_info!r}"

    def to_dbdata( self ) -> DbData:
        return DbData( CLASS = "Handler",
                       subclass = self.__class__.__name__,
                       parameters = self.__hash_info )

    @classmethod
    def create_default_products( cls,
                                 factory: "model.HandlerFactory",
                                 state: ModelState
                                 ) -> Sequence["model.HandlerFactory"]:
        """
        Given our input `factory`, return the factories to make the final
        product(s).
        """
        return factory,

    @classmethod
    def create_default_factories( cls ) -> Sequence["model.HandlerFactory"]:
        """
        Creates the default variation of the handler.
        """
        facs = cls._on_create_default_factories()

        if facs is None:
            return ()
        elif isinstance( facs, model.HandlerFactory ):
            return facs,
        else:
            return tuple( facs )

    @classmethod
    def _on_create_default_factories( cls ) -> Union["model.HandlerFactory", Iterable["model.HandlerFactory"]]:
        """
        Called to create the default variation(s) of the handler.
        
        The base implementation returns a call to the default constructor.
        
        The derived class may override this behaviour.
        """
        return model.HandlerFactory( cls ),

    def bind_columns( self, columns: Iterable[HColumn] ):
        """
        See `bind_column`.
        """
        for column in columns:
            self.bind_column( column )

    def bind_column( self, column: HColumn ):
        """
        Binds a column to the output matrix.
        
        This checks and applies any user-specified overrides
        (`HArgs.column_overrides` or `__column_overrides`) and creates the
        `Column` ultimately reference in the output matrix.
        
        Note that any nested columns (`HColumn.rank_column`) are automatically
        bound at the same time and do not need binding separately.
        Similarly, any `HColumn`\s available as class variables are bound during
        `__init__` and do not need to be bound manually.
        """
        assert column not in self.bound_columns, "Column is already bound."
        assert self.__column_overrides is not None, "Initialisation is already over."

        override: Optional[ColumnOverride]

        for info in self.__column_overrides:
            override_ = info[0]
            key = override_.column

            column.is_match( key )

            # noinspection PyTypeChecker
            info[1] = True
            override = override_
            break
        else:
            override = ColumnOverride( column )

        bound = self.state.acquire_column( column, override, self )
        self.bound_columns[column] = bound

        # Bind any nested columns
        if column.rank_column is not None:
            self.bind_column( column.rank_column )

    def post_instantiate( self ):
        """
        After initialisation, runs any necessary checks.
        """
        # Check overrides
        overrides_unused = [handle for handle, is_used in self.__column_overrides if not is_used]

        if overrides_unused:
            raise ValueError( f"One or more column_overrides were not assigned to any score: {overrides_unused}" )

    @property
    def accession( self ) -> str:
        return self.__accession

    @classmethod
    def find_overridable_columns( cls ) -> List[HColumnBase]:
        """
        Finds columns that can be overridden for this class.
        
        .. important:
        
            If the class does not expose its columns through class-variables
            (i.e. if it creates the columns dynamically) then the columns will
            not be located and cannot be overridden, unless the accession is
            known. Classes should generally expose a `HColumnGroup` to
            allow such columns to be overridden, but this is not guaranteed. 
         
        :return:                Overridable columns.
        """
        r = []

        for name in dir( cls ):
            value = getattr( cls, name, None )

            if isinstance( value, HColumnBase ):
                r.append( value )

                if value.rank_column:
                    r.append( value.rank_column )

        return r

    @classmethod
    def __find_overridable_concrete_columns( cls ) -> Sequence[HColumn]:
        """
        As `find_overridable_columns` but does not include column groups or
        nested (rank) columns.
        """
        r = []

        for name in dir( cls ):
            value = getattr( cls, name, None )

            if isinstance( value, HColumn ):
                r.append( value )

        return r

    @staticmethod
    def clear_all_lru_caches():
        """
        The API assumes future queries will be similar to previous ones and
        therefore persists data in in-memory LRU caches until the program exits.
        However if a lot of different queries are made this may result in an
        unacceptable memory overhead.
        In this case clearing the LRU caches may be necessary between queries.
        The simplest way is to close and restart Python, however the caches may be
        manually cleared if needed, by calling `Handler.clear_all_lru_caches` 
        """
        reflection_helper.clear_lru_caches( *reflection_helper.get_subclasses( Handler ) )

    def do_not_cache( self, subjects: Iterable[TSubjectIn] ):
        self.__backing_withdrawn.update( subjects )

    def query_all( self, subjects: Sequence[TSubjectIn] ) -> Tuple["model.RawScore[TSubjectOut]", ...]:
        """
        Scores the specified subjects.
        """
        assert not self.__is_closed, "Handler is closed"

        # Load any backed scores
        if self.__backing_enabled:
            self.__backing_withdrawn = set()
            cached_scores, subjects2 = self.state.sql_backing.load( self, subjects )
        else:
            cached_scores = ()
            subjects2 = subjects

        # Run normal query routine
        start_time = time.perf_counter()
        new_scores = tuple( self._on_query_all( subjects2 ) ) if subjects2 else ()
        self.state.profiling.append( (f"{self}/query_all", time.perf_counter() - start_time) )

        for score in new_scores:
            exception_helper.assert_type( f"{type( self ).__name__}.query_all.score", score, model.RawScore )

        # Store backed scores
        if self.__backing_enabled:
            self.state.sql_backing.save( self, subjects2, self.__backing_withdrawn, new_scores )

        return tuple( chain( cached_scores, new_scores ) )

    def _query( self, subject: TSubjectIn ) -> Iterable["model.RawScore[TSubjectOut]"]:
        """
        Called to obtain the score of an individual `Peptide`
        
        :param subject:     Subject (e.g. peptide).
        :return:            Score(s).
        """
        result: TScoreResult = self._on_query( subject )

        if result is None:
            result = ()
        elif inspect.isgenerator( result ):
            result = tuple( result )
        elif not isinstance( result, tuple ) and not isinstance( result, list ):
            result = result,

        return result

    def _on_query( self, subject: TSubjectIn ) -> TScoreResult:
        """
        Virtual method called to obtain the score of an individual `Peptide`
        (not used if `_on_query_all` is overridden).
        
        !VIRTUAL !PROTECTED
        
        :param subject: `Peptide` being scored. 
        :return:        Score or scores, either a `RawScore` object or a sequence
                        of `RawScore` objects. 
        """
        raise RuntimeError(
                f"Did not expect this method to be called. The derived class must implement `_on_query` or `_on_query_all` but {type( self ).__name__} does neither." )

    def _on_query_all( self, subjects: Sequence[TSubjectIn] ) -> Iterable[RawScore[TSubjectOut]]:
        """
        Virtual implementation of `query_all`.
        
        The base implementation calls `_on_query` for all `Peptide`\s in `peptides`.
        
        !VIRTUAL !PROTECTED
        
        :param subjects:    Peptides to query. 
        :return:            Dictionary of peptides to scores.
                            See `_on_query` for the format of the scores. 
        """
        results: List[RawScore[TSubject]] = []

        for peptide in standard_log.iterate( subjects, title = f"peptides/{self}" ):
            results.extend( self._query( peptide ) )

        return results

    def _score( self,
                value: AnyScoreType,
                key: Union[ColumnOverride, HColumn],
                subject: TSubjectOut,
                input: TSubjectIn
                ) -> RawScore[TSubjectOut]:
        """
        Convenience method which allows the derived class to generate a score
        from this scorer for the current peptide in question.
        
        The peptide must be specified if `_on_query_all` is overridden and
        `query` is not called. 
        
        !PROTECTED
        """
        return RawScore( origin = self,
                         key = key,
                         subject = subject,
                         value = value,
                         input = input )

    def close( self ):
        self.__is_closed = True
        self._on_close()

    def _on_close( self ):
        pass


class PerOrganismMixin( Handler ):
    """
    Instructs the factory to create one handler per organism.
    
    The handler must take a `taxon_id` argument in it's `__init__` and possess
    a `taxon_id` field to accommodate this.
    
    Does not interfere with a factory that already defines the organism. 
    """
    taxon_id: int
    __abstract__ = True

    @classmethod
    def create_default_products( cls, factory: "model.HandlerFactory", state: ModelState ):
        """
        Create one handler per organism.
        """
        if "taxon_id" in factory.kwargs:
            return [factory]

        r = []

        if not state.organisms:
            state.warnings.append(
                    f"Not instantiating any {factory.klass.__name__!r} handlers because they require the taxon_id to be known and it is not known." )
            return ()

        for organism in state.organisms:
            r.append( model.HandlerFactory(
                    klass = factory.klass,
                    kwargs = { **factory.kwargs, "taxon_id": organism },
                    column_overrides = factory.column_overrides ) )

        return r

    def post_instantiate( self ):
        super().post_instantiate()
        assert hasattr( self, "taxon_id" ), "Requires `self.taxon_id`."

    def __getstate__( self ):
        """
        Since we use `taxon_id` in the `__str__` we must include this.
        """
        return { **super().__getstate__(),
                 "taxon_id": self.taxon_id }

    def __setstate__( self, state ):
        super().__setstate__( state )
        self.taxon_id = state["taxon_id"]


THandler = TypeVar( "THandler", bound = Handler )


class HandlerCollection( NestedSequence[THandler] ):
    def __init__( self, instantiated: Iterable[THandler] ):
        super().__init__( tuple( instantiated ) )


class WarningCollection( List[object] ):
    pass
    # Note: No longer send pipeline warnings to `warnings.warn`, it's annoying for the web gui
