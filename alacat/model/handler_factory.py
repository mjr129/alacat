import re
from functools import lru_cache
from itertools import chain
from typing import Type, Tuple, Dict, Iterable, List, Optional, NoReturn, Union, Sequence

from mhelper import string_helper, utf_table, ansi, exception_helper
from mhelper.ansi_helper import AnsiStr
from mhelper.string_helper import repr_html, indent_value as _iv
from mhelper.generics_helper import NestedSequence
from .handler_base import Handler, InstantiationError, HArgs, HandlerCollection, THandler, ModelState, ColumnOverride
from .debugging import standard_log


class InstantiationWarning:
    """
    Information about a handler that failed to construct.
    """

    def __init__( self, factory: "HandlerFactory", handler: str, message: str ):
        self.factory = factory
        self.handler = handler
        self.message = message

    def _repr_html_( self ):
        from hatmul import forms

        return "".join( ["FACTORY: <samp>",
                         forms.escape( str( self.factory ) ),
                         "</samp>",
                         "HANDLER: <samp>",
                         forms.escape( self.handler ),
                         "</samp>",
                         "MESSAGE: ",
                         forms.escape( f" - {self.message}" )] )

    def __str__( self ):
        return f"{self.factory} - {self.handler} - {self.message}"

    def __repr__( self ):
        return f"{type( self ).__name__}(factory = {self.factory!r}, handler = {self.handler!r}, exception = {self.message!r})"


class InstantiationWarningCollection( List[InstantiationWarning] ):
    def __init__( self, klass : Type ):
        super().__init__()
        self.klass = klass

    def append_warning( self, factory: "HandlerFactory", handler: str, message: str ):
        super().append( InstantiationWarning( factory, handler, message ) )

    def __str__( self ):
        r = []

        r.append( f"The following {len(self)} {self.klass.__name__}(s) failed to initialise and were excluded from the model." )

        for warning in self:
            r.append( "" )
            r.append( f"    FACTORY:   {warning.factory}" )
            r.append( f"    HANDLER:   {warning.handler}" )
            r.append( f"    EXCEPTION: {warning.message}" )

        return "\n".join( r )

    def _repr_html_( self ):
        return ("The following {}(s) failed to initialise and were excluded from the model:"
                "<ul>{}</ul>".format( self.klass.__name__, "".join( f"<li>{repr_html( x )}</li>" for x in self ) ))

    def __repr__( self ):
        return string_helper.auto_repr( self )


class HandlerFactory:
    """
    Holds the information required to instantiate a `Handler`.
    
    Handlers only exist for the lifetime of the pipeline (due to the large
    amount of information they may cache), whereas the factory may be reused.
    """

    def __init__( self,
                  klass: Type[Handler],
                  *,
                  kwargs: Optional[Dict[str, object]] = None,
                  column_overrides: Union[None, Sequence[ColumnOverride]] = None,
                  gui_key: Optional[str] = None
                  ) -> None:
        """
        :param klass:            Handler class. 
        :param kwargs:           Passed to constructor. 
        :param column_overrides: `ColumnOverride`\s.
                                 Any `Score`\s missing `ColumnOverride`\s will assume a default.
        :param gui_key:          A key by which the GUI may access this factory.
                                 Can be blank if it is unambiguous based on its type.
        """
        assert isinstance( klass, type ) and issubclass( klass, Handler )

        if kwargs is None:
            kwargs = { }

        if column_overrides:
            column_overrides = sorted( column_overrides, key = lambda x: x.column_name )

        self.klass: Type[Handler] = klass
        self.kwargs: Dict[str, object] = kwargs
        self.column_overrides: Tuple[ColumnOverride, ...] = tuple( column_overrides or () )
        self.gui_key = gui_key or self.klass.__name__

    def products( self, state: ModelState ) -> Iterable["HandlerFactory"]:
        """
        Factories can make more than one handler.
        
        :return:    Handlers to produce.
                    Each product is itself a factory for simplicitly.
                    Note that this call isn't recursive, so `product`\s won't
                    be called again on the results. 
        """
        return self.klass.create_default_products( self, state )

    def instantiate_handler( self, state: ModelState, accession: str ) -> "Handler":
        """
        Implementation of handler acquisition (`__call__`).
        Instantiates the actual handler.
        
        :param state: 
        :param accession: 
        :return: 
        """
        try:
            hargs = HArgs( state = state, column_overrides = self.column_overrides, accession = accession )

            # noinspection PyArgumentList,Mypy
            instance = self.klass( hargs, **self.kwargs )
            instance.post_instantiate()
            return instance
        except InstantiationError as ex:
            self.__raise_instantiation_error( ex )
        except Exception as ex:
            msg = (f"There was an error instantiating '{self.klass.__qualname__}' due to the error:\n"
                   f"{ex.__class__.__name__}: {ex}")

            if self.kwargs:
                msg += (f"\n\n"
                        f"The following handler arguments were provided:\n"
                        f"{self.kwargs}")

            raise RuntimeError( msg ) from ex

    def __repr__( self ):
        r = []

        r.append( self.klass.__qualname__ )

        if self.column_overrides:
            r.append( _iv( f"column_overrides = {self.column_overrides}" ) )

        if self.kwargs:
            r.append( _iv( f"kwargs = {self.kwargs!r}" ) )

        mn = f"{type( self ).__name__}( "
        rx = f",\n".join( r )
        rx = string_helper.indent( rx, len( mn ), first = False )

        return f"{mn}{rx} )"

    def __str__( self ):
        k = dict( self.kwargs )

        if self.column_overrides:
            k["cols"] = self.column_overrides

        return f"{self.klass.__qualname__}({string_helper.dict_to_string( k, )})"

    def __raise_instantiation_error( self, ex: InstantiationError ) -> NoReturn:
        standard_log( f"* \n* The handler could not be instantiated. The following reason was given:\n*\n* {self.klass.__qualname__!r}:\n* {ex}" )
        raise InstantiationError( ex.handler, ex.message ) from ex


class HandlerFactoryCollection( NestedSequence[HandlerFactory] ):
    """
    Manages a set of `Handler` factories (typically `Type[Handler]`, though
    any factory function is accepted.
    """

    def __init__( self, handlers: Iterable[Union[Type[Handler], HandlerFactory]] = () ):
        # Wrap bare `Type`\s in `HandlerFactory`
        handlers = [HandlerFactory( x ) if not isinstance( x, HandlerFactory ) else x for x in handlers]

        # Sort and convert to tuple 
        handlers = tuple( sorted( handlers, key = str ) )

        # Check remaining
        exception_helper.safe_cast_iter( "handler", handlers, HandlerFactory )

        super().__init__( handlers )

    def __repr__( self ):
        rep = self.__force_repr()

        if rep == self.__default_repr():
            return "HandlerFactoryCollection.create_defaults()"

        return rep

    def __force_repr( self ):
        tn = f"{type( self ).__name__}("
        contents = f",\n".join( repr( x ) for x in self )
        contents = string_helper.indent( contents, len( tn ) )
        return f"{tn}\n{contents} )"

    @staticmethod
    def get_handler_type( handler: HandlerFactory ) -> type:
        """
        Given a `THandlerFactory`, returns the class of the handler.
        """
        if isinstance( handler, HandlerFactory ):
            return handler.klass
        elif isinstance( handler, type ):
            return handler
        else:
            return type( handler )

    @staticmethod
    @lru_cache
    def __default_repr():
        return HandlerFactoryCollection.create_defaults().__force_repr()

    @staticmethod
    def create_defaults() -> "HandlerFactoryCollection":
        """
        Creates the default set of handler factories.
        """
        factories = []

        for klass in Handler.all_handlers:
            factories.extend( klass.create_default_factories() )

        return HandlerFactoryCollection( tuple( chain( factories ) ) )

    def instantiate( self, state: ModelState, klass: Type[THandler] ) -> HandlerCollection[THandler]:
        """
        Instantiates all factories in the collection.
        Has no effect if instantiation has already occurred.
        
        Diagnostics information is sent to `diagnostics`.
        """
        instantiated = []

        standard_log( utf_table.format_box( "Instantiating handlers" ) )

        warnings_ = InstantiationWarningCollection( klass )

        fac_results = { }

        factories = [factory for factory in self if issubclass( factory.klass, klass )]

        factory: HandlerFactory
        for factory in factories:
            for product in factory.products( state ):
                with standard_log.time( f"Instantiating handler: {product}" ) as timer:
                    try:
                        handler = state.acquire_handler( product )
                    except InstantiationError as ex:
                        warnings_.append_warning( factory, ex.handler, ex.message )
                        timer.result = f"failure"
                        fac_results[factory] = f"FAILURE ({ex})"
                        state.profiling.append( (f"{ex.handler}|Failed-instantiations", timer.now()) )
                    else:
                        if handler is None:
                            timer.result = "ignored"
                            fac_results[factory] = "IGNORED"
                        else:
                            timer.result = "success"
                            instantiated.append( handler )
                            fac_results[factory] = "SUCCESS"

                        state.profiling.append( (f"{handler}|Instantiations|{handler}/Instantiate", timer.now()) )

        standard_log()

        if warnings_:
            state.warnings.append( warnings_ )

        if not instantiated:
            lst = ''.join( f'\n    {i}. [{v}] {k}' for i, (k, v) in enumerate( fac_results.items() ) )
            lst2 = ''.join( f'\n    {i}. {k}' for i, k in enumerate( self ) )

            from alacat.model.outputs import PipelineError
            raise PipelineError( f"No `{klass.__qualname__}` handler factories have been specified.\n"
                                 f"At least one must be present in order to continue.\n"
                                 f"Please check your selected handlers and try again.\n"
                                 f"Instantiation results: {lst}\n"
                                 f"Requested handlers: {lst2}" )

        return HandlerCollection( tuple( instantiated ) )

    def detail( self ):
        """
        Creates a description of the handlers in a format suitable for display
        in an ANSI terminal.
        """
        r = []

        r.append( "List of all handlers.\n" )
        r.append( "<-- Internal name                                               CLI argument -->\n" )
        r.append( "                               Output columns                                   \n" )
        r.append( "\n" )

        klass: Type[Handler]

        for klass in Handler.all_handlers:
            qn = klass.__qualname__

            r.append( AnsiStr( f"{ansi.BOLD}{qn}{ansi.BOLD_OFF}" ).ljust( 40 ).__str__() )
            r.append( AnsiStr( f"--{ansi.ITALIC}{klass.__class__.__qualname__}{ansi.ITALIC_OFF}" ).rjust( 40 ).__str__() )

            r.append( "\n" )

            nmro = { klass, object, Handler }
            mro = "; ".join( f"{x.__name__}" for x in klass.mro() if x not in nmro )
            mro = mro.center( 80 )
            r.append( f"{ansi.ITALIC}{mro}{ansi.RESET}" )
            r.append( "\n" )
            r.append( klass.__doc__ or "Not documented :(" )
            r.append( "\n" )
            if klass.__init__.__doc__:
                r.append( f"    {ansi.ITALIC}The following initialisation parameters are available:{ansi.RESET}" )
                doc = str( klass.__init__.__doc__ )
                doc = re.sub( ".+:param hargs:.+", "", doc )
                r.append( doc.replace( ":param", "      " ) )
            else:
                r.append( f"    {ansi.ITALIC}There are no special initialisation parameters for this handler.{ansi.RESET}" )
            r.append( "\n\n" )
            keys = list( klass.find_overridable_columns() )

            if keys:
                r.append( f"    {ansi.ITALIC}The following scores are available:{ansi.RESET}\n\n" )
                for key in keys:
                    r.append( f"        {ansi.UNDERLINE}{key.name}{ansi.UNDERLINE_OFF}: {key.doc}\n" )
            else:
                r.append( f"    {ansi.ITALIC}The scores for this class are created dynamically and cannot be listed here.{ansi.RESET}\n\n" )

            r.append( "\n" )

            r.append( "\n\n" )

        return "".join( r ).strip()
