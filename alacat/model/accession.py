from typing import Union, get_args


class Entity:
    """
    !PUBLIC !ABSTRACT
    
    Objects in our workflow derive from this.
    
    All entities have handles by which they may be retrieved or reconstructed:
    
    | `.accession`              | retrieve                | this workflow |
    | `.to_dbdata().to_json()`  | retrieve or reconstruct | any workflow  |
    | `.to_dbdata().to_shash()` | retrieve                | any workflow  |
    """
    __slots__ = ()

    @property
    def accession( self ) -> str:
        """
        !PUBLIC !ABSTRACT
        
        A value that uniquely defines this entity within the model. This is
        typically used for display purposes in the GUI. It is assigned by the
        relevant `ModelState.acquire*` call.
        """
        raise NotImplementedError( "abstract" )

    def to_dbdata( self ) -> "DbData":
        """
        !PUBLIC !ABSTRACT
        
        Serialises the entity into a JSON-compatible value that uniquely
        identifies it. See `DbData`
        """
        raise NotImplementedError( "abstract" )


class DbData:
    """
    !PUBLIC !FINAL
    
    `DbData` uniquely identifies an `Entity`, across *all models*.
    
    This is:
    
    * `DbData` uniquely identifies the entity.
    
        * So we can find *inputs* in the database.
        * So the user can provide a value that will select a specific entity.
       
    * For *some* entities, `DbData` can be deserialised into the entity.
      
        * When loading *outputs* from the database for a given *input*
        * When the user must select an item (such as a Qblock) that isn't
          guaranteed to actually exist in the model (i.e. because Qblocks are
          stochastically generated and may not exist until requested).
            
      Deserialisation is handled via a call to `ModelState.find_or_create`.
      Deserialisable entities possess the following complimentary API::
        
            from_dbdata( d : DbData, s : ModelState ) -> Optional[TEntity]
            parse_dbdata( d : DbData ) -> TParsed
            
      `parse_dbdata` may also exist for entities who's DbData is required to
      be parsed in order to deserialise another different entity, even if the
      owning entity isn't itself deserialisable. 
      
    See the methods below for more details on use cases.
    """
    TValue = Union[str, dict, list, tuple, int, float]
    __slots__ = "data",

    def __init__( self, *args, **kwargs ):
        """
        !PUBLIC !FINAL
        
        Args and kwargs are as for `dict`. 
        """
        self.data = dict( *args, **kwargs )

        assert all( type( v ) in get_args( DbData.TValue ) for v in self.data.values() )

    def __getitem__( self, item ):
        """
        !PUBLIC !FINAL
        
        Retrieves a field.
        """
        return self.data[item]

    def is_class( self, klass: str ):
        """
        !PUBLIC !FINAL
        
        Checks the "CLASS" property of the data, which is used to specify the
        entity class. 
        """
        return self.data.get( "CLASS" ) == klass

    @staticmethod
    def from_json( x ) -> "DbData":  # ~> JSONDecodeError
        """
        !PUBLIC !STATIC !FINAL
        
        Converse of `to_json`.
        
        :exception JSONDecodeError: Failure
        """
        import json
        return DbData( json.loads( x ) )

    def to_json( self ):
        """
        !PUBLIC !FINAL
        
        Serialises the data to JSON.
        
        e.g. used to serialise *output* entities in the database so that we can
             reconstruct them when we see the same *input* next time. 
        """
        import json
        return json.dumps( self.data, separators = (",", ":") )

    def to_dict( self ) -> dict:
        """
        !PUBLIC !FINAL
        
        Gets the dictionary, this is to support manual JSON serialisation.
        The result should not be modified.
        """
        return self.data

    @staticmethod
    def from_shash( text ) -> bytes:
        """
        !PUBLIC !FINAL
        
        Converts a `to_shash` to `to_hash`.
        """
        import base64
        return base64.b64decode( text )

    def to_shash( self ) -> str:
        """
        !PUBLIC !FINAL
        
        Gets the hash of the entity, as a base64 string.
        
        e.g. used to allow one `DbData` to uniquely identify another entity.  
        """
        import base64
        return base64.b64encode( self.to_hash() ).decode( "ascii" )

    def to_hash( self ) -> bytes:
        """
        !PUBLIC !FINAL
        
        Gets the hash of the entity, in bytes.
        
        e.g. used to allow an entity to be uniquely identified in the database.  
        """
        from mhelper.string_helper import string_to_hash
        return string_to_hash( self.to_json(), hash = "sha256", binary = True )
