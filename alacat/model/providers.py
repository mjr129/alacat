from typing import cast, Generic, Iterable, List, Optional, Sequence, Set, Tuple, Type, TypeVar, Union

from mhelper import exception_helper, reflection_helper, utf_table

from .debugging import standard_log
from .handler_base import Handler, ModelState
from .outputs import ScoresTable, RawScore, RawScoresTable, PipelineError
from .sequences import Peptide, Protein, Qbrick, Qmenu, Qconcat, Qblock, Text


TProteins = Optional[Union[Protein, Iterable[Optional[Protein]]]]
TProvider = TypeVar( "TProvider", bound = Handler )
TCollection = TypeVar( "TCollection" )
TInput = TypeVar( "TInput" )
TOutput = TypeVar( "TOutput" )


class Pending( Generic[TInput, TOutput] ):
    __slots__ = "input", "resolved"


    def __init__( self, input: TInput ):
        self.input: TInput = input
        self.resolved: Optional[Tuple[TOutput]] = None


    def __repr__( self ):
        return f"{type( self ).__name__}[{type( self.input ).__name__}, {type( self.resolved[0] ).__name__ if self.resolved else '0'}](input={self.input!r}, resolved={self.resolved!r})"


PendingProtein = Pending[Text, Protein]
PendingPeptide = Pending[Protein, Peptide]
PendingQbrick = Pending[None, Qbrick]
PendingQblock = Pending[Sequence[Peptide], Qblock]
PendingQconcat = Pending[None, Qconcat]
PendingQmenu = Pending[Sequence[Qbrick], Qmenu]
TOutputLike = Union[TInput, Pending[TInput, TOutput]]
ProteinLike = Union[None, Protein, PendingProtein]
PeptideLike = Union[Peptide, PendingPeptide]
QbrickLike = Union[Qbrick, PendingQbrick]
QblockLike = Union[Qblock, PendingQblock]
QconcatLike = Union[Qconcat, PendingQconcat]
QmenuLike = Union[Qmenu, PendingQmenu]
ProteinScores = Union[Iterable[RawScore[ProteinLike]], RawScore[ProteinLike]]
PeptideScores = Union[Iterable[RawScore[PeptideLike]], RawScore[PeptideLike]]
QblockScores = Union[Iterable[RawScore[QblockLike]], RawScore[QblockLike]]
QbrickScores = Union[Iterable[RawScore[QbrickLike]], RawScore[QbrickLike]]
QconcatScores = Union[Iterable[RawScore[QconcatLike]], RawScore[QconcatLike]]
QmenuScores = Union[Iterable[RawScore[QmenuLike]], RawScore[QmenuLike]]


class ProviderCollectionBase( Generic[TProvider, TInput, TOutput] ):
    __abstract__ = True
    t_provider: Type[TProvider] = None
    t_input: Type[TInput] = None
    t_output: Type[TOutput] = None


    def __init_subclass__( cls, **kwargs ):
        # Checks...
        if reflection_helper.is_explicit_abstract( cls ):
            return

        assert isinstance( cls.t_provider, type ), f"`{cls.__name__}.t_provider` must be a class"
        assert isinstance( cls.t_input, type ), f"`{cls.__name__}.t_input` must be a class"
        assert isinstance( cls.t_output, type ), f"`{cls.__name__}.t_output` must be a class"


    def __init__( self, providers: Union[ModelState, Iterable[TProvider]] ):
        if isinstance( providers, ModelState ):
            providers = providers.instantiate( self.t_provider )

        providers_: Tuple[TProvider, ...] = tuple( sorted( providers, key = lambda x: x.accession ) )
        exception_helper.safe_cast_iter( "providers", providers_, self.t_provider )
        self.handlers: Tuple[TProvider, ...] = providers_


    def provide( self,
                 input: Union[ScoresTable[TInput],
                              Sequence[TInput],
                              Sequence[Pending[TInput, TOutput]]]
                 ) -> ScoresTable[TOutput]:
        """
        Uses the handlers to translate `protein_accessions` to protein
        sequences (`Protein` objects).
        
        :param input:  Request
        :return:       Output
        """
        standard_log( utf_table.format_box( f"STAGE: {self.t_provider.__name__} acquisition" ) )

        if isinstance( input, ScoresTable ):
            input = input.subjects

        if not input:
            raise PipelineError( f"Cannot provide any {self.t_output.__name__}(s) because no input has been provided." )

        state = self.handlers[0].state
        pending: List[Pending[TInput, TOutput]] = [x if isinstance( x, Pending ) else Pending( x ) for x in input]
        output: List[RawScore[TOutputLike]] = []
        tries = 0
        profiling = { }

        while pending:
            # Check we aren't looping
            if tries > 5:
                raise RuntimeError( "Couldn't resolve input and I'm probably stuck in a loop." )

            tries += 1

            # Ask the handlers to convert the "pending" list into a "provided" list
            provided: List[RawScore[TOutputLike]] = []

            for handler in self.handlers:  # type: TProvider
                with standard_log.time( f"Providing `{self.t_output.__name__}` using `{handler}`..." ) as timer:
                    new_protein_scores = handler.query_all( pending )
                    standard_log( f"...done ({len( new_protein_scores )} new scores)." )
                    provided.extend( new_protein_scores )
                    profiling[handler] = profiling.get( handler, 0 ) + timer.now()

            # Check all "pending" have been mapped to *something* in "provided"
            missing = []

            for pending_obj in pending:
                pending_obj.resolved = tuple( provided_obj.subject for provided_obj in provided if provided_obj.input is pending_obj )

                if not pending_obj.resolved:
                    missing.append( pending_obj )

            if missing:
                hs = "\n".join( f" * {x!r}" for x in self.handlers )
                coe = "\n".join( f" * {x!r}" for x in missing )
                state.warnings.append( f"Could not identify {len( missing )} {self.t_output.__name__} inputs:\n"
                                       f"\n"
                                       f"{coe}\n"
                                       f"\n"
                                       f"These {self.t_output.__name__}(s) will be missing from your results.\n"
                                       f"\n"
                                       f"I tried the following handlers:\n"
                                       f"\n"
                                       f"{hs}\n"
                                       f"" )

            pending.clear()

            # Add any proteins to our results, and any PendingProtein to our next iteration
            for provision in provided:
                if isinstance( provision.subject, self.t_output ):
                    output.append( cast( RawScore[Protein], provision ) )
                elif isinstance( provision.subject, Pending ):
                    pending.append( cast( Pending, provision.subject ) )
                else:
                    raise exception_helper.type_error( "provision", provision, Union[TInput, Pending[self.t_input, self.t_output]] )

        standard_log()

        #
        # Add profiling results
        #
        for handler, time_taken in profiling.items():
            state.profiling.append( (f"Acquisitions|{handler.accession}|{handler.accession}/Acquisition", time_taken) )

        #
        # Close the handlers
        #
        for handler in self.handlers:
            handler.close()

        if not output:
            warning_str = "\n\n".join( f"Warning {i}:\n{x}" for i, x in enumerate( state.warnings ) )

            raise PipelineError(
                    f"PipelineError.\n"
                    f"Failed to acquire any {self.t_output.__name__}(s).\n"
                    f"The following warnings were also recorded:\n"
                    f"{warning_str}" )

        vsc = RawScoresTable.create( output )
        fsc = ScoresTable.create( vsc )

        self.on_provided( fsc )
        return fsc


    def on_provided( self, output: ScoresTable[TOutput] ):
        """
        !VIRTUAL 
        """
        pass


class ProteinProvider( Handler[PendingProtein, ProteinLike] ):
    """
    The protein providers translate the user input from the "protein" field of
    the parameters into actual proteins (their names and sequences).
    
    Enabling or disabling the various protein providers will change the types
    of input supported. Generally, all providers should be left enabled unless
    they are causing problems (for instance to resolve ambiguities when it isn't
    clear which provider should parse the input).
    """
    __abstract__ = True
    __slots__ = ()


class ProteinProviderCollection( ProviderCollectionBase ):
    """
    Collection of `ProteinProvider` objects.
    Typically obtained from `HandlerFactoryCollection.create_providers`.
    """
    __slots__ = ()
    t_provider = ProteinProvider
    t_input = str
    t_output = Protein


class PeptideProvider( Handler[PendingPeptide, PeptideLike] ):  # i.e. peptide provider
    """
    The peptide providers digesting the `Protein`\s to `Peptide`\s during the
    digestion stage of the workflow.
    
    Where available, allowing multiple providers to perform the same digestion 
    routine can be useful to compare various simulated digestion methods.
    However, this naturally means the workflow will take longer to process.
    
    Some peptide providers may also provide peptide *scores* at the same time
    as performing the digest. These scores are carried through into the next
    stage of the workflow.
    
    Providers can be disabled to speed up the workflow if their output is
    redundant or of no use. Note that providers themselves already ensure the
    workflow parameters are honoured. For instance, disabling the TRYPSIN
    digester in a PEPSINA workflow will have no effect, since the digester will
    be naturally disabled.
    """
    __abstract__ = True
    __slots__ = ()


class PeptideProviderCollection( ProviderCollectionBase ):
    """
    Collection of `PeptideProvider` objects.
    Typically obtained from `HandlerFactoryCollection.create_digesters`.
    """
    t_provider = PeptideProvider
    t_input = Protein
    t_output = Peptide


    def on_provided( self, output: ScoresTable[TOutput] ):
        # Checks that any peptides loaded along with the proteins now form part
        # of the collection.
        state: ModelState = output.columns[0].handler.state
        obtained: Set[str] = { x.sequence for x in output.subjects }
        pending_peptides = state.pending_peptides
        warn = { }

        for protein, released in pending_peptides.items():
            released_set: Set[str] = set( released )
            diff = released_set - obtained

            if diff:
                warn[protein] = diff

        if warn:
            state.warnings.append(
                    "One or more peptides were produced in the protein-acquisition stage "
                    "but these were not released during the peptide-acquisition (digestion) stage.\n"
                    "\n"
                    "If you are using a protein source (e.g. TsvFileProvider) that loads both "
                    "proteins and peptides simultaneously then you must ensure that a "
                    "CoupledPeptideProvider exists available to allow these peptides to enter the model "
                    "during peptide-acquisition.\n"
                    "\n"
                    "The list of missing peptides follows.\n"
                    "\n"
                    f"{warn}" )


class QblockProvider( Handler[PendingQblock, QblockLike] ):
    """
    The Qblock providers are to Qbricks and Qblocks as the Peptide providers
    are to to Peptides. Please see the `PeptideProvider` documentation for
    more details.
    """
    __abstract__ = True
    __slots__ = ()


class QblockProviderCollection( ProviderCollectionBase ):
    __slots__ = ()
    t_provider = QblockProvider
    t_input = tuple  # Tuple[Peptide]
    t_output = Qblock


class QmenuProvider( Handler[PendingQmenu, QmenuLike] ):
    """
    The Qmenu providers are to Qconcats and Qmenus as the Peptide providers
    are to to Peptides. Please see the `PeptideProvider` documentation for
    more details.
    """
    __abstract__ = True
    __slots__ = ()


class QmenuProviderCollection( ProviderCollectionBase ):
    __slots__ = ()
    t_provider = QmenuProvider
    t_input = tuple  # Tuple[Qbrick]
    t_output = Qmenu
