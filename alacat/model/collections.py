"""
Collections of the core elements.
"""
from collections import defaultdict
from typing import Dict, Iterable, List, Sequence, TypeVar

from mhelper import array_helper, exception_helper
from mhelper.generics_helper import NestedSequence
from mhelper.special_types import NOT_PROVIDED

from .sequences import Qmenu, Peptide, Protein, Qbrick, Qblock
from .outputs import PipelineError


_TSelf = TypeVar( "_TSelf" )
_TAaSubject = TypeVar( "_TAaSubject", bound = "AminoAcidSequence" )


class AminoAcidSequenceCollection( NestedSequence[_TAaSubject] ):
    """
    Collection of things with a sequence.
    
    !ABSTRACT
    """
    
    
    def __init__( self, items: Sequence[_TAaSubject] ):
        super().__init__( items )
    
    
    def get( self, key: str, default = NOT_PROVIDED ) -> _TAaSubject:
        try:
            return self.find( (key,) )[0]
        except KeyError as ex:
            return exception_helper.default( default, ex = ex )
    
    
    def find( self: _TSelf, keys: Iterable[str] ) -> _TSelf:
        """
        Finds a subset of peptides, matching the specified string
        representations or sequences.
        """
        
        if not keys:
            return type( self )( () )
        
        try:
            return type( self )( array_helper.find_all( self, keys, match = lambda aa, key: key in aa.keys ) )
        except KeyError as ex:
            raise KeyError( "At least one of the amino acid sequences that you specified does not exist in the digestion. Please check your input and try again. See the causative exception for more details." ) from ex


class ProteinCollection( AminoAcidSequenceCollection[Protein] ):
    def __init__( self, items: Iterable[Protein] ):
        items = tuple( items )
        exception_helper.safe_cast_iter( "items", items, Protein )
        super().__init__( items )


class PeptideCollection( AminoAcidSequenceCollection[Peptide] ):
    """
    Collection of `Peptide`\s.
    """
    
    
    def __init__( self, items: Iterable[Peptide] ):
        items = tuple( items )
        exception_helper.safe_cast_iter( "items", items, Peptide )
        
        if not items:
            raise PipelineError( "No peptides have been obtained." )
        
        super().__init__( items )
    
    
    def map( self ) -> Dict[Protein, List[Peptide]]:
        """
        Creates a mapping of `Protein`\s to their `Peptide`\s.
        """
        r = defaultdict( list )
        
        for peptide in self:
            r[peptide.protein].append( peptide )
        
        return dict( r )


class QbrickCollection( NestedSequence[Qbrick] ):
    """
    Collection of `QBrick`\s.
    """
    __slots__ = ()
    
    
    def __init__( self, qbricks: Iterable[Qbrick] ):
        super().__init__( tuple( qbricks ) )
    
    
    def find( self, keys: Iterable[str] ) -> "QbrickCollection":
        """
        Finds a subset of qbricks, matching the specified string representations
        or sequences. The result is a subset of this collection. 
        """
        return QbrickCollection( array_helper.find_all( self, keys, lambda qbrick, key: qbrick.sequence == key or str( qbrick ) == key ) )


class QblockCollection( NestedSequence[Qblock] ):
    def __init__( self, qblocks: Iterable[Qblock] ):
        super().__init__( tuple( qblocks ) )


class QmenuCollection( NestedSequence[Qmenu] ):
    """
    Collection of `Qmenu`\s.
    """
    __slots__ = "alacats",
    
    
    def __init__( self, collections: Iterable[Qmenu] ):
        super().__init__( tuple( collections ) )
        self.alacats = tuple( alacat for menu in self for alacat in menu.qconcats )
        
        assert all( isinstance( x, Qmenu ) for x in self )
    
    
    def find( self, keys: Iterable[str] ) -> "QmenuCollection":
        """
        Finds a subset of peptides, matching the specified string
        representations or sequences.
        """
        return QmenuCollection( array_helper.find_all( self, keys, lambda subject, key: str( subject ) == key ) )
