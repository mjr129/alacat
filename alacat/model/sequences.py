"""
Sequences: proteins, peptides, qbricks, alacats and menus (and the things to
support them).
"""
from dataclasses import dataclass
from enum import auto, Enum
from functools import lru_cache
from typing import Iterable, List, Optional, Sequence, TypeVar, Union
from mhelper import array_helper, exception_helper
from mhelper.exception_helper import safe_cast

from alacat.utilities import peptide_helper
from alacat import model
from alacat.model.accession import Entity, DbData


__all__ = "AminoAcidSequence", "Protein", "ETerm", "Peptide", "ESubsequence", "LinkedPeptide", "Qbrick", "Qconcat", "Qmenu"

TAminoAcidSequence = TypeVar( "TAminoAcidSequence", bound = "AminoAcidSequence" )


class Text( Entity ):
    """
    A string with a unique accession.
    """
    __slots__ = "__accession", "text",
    
    
    def __init__( self, accession: str, text: str ):
        assert isinstance( text, str )
        assert len( text ) >= 2
        self.__accession = accession
        self.text = text
    
    
    def to_dbdata( self ) -> DbData:
        return DbData( CLASS = "Text",
                       text = self.text )
    
    
    @property
    def accession( self ) -> str:
        return self.__accession
    
    
    @dataclass()
    class Parsed:
        text: str
    
    
    @staticmethod
    def parse_dbdata( d: DbData ) -> Optional[Parsed]:
        if not d.is_class( "Text" ):
            return None
        
        return Text.Parsed( d["t"] )
    
    
    def __repr__( self ):
        return f"{self.__class__.__name__}({self.text!r})"


class AminoAcidSequence( Entity ):
    """
    Things with an amino acid sequence.
    
    Note that we allow '/' in our sequences, which indicates an enzymatic
    cut-point and permits custom digestion to be specified.
    
    !ABSTRACT
    """
    
    __slots__ = "__sequence", "__accession"
    
    
    def __init__( self, accession: str, sequence: str ):
        self.__accession = accession
        self.__sequence = sequence
    
    
    @property
    def accession( self ) -> str:
        return self.__accession
    
    
    @property
    def sequence( self ) -> str:
        return self.__sequence
    
    
    def to_dbdata( self ) -> DbData:
        raise NotImplementedError( "abstract" )
    
    
    def map_subsequences( self, subsequences: Sequence[TAminoAcidSequence], shortest_first = False ) -> List[Union[str, TAminoAcidSequence]]:
        return self.__map_subsequences( tuple( subsequences ), shortest_first )
    
    
    @lru_cache
    def __map_subsequences( self, subsequences: Sequence[TAminoAcidSequence], shortest_first = False ) -> List[Union[str, TAminoAcidSequence]]:
        """
        Locates subsequences within this sequence.
        
        :param subsequences: Sequences to map. 
        :param shortest_first: Shortest elements take priority, otherwise largest ones do.
        :return: The result is a list, with strings represents unmapped regions and
        `subsequences` elements representing mapped subsequences. 
        """
        peptides = sorted( subsequences, key = lambda x: x.sequence.__len__(), reverse = not shortest_first )
        
        parts = [self.sequence]
        
        for peptide in peptides:
            fnd = peptide.sequence
            new_parts = []
            
            for part_ in parts:
                queue = [part_]
                
                while queue:
                    part = queue.pop( 0 )
                    
                    if not isinstance( part, str ):
                        new_parts.append( part )
                        continue
                    
                    index = part.find( fnd )
                    
                    if index == -1:
                        new_parts.append( part )
                        continue
                    
                    if index:
                        new_parts.append( part[:index] )
                    
                    new_parts.append( peptide )
                    
                    e = index + len( fnd )
                    
                    if e != len( part ):
                        queue.append( part[e:] )
            
            parts = new_parts
        
        return parts


class Protein( AminoAcidSequence ):
    """
    Proteins.
    
    Produced during the protein acquisition (`ProteinProviderCollection`) stage.
    """
    __slots__ = "title", "providers", "organism"
    
    
    def __init__( self,
                  accession: str,
                  sequence: str,
                  title: Optional[str],
                  organism: int ):
        """
        NOTE: The only call to this constructor should be `ModelState.make_protein`.
         
        :param accession:   Accession.
                            The stereotypical lookup for the protein, such as
                            a uniprot-ID.
        :param sequence:    The amino-acid sequence. 
        :param title:       The human-readable title of the protein, or blank
                            if unknown.
        :param organism:    The organism, or 0 if unknown. 
        """
        super().__init__( accession, sequence )
        
        peptide_helper.Peptide.assert_is_sequence( sequence )
        assert sequence
        
        if not title:
            title = f"Untitled protein {self.accession}"
        
        if organism is None:
            organism = -1
        
        self.title = title
        self.providers: List[model.Handler] = []
        self.organism = organism
    
    
    def __repr__( self ):
        return f"{type( self ).__name__}( {self.accession!r}, {self.sequence.__len__()} )"
    
    
    def __str__( self ):
        return f"{self.accession}"
    
    
    @dataclass()
    class Parsed:
        accession: str
        title: str
        sequence: str
        organism: int
    
    
    def to_dbdata( self ) -> DbData:
        return DbData( CLASS = "Protein",
                       accession = self.accession,
                       title = self.title,
                       sequence = self.sequence,
                       organism = self.organism )
    
    
    @staticmethod
    def parse_dbdata( d: DbData ) -> Optional[Parsed]:
        if not d.is_class( "Protein" ):
            return None
        
        return Protein.Parsed( d["accession"], d["title"], d["sequence"], d["organism"] )
    
    
    @staticmethod
    def from_dbdata( d: DbData, s: "model.ModelState" ) -> Optional["Protein"]:
        p = model.Protein.parse_dbdata( d )
        
        if not p:
            return None
        
        return s.acquire_protein( p.accession, p.sequence, p.title, p.organism )


class ETerm( Enum ):
    """
    Peptide terminal types.
    
    :cvar N:        On the N terminal (that's the start)
    :cvar NONE:     Not on a terminal (somewhere in the middle)
    :cvar C:        On the C terminal (that's the end) 
    :cvar FULL:     Spanning both terminals (i.e. the peptide is also the protein)     
    """
    N = -1
    NONE = 0
    C = 1
    FULL = 3


class ComesFromProtein( Entity ):
    """
    Things with a `protein` property.
    Always `Entity`, sometimes `AminoAcidSequence`.
    """
    
    
    def to_dbdata( self ) -> DbData:
        raise NotImplementedError( "abstract" )
    
    
    @property
    def accession( self ) -> str:
        raise NotImplementedError( "abstract" )
    
    
    @property
    def protein( self ):
        """
        Protein this entity comes from.
        """
        raise NotImplementedError( "abstract" )


class Peptide( AminoAcidSequence, ComesFromProtein ):
    """
    Peptides.
    
    The output of the digestion step (`PeptideProviderCollection`).
    """
    __slots__ = "__protein", "start", "end", "terminal", "digestion"
    
    
    def __init__( self, accession: str, sequence: str, protein: Protein ):
        """
        NOTE: The only call to this constructor should be `ModelState.acquire_peptide`.
        
        :param protein:                 Origin protein
        :param sequence:                Amino acid sequence one letter code 
        :exception NotFoundError:       Sequence does not exist in protein.
        :exception ValueError:          Something doesn't look right, such as mismatched start/end/terminal/sequence values.
        """
        
        super().__init__( accession, sequence )
        assert isinstance( protein, Protein )
        assert isinstance( sequence, str ) and sequence, "Sequence is not a string or it is empty."
        
        if not peptide_helper.Peptide.is_sequence( sequence ):
            raise ValueError( f"Bad peptide sequence: {sequence!r}" )
        
        self.__protein: Protein = protein
        
        pep_len = len( sequence )
        prot_len = len( protein.sequence )
        
        start = protein.sequence.find( self.sequence ) + 1
        if not start:
            raise exception_helper.NotFoundError(
                    f"The peptide sequence does not exist in the protein sequence.\n"
                    f" * Peptide sequence: {self.sequence!r}\n"
                    f" * Protein sequence: {self.protein.sequence!r}\n"
                    f" * Protein accession: {self.protein.accession}" )
        
        end = start + len( sequence ) - 1
        
        if start == 1 and end == pep_len:
            terminal = ETerm.N
        elif start == prot_len - pep_len and end == prot_len:
            terminal = ETerm.C
        else:
            terminal = ETerm.NONE
        
        assert start is not None
        assert end is not None
        assert terminal is not None
        
        self.start = start  # 1 offset
        self.end = end  # 1 offset
        self.terminal = safe_cast( "terminal", terminal, ETerm )
        
        # 0123456
        # 1234567
        # ABCDEFG
        #  S   E
        #  2   6
        #  1   6
        expected = self.protein.sequence[self.start - 1:self.end]
        
        if expected != self.sequence:
            raise RuntimeError( "The peptide is not present at the specified region within its protein." )
    
    
    @property
    def protein( self ):
        return self.__protein
    
    
    @property
    def length( self ) -> int:
        """
        Number of amino acids.
        """
        return len( self.sequence )
    
    
    @property
    def n_flank( self ):
        return self.get_flank( ETerm.N, decorate = False )
    
    
    @property
    def c_flank( self ):
        return self.get_flank( ETerm.C, decorate = False )
    
    
    def get_flank( self, terminal: ETerm, length: int = 3, decorate: bool = True ) -> str:
        """
        Gets the flanking sequence.
        
        :param terminal:    `ETerm.N` or `ETerm.C`   
        :param length:      Number of sites to get.
        :param decorate:    Include ``|`` to signal the start/end of the protein and ``~`` is to signal continuation (i.e. *not* the start or the end). 
        :return:            Flanking sequence and decorations.
        """
        end_decor = "|" if decorate else ""
        join_decor = "~" if decorate else ""
        
        if terminal == ETerm.N:
            start = self.start - length - 1
            
            if start < 0:
                start = 0
                ex = end_decor
            else:
                ex = join_decor
            
            return ex + self.protein.sequence[start:self.start - 1]
        elif terminal == ETerm.C:
            end = self.end + length
            
            if end >= len( self.protein.sequence ):
                end = len( self.protein.sequence )
                ex = end_decor
            else:
                ex = join_decor
            
            return self.protein.sequence[self.end: end] + ex
        else:
            raise ValueError( "terminal" )
    
    
    @property
    def abv_sequence( self ) -> str:
        """
        The sequence, or a readable portion of the sequence if it is long.
        """
        return self.get_abv_sequence()
    
    
    def get_abv_sequence( self, max_len = 25 ) -> str:
        if len( self.sequence ) > max_len:
            max_len = (max_len - 1) // 2
            return f"{self.sequence[:max_len]}...{len( self.sequence )}...{self.sequence[-max_len:]}"
        else:
            return self.sequence
    
    
    def __repr__( self ) -> str:
        return f"{type( self ).__name__}( {self.protein.accession!r}, {self.sequence!r})"
    
    
    def __str__( self ) -> str:
        return self.accession
    
    
    @staticmethod
    def __check( current, new ):
        if current is None:
            return new
        
        assert current == new
        return new
    
    
    @dataclass
    class Parsed:
        protein_hash: bytes
        peptide_sequence: str
    
    
    def to_dbdata( self ) -> DbData:
        return DbData( CLASS = "Peptide",
                       protein = self.protein.to_dbdata().to_shash(),
                       sequence = self.sequence )
    
    
    @staticmethod
    def parse_dbdata( d: DbData ) -> Optional[Parsed]:
        if not d.is_class( "Peptide" ):
            return None
        
        return Peptide.Parsed( DbData.from_shash( d["protein"] ), d["sequence"] )
    
    
    @staticmethod
    def from_dbdata( d: DbData, s: "model.ModelState" ):
        parsed = model.Peptide.parse_dbdata( d )
        
        if not parsed:
            return None
        
        protein = s.find( parsed.protein_hash, model.Protein )
        return s.try_acquire_peptide( parsed.peptide_sequence, protein )


class ESubsequence( Enum ):
    """
    Purpose of a `Subsequence` within a `Qbrick`.
    
    :cvar N_FLANK: N-flanking linker of peptide
    :cvar PEPTIDE: Peptide sequence
    :cvar C_FLANK: C-flanking linker of peptide 
    """
    N_FLANK = auto()
    PEPTIDE = auto()
    C_FLANK = auto()


class LinkedPeptide( AminoAcidSequence, ComesFromProtein ):
    """
    A peptide with defined n and c linkers.
    """
    __slots__ = "n_link", "peptide", "c_link"
    
    
    def __init__( self, accession: str, n_link: str, peptide: Peptide, c_link: str ):
        """
        This should only be called from `ModelState.acquire_linked_peptide`.
        
        :param accession: 
        :param n_link: 
        :param peptide: 
        :param c_link: 
        """
        assert isinstance( n_link, str )
        assert isinstance( peptide, Peptide )
        assert isinstance( c_link, str )
        self.n_link = n_link
        self.peptide = peptide
        self.c_link = c_link
        super().__init__( accession, sequence = f"{self.n_link}{self.peptide.sequence}{self.c_link}" )
    
    
    @property
    def protein( self ):
        return self.peptide.protein
    
    
    def to_dbdata( self ) -> DbData:
        return DbData( CLASS = "LinkedPeptide",
                       peptide = self.peptide.to_dbdata().to_shash(),
                       n = self.n_link,
                       c = self.c_link )
    
    
    @dataclass()
    class Parsed:
        n_link: str
        peptide_hash: bytes
        c_link: str
    
    
    @staticmethod
    def parse_dbdata( d: DbData ) -> Optional[Parsed]:
        if not d.is_class( "LinkedPeptide" ):
            return
        
        return LinkedPeptide.Parsed( d["n"], DbData.from_shash( d["peptide"] ), d["c"] )


class Qbrick( AminoAcidSequence, ComesFromProtein ):
    """
    A Q-brick.
    
    Produced from the selected peptides. 
    """
    __slots__ = "linked_peptides", "peptides", "__protein"
    
    
    def __init__( self, accession: str, linked_peptides: Iterable[LinkedPeptide] ):
        """
        NOTE: The only call to this constructor should be `ModelState.acquire_qbrick`.
        
        :param accession:           Accession (arbitrary) 
        :param linked_peptides:     Peptides and linkers 
        """
        exception_helper.safe_cast_iter( "linked_peptides", linked_peptides, LinkedPeptide )
        
        self.linked_peptides: Sequence[LinkedPeptide] = tuple( linked_peptides )
        self.peptides: Sequence[Peptide] = tuple( lpep.peptide for lpep in self.linked_peptides )
        self.__protein: Protein = array_helper.single( frozenset( peptide.protein for peptide in self.peptides ) )
        
        super().__init__( sequence = "".join( lpep.sequence for lpep in self.linked_peptides ),
                          accession = accession )
    
    
    @property
    def protein( self ) -> Protein:
        return self.__protein
    
    
    def __repr__( self ) -> str:
        return f"{type( self ).__name__}( {self.protein!r}, {', '.join( f'{peptide.sequence!r}' for peptide in self.peptides )} )"
    
    
    def to_dbdata( self ) -> DbData:
        return DbData( CLASS = "Qbrick",
                       content = [p.to_dbdata().to_dict() for p in self.linked_peptides] )
    
    
    @dataclass()
    class Parsed:
        linked_peptide_db_data: Sequence[DbData]
    
    
    @staticmethod
    def parse_dbdata( d: DbData ) -> Optional[Parsed]:
        if not d.is_class( "Qbrick" ):
            return None
        
        return Qbrick.Parsed( [DbData( x ) for x in d["content"]] )


class Qblock( ComesFromProtein ):
    """
    A group of one or more qbricks belonging to the same protein.
    
    We generate and score these instead of the qbricks, because the composition
    of one qbrick naturally constrains any other qbricks from the same protein
    (that is, they shouldn't share any peptides).
    """
    
    
    def __init__( self, accession: str, qbricks: Iterable[Qbrick] ):
        self.__accession = accession
        self.qbricks = tuple( qbricks )
    
    
    @property
    def protein( self ):
        return self.qbricks[0].protein
    
    
    def list_peptides( self ):
        return [peptide for qbrick in self.qbricks for peptide in qbrick.peptides]
    
    
    def __str__( self ):
        return self.accession
    
    
    @dataclass()
    class Parsed:
        qbrick_db_data: Sequence[DbData]
    
    
    @staticmethod
    def parse_dbdata( d: DbData ) -> Optional[Parsed]:
        if d["CLASS"] != "Qblock":
            return None
        
        return Qblock.Parsed( [DbData( x ) for x in d["content"]] )
    
    
    @staticmethod
    def from_dbdata( d: DbData, s: "model.ModelState" ) -> Optional["Qblock"]:
        parsed: model.Qblock.Parsed = model.Qblock.parse_dbdata( d )
        
        if not parsed:
            return None
        
        r = []
        
        for qbrick_db_data in parsed.qbrick_db_data:
            parsed2: model.Qbrick.Parsed = model.Qbrick.parse_dbdata( qbrick_db_data )
            assert parsed2
            linked_peptides = []
            
            for linked_peptide_db_data in parsed2.linked_peptide_db_data:
                parsed3: model.LinkedPeptide.Parsed = model.LinkedPeptide.parse_dbdata( linked_peptide_db_data )
                assert parsed3
                
                peptide = s.find( parsed3.peptide_hash, model.Peptide )
                linked_peptide = s.acquire_linked_peptide( parsed3.n_link, peptide, parsed3.c_link )
                linked_peptides.append( linked_peptide )
            
            qbrick = s.acquire_qbrick( linked_peptides )
            r.append( qbrick )
        
        return s.acquire_qblock( r )
    
    
    def to_dbdata( self ) -> DbData:
        return DbData( CLASS = "Qblock",
                       content = [x.to_dbdata().to_dict() for x in self.qbricks] )
    
    
    @property
    def accession( self ) -> str:
        return self.__accession


class Qconcat( AminoAcidSequence ):
    """
    An Qconcat.
    
    Produced from the selected Qbricks.
    """
    
    __slots__ = "qbricks", "menu"
    
    
    def __init__( self, accession: str, qbricks: Iterable[Qbrick] ):
        """
        NOTE: The only call to this constructor should be `ModelState.acquire_qconcat`.
        
        :param accession:   Accession, arbitrary. 
        :param qbricks:     Qbricks, in order. 
        """
        self.qbricks = tuple( qbricks )
        self.menu = None
        super().__init__( accession = accession,
                          sequence = "".join( qbrick.sequence for qbrick in self.qbricks ) )
    
    
    def __repr__( self ):
        contents = ", ".join( repr( qbrick ) for qbrick in self.qbricks )
        return f"{type( self ).__name__}( {contents} )"
    
    
    def to_dbdata( self ) -> DbData:
        return DbData( CLASS = "Qconcat",
                       content = [q.to_dbdata().to_shash() for q in self.qbricks] )
    
    
    @dataclass
    class Parsed:
        qbrick_hashes: Sequence[bytes]
    
    
    @staticmethod
    def parse_dbdata( d: DbData ) -> Optional[Parsed]:
        if not d.is_class( "Qconcat" ):
            return None
        
        return Qconcat.Parsed( [DbData.from_shash( x ) for x in d["content"]] )


class Qmenu( Entity ):
    """
    A 'Qmenu'.
    We use this term to refer to a collection of `Qconcat`\s representing our
    target set of `Qbricks`. Our output is a `Qmenu` and not an `Qconcat` because
    in reality a single `Qconcat` may not have enough space to encompass all of
    our targets.
    """
    __slots__ = "qconcats", "__accession"
    
    
    def __init__( self, accession: str, alacats: Iterable[Qconcat] ):
        """
        NOTE: The only call to this constructor should be `ModelState.acquire_qmenu`.
        
        :param accession:       Accession, arbitrary 
        :param alacats:         Alacats, in any order. 
        """
        self.__accession = accession
        self.qconcats = tuple( alacats )
        
        for alacat in self.qconcats:
            alacat.menu = self
    
    
    @property
    def accession( self ):
        return self.__accession
    
    
    def __repr__( self ):
        contents = ", ".join( repr( alacat ) for alacat in self.qconcats )
        return f"{type( self ).__name__}( {contents} )"
    
    
    def __str__( self ):
        return self.accession
    
    
    def to_dbdata( self ) -> DbData:
        return DbData( CLASS = "Qmenu",
                       content = [x.to_dbdata().to_shash() for x in self.qconcats] )
    
    
    @dataclass
    class Parsed:
        qconcat_hashes: Sequence[bytes]
    
    
    @staticmethod
    def parse_dbdata( d: DbData ) -> Optional[Parsed]:
        if not d.is_class( "Qmenu" ):
            return None
        
        return Qmenu.Parsed( [DbData.from_shash( x ) for x in d["content"]] )
