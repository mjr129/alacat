from mhelper.bio_requests import core as bio_requests_core
from dataclasses import dataclass
from functools import lru_cache
from typing import Optional, Sequence

from mhelper.log_helper import LogGroup, Logger


standard_logs = LogGroup( "alacat.diagnostics_group",
                          doc = "Normal diagnostics and progress messages for ALACAT. "
                                "These are shown in Alacat's CLI and are also available to view from the web GUI." )

# standard_logs.add( web_helper.request_log )
# standard_logs.add( web_helper.request_log_ex )
standard_logs.add( bio_requests_core.core_log )

standard_log = Logger( "alacat.diagnostics",
                       doc = "Alacat main log."
                             "Progress information goes here, as does any extra information that the user may find useful in debugging, such as proteome IDs."
                             "See also `standard_logs`.",
                       group = standard_logs )

backingdb_log = Logger( "alacat.database",
                        doc = "ALACAT database backing."
                              "This is quite verbose."
                              "Usually hidden." )

favoured_map_debugging = Logger( "alacat.favoured",
                                 "Diagnostics for favoured maps."
                                 "Usually hidden." )


def enable_diagnostics(  ):
    """
    Enables console debugging traces.
    
    This is primarily called from `__main__` to allow the user to see progress,
    but it also forms part of the public API to support tracking progress
    through Jupyter. 
    """
    standard_logs.attach_to_console(  )
    standard_log( "ALACAT diagnostics have been enabled." )


@dataclass
class PathInfo:
    index: int  # Index into get_paths() array
    name: str  # Descriptive name
    source: str  # How to obtain in code
    path: str  # Actual path
    __size: Optional[str] = None


    def get_size( self ) -> str:
        """
        For a path `index` into `get_paths`, returns the size of the data, as a
        string. 
        """
        if self.__size is None:
            from mhelper import file_helper
            from mhelper import string_helper

            size = file_helper.get_directory_tree_size( self.path, recurse = True )
            self.__size = string_helper.format_size( size )

        return self.__size


@lru_cache
def get_paths() -> Sequence[PathInfo]:
    """
    Lists application paths and information.
    """
    import sys
    import os
    import alacat
    from alacat.utilities.mysql_backed_cache import AlacatBacking, ESqlBacking
    from mhelper import web_helper
    from mhelper.bio_requests import core

    return (PathInfo( 0,
                      "Python",
                      "sys.prefix",
                      sys.prefix ),
            PathInfo( 1,
                      "Alacat code",
                      "{alacat.__file__}" + os.sep + "..",
                      os.path.dirname( alacat.__file__ ) ),
            PathInfo( 2,
                      "Alacat data",
                      "alacat.paths.get_data_root()",
                      alacat.paths.get_data_root() ),
            PathInfo( 3,
                      "Alacat GUI data only",
                      "alacat.paths.get_gui_models_root()",
                      alacat.paths.get_gui_models_root() ),
            PathInfo( 4,
                      "WebHelper webpage cache",
                      "mhelper.web_helper.CachedRequest.get_default_cache_folder()",
                      web_helper.CachedRequest.get_default_cache_folder() ),
            PathInfo( 5,
                      "BioRequests all data",
                      "mhelper.bio_requests.core.large_data_path",
                      core.large_data_path ),
            PathInfo( 6,
                      "BioRequests webpage cache only",
                      "mhelper.bio_requests.core.web_cache_path",
                      core.web_cache_path ),
            PathInfo( 7,
                      "Alacat backing database",
                      "AlacatBacking( None ).database.get_directory()",
                      AlacatBacking( None, ESqlBacking.READ ).get_db_directory() ))
