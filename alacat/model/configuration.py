"""
!INTERNAL
"""
from enum import auto, Enum
from typing import FrozenSet, Iterable, Optional, Tuple, Union

import alacat
from alacat.model.handler_factory import HandlerFactoryCollection
from mhelper import file_helper, io_helper, string_helper
from mhelper.exception_helper import safe_cast
from mhelper.string_helper import indent_value as _iv


class EError( Enum ):
    IGNORE = auto()
    WARN = auto()
    ERROR = auto()


# noinspection SpellCheckingInspection
class EDigestion( Enum ):
    """
    Digestions that the pipeline is capable of *simulating*. 

    .. warning::
    
        Most of the digestion methods are included to support OpenMs's digester,
        which is a fast regular expression based digester.
        However, digestion and analysis are two different stages of the pipeline.
        
        While we are easily able to simulate non-tryptic digests, the ability of
        many tools and databases to actually provide insight into such peptides
        is questionable. Proceed with caution when using these.    
        
    Regular expressions and descriptions are provided from OpenMs.
        
    
    :cvar OTHER:                         Preprogrammed.
                                         Indicates a digestion method other than those listed
                                         in this enumeration. Typically this means the user has
                                         specified the peptides verbatim, or provided a custom
                                         `Handler` capable of simulating a unique digestion.
                                         *Only for advanced API use, not useful from GUI*.
                                         
    :cvar INHERIT:                       Inherit model.
                                         Inherits the digestion of the parent model.
                                         This is only valid for `Handler`\s and does not make
                                         sense to use on the model itself.
                                         *Only for advanced API use, not useful from GUI*.
                                         
    :cvar CUSTOM:                        Custom digestion, with known flanks.
                                         Indicates the input protein sequences *are* the
                                         peptides.
                                         
                                         * Separate multiple peptides assigned to a single protein
                                           by a slash ('/') in the sequence.
                                         * Peptides between 0 and 3 amino acids will not be included
                                           but should be included to specify the flanks.
                                         * You do *not* need to include all peptides for the
                                           protein(s) - only those you wish to be analysed.
                                         
                                         .. important::
                                         
                                             Flanking sequences must be specified!
                                             If you do not know the flanks, use the "Custom
                                             digestion, with unknown flanks" option instead. 
                                         
                                         Example: Given peptides A, B and C, where A and B
                                         are adjacent and C is at the end of the protein, the
                                         sequence may read::
                                         
                                            xxx/AAAAA/BBBBB/yyy/zzz/CCCCC
                                            
                                         Where x, y and z are the flanks for A's N-terminus, B's
                                         C-terminus and C's N-terminus respectivly. Note that
                                         all flanking sequences could (redundandly) be specified if
                                         this is easier.
                                             
    :cvar CUSTOM_NO_LINKERS:             Custom digestion, with unknown flanks.
    
                                         This is identical to "Custom digestion, with known flanks",
                                         above, but indicates that the peptide linkers are unknown.
                                         
                                         Dummy "AAA" linkers will be assumed.
                                         
                                         Example: Given peptides A, B and C, where their positions 
                                         and flanking sequences are unknown, the sequence may read:: 
                                         
                                            AAAAA/BBBBB/CCCCC
    
    :cvar TRYP_CHYMO:                    TrypChymo
                                         CUT: TrypChymo cuts after F, Y, W, L(or J), K or R if not followed by P.
                                         RGX: ``'(?<=[FYWLJKRX])(?!P)'``
                                         
    :cvar ASP_N_AMBIC:                   Asp-N_ambic
                                         CUT: Asp-N Ammonium bicarbonate cleaves before D(or B) or E(or Z).
                                         RGX: ``'(?=[DBEZX])'``
                                         
    :cvar LYS_C_P:                       Lys-C/P
                                         CUT: Lys-C/P cuts after K.
                                         RGX: ``'(?<=[KX])'``
                                         
    :cvar V8_DE:                         V8-DE
                                         CUT: V8-DE cuts after D(or B) or E(or Z) if not followed by P.
                                         RGX: ``'(?<=[DBEZX])(?!P)'``
                                         
    :cvar V8_E:                          V8-E
                                         CUT: V8-E cuts after E(or Z) if not followed by P.
                                         RGX: ``'(?<=[EZX])(?!P)'``
                                         
    :cvar FORMIC_ACID:                   Formic acid
                                         CUT: Formic_acid cuts after D(or B) and next residue is D (or B).
                                         RGX: ``'((?<=[DBX]))|((?=[DBX]))'``
                                         
    :cvar CHYMOTRYPSIN_P:                Chymotrypsin/P
                                         CUT: Chymotrypsin cleaves following F, Y, W or L(or J) residue.
                                         RGX: ``'(?<=[FYWLJX])'``
                                         
    :cvar LYS_C:                         Lys-C
                                         AKA: lys_c
                                         CUT: Lys-C cuts after K if not followed by P.
                                         RGX: ``'(?<=[KX])(?!P)'``
                                         
    :cvar PEPSINA:                       PepsinA
                                         CUT: PepsinA cuts after F or L(or J).
                                         RGX: ``'(?<=[FLJX])'``
                                         
    :cvar TRYPSIN_P:                     Trypsin/P
                                         CUT: Trypsin/P cuts after K or R.
                                         RGX: ``'(?<=[KRX])'``
                                         
    :cvar ARG_C:                         Arg-C
                                         AKA: arg_c; Clostripain; argc
                                         CUT: Arg-C cleaves following R residue unless the next residue is P.
                                         RGX: ``'(?<=[RX])(?!P)'``
                                         
    :cvar TRYPSIN:                       Trypsin
                                         CUT: Trypsin cleaves following a K or R residue unless the next residue is P.
                                         RGX: ``'(?<=[KRX])(?!P)'``
                                         
    :cvar CHYMOTRYPSIN:                  Chymotrypsin
                                         CUT: Chymotrypsin cleaves following F, Y, W or L(or J) residue unless the next residue is P.
                                         RGX: ``'(?<=[FYWLJX])(?!P)'``
                                         
    :cvar ASP_N:                         Asp-N 
                                         AKA: asp_n
                                         CUT: Asp-N cleaves before D(or B).
                                         RGX: ``'(?=[DBX])'``
                                         
    :cvar C_N_BR:                        CNBr
                                         CUT: CNBr cleaves following M.
                                         RGX: ``'(?<=[MX])'``
                                         
    :cvar ARG_C_P:                       Arg-C/P 
                                         CUT: Arg-C/P cleaves after R residues.
                                         RGX: '(?<=[RX])'``
                                         
    :cvar ASP_N_B:                       Asp-N/B
                                         CUT: Asp-N/B cleaves before D(while B is ignored).
                                         RGX: ``'(?=[DX])'``
                                         
    :cvar LYS_N:                         Lys-N 
                                         AKA: lys_n
                                         CUT: Lys-N cuts before K.
                                         RGX: ``'(?=[KX])'``
                                         
    :cvar LEUKOCYTE_ELASTASE:            leukocyte elastase
                                         CUT: leukocyte elastase cuts after A or L or I(or J) or V if not followed by P.
                                         RGX: ``'(?<=[ALIJVX])(?!P)'``
                                         
    :cvar CYANOGEN_BROMIDE:              cyanogen-bromide
                                         CUT: cyanogen-bromide cuts after M.
                                         RGX: ``'(?<=[MX])'``
                                         
    :cvar IODOSOBENZOATE:                iodosobenzoate
                                         CUT: ?
                                         RGX: ``'(?<=W)'``
                                         
    :cvar STAPHYLOCOCCAL_PROTEASE_D:     staphylococcal protease/D
                                         AKA: staphylococcal protease/D; Glu-C/D
                                         CUT: staphylococcal protease/D cuts after E(or Z).
                                         RGX: ``'(?<=[EZX])'``
                                         
    :cvar PEPSINA_P:                     PepsinA + P
                                         CUT: PepsinA + P cuts after F or L(or J) unless followed by P.
                                         RGX: ``'(?<=[FLJX])(?!P)'``
                                         
    :cvar PROLINE_ENDOPEPTIDASE:         proline endopeptidase
                                         CUT: proline endopeptidase cuts after HP, KP or RP if not followed by P.
                                         RGX: ``'(?<=[HKRX][PX])(?!P)'``
                                         
    :cvar CLOSTRIPAIN_P:                 Clostripain/P
                                         CUT: Clostripain/P cuts after R.
                                         RGX: ``'(?<=[RX])'``
                                         
    :cvar ELASTASE_TRYPSIN_CHYMOTRYPSIN: elastase-trypsin-chymotrypsin
                                         CUT: elastase-trypsin-chymotrypsin cuts after A,L,I(or J),V,K,R,W,F,Y unless followed by P.
                                         RGX: ``'(?<=[ALIVKRWFYX])(?!P)'``
                                         
    :cvar ALPHA_LYTIC_PROTEASE:          Alpha-lytic protease
                                         CUT: Alpha-lytic protease (aLP) cuts after T, A, S, or V.
                                         RGX: ``'(?<=[TASVX])'``
                                         
    :cvar TWO_IODOBENZOATE:              2-iodobenzoate
                                         CUT: 2-iodobenzoate cuts after W.
                                         RGX: ``'(?<=[WX])'``
                                         
    :cvar PROLINE_ENDOPEPTIDASE_HKR:     proline-endopeptidase/HKR
                                         CUT: proline-endopeptidase/HKR cuts after P.
                                         RGX: ``'(?<=[PX])'``
                                         
    :cvar GLUTAMYL_ENDOPEPTIDASE:        glutamyl endopeptidase
                                         AKA: Glu-C; glu_c; staphylococcal protease
                                         CUT: glutamyl endopeptidase cuts after D(or B) or E(or Z).
                                         RGX: ``'(?<=[DBEZX])'``
                                         
    :cvar GLU_C_P:                       Glu-C+P
                                         AKA: staphylococcal protease+P; Glu-C+P
                                         CUT: Glu-C+P cuts after D(or B) or E(or Z) unless followed by P.
                                         RGX: ``'(?<=[DBEZX])(?!P)'``
    """
    OTHER = auto()  # Special value
    INHERIT = auto()  # Special value
    CUSTOM = auto()
    CUSTOM_NO_LINKERS = auto()
    TRYPSIN = auto()
    TRYP_CHYMO = auto()
    ASP_N_AMBIC = auto()
    LYS_C_P = auto()
    V8_DE = auto()
    V8_E = auto()
    FORMIC_ACID = auto()
    CHYMOTRYPSIN_P = auto()
    LYS_C = auto()
    PEPSINA = auto()
    TRYPSIN_P = auto()
    ARG_C = auto()
    CHYMOTRYPSIN = auto()
    ASP_N = auto()
    C_N_BR = auto()
    ARG_C_P = auto()
    ASP_N_B = auto()
    LYS_N = auto()
    LEUKOCYTE_ELASTASE = auto()
    CYANOGEN_BROMIDE = auto()
    IODOSOBENZOATE = auto()
    STAPHYLOCOCCAL_PROTEASE_D = auto()
    PEPSINA_P = auto()
    PROLINE_ENDOPEPTIDASE = auto()
    CLOSTRIPAIN_P = auto()
    ELASTASE_TRYPSIN_CHYMOTRYPSIN = auto()
    ALPHA_LYTIC_PROTEASE = auto()
    TWO_IODOBENZOATE = auto()
    PROLINE_ENDOPEPTIDASE_HKR = auto()
    GLUTAMYL_ENDOPEPTIDASE = auto()
    GLU_C_P = auto()


# BEGIN ATTRIBUTION
# PURPOSE The enzyme regular expressions listed in the documentation above were taken from OpenMS.
# TITLE OpenMS -- Open-Source Mass Spectrometry
# WEBSITE https://www.openms.de/
# AUTHOR Uwe Schmitt
# DATE-RETRIEVED 15/04/2021
# LICENSE-URL http://opensource.org/licenses/BSD-3-Clause
# LICENSE-TEXT
#   OpenMS -- Open-Source Mass Spectrometry
#   
#   Copyright The OpenMS Team -- Eberhard Karls University Tuebingen,
#   ETH Zurich, and Freie Universitaet Berlin 2002-2020.
#   
#   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#   
#   1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#   
#   2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#   
#   3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
#   
#   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# END ATTRIBUTION


enums = EError, EDigestion
json_encoder = io_helper.EnumJsonEncoder( supported_enums = enums, indent = 4, prefix = False )


class EStage( Enum ):
    """
    Stages of the workflow.
    
    We can halt the model at one of these stages if we don't wish to progress
    any further. In this case the stage is inclusive, i.e. it is the final stage
    to be executed.
    """
    NONE = 0
    MAKE_PROTEINS = 1
    MAKE_PEPTIDES = 2
    SCORE_PEPTIDES = 3
    MAKE_QBRICKS = 4
    SCORE_QBRICKS = 5
    MAKE_QMENUS = 6
    SCORE_QMENUS = 7
    SELECT_QMENUS = 8
    ALL = 9


class ESqlBacking( Enum ):
    """
    :cvar NONE:
        No database.
        The database is not used.
    
    :cvar READ:
        Read-only.
        When set, scores are restored from the database.
        Use this to use, but not change, the database.
        
    :cvar WRITE:
        Write-only.
        New scores are stored in the database.
        Use this to update scores with new ones.
        
    :cvar READ_WRITE:
        Read-write.
        Use this to retrieve scores from the database where possible, and to
        remember newly acquired scores in the database.
    """
    NONE = 0
    READ = 1
    WRITE = 2
    READ_WRITE = 3


UHandlers = Union[None, HandlerFactoryCollection, Iterable["alacat.HandlerFactory"], Iterable["alacat.Handler"]]


class Model:
    """
    A description of what we want to start with.
    
    Field details are described in `__init__` and are not repeated in the 
    property descriptors.
    
    Objects of this class should be regarded as immutable - they should not be
    modified by the pipeline, however such objects may be modified by the caller
    prior to executing the pipeline. If setting the fields manually, note that
    the constructor may perform conversion and therefore accept types not
    actually acceptable to the fields themselves. The required field types are
    annotated using PEP484.
    """
    default_config_file = file_helper.normalise_path( "~/.alacat_config.json" )

    __slots__ = ("name",
                 "name",
                 "digestion",
                 "organisms",
                 "__handlers",
                 "proteins",
                 "peptides",
                 "qblocks",
                 "qmenus",
                 "qbrick_size",
                 "alacat_size",
                 "permutations",
                 "stage",
                 "mandate",
                 "alacat_version",
                 "partition_rankings",
                 "random_seed",
                 "min_peptide_length",
                 "sql_backing",
                 "qblock_size",
                 "fix_accessions")

    def __init__( self,
                  *,
                  name: str = "Untitled model",
                  organisms: Iterable[int] = (),  # human: 9606, yeast: 4932
                  digestion: EDigestion = EDigestion.OTHER,
                  handlers: UHandlers = None,
                  proteins: Iterable[str] = (),
                  peptides: Iterable[str] = (),
                  qblocks: Iterable[str] = (),
                  qmenus: Iterable[str] = (),
                  qbrick_size: int = 2,
                  alacat_size: int = 100,
                  stage: EStage = EStage.ALL,
                  mandate: bool = True,
                  partition_rankings: bool = True,
                  random_seed: Optional[int] = 1,
                  min_peptide_length: Optional[int] = None,
                  sql_backing: ESqlBacking = ESqlBacking.READ_WRITE,
                  alacat_version: Optional[str] = None,
                  qblock_size: int = 1,
                  fix_accessions:bool=False):
        """
        !CONSTRUCTOR
        
        :param name:    
            
                Model title, for your reference only.
                Can be left blank.
                
                GUI: If the model already exists on the system, this field will be ignored and the existing title will be used.
        
        :param organisms:      
        
                    Specify one or more organisms to run your query against.
                    You should specify organisms by their NCBI taxonomy ID, or their scientific name.
                    If you leave this field blank the field will be completed automatically based on the protein sequences.
                    
                    GUI: Use a comma to delimit multiple organisms.
                                
        :param digestion:       
        
                    Specify the digestion you are using.
                    This will almost always be set to TRYPSIN.
                                 
        :param handlers:        
        
                    Handlers to use.
                    For protein/peptide/qbrick/qconcats, governs the input formats selected, the methods used for
                    generation (e.g. digestion) and the scoring algorithms.
         
        :param proteins:        
        
                    Specify the proteins to query. 
                               
                    This field is mandatory.
                    The formats accepted are[*]_:
                                
                    * Uniprot or Ensembl accessions
                    * FASTA protein sequences
                    * FASTA peptide sequences
                    * TSV format
                    * Alacat-UID
                                   
                    Uniprot or Ensembl accessions
                    -----------------------------
                    
                    Provide a list of database accessions, one per line.
                    
                    GUI: See examples E1 and E5.
                    Example E3 also uses this input format, but then goes on to
                    focus the peptide search.
                    
                    FASTA protein sequences
                    -----------------------
                    
                    Provide FASTA content with protein accessions and sequences.
                    
                    GUI: See example E2.
                    
                    FASTA peptide sequences
                    -----------------------
                    
                    Sometimes we want to evaluate specific peptides without being 
                    concerned about other peptides in the protein. In this case a
                    slash (/) can be used to denote the splits in the protein.
                    Small (l=3) peptides should be included to correctly identify the
                    flanking sequences for each peptide.
                    
                    GUI: See example E4.
                    
                    TSV format
                    ----------
                    
                    Protein accessions and sequences can also be provided in tabular
                    format, e.g. CSV or TSV.
                    
                    Alacat-UID
                    ----------
                    
                    This is a JSON format internal to the software. You won't
                    generally type it manually, but might see it if you export and
                    recover a previous model.
                    
                    Notes
                    -----
                    
                    .. [*] This list gives the default values.
                           GUI: The exact protein formats supported depend on the *providers*
                                enabled in the *advanced* section below.
                           API: The exact protein formats supported depend on the `handlers`
                                argument.
                                
        :param peptides:
         
                    Specify the peptides to use. 
                    Peptides in the following formats are generally accepted [*]_. 
                    
                        Peptide list::
                        
                            (PEPTIDE 1)
                            (PEPTIDE 2)
                            ...
                        
                        FASTA-like peptide list per protein::
                        
                            >(PROTEIN 1)
                            (PEPTIDE 1)
                            (PEPTIDE 2)
                            ...
                    
                        Network edge-list::
                        
                            (PROTEIN 1):(PEPTIDE 1)
                            (PROTEIN 1):(PEPTIDE 2)
                            ...
                            
                    Alacat-UID (obtained from a previous model) are also accepted.
                    
                    If you leave this field blank the system will suggest the peptides for you.
                    
                    .. [*] This list gives the default values.
                           GUI: The exact protein formats supported depend on the *providers*
                                enabled in the *advanced* section below.
                           API: The exact protein formats supported depend on the `handlers`
                                argument.
        
        :param qblocks:         
        
                    Specify the qbrick sets (qblocks) to use.
                    Qbricks are generally provided as an Alacat-UID (obtained from a previous model).
                    The exact formats supported depend on the handlers selected.
                    If you leave this field blank the system will suggest the qblocks for you.
                                
        :param qmenus:
        
                    Specify the lists of qconcat (qmenus) to favour.
                    Qmenus are generally provided as an Alacat-UID (obtained from a previous model).
                    The exact formats supported depend on the handlers selected.
                    If you leave this field blank the system will suggest the qmenus for you.
                                
        :param qbrick_size:     
        
                    Specify the number of peptides in a qbrick.
        
        :param alacat_size:     
        
                    Specify the maximum size of an Qconcat.
                    Qconcats longer than this will be split into a "Qmenu" of multiple Qconcats.
                                
        :param stage:           
        
                    Stage of the pipeline to run to.
        
        :param partition_rankings:  
        
                When set rankings (ranks, quantiles and quartiles) are generated on a per-protein
                basis, meaning highly ranked subjects (peptides or qblocks) are good for their
                protein.
                
                When unset rankings are generated on a model basis, meaning highly ranked subjects
                are good when compared to all other subjects in the pipeline.
                
                Note that per-protein-ranking is the default, and the documentation will often
                be described assuming this, even when the model-ranking is selected.
                
                Per-protein-ranking allows the "best" peptides to be selected, even for proteins
                with no or only quantotypic peptides. 
                
                Model-ranking may be useful where proteins have a limited number of peptides, where
                all peptides are not specified, or when cross examination of peptides across
                different proteins is desired.
                
                This parameter affects the following rankings:
                
                * Generated "rank" columns
                * Final ranking of subjects across scores
                * Summary statistics such as "ranked out of" or "identical values" (usually seen in the GUI)
                
        :param min_peptide_length:  
        
                Minimum peptide length.
                Peptides shorter than this are dropped immediately and not evaluated.
                The default is 3 for custom digestions, or 5 for simulated digestions.
                                                    
                This does not affect `EDigestion.CUSTOM`\s mandatory lower bound of 3, or any
                other limit built into a digester itself. A value of less than 3 is not recommended.
        
        :param sql_backing:         
        
                SQL backing mode. Turning backing on means you might not get the latest version of
                the scores, but operation will be considerably faster. Old scores can be purged from
                the database manually using their date column, see the
                `alacat.utilities.sql_backing` module for details.
                
        :param mandate: 
        
                In mandated mode all of the peptides you specify will be used.
                This may result in multiple qbricks per protein.
                In non-mandated mode if you specify more peptides than necessary those that the
                system considers least quantotypic will be dropped.
                If you specify fewer peptides than in a qbrick then, regardless of this selection, 
                the system will always supplement your selection with those from the pool of 
                remaining peptides that it considers most quantotypic. 
                
                The same logic is applied to qblock and qmenu selection.
                
        :param qblock_size: 
        
                Minimum number of qbricks per protein.
                
        :param fix_accessions:
        
               Some FASTA files have meta-data crammed into the accession field like this::
               
                    >sp|N1P3Y5|FDH1_YEASC Formate dehydrogenase 1 OS=Saccharomyces cerevisiae (strain CEN.PK113-7D) OX=889517 GN=FDH1 PE=2 SV=1
                    
               When this option is set we remove the meta-data and keep only the second field::
                    
                    N1P3Y5
                    
               This will not work for accessions not adhering to the above format.
               Note that this applies to *all* proteins, not just those originating from FASTA-files.
        """
        if handlers is None:
            handlers = HandlerFactoryCollection.create_defaults()

        if min_peptide_length is None:
            if digestion in (alacat.EDigestion.OTHER, alacat.EDigestion.CUSTOM, alacat.EDigestion.CUSTOM_NO_LINKERS):
                min_peptide_length = 3
            else:
                min_peptide_length = 5

        self.name: str = name
        self.alacat_version = alacat_version or alacat.__version__
        self.digestion: EDigestion = safe_cast( "digestion", digestion, EDigestion )
        self.organisms: FrozenSet[int] = frozenset( organisms )
        self.handlers: HandlerFactoryCollection = handlers
        self.proteins: Tuple[str, ...] = tuple( sorted( proteins ) )
        self.peptides: Tuple[str, ...] = tuple( sorted( peptides ) )
        self.qblocks: Tuple[str, ...] = tuple( sorted( qblocks ) )
        self.qmenus: Tuple[str, ...] = tuple( sorted( qmenus ) )
        self.qbrick_size: int = qbrick_size
        self.alacat_size: int = alacat_size
        self.stage: EStage = stage
        self.mandate: bool = mandate
        self.partition_rankings = partition_rankings
        self.random_seed = random_seed
        self.min_peptide_length = min_peptide_length
        self.sql_backing = sql_backing
        self.qblock_size = qblock_size
        self.fix_accessions=fix_accessions

        super().__init__()

    @staticmethod
    def explicit_init( *, name, organisms, digestion, handlers, proteins, peptides, qblocks, qmenus, qbrick_size, alacat_size, stage, mandate, partition_rankings, random_seed, min_peptide_length, sql_backing, qblock_size,fix_accessions ):
        """
        Model(*) with mandatory arguments.
        """
        return Model( name = name, organisms = organisms, digestion = digestion, handlers = handlers, proteins = proteins, peptides = peptides, qblocks = qblocks, qmenus = qmenus, qbrick_size = qbrick_size, alacat_size = alacat_size, stage = stage, mandate = mandate, partition_rankings = partition_rankings, random_seed = random_seed, min_peptide_length = min_peptide_length, sql_backing = sql_backing, qblock_size = qblock_size,fix_accessions=fix_accessions )

    @property
    def handlers( self ) -> HandlerFactoryCollection:
        return self.__handlers

    @handlers.setter
    def handlers( self, value: UHandlers ):
        if value is None:
            value = ()

        if not isinstance( value, HandlerFactoryCollection ):
            value = HandlerFactoryCollection( value )

        self.__handlers = value

    def __str__( self ):
        return self.name

    def __repr__( self ):
        r = []

        ind = " " * (len( self.__class__.__name__ ) + 2)

        r.append( f"{self.__class__.__name__}( " + _iv( f"name               = {self.name!r}," ) )
        r.append( ind + _iv( f"alacat_version     = {self.alacat_version!r}," ) )
        r.append( ind + _iv( f"digestion          = {string_helper.enum_to_repr( self.digestion )}," ) )
        r.append( ind + _iv( f"organisms          = {self.organisms!r}," ) )
        r.append( ind + _iv( f"handlers           = {self.handlers!r}," ) )
        r.append( ind + _iv( f"proteins           = {self.proteins!r}," ) )
        r.append( ind + _iv( f"peptides           = {self.peptides!r}," ) )
        r.append( ind + _iv( f"qblocks            = {self.qblocks!r}," ) )
        r.append( ind + _iv( f"qmenus             = {self.qmenus!r}," ) )
        r.append( ind + _iv( f"qbrick_size        = {self.qbrick_size!r}," ) )
        r.append( ind + _iv( f"alacat_size        = {self.alacat_size!r}," ) )
        r.append( ind + _iv( f"stage              = {string_helper.enum_to_repr( self.stage )}," ) )
        r.append( ind + _iv( f"mandate            = {self.mandate!r}," ) )
        r.append( ind + _iv( f"partition_rankings = {self.partition_rankings!r}," ) )
        r.append( ind + _iv( f"random_seed        = {self.random_seed!r}," ) )
        r.append( ind + _iv( f"min_peptide_length = {self.min_peptide_length!r}," ) )
        r.append( ind + _iv( f"sql_backing        = {string_helper.enum_to_repr( self.sql_backing )} )" ) )
        r.append( ind + _iv( f"qblock_size        = {self.qblock_size!r} )" ) )

        return "\n".join( r )

    def exact( self ) -> "alacat.Pipeline":
        """
        See `Pipeline.exact`.
        """
        from alacat.pipeline import Pipeline
        return Pipeline.exact( self )
