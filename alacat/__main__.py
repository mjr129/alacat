def main():
    """
    Entry point.
    """
    from mhelper import arg_parser
    arg_parser.branch( { "cli"   : __cli_main,
                         "gui"   : __gui_main,
                         "cached": __diag_main } )


def __cli_main():
    """
    Invokes the Command Line Interface (CLI).
    """
    from alacat.cli import cli_main
    return cli_main()


def __gui_main():
    """
    Starts the Graphical User Interface (GUI).
    """
    from alacat.gui import gui_main
    return gui_main()


def __diag_main():
    """
    Dumps diagnostic information.
    """
    from alacat import debugging
    from mhelper import ansi
    import sys

    print( "The following data file locations have been discovered." )
    print( "Note that these may not include any manual overrides." )
    print( "See the documentation of the respective modules for full details." )
    print()

    for inf in debugging.get_paths():
        print( ansi.FORE_BRIGHT_CYAN + inf.name + ansi.RESET )
        print( ansi.FORE_CYAN + "    " + inf.source + ansi.RESET )
        print( ansi.FORE_BRIGHT_YELLOW + "    " + inf.path + ansi.RESET )
        print( ansi.FORE_RED + "    ...", end = "" )
        sys.stdout.flush()
        print( "\r" + ansi.FORE_BRIGHT_RED + "    " + inf.get_size() + ansi.RESET )


if __name__ == "__main__":
    exit( main() )
