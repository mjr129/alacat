Alacat Server Scripts
---------------------

A selection of scripts that demonstrate how to run the Alacats project on an Nginx server.

The scripts are for example only, they will require editing, for instance to change the paths,
before they can be executed.

* `start_bgts.sh` - Alacat BGTS
* `start_gunicorn.sh` - Gunicorn+Alacat
* `start_nginx.sh` - Nginx

    *   Run the applications (Alacat BGTS, Gunicorn+Alacat and Nginx).
        When all 3 are running you should see the server.

* `nginx_config.nginx`

    * Must be added to Nginx before `start_nginx.sh` is run.

* `alacat_service.service`

    * The equivalent to `start_gunicorn.sh`, but as an auto-start Unix service.
      You will probably want to create one for the BGTS too.